<?php

/**
 * Created by PhpStorm.
 * User: lldl
 * Date: 23.12.14
 * Time: 19:31
 */
class StreamParser extends AbstractParser
{
    public $url = 'http://egamingbets.com/ajax.php?key=modules_tables_update_UpdateStrimTitle&act=UpdateStrimTitle&ajax=update&ind=streams&type=modules';

    /** parse video streams from egb site
     * @return string
     */
    public function parse()
    {
        $response = $this->getResponse($this->url);
        return $response;
    }
}