<?php

/**
 * Created by PhpStorm.
 * User: lldl
 * Date: 17.12.14
 * Time: 23:30
 */
abstract class AbstractParser
{
    protected $userAgentStrings = array(
        'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; Acoo Browser; SLCC1; .NET CLR 2.0.50727; Media Center PC 5.0; .NET CLR 3.0.04506)',
        'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0; Trident/4.0; Acoo Browser; GTB5; Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1) ; InfoPath.1; .NET CLR 3.5.30729; .NET CLR 3.0.30618)',
        'Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 6.0; Trident/4.0; Acoo Browser 1.98.744; .NET CLR 3.5.30729)',
        'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; Acoo Browser; GTB5; Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1) ; Maxthon; InfoPath.1; .NET CLR 3.5.30729; .NET CLR 3.0.30618)',
        'Mozilla/5.0 (Windows; U; Windows NT 5.1; pl; rv:1.9.2.3) Gecko/20100401 Lightningquail/3.6.3',
        'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; MyIE2; .NET CLR 2.0.50727)',
    );

    protected $lastError;

    /** return user agent string
     * @return string
     */
    protected function getUserAgent()
    {
        return array_rand($this->userAgentStrings);
    }

    /** return last curl error
     * @return string
     */
    public function getLastError()
    {
        return $this->lastError ? $this->lastError : false;
    }

    /** return Html page usint curl and url ( if param url is empty, will be uset $this->url property)
     * @param string $url
     * @param array $parameters
     * @param array $curlopts
     * @return string/boolean
     */
    protected function getResponse($url, $parameters = array(), $curlopts = array())
    {
        if(!empty($parameters)) {
            $url .= (strpos($url,'?') !== false) ? '' : '?';
            foreach($parameters as $name  => $value) {
                $url .= "$name=$value&";
            }
        }
        $c = curl_init($url);
        curl_setopt($c, CURLOPT_USERAGENT, $this->getUserAgent());
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);

        foreach ($curlopts as $option => $value) {
            curl_setopt($c, $option, $value);
        }

        // Grab the data.
        $response = curl_exec($c);
        if (!$response) {
            $this->lastError = curl_error($c);
        }

        return $response;
    }

    /** makes post request
     * @param string $url
     * @param array $postfieldsArray
     * @param array $curlopts
     * @return string/boolean
     */
    protected function makePostRequest($url, $postfieldsArray = array(), $curlopts = array())
    {
        $postfields = '';
        foreach ($postfieldsArray as $name => $value) {
            $postfields .= "$name=$value&";
        }

        if ($curl = curl_init()) {
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $postfields);
            curl_setopt ($curl, CURLOPT_USERAGENT, $this->getUserAgent());
            curl_setopt ($curl, CURLOPT_TIMEOUT, 60);
            curl_setopt ($curl, CURLOPT_FOLLOWLOCATION, 0);

            foreach ($curlopts as $option => $value) {
                curl_setopt($curl, $option, $value);
            }

            $out = curl_exec($curl);
            if ($out !== false)
                return $out;
            else
                $this->lastError = curl_error($curl);
            curl_close($curl);
        }
        return false;
    }
}