<?php
/**
 * Created by PhpStorm.
 * User: lldl
 * Date: 07.12.14
 * Time: 17:47
 */

/** log everything in /logs/*.log files */
class Logger {
    /**
     *@param string $msg
     *@param string $filename
     * @return boolean
     * */
    public static function log($msg, $filename = 'parser'){
        $logDir = self::getlogDir();
        $datetime = date('Y-m-d H:i:s');
        $logRecord = '[ ' . $datetime .' ] - ' . $msg ."\n";
        $result = file_put_contents($logDir.DIRECTORY_SEPARATOR.$filename.'.log', $logRecord, FILE_APPEND);
        return $result;
    }

    protected static function getlogDir(){
        return __DIR__.'/../logs';
    }
} 