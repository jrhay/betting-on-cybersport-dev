<?php
date_default_timezone_set('UTC');
require_once('autoload.php');

Logger::log('new tournaments  parse request');

//Parse  tournaments on egb site

/** @var TournamentsParser $parser */
$tournamentsParser = new TournamentsParser();
if (!$tournamentsParser->login()) {
    Logger::log('login error: ', $tournamentsParser->getLastError(), 'tournaments_parser');
    Logger::log("login tournaments error: " . $tournamentsParser->getLastError(), 'errors');
}
/**@var string $res */
if (!$res = $tournamentsParser->parse()) {
    Logger::log('parse error: ', $tournamentsParser->getLastError(), 'tournaments_parser');
    Logger::log("parse tournaments error: " . $tournamentsParser->getLastError(), 'errors');
    exit();
}
Logger::log('got response', 'tournaments_parser');

$info = json_decode($res, true);
$receiverConnector = new ReceiverConnector();
$url = 'cybbet.com/site/createTournaments';
$sendRes = $receiverConnector->sendData($res, $url);

if ($sendRes) {
    Logger::log('successfully sent to receiver' . "\n", 'tournaments_parser');
} else {
    Logger::log('error, trying to send to receiver' . "\n", 'tournaments_parser');
    Logger::log('error, trying to send to receiver', 'error' . "\n");
}

