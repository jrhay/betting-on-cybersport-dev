<?php
date_default_timezone_set('UTC');
require_once('autoload.php');
Logger::log('new parse request');
//Parse egb site
/** @var EGBParser $parser */
$parser = new EGBParser();
/** @var string $games */
$response = $parser->newParse();
$info = json_decode($response, true);
if ($response) {
    Logger::log("got response");
} else {
    Logger::log("error response! : " . $parser->getLastError());
    Logger::log("error response! : " . $parser->getLastError(), 'errors');
}
$receiverConnector = new ReceiverConnector();
$url = 'cybbet/site/createGames';
$sendRes = $receiverConnector->sendData($response, $url);
if ($sendRes) {
    Logger::log('successfully sent to receiver'."\n");
} else {
    Logger::log('error, trying to send to receiver'."\n");
    Logger::log('error, trying to send to receiver','error'."\n");
}