<?php
date_default_timezone_set('UTC');
require_once('autoload.php');
Logger::log('new stream parse request');

/** @var StreamParser $streamParser */
$streamParser = new StreamParser();
$response = $streamParser->parse();


/** @var ReceiverConnector $receiverConnector */
$receiverConnector = new ReceiverConnector();
$info = json_decode($response);
if(!$sendResult = $receiverConnector->sendData($response, 'cybbet.com/site/createStreams')) {
    Logger::log('error trying to send data ( streams ) to reciever : '. json_encode($receiverConnector->error) , 'streams');
}

    Logger::log('successfully sent streams to receiver', 'streams');
