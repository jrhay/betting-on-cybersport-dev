<?php  /** @var CActiveForm $form */ ?>
<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'statistics-category-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // See class documentation of CActiveForm for details on this,
    // you need to use the performAjaxValidation()-method described there.
    'enableAjaxValidation' => false,
)); ?>
<?php
$categories = array();
/** @var GameCategories $gameCategories */
foreach ($gameCategories as $ctg) {
    $categories[$ctg->id] = $ctg->title;
}
?>
    <!--    <div class="row">-->
<?php echo $form->labelEx($selectGameCategoryForm, 'category_id', array(
    'style' => 'float:left'
)) ?>
<?php /** @var int $currentGameCategoryId */
    if(!$currentGameCategoryId)
        $currentGameCategoryId = 13;
    ?>
<?php echo $form->dropDownList($selectGameCategoryForm, 'category_id', $categories, array(
    'class' => 'btn-style2',
            'style' => 'float:left;margin-left: 1%',
    'options' => array($currentGameCategoryId=>array('selected'=>true))
)) ?>
    <!--    </div>-->
<?php echo CHtml::submitButton(Yii::t('site', 'O.K.'), array(
    'class' => 'btn-style1',
    'style' => 'float: right'
)); ?>
    <br>
<?php $this->endWidget(); ?>