<?php
/* @var $this TimeRangeFormController */
/* @var $model TimeRangeForm */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'time-range-form-timeRangeForm-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// See class documentation of CActiveForm for details on this,
	// you need to use the performAjaxValidation()-method described there.
	'enableAjaxValidation'=>false,
)); ?>

<!--    <div class="row" style="float: left">-->
        <?php echo $form->labelEx($timeRangeForm, 'start_time', array(
            'style'=> 'float:left')); ?>
        <?php
        Yii::import('application.extensions.yiiDateTimePicker.CJuiDateTimePicker');
        $this->widget('CJuiDateTimePicker',
            array(
                'name' => 'TimeRangeForm[start_time]',
                'id' => 'recommendTimeInput',
                'value' => $timeRangeForm['start_time'],
                'mode' => 'datetime',
                'options' => array(
                    'dateFormat' => 'yy-mm-dd',
                    'timeFormat' => 'hh:mm:ss',
//                'minDate' => 'Today'
                ),
                'htmlOptions' => array(
                    'autocomplete' => 'off',
//                    'required' => true,
                    'style' => 'width: 30%;float:left;margin-left:1%'
                ),
                'language' => 'ru'
            ));
        ?>
        <?php echo $form->labelEx($timeRangeForm, 'end_time',array(
            'style' => 'float:left;margin-left:1%'
        )); ?>
        <?php
        $this->widget('CJuiDateTimePicker',
            array(
                'name' => 'TimeRangeForm[end_time]',
                'value' => $timeRangeForm['end_time'],
                'id' => 'recommendTimeInput2',
                'mode' => 'datetime',
                'options' => array(
                    'dateFormat' => 'yy-mm-dd',
                    'timeFormat' => 'hh:mm:ss',
//                'minDate' => 'Today'
                ),
                'htmlOptions' => array(
                    'autocomplete' => 'off',
                    'style' => 'width: 30%;margin-left:1%'
//                    'required' => true,
                ),
                'language' => 'ru'
            ));
        ?>
        <?php echo $form->error($timeRangeForm, 'end_date'); ?>
<!--    </div>-->

	<?php echo $form->errorSummary($timeRangeForm); ?>


<!--	<div class="row buttons">-->
		<?php echo CHtml::submitButton(Yii::t('site','O.K.'),array(
            'class' => 'btn-style1',
            'style' => 'float: right;margin-left:1%'
        )); ?>
<!--	</div>-->

<?php $this->endWidget(); ?>

</div><!-- form -->