<?php  /** @var Result[] $results */ ?>

<div class="page-content">
    <div class="wrap">
        <div class="g-clearfix">
            <div class="leftcol">
                <div id="game-video" class="video">
                    <!--                    <iframe width="630" height="350" src="http://www.youtube.com/embed/DhaRkWfaq10?wmode=opaque"-->
                    <!--                            frameborder="0" allowfullscreen></iframe>-->
                </div>
                <div class="results style6">
                    <div class="filter">
                        <?php  include('timeRangeForm.php') ?>
                        <br>
                        <?php  include('selectCategoryForm.php') ?>
                    </div>
                    <table class="res-tab">
                        <thead>
                        <tr>
                            <th><?php echo  Yii::t('site', 'Время') ?></th>
                            <th class="tab-event"><?php echo  Yii::t('site', 'Событие') ?></th>
                            <th><?php echo  Yii::t('site', 'Результат') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php  foreach ($results as $result) : ?>
                            <tr>
                                <td class="tab-time"><?php echo  gmdate("Y-m-d H:i:s ", $result->add_result_time) ?></td>
                                <td>
                                    <img class="game_icon"
                                         src="<?php echo  Yii::app()->baseUrl ?>/images/games/<?php echo  $result->game->category->image ?>">
                                    <?php echo  $result->game->category->title ?>
                                    <div><?php echo  $result->game->event->text ?></div>
                                    <div><?php echo  $result->game->team1->name ?> vs <?php echo  $result->game->team2->name ?></div>
                                </td>
                                <td>
                                    <div>
                                        <div><?php echo  $result->score ?></div>
                                    </div>
                                    <a onclick='playVideo(<?php echo  json_encode($result->game->betvideo) ?>)' class="link"
                                       href="#" title=""><?php echo  Yii::t('site', 'Смотреть видео') ?></a>

                                </td>
                            </tr>
                        <?php  endforeach ?>
                        </tbody>
                    </table>
                    <table class="res-tab">
                        <tbody>
                        <?php $this->widget('CLinkPager', array(
                            'pages' => $resultPages,
                            'cssFile' => Yii::app()->request->baseUrl . '/css/pagination.css'
                        )) ?>
                        <tr>
                            <td></td>
                            <td style="">
                                <h2><?php echo  Yii::t('results', 'Canceled Games') ?></h2>
                            </td>
                            <td></td>
                        </tr>
                        <?php  /** @var Games[] $canceledGames */ ?>
                        <?php  foreach ($canceledGames as $game) : ?>
                            <tr>
                                <td class="tab-time"><?php echo  gmdate("Y-m-d H:i:s ", $game->game_end) ?></td>
                                <td>
                                    <img class="game_icon"
                                         src="<?php echo  Yii::app()->baseUrl ?>/images/games/<?php echo  $game->category->image ?>">
                                    <?php echo  $game->category->title ?>
                                    <div><?php echo  $game->event->text ?></div>
                                    <div><?php echo  $game->team1->name ?> vs <?php echo  $game->team2->name ?></div>
                                </td>
                                <td>
                                    <div>
                                        <div> CANCELED</div>
                                    </div>
                                    <a onclick='playVideo(<?php echo  json_encode($game->betvideo) ?>)' class="link" href="#"
                                       title=""><?php echo  Yii::t('site', 'Смотреть видео') ?></a>

                                </td>
                            </tr>
                        <?php  endforeach ?>

                        </tbody>
                    </table>
                    <?php $this->widget('CLinkPager', array(
                        'pages' => $gamesPages,
                        'cssFile' => Yii::app()->request->baseUrl . '/css/pagination.css'
                    )) ?>
                    <?php  echo '
                                    <script type="text/javascript">
                                        function playVideo(videoFrame) {
                                            var videoDiv = document.getElementById("game-video");
                                            videoDiv.innerHTML= videoFrame;
                                            var temp;
                                        }
                                    </script>' ?>
                </div>
            </div>
            <div class="rightcol-s">
                <?php $this->widget('application.widgets.banner.banner', array(
                    'height' => '350',
                    'width' => '310',
                )); ?>
                <?php $this->widget('application.widgets.banner.banner', array(
                    'height' => '350',
                    'width' => '310',
                )); ?>
                <?php $this->widget('application.widgets.banner.banner', array(
                    'height' => '350',
                    'width' => '310',
                )); ?>
            </div>
        </div>
    </div>
</div>
<div class="gag"></div>
</div>