<?php  $this->pageTitle = Yii::app()->name . ' - Privacy & Police'; ?>
<!--        Police and privacy-->


<div class="" style="" id="popup1">
    <div class="">
        <br>

        <h1>Terms Cybbet.com</h1>
        <br>
        <textarea style="overflow: auto;height: 100%;width: 100%; " rows="50" readonly>


            Cybbet.com Terms of Use


            Please read these Rules of the Website (hereinafter referred to as the Rules) before using cybbet.com
            (hereinafter referred to as the Website).
            Your use of the Website, including registration on the Website, means your full and unconditional consent to
            the Rules with the following contents and on the following terms and conditions:

            I. Main Terms and Definitions

            Client, Bettor means a Website user that placed a bet with the Bookmaker.
            Bookmaker means the Website administrator, the bookmaker chain that takes bets on events.
            Events mean events of e-sport, sport and other character. The event can refer to either game / match or to a
            series of games / matches with the same players / teams to determine the total points (best of 3, best of 5,
            etc.). The event can also refer to any other condition related with the game / match / series / tournament,
            on which the Bookmaker takes bets.
            Wager means an agreement concluded between the Client and the Bookmaker, on conditions of which the losing
            party has to fulfill its obligation. Wager is made by taking bets from wager participants on the terms and
            conditions offered by the Bookmaker in Money Line.
            Money Line means a list of events and their outcomes with winning odds offered by the Bookmaker for
            wagering.
            Bet means the amount wagered by the Client and written off from the Bettor’s account by the Bookmaker.
            Outcome is the result of the event wagered.
            Winning odds mean coefficient automatically determined by the Bookmaker that depends on the amount of bets
            on each outcome.
            Account means the internal virtual account of the Client on the Website.
            Content means source code and object code that make up the Website, its design, including audio, visual,
            graphic and video materials, images, other graphics and design works and other intellectual property and
            information placed in the Website.

            II. General Provisions

            Registration, opening the deposit and betting are permitted only for persons above the age of 18. The Client
            shall be held liable for legality of online gambling in Client’s country and region of residence and / or
            location, and age restrictions in force in the Client’s country of residence and / or location. By
            registering on the Website the Client takes full responsibility for website use. The Client is also liable
            for reporting data on its income resulted from using the Website and payment of applicable taxes.
            The Bookmaker is not held liable for the information provided or misleadingly provided by the Client when
            registering in the Website and opening the account.
            For the purpose of preventing frauds and other conflicts, the Bookmaker may, in exceptional cases, demand
            Client identity validation to verify the information provided by the Client. When opening the account,
            Client agrees to provide all required documents in this case upon the Bookmaker’s request.
            Should the Client provide misleading information during registration, the Website administration is entitled
            to amend the data after verification, and in exceptional cases, to block Client’s access to the Website.
            A Client is permitted to have only one betting virtual currency account in one of the currencies. The
            exceptions are allowed on the case-by-case basis and on conditions of the Website administration.
            The Client has no right to allow third parties to use its account.
            The Client is held personally liable for safety of its account, login and password. The Bookmarker
            guarantees non-disclosure of the Client’s data by Bookmaker employees, but is not held liable for
            consequences caused by disclosure of the Client information in case of official requests by law-enforcement
            and/or tax authorities, as well as for the purpose of protecting its interests and fighting against frauds.
            All Client transactions confirmed by entering the Client login (account number) and password are deemed
            valid and having legal effect, with the only transaction restriction being the actual account balance.
            In case of suspicions of the loss of login (account number) and/or password, the Client is entitled to
            request change of its account data from the Bookmaker as soon as possible.
            Loss of password and other account data may not serve as the reason to cancel bets or payout request.

            III. Opening, Replenishing and Using the Account

            To open an Account, the Client has to get registered in the Website.
            After the registration, three sections of Account are available to the Client:
            Betting account with USD equivalent (hereinafter referred to as the Primary Account);
            Betting account without cash equivalent (hereinafter referred to as the Play Money Account);
            Bonus non-betting account without cash equivalent (hereinafter referred to as the Bonus Account).
            To make a wager with the Bookmaker, the Client may use either Primary Account or Play Money Account. Bonus
            Account is used for the Client to get additional prizes from the Bookmaker.
            The following rules apply when the Client uses its Primary Account:
            The initial balance after registration is 0 units.
            Payment is credited to the Primary Account opened during registration and only in US Dollars. US Dollar
            exchange rate of the Primary Account is 1 US Dollar to 1 Primary Account unit. The Account allows for
            non-integer values that are corrected to the nearest hundredths.
            The Primary Account is replenished in “Make Deposit” menu. The Client may use any method of funds transfer
            to the Bookmaker from those available in the Website at his / her own discretion. The Bookmaker is entitled
            to restrict or extend a range of available transfer methods, and to restrict methods available to the Client
            depending the Client’s country of registration and other factors without prior notice to the Client thereof.
            All funds transfer costs are borne by the Client.
            Allowable time of payment registration in the system is 5 minutes. If the Primary Account has not been
            replenished during this time, the Client should notify the Bookmaker in writing by e-mailing to:
            support@cybbet.com.
            For the application to be considered, the Client has to provide the Bookmaker with all possible payment
            details, in some cases it may need to provide the receipt, confirming message or screenshot of the payment
            system transaction that would allow to identify the transaction. If no reply is given within 30 minutes the
            Client should apply to the Online Support.
            The Bookmaker is not held liable in cases, when the Primary Account is not replenished in the specified
            terms through the fault of third parties, in particular, banks, payment systems and agents, or because of
            force majeure (network failures, power outages, hardware or software failures etc.). The Bookmaker shall not
            be held liable either, when the payment system refused to conduct Client’s transaction.


            The following rules apply when the Client uses its Cyber Money Account:
            The initial balance of the Cyber Money Account is 100 units.
            The Cyber Money Account is replenished by the Bookmaker after calculations of all Wagers made using this
            account and in case the Client has zero balance in this account. To replenish Cyber Money Account, it is
            necessary to use system capabilities.
            The Bonus Account is opened automatically during registration. The following rules apply to the account:
            Initial balance of the Bonus Account is 0 points.
            Bonus points are accrued by the Bookmaker to the Client when using the Primary Account in cases specified by
            the Bookmaker and solely at the Bookmaker’s discretion.
            Bonus points received for the Wager made are also canceled if the Bets are returned or the Wager is
            terminated.
            The Client may use received Bonus points pursuant to Bonus section placed in the Bookmaker website. The
            Bookmaker is entitled to amend the conditions governing use of Bonuses and bonus points at its own
            discretion without prior notice to the Client.

            IV. Betting Conditions

            Bets are taken according to the Money Line, which is a list of upcoming events with quotes (winning odds)
            for outcomes of these events automatically calculated by the Bookmaker. Bet quotes can vary continuously
            depending on the amount and number of bets by other bettors.
            Changes in the Money Line (head starts, winning odds, totals, express restrictions, maximum bet amount
            restrictions etc.) may take place after any bet, but conditions of previously made bets remain unchanged.
            The bets are taken only in the amount not exceeding actual balance of the Client account.
            Throughout the betting period, the amount of bet on any event cannot exceed maximum amount specified in it.
            Bets for the same event are not taken when reaching the total maximum bet of each Bettor for this event.
            Bets are taken before the event starts. Bets made after the actual start of the event for any reason
            whatsoever are deemed invalid and shall be returned and excluded from “express” options.
            Date and time of the event start specified in the line are provided for reference. The mistakenly specified
            date is not the reason to cancel the bet. In this case, bets are deemed valid if made before the event
            start. When calculating bets, time of the event start is considered to be actual time of the event beginning
            that is determined pursuant to official sources (Internet sites referred to by the Website when gathering
            information, sites of the organization that manages the event, match, etc.).
            The Bookmaker is not responsible for exact translation of names of players, teams, cities, where events are
            held, on whose outcome the wager is made.
            In case of mistakes made by Website personnel or software failures when taking bets (obvious typing errors
            in quotes, mismatch of quotes in different positions, etc.) and under other circumstances confirming
            incorrect character of the bet, the Bookmaker is entitled to declare these bets invalid. The Bookmaker
            reserves the right to cancel any bet made under the knowingly erroneous line or after the beginning of the
            event.
            The Client cannot change or cancel the bet after placing it in the Website server and receiving the Website
            notice of taking the bet.
            Communication faults or other technical failures in communications of the Client are not the reason to
            cancel the bet, if the bet is registered in the Website server.
            The Bookmaker reserves the right to refuse to take the bet from any person without explaining the reasons
            and without prior written notice.


            V. Special Conditions

            If the event did not take place in the specified time, was postponed or interrupted, all bets on the event
            are calculated with the coefficient of 1. In exceptional situations, the Bookmaker reserves the right to
            retain bets, but for the period not exceeding 72 hours since the time of the beginning of the event
            specified in the official source of the event.
            In case the result is canceled or changed, the initial result is used to make calculations.
            If in the series of matches one of the participants refuses to continue the game for any reason or is
            disqualified, he / she is considered to have lost in all other games of the series. In this case, payout is
            made on the basis of the obtained result. If refusal / disqualification occur before the beginning of the
            event, bets are to be returned. The event is not considered to have started, if the game / match / series
            continued for less than 1 minute 30 seconds and is stopped for technical or any other reasons not related
            with the gaming process, of if the decision of replay is made.
            If the final outcome of the event was not initially provided for by the Bookmaker in the Money Line, all win
            and draw bets can be recalculated with the coefficient of 1. This clause can also apply to the cases, when
            the wager is made as tournament win bet, and several participants (teams) were announced to be the
            tournament winners.
            The Bookmaker announces valid results on the basis of official reports, notices on the official sites of the
            event and other sources of information after the event is complete.
            In case of disputable situations, mismatch of results published by different sources, obvious mistakes,
            final decision of determining the bet calculation results is made by the Bookmaker.
            The Client is obliged to check, whether the bet page is filled in correctly meeting requirements of the
            Rules, as in case of mistake, irrespective of the reason of this situation and the party at fault, the
            Bookmaker reserves the right to return the bet.
            If, as a result of bet recalculation because of incorrectly entered result, the Bettor has negative balance,
            his / her account is locked until the Bettor replenishes the account for the amount not less than the amount
            of the negative balance. Otherwise, the Bookmaker reserves the right to stop taking bets from this Bettor.
            Bets made before the recalculation remain valid.
            Claims on disputable issues are accepted on the basis of the written application within 10 days since the
            day of determining the outcome for the respective bet. No claims are accepted upon expiry of this term. In
            disputable situations beyond the scope of the Rules, the final decision is made by the Bookmaker.
            If wrong number of games in the match was specified, then, at the Bookmaker’s discretion, all bets, express
            bets and advantages (as well as each of the kind of bets separately) can be calculated with the coefficient
            of 1.

            VI. Financial Restrictions

            Minimum bet on any event is equivalent to 1 (one) unit of the used betting virtual account.
            Maximum winning odds for the bet are 200. The payout is made only to the betting virtual account, from which
            the Wager was made.
            Maximum amount of bet for an event depends on the game and the event, is determined by the Bookmaker on a
            case-by-case basis for each event and each kind of the bet, and is subject to change without prior notice to
            Clients. The Bookmaker reserves the right to restrict maximum bet on certain events, and to introduce and
            cancel special restrictions on accounts of individual Bettors without notice and explaining the reasons.
            Maximum payout per bet is equal to equivalent of 10000 (Ten Thousand) units in the currency of used betting
            virtual currency account. The Bookmaker is entitled either to increase or to reduce maximum amount of bet
            payout for the Client at its own discretion.


            VII. Rules of Calculating the Win and Withdrawing Funds from the Account

            Winning amount is credited only to the Primary Account.
            In case the balance of the Primary Account is positive, and provided the Client has over 15 units in the
            Primary Account, the Client is entitled to order payout not exceeding the Primary Account Balance. The
            payout is made in US Dollars in accordance with the rate of the Primary Account unit established by the
            Bookmaker to US Dollar (one to one).
            After the payout is ordered, the equivalent required for payout is locked in the Client’s Primary Account
            and cannot be used by the Client to make wagers.
            The Bookmaker is entitled to restrict possible payout methods to the Client by the methods that were used by
            the Client to replenish the Primary Account.
            Additionally, the Bookmaker is entitled to restrict maximum amount of payout proceeding from the amount of
            made bets, and in some cases to restrict it completely at its own discretion to prevent fraud and combat
            money laundering.
            Administration reserves its right to refuse the cash out if it was made before player gained the certain
            number of bonus points after last deposit. The formula of necessary points gained is amount of requested
            cash out multiplied by 30.
            If the payout to a plastic card is ordered, the Bookmaker may request that the Client submits additional
            documents to verify and confirm his / her identity.
            If the Client uses Skrill or Neteller payment system to withdraw funds, it is allowed to use only the same
            Skrill or Neteller account that was used to replenish the Primary Account. In this case, additional
            verification is usually not required.
            Payouts are usually made by the Bookmaker within 24 hours, if verification is not required, and within three
            days, if verification is required. The Bookmaker is not held liable for terms and duration of crediting
            funds by payment systems.
            The Bookmaker establishes restrictions on number of commission-free funds withdrawals within a certain
            calendar period. The Client is entitled to order commission-free payout not oftener than once in 30 calendar
            days. Should the Client withdraw funds more frequently than once in 30 days, the Client is charged an
            additional commission in the amount of 4% of the ordered amount.

            VIII. Website Content Use

            All rights to the Website Content belong to the Bookmaker and are protected against illegal use.
            Clients and other parties are not transferred or assigned any rights to the Content, except for the right to
            view (reproduce) the Website and its Content in the browser for personal use and within the limits required
            for Website running on user hardware. It is not permitted to distribute (whether free of charge or for a
            fee), copy, save any part of the Content on any media, print, reproduce, process and modify the Content,
            reprint, or otherwise replicate the Content or part thereof without prior written consent of the Bookmaker.
            The Bookmaker and its legal representatives reserve the right, without warning and notice to the wrongdoer,
            to apply to the court and bodies of inquiry and investigation to seek protection of its copyright.

            IX. Website Use Rules

            The Website uses cookies and javascript, and also gathers anonymous information of Website use by employing
            various means, including, without limitation, Google Analytics and Facabook Marketing Tools. The Client
            agrees to use hardware required to ensure correct operation of the Website, and agrees to provide anonymous
            information, including IP address and cookies, to the Website and third-party services employed at the
            Website.
            The following is prohibited in the Website, include the Website chat:
            any actions, including publishing information of insulting character, using foul expressions, obscenities,
            threats, pornography, scenes of violence, race hatred appeals and other means and manifestations of
            extremism, aggression, disorderly conduct, insults, slander, distribution of unreliable or discrediting
            information, disrespectful attitude, etc;
            trolling, flood, spam;
            access or attempt to access Website sections closed by the Bookmaker, bypassing authentication and
            protection systems, and any other actions that can hamper Website operation and (or) threaten security of
            Clients, the Bookmaker or third parties;
            infringement of a copyright and other intellectual property rights of third parties;
            publication of advertisement, inter alia, in the hidden form, without prior consent of the Bookmaker;
            mentioning other Bookmaker chains and sites, including their quotes and promotions;
            extracting Website Content by any methods, including through automated means.
            When detecting the prohibited actions, the Bookmaker, at its own discretion, shall apply the following
            measures (whether separately or in any combination) to a person violating the Rules:
            warning;
            temporary denied access to the Website or certain pages;
            deletion of the Client’s account without the right to restore it;
            Website access lock;
            moderation or deletion of the information published in violation of the Rules;
            other actions at the Bookmaker’s discretion.

            X. Limitation of Liability

            In case of identifying fraud related with financial transactions and betting the persons at fault shall be
            subject to criminal liability.
            The Bookmaker shall not provide guarantee and shall not undertake obligations regarding the Website except
            those specified in these Rules. In particular, the Bookmaker shall not undertake any obligations concerning
            Website Content, functionality, reliability, availability, complete and reliable character of data,
            information and meeting users’ needs.
            The Bookmaker shall not be held liable:
            for consequences arising out of using the Website and information placed in it. The user shall not put
            claims to the Bookmaker concerning consequences of his / her decisions arising out of or related with the
            Website Content or functionality;
            for damage, loss of profit, loss of income, loss of data, financial loss, as well as direct or indirect
            damage incurred by Clients and other parties as a result of using the Website, its Content or functionality;
            for disclosure of Client’s confidential data in case of requests by law-enforcement authorities, as well in
            cases of preventing frauds and protecting its interests.
            Website functionality is provided to Clients as is. In particular, this means that the Bookmaker is not held
            liable and shall not compensate for damage arising out of:
            issues arising during update, support and operation of the Website;
            results of Website, Content or functionality use failing to meet expectations of the Client and other
            persons;
            deletion, failure, distortion or impossibility to save any information element and other communications
            data;
            hardware troubles and electric equipment failures.
            The Bookmaker may suspend user’s access to the Website on a temporary basis for the time required for
            technical deployment of updates and other maintenance.

            XI. Miscellaneous

            The Bookmaker is entitled to amend these Rules without agreement with Clients and other parties by notifying
            them thereof through publishing new revision of the Rules in the Website.
            The Client shall regularly, and as a rule, at least once per month, read content of the Rules in order to
            follow their amendments in a timely manner.
            New revision of the Rules shall come into force since the time they are published in the Website, unless
            other term of amendments coming into force is defined by the Bookmaker. Conditions of the previously made
            bet remain unchanged, while bets made since the date amendments come into force shall be subject to revised
            conditions.
            The Bookmaker may terminate, restrict or amend conditions of providing access to the Website either to all,
            group of or one Client at any time. The Bookmaker shall exercise its reasonable efforts to warn the Clients
            there of by notice in the Website (except for force majeure or bad faith or wrongful conduct of the Client).

            These Rules are governed by the registration of the Republic of Costa Rica.
            The valid revision of the Rules is as of October 10, 2014.
        </textarea>
        <br>
        <br>
        <br>
    </div>
</div>