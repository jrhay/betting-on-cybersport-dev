<div class="main-leftcol">

    <div class="b-slider1">

        <div class="slider1 style6">

            <div id="slider1">

                <ul>
                    <?php  /** @var IndexMainSlides[] $indexMainSlides */ ?>
                    <?php  foreach ($indexMainSlides as $slide): ?>
                        <li class="slide">

                            <a href="<?php echo  $slide->ref ?>">
                                <img
                                    src="<?php echo Yii::app()->baseUrl ?>/../images/indexSlides/<?php echo  Yii::app()->language == 'ru' ? $slide->slide : $slide->slide_eng ?>"
                                    alt=""/>
                            </a>

                            <div class="text">

                                <h2 class="title"><?php echo Yii::app()->language == 'ru' ? $slide->title : $slide->title_eng ?></h2>

                                <p><?php echo  Yii::app()->language == 'ru' ? $slide->text : $slide->text_eng ?></p>

                            </div>

                        </li>
                    <?php  endforeach ?>

                </ul>

            </div>

        </div>

        <a class="link btn-style2" href="<?php echo  Yii::app()->createUrl('/games') ?>"
           title=""><?php echo  Yii::t('site', 'Сделать ставку прямо сейчас') ?></a>

    </div>

</div>

<div class="main-rightcol">

    <div class="b-slider2">

        <div id="slider2" class="style6">

            <ul>
                <?php  /** @var IndexSlides[] $indexSlides */ ?>
                <?php  foreach ($indexSlides as $slide): ?>

                    <li style="background: url(<?php echo  Yii::app()->baseUrl . '/../images/indexSlides/' . (Yii::app()->language == 'ru' ? $slide->slide : $slide->slide_eng) ?>) ">

                        <p class="top-text"><?php echo  Yii::app()->language == 'ru' ? $slide->title : $slide->title_eng ?></p>

                        <p class="bottom-text"><?php echo  Yii::app()->language == 'ru' ? $slide->text : $slide->text_eng ?></p>

                    </li>
                <?php  endforeach ?>

            </ul>

        </div>

    </div>

    <div id="tabs" class="tabs">

        <div class="g-clearfix h-link">

            <a href="#tab1" class="tab-link first"><?php echo  Yii::t('site', 'Рекомендуем') ?></a>

            <a href="#tab2" class="tab-link"><?php echo  Yii::t('site', 'Крайняя ставка') ?></a>

            <a href="#tab3" class="tab-link last"><?php echo  Yii::t('site', 'Выплата') ?></a>

        </div>


        <div id="tab1" class="section">

            <table class="main-tab">

                <thead>

                <tr>

                    <th class="first"><?php echo  Yii::t('site', 'Дата') ?></th>
                    <th class=""></th>

                    <th><?php echo  Yii::t('site', 'Игра') ?></th>

                    <!--                    <th>--><?php  //= Yii::t('site','Кое-нт')?><!--</th>-->

                    <th class="last"><?php echo  Yii::t('site', 'Сумма') ?></th>

                </tr>

                </thead>

                <tbody>

                <?php  /** @var Userbets[] $bets */
                /** @var RecommendedBets $recommendedBets */
                ?>
                <?php  foreach ($recommendedBets as $bet): ?>
                    <tr>

                        <td class="first">&nbsp<?php echo  date('d.m', $bet->date) ?></td>
                        <td><img width="20" height="20"
                                 src="<?php echo  Yii::app()->baseUrl ?>/images/games/<?php echo $bet->game->category->image ?>"
                                 alt=""/></td>
                        <td>

                            <?php echo
                            CHtml::link(
                                $bet->text,
                                array('site/bet'),
                                array(
                                    'submit' => array('site/bet'),
                                    'params' => array('gameId' => $bet->game->id),
                                    'style' => 'text-decoration: none;color: #CBCBCB'
                                )
                            );
                            /*$bet->text*/
                            ?>
                        </td>

                        <!--                        <td>--><?php  //= Helper::formatKoef($bet->koef) ?><!--</td>-->

                        <td class="last"><?php echo  $bet->summ ?>$</td>

                    </tr>
                <?php  endforeach ?>

                </tbody>

            </table>

        </div>

        <div id="tab2" class="section">

            <table class="main-tab">

                <thead>

                <tr>

                    <th class="first"><?php echo  Yii::t('site', 'Дата') ?></th>
                    <th class=""></th>
                    <th><?php echo  Yii::t('site', 'Игра') ?></th>

                    <th><?php echo  Yii::t('site', 'Кое-нт') ?></th>

                    <th class="last"><?php echo  Yii::t('site', 'Сумма') ?></th>

                </tr>

                </thead>

                <tbody>
                <?php  /** @var Userbets[] $bets */ ?>
                <?php  foreach ($bets as $bet):?>
                    <tr>

                        <td class="first">&nbsp<?php echo  date('d.m', $bet->game->game_start_at) ?></td>
                        <td><img width="20" height="20"
                                 src="<?php echo  Yii::app()->baseUrl ?>/images/games/<?php echo $bet->game->category->image ?>"
                                 alt=""/></td>
                        <td><?php echo  $bet->game->team1->name . ' vs ' . $bet->game->team2->name ?></td>

                        <td><?php echo  Helper::formatKoef($bet->koef) ?></td>

                        <td class="last"><?php echo  $bet->summ ?>$</td>

                    </tr>
                <?php  endforeach ?>

                </tbody>

            </table>

        </div>

        <div id="tab3" class="section">

            <table class="main-tab">

                <thead>

                <tr>

                    <th class="first "><?php echo  Yii::t('site', 'Дата') ?></th>
                    <th></th>
                    <th><?php echo  Yii::t('site', 'Игра') ?></th>

                    <th><?php echo  Yii::t('site', 'Кое-нт') ?></th>

                    <th class="last"><?php echo  Yii::t('site', 'Выигрыш') ?></th>

                </tr>

                </thead>

                <tbody>

                <?php  /** @var Userbets[] $paidBets */ ?>
                <?php  foreach ($paidBets as $bet): ?>
                    <?php  if ($bet->isWin): ?>
                        <tr>

                            <td class="first">&nbsp<?php echo  date('d.m', $bet->game->game_start_at) ?></td>
                            <td><img width="20" height="20"
                                     src="<?php echo  Yii::app()->baseUrl ?>/images/games/<?php echo $bet->game->category->image ?>"
                                     alt=""/></td>
                            <td><?php echo  $bet->game->name ?></td>

                            <td><?php echo  Helper::formatKoef($bet->koef) ?></td>

                            <td class="last"><?php echo  Helper::formatMoney($bet->summ * $bet->koef) ?>$</td>

                        </tr>
                    <?php  endif ?>
                <?php  endforeach ?>

                </tbody>

            </table>

        </div>

    </div>

</div>

</div>

<div class="g-clearfix">

    <div class="leftcol-s">

        <?php $this->widget('application.widgets.banner.banner', array(
            'height' => '350',
            'width' => '310',
        )); ?>

        <div class="b-comments">

            <div class="title1 style3"><?php echo  Yii::t('site', 'Отзывы') ?></div>

            <ul class="list style3">
                <?php  /** @var Reviews[] $reviews */ ?>
                <?php  foreach ($reviews as $review): ?>
                    <li class="b-comment">

                        <div class="user style4 g-clearfix">

                            <div class="ico style2"><span></span></div>

                            <div class="date style2"><?php echo  $review->team ?></div>

                            <div
                                class="name"><?php echo  Yii::app()->language === 'ru' ? $review->name : $review->name_eng ?></div>

                        </div>

                        <div class="body style4">

                            <div class="text">

                                <?php echo  Yii::app()->language === 'ru' ? $review->text : $review->text_eng ?>

                            </div>

                        </div>

                    </li>
                <?php  endforeach ?>


            </ul>

            <a class="link-style1 btn-style1" href="<?php echo  Yii::app()->createUrl('site/feedback')?>" title=""><?php echo  Yii::t('site', 'Читать все комментарии') ?></a>

        </div>

    </div>

    <div class="rightcol">

        <div class="games">

            <div class="g-clearfix">

                <div class="b-game">

                    <a href="<?php echo  Yii::app()->createUrl('games')?>" title="">

                        <div class="visual">

                            <img src="<?php echo Yii::app()->theme->baseUrl ?>/img/dota.jpg" alt=""/>

                        </div>

                        <div class="title">DOTA 2</div>

                    </a>

                </div>

                <div class="b-game">

                    <a href="<?php echo  Yii::app()->createUrl('games')?>"" title="">

                        <div class="visual">

                            <img src="<?php echo Yii::app()->theme->baseUrl ?>/img/fighting.png" alt=""/>

                        </div>

                        <div class="title">FIGHTING</div>

                    </a>

                </div>

                <div class="b-game">

                    <a href="<?php echo  Yii::app()->createUrl('games')?>"" title="">

                        <div class="visual">

                            <img src="<?php echo Yii::app()->theme->baseUrl ?>/img/shooter1.png" alt=""/>

                        </div>

                        <div class="title">SHOOTER</div>

                    </a>

                </div>

                <div class="b-game">

                    <a href="<?php echo  Yii::app()->createUrl('games')?>"" title="">

                        <div class="visual">

                            <img src="<?php echo Yii::app()->theme->baseUrl ?>/img/simulator.png" alt=""/>

                        </div>

                        <div class="title">SIMULATOR</div>

                    </a>

                </div>

                <div class="b-game">

                    <a href="<?php echo  Yii::app()->createUrl('games')?>"" title="">

                        <div class="visual">

                            <img src="<?php echo Yii::app()->theme->baseUrl ?>/img/strategy.png" alt=""/>

                        </div>

                        <div class="title">STRATEGY</div>

                    </a>

                </div>

                <div class="b-game">

                    <a href="<?php echo  Yii::app()->createUrl('games')?>"" title="">

                        <div class="visual">

                            <img src="<?php echo Yii::app()->theme->baseUrl ?>/img/special.png" alt=""/>

                        </div>

                        <div class="title">SPECIAL</div>

                    </a>

                </div>

            </div>

            <a class="link-style1 btn-style1" href="<?php echo  Yii::app()->createUrl('/games') ?>"
               title=""><?php echo  Yii::t('site', 'Сделать ставку прямо сейчас') ?></a>

        </div>

        <div class="b-reasons">

            <div class="title1 style3"><?php echo  Yii::t('site', '7 Причин работать с нами') ?></div>

            <div class="body style6">

                <div class="b-reason">

                    <div class="visual style4">

                        <div class="visual-inner style6">

                            <div class="inner">

                                <img src="<?php echo Yii::app()->theme->baseUrl ?>/img/reason01.png" alt=""/>

                            </div>

                        </div>

                    </div>

                    <div class="title"><?php echo  Yii::t('site', 'Кибер спорт') ?><br> - <?php echo  Yii::t('site', 'растущая') ?><br/>

                        <?php echo  Yii::t('site', '(из точки А в точку Б)') ?>
                    </div>

                </div>

                <div class="b-reason">

                    <div class="visual style4">

                        <div class="visual-inner style6">

                            <div class="inner">

                                <img src="<?php echo Yii::app()->theme->baseUrl ?>/img/reason02.png" alt=""/>

                            </div>

                        </div>

                    </div>

                    <div class="title"><?php echo  Yii::t('site', 'Регулярные') ?><br> Live<br/>

                    </div>

                </div>

                <div class="b-reason">

                    <div class="visual style4">

                        <div class="visual-inner style6">

                            <div class="inner">

                                <img src="<?php echo Yii::app()->theme->baseUrl ?>/img/reason03.png" alt=""/>

                            </div>

                        </div>

                    </div>

                    <div class="title"><?php echo  Yii::t('site', 'Эксклюзивные') ?><br> <?php echo  Yii::t('site', 'ставки') ?><br/>

                        <?php echo  Yii::t('site', '(из точки А в точку Б)') ?>
                    </div>

                </div>

                <div class="b-reason">

                    <div class="visual style4">

                        <div class="visual-inner style6">

                            <div class="inner">

                                <img src="<?php echo Yii::app()->theme->baseUrl ?>/img/reason06.png" alt=""/>

                            </div>

                        </div>

                    </div>

                    <div class="title"><?php echo  Yii::t('site', '24/7 поддержка') ?></div>

                </div>

                <div class="b-reason">

                    <div class="visual style4">

                        <div class="visual-inner style6">

                            <div class="inner">

                                <img src="<?php echo Yii::app()->theme->baseUrl ?>/img/reason05.png" alt=""/>

                            </div>

                        </div>

                    </div>

                    <div class="title"><?php echo  Yii::t('site', '100% выплаты') ?><br/>
                        <?php echo  Yii::t('faq', 'победителям') ?>
                    </div>

                </div>

                <div class="b-reason">

                    <div class="visual style4">

                        <div class="visual-inner style6">

                            <div class="inner">

                                <img src="<?php echo Yii::app()->theme->baseUrl ?>/img/reason04.png" alt=""/>

                            </div>

                        </div>

                    </div>

                    <div class="title"><?php echo  Yii::t('site', 'Зарабатывайте на ') ?><br/>
                        <?php echo  Yii::t('site', 'своих ставках') ?>
                    </div>

                </div>

                <div class="b-reason">

                    <div class="visual style4">

                        <div class="visual-inner style6">

                            <div class="inner">

                                <img src="<?php echo Yii::app()->theme->baseUrl ?>/img/reason07.png" alt=""/>

                            </div>

                        </div>

                    </div>

                    <div class="title"><?php echo  Yii::t('site', 'Официальный спонсор') ?><br/>
                        <?php echo  Yii::t('site', 'Киберкоманд') ?>
                    </div>

                </div>

            </div>

        </div>

    </div>
