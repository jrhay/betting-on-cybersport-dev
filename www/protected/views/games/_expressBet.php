<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'expressBet-form',
    'enableAjaxValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'validateOnChange' => true,
    ),
    'htmlOptions' => array(
        'class' => 'form-inline tournaments-tab',
        'style' => "width: 800px")
));
?>
    <h1><?php echo Yii::t('site','Экспрессы')?></h1>
    <br>
    <br>
    <table class="items" >
        <thead>
        <tr>
            <th id="yw0_c0"><a class="sort-link" href="#"><?php echo Yii::t('site', 'Игра стартует')?></a></th>
            <th id="yw0_c1"><a class="sort-link" href="#"><?php echo Yii::t('site', 'Игра')?></a></th>
            <th id="yw0_c2"><a class="sort-link" href="#"><?php echo Yii::t('site', 'Игра 1')?></a></th>
            <th id="yw0_c3"><a class="sort-link" href="#"><?php echo Yii::t('site', 'Игра 2')?></a></th>
            <th id="yw0_c4"><a class="sort-link" href="#"><?php echo Yii::t('site', 'Коэфф 1')?></a></th>
            <th id="yw0_c5"><a class="sort-link" href="#"><?php echo Yii::t('site', 'Ничья')?></a></th>
            <th id="yw0_c6"><a class="sort-link" href="#"><?php echo Yii::t('site', 'Коэфф 2')?></a></th>
            <th id="yw0_c7"><a class="sort-link" href="#"><?php echo Yii::t('site', 'Событие')?></a></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($games as $key => $game) {
            $trClass = ($key % 2) ? 'even' : 'odd';
            ?>
            <?php
            if (!Yii::app()->user->isGuest) {
                $userId = Yii::app()->user->id;
                $userModel = User::model()->findByPk($userId);
                $dateFormatted = Helper::getDateFormattedForUser(Yii::t('information', 'Date format 1:: dd MMM, HH:mm'), $game->game_start_at, $userModel);
            } else {
                //getting server TimeZone
                $origin_tz = date_default_timezone_get();
                $origin_dtz = new DateTimeZone($origin_tz);
                $origin_dt = new DateTime("now", $origin_dtz);
                $serverTimeZoneInHours =  $origin_dtz->getOffset($origin_dt);

                //getting now timestamp
                $utcTimestamp = $game->game_start_at;
                $dateTime = new DateTime('now', new DateTimeZone('UTC'));
                $dateTime->setTimestamp($utcTimestamp);
                $timestamp = $dateTime->getTimestamp();
                $timestampForYii = $timestamp - $serverTimeZoneInHours * 3600;

                $formatString = "Date format 1:: dd MMM, HH:mm";
                $format = Helper::getDateFormat($formatString);
                $dateFormatted = Yii::app()->dateFormatter->format($format, $timestampForYii);
            }
            ?>
            <tr class="<?php echo $trClass ?>">
                <td><?php echo $dateFormatted; ?></td>
                <td><img width="20" height="20" src="<?php echo  Yii::app()->baseUrl?>/images/games/<?php echo $game->category->image ?>" alt=""/></td>
                <td><?php echo $game->team1->name ?></td>
                <td><?php echo $game->team2->name ?></td>
                <td><?php echo $game->team1_koef ?><?php echo CHtml::checkBox('games[' . $game->id . '][1]', false, array('class' => 'dis express-bet-game-' . $game->id, 'id' => 'games_' . $game->id . '_1')); ?></td>
                <td><?php echo ($game->draw_koef) ? $game->draw_koef . CHtml::checkBox('games[' . $game->id . '][0]', false, array('class' => 'dis express-bet-game-' . $game->id, 'id' => 'games_' . $game->id . '_0')) : '' ?></td>
                <td><?php echo $game->team2_koef ?><?php echo CHtml::checkBox('games[' . $game->id . '][2]', false, array('class' => 'dis express-bet-game-' . $game->id, 'id' => 'games_' . $game->id . '_2')); ?></td>
                <td><?php echo $game->event->text ?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <div class="btn-dis"  title=""><?php echo Yii::t('site','Коэф')?>: <span id="summ_koef_js">0</span></div>

    <div class="express-summ-div">
        <?php echo $form->label($model, 'summ'); ?>
        <?php echo $form->textField($model, 'summ', array()); ?> $
        <?php echo $form->error($model, 'summ'); ?>
    </div>
    <div id="sum1_koef" class="info"><?php echo Yii::t('site', 'Вы можете выиграть')?>: <span id="sum1_div">0</span>$</div>
    <div id = "warning_m"class="info"  title=""><span id="summ_koef_js"></span></div>
<?php

$url = '';
$onclick = array();
if(Yii::app()->user->isGuest) {
    $url = Yii::app()->createUrl('/user/register');
    $onclick = array('onclick' => 'changeLocation()');
}

echo CHtml::submitButton(Yii::t('site','Make a bet'), array_merge(array(
    'class' => 'link btn-style1',
), $onclick)); ?>
<script>
    function changeLocation() {
        window.location = "<?php echo $url?>";
    }
</script>
<?php $this->endWidget(); ?>
