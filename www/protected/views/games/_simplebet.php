<?php
/** @var Games $game */
$timeLabel = time();
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'simpleBet-form',
    'enableAjaxValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'validateOnChange' => true,
    ),
    'htmlOptions' => array('class' => 'form-inline')
));
?>
<div class="g-clearfix">
    <div class="col1">
        <div class="name"><?php echo $game->team1->name ?></div>
        <div class="img">
            <div class="valign">
                <?php  if (!empty($game->team1->teamLogo)): ?>
                    <img src="<?php echo  Yii::app()->baseUrl . '/../images/teamsLogos/' . $game->team1->teamLogo ?>" alt=""/>
                <?php  endif ?>
            </div>
        </div>
        <div class="icos">
            <!--            <img src="img/avatar1.jpg" alt=""/>-->
            <!--            <img src="img/country1.jpg" alt=""/>-->
        </div>
        <div class="k"><?php echo  Yii::t('site', 'Коэффициент') ?> :<?php echo $game->team1_koef ?></div>
    </div>
    <div class="col2">
        <div class="big">VS</div>
        <div class="game">
            <span><?php echo $game->category->title ?></span><br/>
            <!--            Proleague-->
        </div>
        <div class="time">
            <?php
            if(!Yii::app()->user->isGuest) {
                echo Helper::getDateFormattedForUser(Yii::t('information', 'Date format 1::MMMM dd, y HH:mm'), $game->game_start_at, User::model()->findByPk(Yii::app()->user->id));
            } else {
                echo gmdate('F d, Y H:i', $game->game_start_at);
            }
            ?>
            <br/>
            <!--            <div id="countdown"></div>-->
            <!--            --><?php  // if($game < time()): ?>
            <div style="display: <?php  echo $game->game_start_at < time() ? 'none' : 'block' ?>"
                 id="countdown_time<?php echo $timeLabel ?>"></div>
            <!--            --><?php  // endif ?>
        </div>
        <div class="info"><?php echo  Yii::t('site', 'Макс. ставка') ?> :<?php echo $game->max_sum ?> $</div>
        <div class="k"><?php echo  Yii::t('site', 'Коэффициент') ?> :<?php echo $game->draw_koef ?></div>
    </div>
    <div class="col1">
        <div class="name"><?php echo $game->team2->name ?></div>
        <div class="img">
            <div class="valign">
                <?php  if (!empty($game->team2->teamLogo)): ?>
                    <img src="<?php echo  Yii::app()->baseUrl . '/../images/teamsLogos/' . $game->team2->teamLogo ?>" alt=""/>
                <?php  endif ?>
            </div>
        </div>
        <div class="icos">
            <!--            <img src="img/avatar2.jpg" alt=""/>-->
            <!--            <img src="img/country2.jpg" alt=""/>-->
        </div>
        <div class="k"><?php echo  Yii::t('site', 'Коэффициент') ?> :<?php echo $game->team2_koef ?></div>
    </div>
    <!--    <div class="map">MAP 1</div>-->
    <div style="margin-bottom: 10%">
        <?php  if (!empty($game->result) && ($game->status != Games::GAME_STATUS_CANCELED)): ?>
            <h1><?php echo  Yii::t('site', 'Results') ?>:</h1>
            <h3><?php echo  Yii::t('site', 'WIN ') ?>:</h3>
            <h3 class="k">
                <?php  switch ($game->result->result) {
                    case Userbets::DRAW_WIN:
                        echo 'Draw';
                        break;
                    case Userbets::TEAM1_WIN:
                        echo $game->team1->name;
                        break;
                    case Userbets::TEAM2_WIN:
                        echo $game->team2->name;
                        break;
                }?>
            </h3>
            <h3><?php echo  Yii::t('site', 'score ') ?>:</h3>
            <h3 class="k"><?php echo  $game->result->score ?></h3>
        <?php  elseif( empty($game->result) && ($game->status != Games::GAME_STATUS_CANCELED)): ?>
            <h3><?php echo  Yii::t('site', 'score ') ?></h3>
            <h3 class="k">0:0</h3>
        <?php  elseif($game->status == Games::GAME_STATUS_CANCELED): ?>
            <h3><?php echo  Yii::t('site', 'GAME IS CANCELED') ?></h3>
            <h3 class="k">0:0</h3>
        <?php  endif?>
    </div>

    <div class="links">
        <?php  if (empty($game->result) && $game->game_start_at > time()): ?>
            <span class="toggle-bet link btn-style1"><?php echo  Yii::t('site', 'Сделать ставку') ?> </span>
        <?php  endif ?>
        <a class="link btn-style2 show-user-bets" href="javascript:void(0)"
           title=""><?php echo  Yii::t('site', 'Ваша ставка') ?></a>
    </div>
    <div class="hidden my-bets" style="display: none">
        <?php $this->renderPartial('_mybets', array('alreadyMadeBets' => $alreadyMadeBets)); ?>
    </div>
    <div class="hidden to-bet" style="display: none">
        <!--            <div class="switch-money style4 g-clearfix">-->
        <!--                <input id="switch-money1" name="switch-money" type="radio" class="select-styled s1"/>-->
        <!--                <label for="switch-money1">РЕАЛЬНЫЕ ДЕНЬГИ</label>-->
        <!--                <input id="switch-money2" name="switch-money" type="radio" class="select-styled s2" checked="checked"/>-->
        <!--                <label for="switch-money2">PLAY MONEY</label>-->
        <!--            </div>-->

        <div class="switch-money style4 g-clearfix">
            <?php echo $form->radioButtonList($model, 'moneyType', array(Usercash::REAL_MONEY => 'Real Money', Usercash::PLAY_MONEY => 'cyberMoney'), array('class' => 'elect-styled s1', 'separator' => '', 'template' => '{input}{label}')) ?>
        </div>

        <div class="g-clearfix">
            <div class="col" style="width: 100%">
                <?php $teamsValues = array();
                $teamsValues[1] = $game->team1->name;
                if ($game->draw_koef) {
                    $teamsValues[0] = Yii::t('games', 'Ничья');
                }
                $teamsValues[2] = $game->team2->name;
                ?>
                <div
                    class="name<?php echo (!$game->draw_koef) ? ' twoResultsOnly' : ''; ?>"><?php echo $form->radioButtonList($model, 'teamID', $teamsValues, array('separator' => '', 'template' => '<div class="radio_group">{input} {label}</div>')) ?>

                </div>

                <div class="input" style="width: 100%;overflow: hidden;">
                    <label for="sum1"><?php echo  Yii::t('site', 'Сумма') ?>:</label>
                    <?php echo $form->textField($model, 'summ', array()); ?><span id="sign">$</span>
                    <?php echo $form->error($model, 'summ'); ?>
                    <?php echo $form->error($model, 'teamID'); ?>
                    <?php echo $form->error($model, 'moneyType'); ?>
                </div>
                <?php echo $form->hiddenField($model, 'gameID', array('value' => $game->id)); ?>
                <div class="info"><?php echo  Yii::t('site', 'Вы можете выиграть') ?>: <span id="sum1_div">0</span>&nbsp<span id="sign_can_win">$</span></div>
            </div>
        </div>

        <?php

        $url = '';
        $onclick = array();
        if(Yii::app()->user->isGuest) {
            $url = Yii::app()->createUrl('/user/register');
            $onclick = array('onclick' => 'changeLocation()');
        }

        echo CHtml::submitButton(Yii::t('site','Make a bet'),
            array_merge(array(
                'class' => 'link-style1 btn-style1',
            ),$onclick)
        ); ?>

    </div>
</div>
<?php $this->endWidget(); ?>
<script>
    function changeLocation() {
        window.location = "<?php echo $url?>";
    }
    $('#standartBetForm_moneyType_0').on('change',function(){
        $('#sign').html('$');
        $('#sign_can_win').html('$');
    });
    $('#standartBetForm_moneyType_1').on('change',function(){
        $('#sign').html('CM');
        $('#sign_can_win').html('CM');
    });
    $('#standartBetForm_summ').on('input', function () {
        var k1 = <?php echo $game->team1_koef ?>;
        var k2 = <?php echo $game->team2_koef ?>;
        <?php if ($game->draw_koef) { ?>
        var k0 = <?php echo $game->draw_koef ?>;
        <?php } ?>
        koefSuffix = $('input[name="standartBetForm[teamID]"]:checked', '#simpleBet-form').val();
        if (typeof(koefSuffix) != 'undefined') {
            jQuery('#sum1_div').text((jQuery(this).val() * eval('k' + koefSuffix)).toFixed(2));
            if (jQuery(this).val() ><?php echo $game->max_sum ?>) {
                jQuery(this).val(0);
                alert('<?php echo Yii::t("games","Max Bet limit") ?>');
            }
        }
    });
    $('input[name="standartBetForm[teamID]"]', '#simpleBet-form').on('change', function () {
        $("#standartBetForm_summ").trigger("input");
    });


    var diff =
    <?php echo $game->game_start_at - time() ?>*
    1000; // Move this outside the function so that it can be changed.
    // If you don't, it will get reset to its original value
    // every time updateETime runs.

    function updateETime() {

        function pad(num) {
            return num > 9 ? num : '0' + num;
        };


        days = Math.floor(diff / (1000 * 60 * 60 * 24)),
            hours = Math.floor(diff / (1000 * 60 * 60)),
            mins = Math.floor(diff / (1000 * 60)),
            secs = Math.floor(diff / 1000),

            dd = days,
            hh = hours - days * 24,
            mm = mins - hours * 60,
            ss = secs - mins * 60;
        if (typeof(document.getElementById("countdown_time<?php echo $timeLabel ?>")) !== 'undefined') {
            document.getElementById("countdown_time<?php echo $timeLabel ?>")
                .innerHTML =
                dd + ' дней ' +
                pad(hh) + ':' + //' hours ' +
                pad(mm) + ':' + //' minutes ' +
                pad(ss); //+ ' seconds' ;

            diff -= 1000; // Every time the function runs, subtract one second from diff.
        }

        //TODO: diff = 0;
    }
    var timerInterval = setInterval(updateETime, 1000);
    //clearInterval(timerInterval);
</script>