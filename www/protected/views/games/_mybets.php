<?php if ($alreadyMadeBets) { ?>
    <table class="game-tab">
        <thead>
        <tr class="heading">
            <th><?php echo Yii::t('site','Исход')?></th>
            <th><?php echo Yii::t('site','Коэфф')?></th>
            <th><?php echo Yii::t('site','Сумма ставки')?></th>
            <th><?php echo Yii::t('site','Тип денег')?></th>
            <th  class="last"><?php echo Yii::t('site','Возможный выигрыш')?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($alreadyMadeBets as $alreadyMadeBet) { ?>
                <tr>
                    <td><?php
                        if (!$alreadyMadeBet->result) {
                            $text = Yii::t('site','Draw');
                        } else {
                            $suffix = 'team'.$alreadyMadeBet->result;
                            $text = $alreadyMadeBet->game->$suffix->name;
                        }
                        echo $text;
                        ?></td>
                    <td><?php echo $alreadyMadeBet->koef ?></td>

                    <td><?php echo $alreadyMadeBet->summ ?> $</td>
                    <td><?php echo ($alreadyMadeBet->moneyType==Usercash::REAL_MONEY)?Yii::t('site','Real Money'):Yii::t('site','Cyber Money') ?></td>
                    <td><?php echo Helper::formatMoney($alreadyMadeBet->koef*$alreadyMadeBet->summ) ?> $</td>
                </tr>
       <?php } ?>
        </tbody>
    </table>
<?php }
?>
