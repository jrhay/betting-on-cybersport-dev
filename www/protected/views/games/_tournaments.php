<?php
/** @var Tournamentsdetails $tournaments */
/** @var Tournaments $choosedTournament */


$timeLabel = time();
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'tournaments-form',
    'enableAjaxValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'validateOnChange' => true,
    ),
    'htmlOptions' => array(
        'class' => 'form-inline',
    )
));

?>
<div class="item-list2" style="top: -1400px">
    <div class="g-clearfix">
        <label class="tournamentLabel"><?php echo Yii::t('games', 'Please select tournament from the list: ') ?></label>
        <!--        <img class="tournaments-logo"-->
        <!--             src="-->
        <?php //echo Yii::app()->baseUrl . "/../images/tournaments/logo/$choosedTournament->logo"; ?><!--">-->
        <?php echo CHtml::activeDropDownList($model, 'tournamentID', CHtml::listData($tournaments, 'id', 'name'), array(
            'onchange' => 'changeTournament(this)',
        )) ?>
        <?php echo
        '
        <script type="text/javascript">
            function changeTournament(list) {
            list.tournamenID = 4;
            }
        </script>
        ' ?>
    </div>
    <?php if ($model->tournamentID) { ?>
        <div class="style5 hidden" style="display: block">
            <div class="filter-section">
                <div class="tournaments-tab game-tab-tournaments" id="yw0">
                    <div class="summary"></div>
                    <img class="tournaments-logo"
                         src="<?php echo Yii::app()->baseUrl . "/../images/tournaments/logo/$choosedTournament->logo"; ?>">

                    <div style="margin-top: %;margin-bottom: 0px">
                        <p style="white-space: normal;margin-top: -10%;margin-left:10%; ">
                            Tournament: <?php echo $choosedTournament->name ?>
                            <?php /** @var GameCategories $gameCategory */
                            $gameCategory = GameCategories::model()->findByPk($choosedTournament->gameCategory); ?>&nbsp
                            <img width="20" height="20"
                                 src="<?php echo Yii::app()->baseUrl ?>/images/games/<?php echo $gameCategory->image ?>"
                                 alt=""/>&nbsp<?php //echo $gameCategory->title ?>
                            Game: <?php echo $gameCategory->title ?> </p>

                    </div>
                    <!--                    --><?php //endif ?>
                    <table class="items" style="position:inherit; height: 100%;margin-bottom: 0%;margin-top: -5%">
                        <thead>
                        <tr>
                            <th><?php echo Yii::t('site', 'Name') ?></th>
                            <th><?php echo Yii::t('site', 'Rate') ?></th>
                            <th><?php echo Yii::t('site', 'Ratio') ?></th>
                            <th><?php echo Yii::t('site', 'Bet Sum') ?></th>
                            <th><?php echo Yii::t('site', 'Possible win') ?></th>
                            <!--                            <th></th>-->
                        </tr>
                        </thead>
                        <tbody>

                        <?php
                        /** @var Tournamentsdetails[] $tournamentInfo */
                        foreach ($tournamentInfo as $key => $tournament) {
                            ?>
                            <tr class="odd" id="team<?php echo $tournament->team->id ?>">
                                <td><?php echo $tournament->team->name ?></td>
                                <td id="teamkoef<?php echo $tournament->team->id ?>"><?php echo Helper::formatKoef($tournament->teamKoef) ?></td>
                                <td>              <?php
                                    $sumKoef = 0;
                                    foreach ($tournamentInfo as $tr) {
                                        $sumKoef += $tr->teamKoef;
                                    }
                                    echo Helper::formatMoney(($tournament->teamKoef * 100) / $sumKoef) . '%';
                                    ?>
                                </td>
                                <td><?php
                                    echo (isset($userTournamentBetsInfo[$tournament->team->id])) ? $userTournamentBetsInfo[$tournament->team->id]['summ'] : '';
                                    ?></td>
                                <td><?php
                                    echo (isset($userTournamentBetsInfo[$tournament->team->id])) ? $userTournamentBetsInfo[$tournament->team->id]['canWin'] : '';
                                    ?></td>
                            </tr>
                        <?php
                        }
                        ?>
                        <tr class="">
                            <!--                                                        <td></td>-->
                            <td><?php echo $form->label($model, 'teamID'); ?></td>
                            <td><?php echo CHtml::activeDropDownList($model, 'teamID', CHtml::listData($tournamentInfo, 'team.id', 'team.name'), array(
                                    'style' => 'border-radius: 4px'
                                )); ?></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>

                            <td><label style="vertical-align: bottom;text-align: center"
                                       for="bet"><?php echo Yii::t('site', 'Размер ставки') ?></label>
                            </td>
                            <td><?php echo $form->textField($model, 'summ', array(
                                    'class' => 'bet-input',
                                    'style' => 'width:45%;margin-left: 5%;')); ?> $
                            </td>
                            <td></td>
                            <td><label style="margin-left: 0;vertical-align: middle"
                                       for="bet"><?php echo Yii::t('site', 'Возможный выигрыш') ?></label></td>
                            <td><input style="width:50%;" id="winAmount" class="input"
                                       type="text" disabled="true">$
                            </td>
                            <!--                            <td></td>-->
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td></td>
                        </tr>

                        </tbody>
                    </table>
                    <div class="errors tournament-bet-form-errors" style="margin-top: 0%;margin-bottom: 0%">
                        <?php echo $form->error($model, 'summ'); ?>
                    </div>
                    <?php if (isset($choosedTournament->isFloatingKoef) && $choosedTournament->isFloatingKoef) : ?>
                    <p style="margin-left: -20%;white-space:pre;margin-top: 0%;margin-bottom: 0%"><?php echo Yii::t('site', "
                            Who will win  $choosedTournament->name ?
                            Coefficient is not fixed. You will be payed for the
                            last index when bets on tournament will be closed.</p>") ?>
                        <?php endif ?>
                        <?php

                        if ($choosedTournament->status != Games::GAME_STATUS_FINISHED) {

                            $url = '';
                            $onclick = array();
                            if (Yii::app()->user->isGuest) {
                                $url = Yii::app()->createUrl('/user/register');
                                $onclick = array('onclick' => 'changeLocation()');
                                echo '
                                <script>
                                    function changeLocation() {
                                        window.location = "<?php echo $url?>";
                                    }
                                </script>
                                ';
                            }

                            echo CHtml::submitButton(Yii::t('site', 'Make a bet'), array_merge(array(
                                'class' => 'btn-submit btn-style1 sab',
                            ), $onclick));
                        } else {
                            echo '<br><h2>The winner is : </h2><br>';
                            echo CHtml::textField('Winner', Teams::model()->findByPk($choosedTournament->winnerId)->name);
                        }
                        ?>

                </div>

            </div>
        </div>

    <?php } ?>
</div>

<?php $this->endWidget(); ?>
