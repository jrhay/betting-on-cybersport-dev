<?php
$baseUrl = Yii::app()->theme->baseUrl;
Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/jquery.scrollbar.js');

Yii::app()->clientScript->registerCssFile($baseUrl . '/css/modal-custom.css');
Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/jquery.form.js');
Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/games.js');
?>
<?php $this->widget('application.widgets.gamesMenu.gamesMenu');

?>
    <div class="g-clearfix">
        <div class="leftcol">
            <?php $this->widget('application.widgets.bets.SimpleBetVideo', array()); ?>
        </div>
        <div class="rightcol-s" style="margin-top: -3%">
            <?php $this->widget('application.widgets.banner.banner', array(
                'height' => '350',
                'width' => '310',
            )); ?>
        </div>

    </div>
    <div class="table-wrap style6 games-table-common-wrap">
        <?php $this->widget('application.widgets.bets.SimpleBet', array('gameCategories' => $gameCategories, 'games' => $games)); ?>

        <!--<a class="btn-update btn-style1" href="#" title="">Обновить список</a>-->
    </div>
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'bets-dialog',
    'cssFile' => null,
    'options' => array(
        'cssClass' => 'style6',
//        'title' => Yii::t('site', 'Make a bet'),
        'autoOpen' => false,
        'modal' => true,
        'hide' => 'drop',
        'show' => 'drop',
        'position' => 'center',
        'width' => 800,
        'resizable' => false,
        'close' => 'js:function(){ clearInterval(timerInterval); }',
        'buttons' => array(
            array(
                'text' => Yii::t('general', 'Close'),
                'click' => 'js:function(){ $(this).dialog("close");}',
                'class' => 'cancel'
            ),
        ),
    ),
//    'events'=>array(
//        'beforeClose'=>'js:function(){
//            clearInterval(timerInterval);
//        }',
//    ),
    'htmlOptions' => array('class' => 'pop-up-event'),
));
?>
    <div id="bets-dialog-container"></div>
<?php $this->endWidget('zii.widgets.jui.CJuiDialog');
?>
<?php
Yii::app()->clientScript->registerScript('helpers', '
    baseUrl = ' . CJSON::encode(Yii::app()->baseUrl) . ';
    '); ?>
<?php
Yii::app()->clientScript->registerScript(uniqid(), "

$('body').on('submit', '#simpleBet-form', function () {
    if (confirm('" . Yii::t('bets', 'Do you want to continue?') . "')) {
    $(this).ajaxSubmit({
        success:function (data) {
            $('#bet-dialog').dialog('close');
            window.location.reload();
            return false;
        },
    });
    }

    return false;
});

$('body').on('submit', '#tournaments-form', function () {
    if (confirm('" . Yii::t('bets', 'Do you want to continue?') . "')) {
    $(this).ajaxSubmit({
        success:function (data) {
            $('#tournaments-dialog').dialog('close');
            window.location.reload();
            return false;
        },
    });
    }

    return false;
});

$('body').on('submit', '#expressBet-form', function () {
    if (confirm('" . Yii::t('bets', 'Do you want to continue?') . "')) {
    $(this).ajaxSubmit({
        success:function (data) {
            $('#bet-dialog').dialog('close');
            window.location.reload();
            return false;
        },
    });
    }

    return false;
});

    jQuery(document).on('click','#game-categories-common a',function(e){
        var gameID = jQuery(this).attr('id').split('game-table-');
            $.ajax({
                url: \"games\",
                data: \"ajax&\" + gameID[1],
                success: function(data){
                    var containerDiv = document.getElementsByClassName(\"filter-section\");
                    containerDiv[0].innerHTML = data;

//                    scroll-wrapper scrollbar-janos theme-dark
  }
});
        jQuery('#game-categories-common a').removeClass('active');
        this.removeAttribute('href');
        this.classList.add('active');
        window.history.pushState(\"\", \"\", \"/games?\"+gameID[1]);
//        if (gameID[1]=='all') {
//            jQuery('.game-tab tr').css('display','table-row');
//        } else {
//            jQuery('.game-tab tr').css('display','none');
//            jQuery('.game-tab tr.heading').css('display','table-row');
//            jQuery('.game-tab tr.game-tr-' + gameID[1]).css('display','table-row');
//        }
    });

", CClientScript::POS_END);
if (true) {
    if (isset($_REQUEST['gameId']) && isset($_REQUEST['openPopUp'])) {
        Yii::app()->clientScript->registerScript(uniqid(), "$(document).ready(function(){
       gameID = 'gameId-" . $_REQUEST['gameId'] . "';
       baseUrl = '';
        $.post(
            baseUrl+'/index.php/games/makebet',
            {'gameID': gameID},
            function (data) {

                $('#bets-dialog-container').html(data);
                $('.select-styled').styler({ selectSmartPositioning: false });
                $('#bets-dialog').dialog('open');

            },
            'html'
        );
        return false;
    })");
    }
    Yii::app()->clientScript->registerScript(uniqid(), "

        jQuery(document).on('click','.game-tab tr  ',function(e){
        gameID = jQuery(this).attr('id');
        if(typeof gameID === 'undefined') {
            return
        }
        $.post(
            baseUrl + '/index.php/games/makebet',
            {'gameID': gameID},
            function (data) {

                $('#bets-dialog-container').html(data);
                $('.select-styled').styler({ selectSmartPositioning: false });
                $('#bets-dialog').dialog('open');

            },
            'html'
        );
        return false;
    });

    jQuery(document).on('click','#express_bets_btn',function(e){
        $.post(
            baseUrl+'/index.php/games/expressbet',
            {},
            function (data) {

                $('#express-bets-dialog-container').html(data);
//                $('.select-styled').styler({ selectSmartPositioning: false });
                $('#express-bets-dialog').dialog('open');
            },
            'html'
        );
        return false;
    });

    jQuery(document).on('click','#tournaments_btn',function(e){
        $.post(
            baseUrl + '/index.php/games/tournaments',
            {},
            function (data) {

                $('#tournaments-dialog-container').html(data);
//                document.getElementById('tournaments-dialog-container').parentNode.style.marginTop='20%';

                $('.select-styled').styler({ selectSmartPositioning: false });
                $('#tournaments-dialog').dialog('open');
                document.getElementById('tournaments-dialog-container').parentNode.style.width='100%';
            },
            'html'
        );
        return false;
    });
    jQuery(document).on('click','.game-tab-tournaments tr',function(e){
        if (jQuery(this).attr('id')) {
             splittedID = jQuery(this).attr('id').split('team');
             jQuery('#tournamentBetForm_teamID').val(splittedID[1]);
        }

        return false;
    });

    jQuery(document).on('change','.dis', function(e) {

        if (this.checked) {
          var gameParts = jQuery(this).attr('id').split('_');
          jQuery('.express-bet-game-'+ gameParts[1]).attr('disabled','disabled');
          jQuery(this).removeAttr('disabled');
        } else {
          var gameParts = jQuery(this).attr('id').split('_');
          jQuery('.express-bet-game-'+ gameParts[1]).removeAttr('disabled');
        }
        summKoef = 1;
        countChecked = 0;
        jQuery('.dis').each(function(e,obj) {

            if (this.checked) {
                summKoef = summKoef * parseFloat(jQuery(this).parent('td')[0].textContent);
                countChecked = countChecked + 1;
                if(countChecked >= 10) {
                    jQuery('#warning_m').text('" . Yii::t('site', 'Too many games are selected ( 10 max allowed )') . "');
                    return
                }
            }

             if(summKoef > 200) {
                jQuery('#warning_m').text('" . Yii::t('site', 'Your koef is too big, make another choice') . "');
                return
            } else {
                jQuery('#warning_m').text('');
            }

             jQuery('#summ_koef_js').text(summKoef.toFixed(3));
             if (summKoef.toFixed(0)==1) {
                jQuery('#expressBet-form input[type=\"submit\"]').attr('disabled','disabled');
             } else {
                jQuery('#expressBet-form input[type=\"submit\"]').removeAttr('disabled');
             }
        });
        calcPossibleProfit();
    });

     jQuery(document).on('input','#expressBetForm_summ', function(e) {
        calcPossibleProfit();

    });

    function calcPossibleProfit() {

        summ = jQuery('#expressBetForm_summ').val();
        summKoef = 1;

        jQuery('.dis').each(function(e,obj) {
             if (this.checked) {
                currentKoef = jQuery(this).parent('td')[0].textContent;
                summKoef = summKoef * parseFloat(currentKoef);
            }
        });



        winMoney = parseFloat(summ) * summKoef.toFixed(3);

        jQuery('#sum1_div').text(winMoney.toFixed(2));
    }
    jQuery(document).on('change','#tournamentBetForm_tournamentID', function(e) {
        $.post(
            baseUrl + '/index.php/games/tournaments?id=' + jQuery(this).val(),
            {},
            function (data) {

                $('#tournaments-dialog-container').html(data);
//                $('#tournaments-dialog').dialog('open');
            },
            'html'
        );
        return false;
    });
        jQuery('#tournaments-dialog-container').on('input','#tournamentBetForm_summ', function() {
            changeCanWinTournaments();
        });
        jQuery('#tournaments-dialog-container').on('change','#tournamentBetForm_teamID', function() {
            changeCanWinTournaments();
        });

        function changeCanWinTournaments() {
            if (jQuery('#tournamentBetForm_summ').val()=='') {
                jQuery('#tournamentBetForm_summ').val('');
                jQuery('#winAmount').val(0);
                return;
            }

            teamID = jQuery('#tournamentBetForm_teamID').val();
            console.log(jQuery('#teamkoef'+teamID).text());
            canWin = parseFloat(jQuery('#teamkoef'+teamID).text())*parseInt(jQuery('#tournamentBetForm_summ').val());
            jQuery('#winAmount').val(canWin.toFixed(2));
        }

        jQuery(document).on('keypress','.numericOnly', function(e) {
            if (!String.fromCharCode(e.keyCode).match(/[^0-9]/g)) {
                return true;
            } else {
                return false;
            }
        });
    ", CClientScript::POS_END);
}

$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'express-bets-dialog',
    'cssFile' => null,
    'options' => array(
        'cssClass' => 'style6',
//        'title' => Yii::t('site', 'Make a bet'),
        'autoOpen' => false,
        'modal' => true,
        'hide' => 'drop',
        'show' => 'drop',
        'position' => 'center',
        'width' => 800,
        'resizable' => false,
        'close' => 'js:function(){ }',
        'buttons' => array(
            array(
                'text' => Yii::t('general', 'Close'),
                'click' => 'js:function(){ $(this).dialog("close");}',
                'class' => 'cancel'
            ),
        ),
    ),
//    'events'=>array(
//        'beforeClose'=>'js:function(){
//            clearInterval(timerInterval);
//        }',
//    ),
    'htmlOptions' => array('class' => 'pop-up-event', 'style' => 'width: 100%'),
));
?>
    <div id="express-bets-dialog-container"></div>
<?php $this->endWidget('zii.widgets.jui.CJuiDialog');

$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'tournaments-dialog',
    'cssFile' => null,
    'options' => array(
        'cssClass' => 'style6',
        'autoOpen' => false,
        'modal' => true,
        'hide' => 'drop',
        'show' => 'drop',
        'position' => 'center',
        'width' => 800,
        'resizable' => true,
        'close' => 'js:function(){ }',
        'buttons' => array(
            array(
                'text' => Yii::t('general', 'Close'),
                'click' => 'js:function(){ $(this).dialog("close");}',
                'class' => 'cancel'
            ),
        ),
    ),
//    'events'=>array(
//        'beforeClose'=>'js:function(){
//            clearInterval(timerInterval);
//        }',
//    ),
    'htmlOptions' => array('class' => 'pop-up-event'),
));
?>
    <div id="tournaments-dialog-container" style="width:95%;margin-left:3%;margin-right: 3%"></div>
<?php $this->endWidget('zii.widgets.jui.CJuiDialog');