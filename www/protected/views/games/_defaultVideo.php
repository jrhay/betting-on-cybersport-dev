<?php /** @var DefaultGamesVideoFrame $defaultGamesVideoFrame */?>
<div class="strimcontent" id="strimcontent">
    <object type="application/x-shockwave-flash" height="265" width="500" id="live_embed_player_flash"
            data="http://www.twitch.tv/widgets/live_embed_player.swf?channel=<?php echo $defaultGamesVideoFrame->frame?>" bgcolor="#000000">
        <param name="allowFullScreen" value="true"/>
        <param name="allowScriptAccess" value="always"/>
        <param name="allowNetworking" value="all"/>
        <param name="movie" value="http://www.twitch.tv/widgets/live_embed_player.swf"/>
        <param name="flashvars"
               value="hostname=www.twitch.tv&channel=<?php echo $defaultGamesVideoFrame->frame?>&auto_play=false&start_volume=25"/>
    </object>
</div>

