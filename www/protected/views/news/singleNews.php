<div class="page-content">
    <div class="wrap">
        <div class="g-clearfix">
            <div class="leftcol">

                <div id="" class="news-page col-inner style6 tabs " style="margin: 30px 0px">

                    <ul class="news-list">
                            <li>

                                <h4 class="title">
                                    <a href="#" title=""><?php echo  $news->title ?></a>

                                    <div style="color: blue">
                                        <?php  if (!Yii::app()->user->isGuest && Usertype::needAccess(Usertype::USERTYPE_MODERATOR)): ?>
                                            <a style="color: blue; font-size: 1em"
                                               href="<?php echo  Yii::app()->baseUrl ?>/news/index?deleteNewsId=<?php echo  $news->id ?>"><?php echo  Yii::t('news', 'Удалить') ?></a>
                                        <?php  endif ?>
                                    </div>
                                </h4>


                                <div class="date">(<?php echo  date('m.d.Y', $news->date) ?>)</div>
                                <br>
                                <div>
                                <img class="img-size big-img"
                                     src="<?php echo Yii::app()->baseUrl ?>/../images/news/<?php echo  Yii::app()->language==='ru'? $news->img : $news->img_eng ?>"
                                    />
                                </div>
                                <br>
                                <div style="text-align: left" class="text"><?php echo  Yii::app()->language==='ru'? $news->text : $news->text_eng ?>
                                </div>
                            </li>
                    </ul>

                    <a class="btn btn-style1" href="<?php echo  Yii::app()->createUrl('games') ?>" title=""><?php echo  Yii::t('news','Сделать ставку
                        прямо сейчас')?></a>
                </div>
            </div>
            <div class="rightcol-s">
                <?php $this->widget('application.widgets.banner.banner', array(
                    'height' => '350',
                    'width' => '310',
                ));?>
                <?php $this->widget('application.widgets.banner.banner', array(
                    'height' => '350',
                    'width' => '310',
                ));?>
                <?php $this->widget('application.widgets.banner.banner', array(
                    'height' => '350',
                    'width' => '310',
                ));?>
            </div>
        </div>
    </div>
</div>
<div class="gag"></div>
