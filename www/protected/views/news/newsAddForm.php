<?php
/* @var $this NewsController */
/* @var $model News */
/* @var $form CActiveForm */
?>
<h2><?php echo  Yii::t('site', 'Добавьте новость') ?></h2>
<br>
<div class="form" style="">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'news-newsForm2-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // See class documentation of CActiveForm for details on this,
        // you need to use the performAjaxValidation()-method described there.
        'enableAjaxValidation' => false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    )); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <br>
<!--    --><?php //echo $form->errorSummary($model); ?>

    <!--	<div class="row">-->
    <!--		--><?php //echo $form->labelEx($model,'category_id'); ?>
    <!--		--><?php //echo $form->textField($model,'category_id'); ?>
    <!--		--><?php //echo $form->error($model,'category_id'); ?>
    <!--	</div>-->

    <?php
    /** @var NewsCategories[] $categories */
    $categories = array();
    foreach( $newsCategories as $ct) {
        $categories[$ct->id] = $ct->name;
    }
    ?>
    <div class="row ">
        <?php echo $form->labelEx($model, 'category_id'); ?>
        <?php echo $form->dropDownList($model, 'category_id', $categories, array(
            'class' => 'btn-style1'
        )); ?>
        <?php echo $form->error($model, 'category_id'); ?>
    </div>
    <br>
    <div class="row">
        <?php echo $form->labelEx($model, 'title'); ?>
        <?php echo $form->textField($model, 'title', array(
            'required' => 'required,'
        )); ?>
<!--        --><?php //echo $form->error($model, 'title'); ?>
    </div>
    <br>
    <div class="row">
        <?php echo $form->labelEx($model, 'title_eng'); ?>
        <?php echo $form->textField($model, 'title_eng', array(
            'required' => 'required,'
        )); ?>
        <!--        --><?php //echo $form->error($model, 'title'); ?>
    </div>
    <br>
    <div class="row">
        <?php echo $form->labelEx($model, 'text'); ?>
        <?php echo $form->textArea($model, 'text', array(
            'placeholder' => '',
            'required' => 'required,'
        )); ?>
<!--        --><?php //echo $form->error($model, 'text'); ?>
    </div>
    <br>
    <div class="row">
        <?php echo $form->labelEx($model, 'text_eng'); ?>
        <?php echo $form->textArea($model, 'text_eng', array(
            'placeholder' => '',
            'required' => 'required,'
        )); ?>
        <!--        --><?php //echo $form->error($model, 'text'); ?>
    </div>
    <br>
    <!--	<div class="row">-->
    <!--		--><?php //echo $form->labelEx($model,'date'); ?>
    <!--		--><?php //echo $form->textField($model,'date'); ?>
    <!--		--><?php //echo $form->error($model,'date'); ?>
    <!--	</div>-->

    <?php echo $form->labelEx($model, 'img'); ?>
    <?php echo $form->fileField($model, 'img', array(
        'required' => 'required',
        'class' =>'btn-style1'
    )); ?>
<!--    --><?php //echo $form->error($model, 'img'); ?>
    <br>
    <?php echo $form->labelEx($model, 'img_eng'); ?>
    <?php echo $form->fileField($model, 'img_eng', array(
        'required' => 'required',
        'class' =>'btn-style1'
    )); ?>
    <!--    --><?php //echo $form->error($model, 'img'); ?>
    <br>
    <br>
    <br>
    <div class="row buttons ">
        <?php echo CHtml::submitButton('Submit', array(
            'class' => 'btn-style1',
            'style' => 'width: 30%',
        )); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->