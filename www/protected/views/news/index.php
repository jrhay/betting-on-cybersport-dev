<div class="page-content">
    <div class="wrap">
        <div class="g-clearfix">
            <div class="leftcol">
                <?php  if (Usertype::needAccess(Usertype::USERTYPE_MODERATOR)): ?>
                    <div class=" news-page style6 "
                         style=";margin: 30px 0px;min-height: 350px;border-radius: 3px;border-top:0px">
                        <div style="padding: 5%">
                            <?php  include('newsAddForm.php'); ?>
                        </div>
                    </div>
                <?php  endif ?>
                <div id="" class="news-page col-inner style6 tabs " style="margin: 30px 0px">

                    <ul class="news-menu style4 g-clearfix">
                        <?php  foreach ($newsCategories as $ct): ?>
                            <?php  if ($ct->id == $currentCategory) {
                                $active = 'active';
                            } else {
                                $active = '';
                            } ?>
                            <li class=" <?php echo  $active ?>"><a
                                    href="<?php echo  Yii::app()->baseUrl ?>/news/index?currentCategory=<?php echo  $ct->id ?>"
                                    title=""><?php echo  Yii::t( 'news', $ct->name  )?></a></li>
                        <?php  endforeach ?>
                    </ul>

                    <ul class="news-list">
                        <?php  foreach ($news as $n): ?>
                            <li>
                                <div class="">
                                <img class=" pre-img img-size"
                                     src="<?php echo Yii::app()->baseUrl ?>/../images/news/<?php echo  Yii::app()->language==='ru'? $n->img : $n->img_eng ?>"/>
                                </div>
                                <br>
                                <h4 class="title" style="margin-bottom: 15px">
                                    <a href="<?php echo
                                    Yii::app()->createUrl('news/viewsinglenews'.'?news_id='.$n->id)?>" title=""><?php echo  Yii::app()->language==='ru'? $n->title : $n->title_eng ?></a>

                                    <div style="color: blue">
                                        <?php  if (!Yii::app()->user->isGuest && Usertype::needAccess(Usertype::USERTYPE_MODERATOR)): ?>
                                            <a style="color: blue; font-size: 1em"
                                               href="<?php echo  Yii::app()->baseUrl ?>/news/index?deleteNewsId=<?php echo  $n->id ?>"><?php echo  Yii::t('news', 'Удалить') ?></a>
                                        <?php  endif ?>
                                    </div>
                                </h4>

                                <div class="date">(<?php echo  date('m.d.Y', $n->date) ?>)</div>
                                <div style="text-align: left" class="text"><?php echo  Yii::app()->language==='ru'? $n->text : $n->text_eng ?>
                                </div>
                            </li>
                        <?php  endforeach ?>
                    </ul>

                    <br>
                    <br>
                    <?php  $this->widget('CLinkPager', array(
                        'pages' => $pages,
                        'cssFile' => Yii::app()->request->baseUrl . '/css/pagination.css'
                    )) ?>

                    <a class="btn btn-style1" href="<?php echo  Yii::app()->createUrl('games') ?>" title=""><?php echo  Yii::t('news','Сделать ставку
                        прямо сейчас')?></a>
                </div>
            </div>
            <div class="rightcol-s">
                <?php $this->widget('application.widgets.banner.banner', array(
                    'height' => '350',
                    'width' => '310',
                ));?>
                <?php $this->widget('application.widgets.banner.banner', array(
                    'height' => '350',
                    'width' => '310',
                ));?>
                <?php $this->widget('application.widgets.banner.banner', array(
                    'height' => '350',
                    'width' => '310',
                ));?>
            </div>
        </div>
    </div>
</div>
<div class="gag"></div>
