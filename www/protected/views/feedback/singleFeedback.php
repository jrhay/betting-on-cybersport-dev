<div class="page-content">
    <div class="wrap">
        <div class="g-clearfix">
            <div class="leftcol">

                <div id="" class="news-page col-inner style6 tabs " style="margin: 30px 0px">

                    <ul class="news-list">
                        <li>

                            <h4 class="title">
                                <a href="#" title=""><?php echo   Yii::app()->language==='ru' ? $feedback->title : $feedback->title_eng?></a>

                                <div style="color: blue">
                                    <?php  if (!Yii::app()->user->isGuest && in_array(Yii::app()->user->usertypeId, array(1, 3))): ?>
                                        <a style="color: blue; font-size: 1em"
                                           href="<?php echo  Yii::app()->baseUrl ?>/site/feedback?deleteFeedbackId=<?php echo  $feedback->id ?>"><?php echo  Yii::t('site', 'Удалить') ?></a>
                                    <?php  endif ?>
                                </div>
                            </h4>


                            <div class="date">(<?php echo  date('m.d.Y', $feedback->date) ?>)</div>
                            <br>
                            <div>
                                <img class="img-size big-img"
                                     src="<?php echo Yii::app()->baseUrl ?>/../images/feedback/<?php echo   Yii::app()->language==='ru' ? $feedback->img : $feedback->img_eng ?>"
                                    />
                            </div>
                            <br>
                            <div style="text-align: left" class="text"><?php echo   Yii::app()->language==='ru' ? $feedback->text : $feedback->text_eng ?>
                            </div>
                        </li>
                    </ul>

                    <a class="btn btn-style1" href="<?php echo  Yii::app()->createUrl('games') ?>" title=""><?php echo  Yii::t('feedback', 'Сделать ставку
                        прямо сейчас')?></a>
                </div>
            </div>
            <div class="rightcol-s">
                <?php $this->widget('application.widgets.banner.banner', array(
                    'height' => '350',
                    'width' => '310',
                ));?>
                <?php $this->widget('application.widgets.banner.banner', array(
                    'height' => '350',
                    'width' => '310',
                ));?>
                <?php $this->widget('application.widgets.banner.banner', array(
                    'height' => '350',
                    'width' => '310',
                ));?>
            </div>
        </div>
    </div>
</div>
<div class="gag"></div>
