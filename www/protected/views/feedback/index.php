<div class=" page-content" style="">
    <div class="wrap">
        <div class="g-clearfix">
            <div class="leftcol">
                <?php  if (Usertype::needAccess(Usertype::USERTYPE_MODERATOR)): ?>
                    <div class="news-page style6 "
                         style="margin: 30px 0px;min-height: 350px;border-radius: 3px;border-top:0px">
                        <div style="padding: 5%">
                            <?php  include('feedbackAddForm.php'); ?>
                        </div>
                    </div>
                <?php  endif ?>
                <div id="" class="news-page col-inner style6 tabs " style="margin: 30px 0px">

                    <ul class="news-menu style4 g-clearfix">
                        <li class=" <?php echo  Yii::app()->controller->action->id === 'feedback'? 'active' : '' ?>"><a
                                href="<?php echo  Yii::app()->baseUrl ?>/feedback"
                                title=""><?php echo  Yii::t('site','Отзывы') ?></a></li>
                        <li class=" <?php echo  Yii::app()->controller->action->id === 'paidBets'? 'active' : '' ?>"><a
                                href="<?php echo  Yii::app()->baseUrl ?>/paidBets"
                                title=""><?php echo  Yii::t('site','Выплаты') ?></a></li>
                    </ul>

                    <ul class="news-list">
                        <?php  foreach ($feedback as $n): ?>
                            <li>
                                <div class="">
                                    <img class=" pre-img img-size"
                                         src="<?php echo Yii::app()->baseUrl ?>/../images/feedback/<?php echo  Yii::app()->language==='ru' ? $n->img : $n->img_eng?>"/>
                                </div>
                                <br>
                                <h4 class="title" style="margin-bottom: 15px">
                                    <a href="<?php echo
                                    Yii::app()->createUrl('feedback/viewsinglefeedback' . '?feedback_id=' . $n->id)?>"
                                       title=""><?php echo Yii::app()->language==='ru' ? $n->title : $n->title_eng?></a>

                                    <div style="color: blue">
                                        <?php  if (!Yii::app()->user->isGuest && in_array(Yii::app()->user->usertypeId, array(1, 3))): ?>
                                            <a style="color: blue; font-size: 1em"
                                               href="<?php echo  Yii::app()->baseUrl ?>/feedback/index?deleteFeedbackId=<?php echo  $n->id ?>"><?php echo  Yii::t('site', 'Удалить') ?></a>
                                        <?php  endif ?>
                                    </div>
                                </h4>

                                <div class="date">(<?php echo  date('m.d.Y', $n->date) ?>)</div>
                                <div style="text-align: left" class="text"><?php echo  Yii::app()->language==='ru' ? $n->text : $n->text_eng ?>
                                </div>
                            </li>
                        <?php  endforeach ?>
                    </ul>
<!---->
                    <br>
                    <br>
                    <?php  $this->widget('CLinkPager', array(
                        'pages' => $pages,
                        'cssFile' => Yii::app()->request->baseUrl . '/css/pagination.css'
                    )) ?>

                    <a class="btn btn-style1" href="<?php echo  Yii::app()->createUrl('games') ?>" title=""><?php echo  Yii::t('site','Сделать ставку прямо сейчас')?></a>
                </div>
            </div>
            <div class="rightcol-s">
                <?php $this->widget('application.widgets.banner.banner', array(
                    'height' => '350',
                    'width' => '310',
                ));?>
                <?php $this->widget('application.widgets.banner.banner', array(
                    'height' => '350',
                    'width' => '310',
                ));?>
                <?php $this->widget('application.widgets.banner.banner', array(
                    'height' => '350',
                    'width' => '310',
                ));?>
            </div>
        </div>
    </div>
</div>
<div class="gag"></div>
