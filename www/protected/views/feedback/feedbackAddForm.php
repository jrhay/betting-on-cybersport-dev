<?php
/* @var $this FeedbackController */
/* @var $model Feedback */
/* @var $form CActiveForm */
?>

<div class="form">
    <h2><?php echo  Yii::t('site', 'Добавьте отзыв') ?></h2>
    <br>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'feedback-feedbackAddForm-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// See class documentation of CActiveForm for details on this,
	// you need to use the performAjaxValidation()-method described there.
	'enableAjaxValidation'=>false,
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>
    <br>
	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title'); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>
    <br>
    <div class="row">
        <?php echo $form->labelEx($model,'title_eng'); ?>
        <?php echo $form->textField($model,'title_eng'); ?>
        <?php echo $form->error($model,'title_eng'); ?>
    </div>
    <br>
	<div class="row">
		<?php echo $form->labelEx($model,'text'); ?>
		<?php echo $form->textArea($model,'text'); ?>
		<?php echo $form->error($model,'text'); ?>
	</div>
    <br>
    <div class="row">
        <?php echo $form->labelEx($model,'text_eng'); ?>
        <?php echo $form->textArea($model,'text_eng'); ?>
        <?php echo $form->error($model,'text_eng'); ?>
    </div>
    <br>
	<div class="row">
		<?php echo $form->labelEx($model,'img'); ?>
        <?php echo $form->fileField($model, 'img', array(
            'required' => 'required',
            'class' =>'btn-style1'
        )); ?>
		<?php echo $form->error($model,'img'); ?>
	</div>
    <br>
    <div class="row">
        <?php echo $form->labelEx($model,'img_eng'); ?>
        <?php echo $form->fileField($model, 'img_eng', array(
            'required' => 'required',
            'class' =>'btn-style1'
        )); ?>
        <?php echo $form->error($model,'img_eng'); ?>
    </div>
    <br>

    <div class="row buttons ">
        <?php echo CHtml::submitButton('Submit', array(
            'class' => 'btn-style1',
            'style' => 'width: 30%',
        )); ?>
    </div>
    <br>
<?php $this->endWidget(); ?>

</div><!-- form -->