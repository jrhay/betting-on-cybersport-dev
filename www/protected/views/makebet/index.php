<div class="g-clearfix">
    <div class="leftcol-m">
        <div class="b-slider-bet1">
            <div class="slider style3">
                <div id="slider-bet1">
                    <ul>
                        <li class="slide">
                            <h3 class="title"><?php echo  Yii::t('makebet', 'How to make bet?') ?></h3>

                            <div class="ico">1</div>
                            <div class="subtitle"><?php echo  Yii::t('makebet', 'Sign up') ?></div>
                            <div class="text">
                                <?php echo  Yii::t('makebet', 'To carry out rates - the first step is to register. In the upper right corner there is a
                                corresponding "Register" button.') ?>
                            </div>
                            <div class="progress"></div>
                        </li>
                        <li class="slide">
                            <h3 class="title"><?php echo  Yii::t('makebet', 'How to make bet?') ?></h3>

                            <div class="ico">2</div>
                            <div class="subtitle"><?php echo  Yii::t('makebet', 'Enrich account') ?></div>
                            <div class="text">
                                <?php echo  Yii::t('makebet', 'After checking in - go to "Balance" inyour personal account and recharge your account
                                with a more system, moresuitable for you.') ?>
                            </div>
                            <div class="progress"></div>
                        </li>
                        <li class="slide">
                            <h3 class="title"><?php echo  Yii::t('makebet', 'How to make bet?') ?></h3>

                            <div class="ico">3</div>
                            <div class="subtitle"><?php echo  Yii::t('makebet', 'To the table') ?></div>
                            <div class="text">
                                <?php echo  Yii::t('makebet', 'Then go to the table of games.
                                Click on the game in the table and enter the amount to the kind of money
                                (real / virtual) team, you bet and click on "Make a bet" in the appeared window') ?>
                            </div>
                            <div class="progress"></div>
                        </li>
                        <li class="slide">
                            <h3 class="title"><?php echo  Yii::t('makebet', 'How to make bet?') ?></h3>

                            <div class="ico">4</div>
                            <div class="subtitle"><?php echo  Yii::t('makebet', 'Confirmation') ?></div>
                            <div class="text">
                                <?php echo  Yii::t('makebet', 'Once a bid is accepted,a notification "Your bid is done" will pop up at the top of the
                                monitor. Alternatively, you can follow to    make a bet - in your account, in the "Rate".') ?>
                            </div>
                            <div class="progress"></div>
                        </li>
                        <li class="slide">
                            <h3 class="title"><?php echo  Yii::t('makebet', 'How to make bet?') ?></h3>

                            <div class="ico">5</div>
                            <div class="subtitle"><?php echo  Yii::t('makebet', 'Congratulations!') ?></div>
                            <div class="text">
                                <?php echo  Yii::t('makebet', 'Wait until the end of the game and receive your winnings. Good luck!') ?>
                            </div>
                            <div class="progress"></div>
                        </li>
                    </ul>
                </div>
            </div>
            <a class="link btn-style1" href="<?php echo  Yii::app()->createUrl('/games') ?>" title="">
                <div style="font-weight: bold">
                    <?php echo  Yii::t('makebet', 'Make bet now?') ?>
                </div>
            </a>
        </div>
    </div>
    <div class="rightcol-m">
        <div class="b-slider-bet2">
            <div class="slider style3">
                <div id="slider-bet2">
                    <ul>
                        <li class="slide">
                            <h3 class="title"><?php echo  Yii::t('makebet', 'How to make payment?') ?></h3>

                            <div class="ico">1</div>
                            <div class="subtitle"><?php echo  Yii::t('makebet', 'Personal account') ?></div>
                            <div class="text">
                                <?php echo  Yii::t('makebet', 'Go to the section "Balance" in your personal account') ?>
                            </div>
                            <div class="progress"></div>
                        </li>
                        <li class="slide">
                            <h3 class="title"><?php echo  Yii::t('makebet', 'How to make payment?') ?></h3>

                            <div class="ico">2</div>
                            <div class="subtitle"><?php echo  Yii::t('makebet', 'Payment method?') ?></div>
                            <div class="text">
                                <?php echo  Yii::t('makebet', 'Select the most appropriate payment method?') ?>
                            </div>
                            <div></div>
                            <div class="progress"></div>
                        </li>
                        <li class="slide">
                            <h3 class="title"><?php echo  Yii::t('makebet', 'How to make payment?') ?></h3>

                            <div class="ico">3</div>
                            <div class="subtitle"><?php echo  Yii::t('makebet', 'Payment System') ?></div>
                            <div class="text">
                                <?php echo  Yii::t('makebet', 'Follow the instructions for this payment system') ?>
                            </div>
                            <div class="progress"></div>
                        </li>
                        <li class="slide">
                            <h3 class="title"><?php echo  Yii::t('makebet', 'How to make payment?') ?></h3>

                            <div class="ico">4</div>
                            <div class="subtitle"><?php echo  Yii::t('makebet', 'Statistics') ?></div>
                            <div class="text">
                                <?php echo  Yii::t('makebet', 'Upon payment, the money will appear on your panel at theright of thetop.') ?>
                            </div>
                            <div class="progress"></div>
                        </li>
                        <li class="slide">
                            <h3 class="title"><?php echo  Yii::t('makebet', 'How to make payment?') ?></h3>

                            <div class="ico">5</div>
                            <div class="subtitle"><?php echo  Yii::t('makebet', 'Game') ?></div>
                            <div class="text">
                                <?php echo  Yii::t('makebet', 'Choose a game and bet - the withdrawal of money available in the balance section, personal cabinet. Good luck!') ?>
                            </div>
                            <div class="progress"></div>
                        </li>
                    </ul>
                </div>
            </div>
            <a class="link btn-style2"
               href="<?php echo  Yii::app()->user->isGuest ? Yii::app()->createUrl('user/register') : Yii::app()->createUrl('/account/index/cash') ?>"
               title="">
                <div style="font-weight: bold">
                    <?php echo  Yii::t('makebet', 'Make to make payment right now?') ?>
                </div>
            </a>
        </div>
    </div>
</div>