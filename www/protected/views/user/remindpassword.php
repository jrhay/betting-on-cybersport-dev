<h1><?php echo Yii::t('general','Password Reminder')?></h1>
<br />
    <p class="note"><?php echo Yii::t('general', 'If you forgot your password please fill the fields bellow:') ?></p>
<div class="form">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id'                   => 'user-password-reminder',
        'enableAjaxValidation' => true,
        'clientOptions'        => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
        ),
        'htmlOptions'          => array('class' => 'reg-form g-clearfix','enctype'=>'multipart/form-data',)

    )); ?>
    <br />
    <div class="row">
        <div class="label">
            <?php echo $form->labelEx($model, 'email'); ?>
        </div>

            <?php echo $form->textField($model, 'email') ?>
            <?php echo $form->error($model, 'email') ?>
    </div>

    <div class="row">


            <div class="captch-widget" style="margin: 0 0 10px 169px;">
            <?php $this->widget('CCaptcha')?>
            <?php if (!Yii::app()->request->isPostRequest)
                Yii::app()->clientScript->registerScript(
                    'initCaptcha',
                    '$(".form-captcha-div a").trigger("click");',
                    CClientScript::POS_READY
                ); ?>
            </div>
        <div class="label">
            <?php echo CHtml::activeLabelEx($model, 'verifyCode')?>
        </div>

            <?php echo CHtml::activeTextField($model, 'verifyCode')?>
            <?php echo $form->error($model, 'verifyCode')?>

    </div>

    <div class="row">
            <?php echo CHtml::submitButton(Yii::t('general', 'Send'), array('id' => 'signin_submit','class'=>'btn-reg btn-style1')); ?>
    </div>

<?php $this->endWidget(); ?>
</div><!-- form -->