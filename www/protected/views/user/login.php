<?php
$this->pageTitle = Yii::app()->name . ' - ' . Yii::t('general', 'Login');
?>

<h1><?php echo Yii::t('general', 'Login') ?></h1>

<br/>
<?php
foreach (Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>
<p><?php echo Yii::t('general', 'Please fill out the following form with your login credentials') ?>:</p>
<div class="form">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'login-form',
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
        'htmlOptions' => array('class' => 'inline-form'),
    ));
    ?>

    <p class="note"><?php echo Yii::t('general', 'Fields with <span class="required">*</span> are required.') ?></p>
    <br/>

    <div class="row">

        <div class="field-label">
            <?php echo $form->labelEx($model, 'username'); ?>
        </div>
        <div class="field-input">
            <?php echo $form->textField($model, 'username', array('class' => 'w3')); ?>
            <?php echo $form->error($model, 'username'); ?>
        </div>
    </div>

    <div class="row">
        <div class="field-label">
            <?php echo $form->labelEx($model, 'password'); ?>
        </div>
        <div class="field-input">
            <?php echo $form->passwordField($model, 'password', array('class' => 'w3')); ?>
            <?php echo $form->error($model, 'password'); ?>
        </div>

    </div>

    <div class="row rememberMe">
        <div class="field-label">
            <?php echo $form->labelEx($model, 'rememberMe'); ?>
        </div>
        <div class="field-input">
            <?php echo $form->checkBox($model, 'rememberMe'); ?>
            <?php echo $form->error($model, 'rememberMe'); ?>
        </div>
    </div>

    <div class="row buttons">
        <div class="field-label">&nbsp;</div>
        <div class="field-input">
            <input type="submit" id="signin_submit" name="signin_submit" value="<?php echo Yii::t('general', 'Login'); ?>" />
        </div>
    </div>

    <?php echo CHtml::hiddenField('browser_tz_offset',''); ?>

    <?php $this->endWidget(); ?>
</div><!-- form -->
