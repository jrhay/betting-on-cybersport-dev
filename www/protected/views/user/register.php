<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */

$baseUrl = Yii::app()->theme->baseUrl;
Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/jquery.form.js');
?>
<h1 class="page-title"><?php echo Yii::t('site', 'Register account on CyberBet') ?></h1>
<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'register-form',
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
        ),
        'htmlOptions' => array('class' => 'reg-form g-clearfix'),
    )); ?>

    <p class="note"><?php echo  Yii::t('register','Fields with')?> <span class="required">*</span><?php echo  Yii::t('register', 'are required.')?></p>

    <?php echo $form->errorSummary($model); ?>
    <div class="row">
        <div class="label"><?php echo $form->labelEx($model, 'userName'); ?></div>
        <?php echo $form->textField($model, 'userName', array('size' => 45, 'maxlength' => 45)); ?>
        <?php echo $form->error($model, 'userName'); ?>
    </div>

    <div class="row">
        <div class="label"><?php echo $form->labelEx($model, 'firstName'); ?></div>
        <?php echo $form->textField($model, 'firstName', array('size' => 45, 'maxlength' => 45)); ?>
        <?php echo $form->error($model, 'firstName'); ?>
    </div>

    <div class="row">
        <div class="label"><?php echo $form->labelEx($model, 'lastName'); ?></div>
        <?php echo $form->textField($model, 'lastName', array('size' => 45, 'maxlength' => 45)); ?>
        <?php echo $form->error($model, 'lastName'); ?>
    </div>

    <div class="row">
        <div class="label"><?php echo $form->labelEx($model, 'country'); ?></div>
        <?php echo $form->dropDownList($model, 'country', $countries); ?>
        <?php echo $form->error($model, 'country'); ?>
    </div>

    <div class="row">
        <div class="label"><?php echo $form->labelEx($model, 'password'); ?></div>
        <?php echo $form->passwordField($model, 'password', array('size' => 45, 'maxlength' => 45, 'class' => 'w2', 'autocomplete' => 'off')); ?>
        <?php echo $form->error($model, 'password'); ?>
    </div>

    <div class="row">
        <div class="label"><?php echo $form->labelEx($model, 'repeat_password'); ?></div>
        <?php echo $form->passwordField($model, 'repeat_password', array('size' => 45, 'maxlength' => 45, 'class' => 'w2', 'autocomplete' => 'off')); ?>
        <?php echo $form->error($model, 'repeat_password'); ?>
    </div>


    <div class="row">
        <div class="label"><?php echo $form->labelEx($model, 'language'); ?></div>
        <?php echo $form->dropDownList($model, 'language', CHtml::listData(Languages::model()->findAll(), 'id', 'name'), array('class' => '')); ?>
        <?php echo $form->error($model, 'language'); ?>
    </div>

    <div class="row">
        <div class="label"><?php echo $form->labelEx($model, 'email'); ?></div>
        <?php echo $form->textField($model, 'email', array('size' => 60, 'maxlength' => 100)); ?>
        <?php echo $form->error($model, 'email'); ?>
    </div>

    <div class="rules-agree" style="font-size: 16px;color: #FAFAFA">
        <input required class="select-styled" id="rules-agree" type="checkbox"/>
        <label for="rules-agree"><?php echo  Yii::t('register','I read')?> </label><a style="color:darkgrey; text-decoration: none" class="link"
                                           href="<?php echo  Yii::app()->createUrl('site/terms') ?>"><?php echo  Yii::t('register','Terms')?></a>,<a
                style="color:darkgrey; text-decoration: none" class="link"
                href="<?php echo  Yii::app()->createUrl('site/pp') ?>"><?php echo  Yii::t('register','Security
                & Privacy')?></a><?php echo  Yii::t('register','in links below')?>
    </div>
<!--    <div style="text-align: center;">-->
<!--        <a style="color:darkgrey; text-decoration: none" class="link" href="--><?php //= Yii::app()->createUrl('site/pp') ?><!--">Security-->
<!--            & Privacy</a>-->
<!--        &nbsp&nbsp&nbsp-->
<!--        <a style="color:darkgrey; text-decoration: none" class="link" href="--><?php //= Yii::app()->createUrl('site/terms') ?><!--">Terms</a>-->
<!--    </div>-->

    <div class="row buttons">

        <?php echo CHtml::submitButton(Yii::t('site', 'Create'), array('class' => 'btn-reg btn-style1')); ?>
        <!--        --><?php //echo CHtml::link(Yii::t('admin', 'Cancel'), array('/user/index'), array('class' => 'btn btn-common',)); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->