<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */

$baseUrl = Yii::app()->theme->baseUrl;
Yii::app()->clientScript->registerScriptFile($baseUrl.'/js/jquery.form.js');
?>
<h1 class="page-title"><?php echo Yii::t('site','Restore Cyber Money') ?></h1>
<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'cybermoney-form',
        'enableAjaxValidation'=>true,
        'clientOptions'        => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
        ),
        'htmlOptions'=>array('class'=>'reg-form g-clearfix'),
    )); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>
    <div class="row">
        <div class="label"><?php echo $form->labelEx($model,'summ'); ?></div>
        <?php echo $form->textField($model,'summ',array('size'=>45,'maxlength'=>45, 'value' => 100, 'readonly' => 'true')); ?>
        <?php echo $form->error($model,'summ'); ?>
    </div>

    <div class="row buttons">

        <?php echo CHtml::submitButton(Yii::t('site','Restore'), array('class'=>'btn-reg btn-style1')); ?>
        <!--        --><?php //echo CHtml::link(Yii::t('admin', 'Cancel'), array('/user/index'), array('class' => 'btn btn-common',)); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->