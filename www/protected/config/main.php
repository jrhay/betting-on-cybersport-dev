<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return CMap::mergeArray(array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'CybBet',
    'defaultController' => 'games',
    'sourceLanguage' => 'xx',
    'language' => 'en',
    'theme' => 'site',
    // preloading 'log' component
    'preload' => array('log'),

    // autoloading model and component classes
    'import' => array(
        'application.vendors.payeer.*',
        'application.models.*',
        'application.components.*',
        'application.modules.admin.AdminModule',
        'ext.eoauth.*',
        'ext.eoauth.lib.*',
        'ext.lightopenid.*',
        'ext.eauth.services.*',
    ),

    'modules' => array(
        // uncomment the following to enable the Gii tool
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => 'qwezxc',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array('127.0.0.1', '::1'),
        ),
        'admin' => array(
            'import' => array(
                'admin.components.*',
                'admin.models.*',
            ),
            'layout' => 'main',
            'defaultController' => 'index',
        ),
        'account' => array(
            'import' => array(
                'admin.components.*',
                'admin.models.*',
            ),
            'layout' => 'main',
            'defaultController' => 'index',
        ),
    ),

    // application components
    'components' => array(

        'user' => array(
            'class' => 'application.components.WebUser',
            'loginUrl' => array('/user/login'),
            'allowAutoLogin' => true,
            'autoUpdateFlash' => false,
        ),
        'authManager' => array(
            'class' => 'PhpAuthManager',
            'defaultRoles' => array('Guest'),
        ),
        'loid' => array(
            'class' => 'ext.lightopenid.loid',
        ),
        'eauth' => array(
            'class' => 'ext.eauth.EAuth',
            'popup' => true, // Use the popup window instead of redirecting.
            'services' => array( // You can change the providers and their classes.
                'google' => array(
                    'class' => 'GoogleOpenIDService',
                ),
                'yandex' => array(
                    'class' => 'YandexOpenIDService',
                ),
                'twitter' => array(
                    'class' => 'TwitterOAuthService',
                    'key' => '...',
                    'secret' => '...',
                ),
                'facebook' => array(
                    'class' => 'FacebookOAuthService',
                    'client_id' => 'xxxxxxxxxxxxxx',
                    'client_secret' => 'xxxxxxxxxxxxxx',
                ),
                'vkontakte' => array(
                    'class' => 'VKontakteOAuthService',
                    'client_id' => 'xxxxxx',
                    'client_secret' => 'xxxxxxxxxx',
                ),
                'mailru' => array(
                    'class' => 'MailruOAuthService',
                    'client_id' => '...',
                    'client_secret' => '...',
                ),
            ),
        ),
        'input' => array(
            'class' => 'CmsInput',
            'cleanPost' => true,
            'cleanGet' => true,
        ),
        // uncomment the following to enable URLs in path-format

        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false,
//            'routeVar'=>'route',
            'rules' => array(
//                '<language:(ru)>' => '<controller>/<action>/ru',
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
//                'games/<[a-zA-Z0-9].*>'=>'games/index',
            ),
        ),

//		'db'=>array(
//			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
//		),
        // uncomment the following to use a MySQL database
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning,info,debug',
                ),
//                array(
//                    'class' => 'CWebLogRoute',
//                    'categories' => 'application',
//                    'showInFireBug' => true
//                ),
                // uncomment the following to show log messages on web pages
                /*
                array(
                    'class'=>'CWebLogRoute',
                ),
                */
            ),
        ),
        'mailer' => array(
            'class' => 'application.extensions.mailer.yiimailer',
        ),
        'messages' => array(
            'class' => 'NewCybMessageSource',
            'translatedMessageTable' => 'yii_message',
            'sourceMessageTable' => 'yii_sourcemessage',
            'onMissingTranslation' => array('AdminModule', 'missingTranslation'),
            //'cachingDuration' => 3600,//1 hour cache time
        ),
        'coreMessages' => array(
            'class' => 'NewCybMessageSource',
            'translatedMessageTable' => 'yii_message',
            'sourceMessageTable' => 'yii_sourcemessage',
            'onMissingTranslation' => array('AdminModule', 'missingTranslation'),
            //'cachingDuration' => 3600,//1 hour cache time
        ),
        'cache' => array(
            'class' => 'CFileCache',
            'embedExpiry' => true,
        ),
    ),

    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        // this is used in contact page
        'adminEmail' => 'noreply@cybbet.com',
        'default_lang_id' => 1, //english
        'enable_i18n_debug' => true,
        'default_pagesize' => 10,
        'enable_email_notification' => true,
        'enable_email_debug' => false,
        'email_debug_email' => 'noreply@cybbet.com',
        'onePercentMoney' => 100,
        'outgoingMessageMarkerColor' => 'dodgerblue',
        'winBetsCorol' => 'green',
        'lostBetsColor' => 'red',
        'currentDomain' => 'cybbet',
        'dbTimeFormat' => 'Y-m-d H:i:s',
        'enableAutoPayout' => true,
        'payeerAccount' => 'xxxxxxxxx',
        'payeerApiId' => 'xxxxxxxx',
        'payeerSecretApiKey' => 'xxxxxxxxxxx',
        'payeerShop' => 'xxxxxxxxx',
        'payeerSecretKey' => 'xxxxxxxxxxxxxxxx',
        'webmoneyWalletNumber' => 'xxxxxxxxxxxx',
        'webmoneySecretKey' => 'xxxxxxxxxxx',

    ),
), require(dirname(__FILE__) . '/main.custom.php')
);