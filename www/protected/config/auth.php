<?php

return array(
    'admin.*' =>
    array(
        'type' => 0,
        'description' => 'All admin actions',
        'bizRule' => NULL,
        'data' => NULL,
    ),
    'account.*' =>
        array(
            'type' => 0,
            'description' => 'All account actions',
            'bizRule' => NULL,
            'data' => NULL,
        ),
    'site.*' =>
    array(
        'type' => 0,
        'description' => 'All site actions',
        'bizRule' => NULL,
        'data' => NULL,
    ),
    'games.*' =>
        array(
            'type' => 0,
            'description' => 'All games actions',
            'bizRule' => NULL,
            'data' => NULL,
        ),
    'news.*' =>
    array(
        'type' => 0,
        'description' => 'All news actions',
        'bizRule' => NULL,
        'data' => NULL,
    ),
    'makebet.*' =>
        array(
            'type' => 0,
            'description' => 'All makebet actions',
            'bizRule' => NULL,
            'data' => NULL,
        ),
    'results.*' =>
        array(
            'type' => 0,
            'description' => 'All results actions',
            'bizRule' => NULL,
            'data' => NULL,
        ),
    'faq.*' =>
        array(
            'type' => 0,
            'description' => 'All faq actions',
            'bizRule' => NULL,
            'data' => NULL,
        ),
    'feedback.*' =>
        array(
            'type' => 0,
            'description' => 'All feedback actions',
            'bizRule' => NULL,
            'data' => NULL,
        ),
    'paidBets.*' =>
        array(
            'type' => 0,
            'description' => 'All paidBets actions',
            'bizRule' => NULL,
            'data' => NULL,
        ),
    'contacts.*' =>
        array(
            'type' => 0,
            'description' => 'All contacts actions',
            'bizRule' => NULL,
            'data' => NULL,
        ),
    'usercash.*' =>
        array(
            'type' => 0,
            'description' => 'All UserCash actions',
            'bizRule' => NULL,
            'data' => NULL,
        ),
    'admin.user.login' =>
    array(
        'type' => 0,
        'description' => 'login user',
        'bizRule' => NULL,
        'data' => NULL,
    ),
    'user.login' =>
    array(
        'type' => 0,
        'description' => 'Frontend user login',
        'bizRule' => NULL,
        'data' => NULL,
    ),
    'user.register' =>
        array(
            'type' => 0,
            'description' => 'Frontend user registration',
            'bizRule' => NULL,
            'data' => NULL,
        ),
    'user.logout' =>
    array(
        'type' => 0,
        'description' => 'Frontend user logout',
        'bizRule' => NULL,
        'data' => NULL,
    ),
    'user.remindpassword' =>
    array(
        'type' => 0,
        'description' => 'Password reminder',
        'bizRule' => NULL,
        'data' => NULL,
    ),
    'user.activation' =>
        array(
            'type' => 0,
            'description' => 'Account activation',
            'bizRule' => NULL,
            'data' => NULL,
        ),
    'user.passwordchange'=>
    array(
        'type' => 0,
        'description' => 'Password update',
        'bizRule' => NULL,
        'data' => NULL,
    ),
    'user.captcha' =>
    array(
        'type' => 0,
        'description' => 'Captcha',
        'bizRule' => NULL,
        'data' => NULL,
    ),
    'Guest' =>
    array(
        'type' => 2,
        'description' => '',
        'bizRule' => NULL,
        'data' => NULL,
        'children' =>
        array(
            0 => 'site.*',
            1 => 'admin.user.login',
            2 => 'user.login',
            3 => 'user.logout',
            4 => 'user.remindpassword',
            5 => 'user.captcha',
            6 => 'user.passwordchange',
            7 => 'user.register',
            8 => 'user.activation',
            9 => 'user.remindpassword',
            10 => 'games.*',
            11 => 'news.*',
            12 => 'makebet.*',
            13 => 'faq.*',
            14 => 'contacts.*',
            15 => 'feedback.*',
            16 => 'results.*',
            17 => 'paidBets.*',
        ),
    ),
    'Registered' =>
        array(
            'type' => 2,
            'description' => '',
            'bizRule' => NULL,
            'data' => NULL,
            'children' =>
                array(
                    0 => 'Guest',
                    1 => 'account.*',
                    2 => 'usercash.*',
                ),
        ),
    'ClientPro' =>
        array(
            'type' => 2,
            'description' => '',
            'bizRule' => NULL,
            'data' => NULL,
            'children' =>
                array(
                    0 => 'Registered',
                ),
        ),
    'Moderator' =>
        array(
            'type' => 2,
            'description' => '',
            'bizRule' => NULL,
            'data' => NULL,
            'children' =>
                array(
                    0 => 'ClientPro',
//                    1 => 'admin.*'
                ),
        ),
    'AdminModerator' =>
        array(
            'type' => 2,
            'description' => '',
            'bizRule' => NULL,
            'data' => NULL,
            'children' =>
                array(
                    0 => 'Moderator',
                    1 => 'admin.*',
                ),
        ),
    'Analyst' =>
        array(
            'type' => 2,
            'description' => '',
            'bizRule' => NULL,
            'data' => NULL,
            'children' =>
                array(
                    0 => 'ClientPro',
                    1 => 'admin.*',
                ),
        ),
    'Administrator' =>
    array(
        'type' => 2,
        'description' => '',
        'bizRule' => NULL,
        'data' => NULL,
        'children' =>
        array(
            0 => 'AdminModerator',
            1 => 'Analyst',
            2 => 'admin.*',
        ),
    ),
);
