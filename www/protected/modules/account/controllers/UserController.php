<?php

/**
 * Class UserController
 */
class UserController extends CybController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
//			'postOnly + delete', // we only allow deletion via POST request
		);
	}

    /**
     * @return array
     */
    public function actions(){
		return array(
			'captcha'=>array(
				'class'=>'CCaptchaAction',
			),
		);
	}


    /**
     * @param $id
     * @return CActiveRecord|EMongoDocument|null
     * @throws CHttpException
     */
    public function loadModel($id)
	{
		$model=User::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}


    /**
     *
     */
    public function actionAccountEdit() {

        $this->layout='//layouts/rightcol';
        $model = new AccountEditForm();

        $user = User::model()->findByPk(Yii::app()->user->id);
        $this->performAjaxValidation($model,'user-form');
        $model->language = Userlanguage::model()->findByAttributes(array('user_id'=>Yii::app()->user->id))->Languages_id;
//        var_dump($_POST['AccountEditForm']);die;
        if (isset($_POST['AccountEditForm'])) {
            $model->attributes=$_POST['AccountEditForm'];
            $user->firstName = $model->firstName;
            $user->lastName = $model->lastName;
            $model->language = $_POST['language'];
            $user->timezone = $model->timezone;
            if ((int)$_POST['AccountEditForm']['change_password']>0 && $_POST['AccountEditForm']['old_password'] && $_POST['AccountEditForm']['password']) {
                $user->password = $user->repeat_password = $_POST['AccountEditForm']['password'];
            } else {
                $user->password = $user->repeat_password = null;
            }
            $user->validate();
            if($user->save()) {

                $this->_changeUserLanguage(Yii::app()->user->id,$model->language);
                Yii::app()->user->setFlash('success', Yii::t('games', 'Your account updated.'));
                $this->redirect(array('accountedit'));
            }
        }
        $model->attributes = $user->attributes;
        $model->password = null;
        $this->render('update',array(
            'model'=>$model,'user'=>$user
        ));

    }



	/**
	 * Performs the AJAX validation.
     *
	 * @param User $model the model to be validated
	 */
	protected function performAjaxValidation($model, $form)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']===$form)
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    private function _changeUserLanguage($user_id,$language_id,$isNew = false) {
        if (!$isNew) {
            $userLang = Userlanguage::model()->findByAttributes(array('user_id'=>$user_id));

            Yii::app()->db->createCommand("UPDATE userlanguage SET Languages_id=".$language_id." WHERE id=".$userLang->id)->query();
        } else {
            $userLang = new Userlanguage();
            $userLang->user_id = $user_id;
            $userLang->Languages_id = $language_id;

            $userLang->save();
        }
//        var_dump($userLang->save());die;
        Helper::changeUserLanguage($user_id,$language_id);
        return true;
    }

}
