<?php
/**
 * Created by PhpStorm.
 * User: lldl
 * Date: 11/21/14
 * Time: 5:02 PM
 */
//Todo: move all usercash operations to thid controller
class UsercashController extends CybController
{
    /**
     * @return array action filters
     */
    public $layout = '//layouts/account';

    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
//			'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * @return array
     */
    public function actions()
    {
        return array(
            'captcha' => array(
                'class' => 'CCaptchaAction',
            ),
        );
    }

    public function analyticsAction(){
        if(!isset($_POST['ajax']) or Yii::app()->user->isGuest) {
            exit();
        }
        $productRevenue = $_POST['productRevenue'];
        $productName = $_POST['productName'];
        $userId = Yii::app()->user->id;
        $this->render('analytics', array(
            'userId' => $userId,
            'productRevenue' => $productRevenue,
            'productName' => $productName,
        ));
    }
    public function actionPayout()
    {
        Yii::log('action Payout',CLogger::LEVEL_INFO);
        $model = new PayoutForm();
        if (isset($_POST['PayoutForm'])) {
            if($_POST['PayoutForm']['amount'] < 1 ) {
                $flashMessage = Yii::t('site', 'Error : min amount 15$');
                Yii::app()->user->setFlash('error', $flashMessage);
                return $this->actionCash();
            }

            /** @var User $user */
            $user = User::model()->findByPk(Yii::app()->user->id);
            if($_POST['PayoutForm']['amount'] > $user->usercash->realMoney) {
                $flashMessage = Yii::t('site', 'Error : You cann\'t take away more money, than you have !$');
                Yii::app()->user->setFlash('error', $flashMessage);
                return $this->actionCash();
            }
            $payout = new Payout();
            $payout->system_id = $_POST['PayoutForm']['system'];
            $payout->e_wallet = $_POST['PayoutForm']['eWalletNumber'];
            $payout->summ = $_POST['PayoutForm']['amount'];
            $payout->user_id = Yii::app()->user->id;
            $payout->status = Payout::STATUS_CREATED;
            $payout->date = time();
            if ($payout->save()) {
                $flashMessage = Yii::t('site', 'Your request for payment was successfully created');
                Yii::app()->user->setFlash('success', $flashMessage);

                // make automatic payout
                if (Yii::app()->params['enableAutoPayout']) {
                    if($payoutRes = $payout->autoPayout()) {
                        $user = User::model()->findByPk(Yii::app()->user->id);
                        $user->usercash->realMoney -= $payout->summ;
                        $user->usercash->save();
                    }
                }
            } else {
                $flashMessage = Yii::t('site', 'Your request for payment was  not successfully created');
                Yii::app()->user->setFlash('error', $flashMessage);
            }
            return $this->actionCash();
        }

        $paymentSystems = PaymentSystem::model()->findAll();
        return $this->renderPartial('_payoutForm', array(
            'model' => $model,
            'paymentSystems' => $paymentSystems
        ), false, true);
    }

    /*Todo: move different payment systems to different actions */
    public function actionCash()
    {
        /**Todo: check this: */
        if (isset($_POST['summ']) && isset($_POST['accountIdent']) && isset($_POST['secureId']) && isset($_POST['system'])) {
            $incomeDepositHistory = new IncomeDepositHistory();
            $user = Yii::app()->getUser();
            $user_id = $user->id;
            $incomeDepositHistory->status = 0;
            $incomeDepositHistory->userId = $user_id;
            $incomeDepositHistory->system = $_REQUEST['system'];
            $incomeDepositHistory->summ = $_REQUEST['summ'];
            $incomeDepositHistory->time = time();
            $incomeDepositHistory->operationType = 3;
            if (isset($_POST['accountIdent'])) {
                $incomeDepositHistory->accountIdent = $_POST['accountIdent'];
            }
            if (isset($_POST['secureId'])) {
                $incomeDepositHistory->secureId = $_POST['secureId'];
            }
            $result = $incomeDepositHistory->save();
            if ($result) {
                // activating group bonuses for incoming deposit
                $groupBonusActivator = new GroupBonusActivator();
                $groupBonusActivator->ActivateGroupBonuses(4, Yii::app()->getUser()->id);

                $flashMessage = Yii::t('site', 'Real money succesfully added.');
                Yii::app()->user->setFlash('success', $flashMessage);
            } else {
                $flashMessage = Yii::t('site', 'Error adding money !');
                Yii::app()->user->setFlash('error', $flashMessage);
            }
        } else if (isset($_REQUEST['m_operation_id']) && isset($_REQUEST['m_sign'])) {
            $m_key = Yii::app()->params['payeerSecretKey'];;
            $arHash = array(
                $_REQUEST['m_operation_id'],
                $_REQUEST['m_operation_ps'],
                $_REQUEST['m_operation_date'],
                $_REQUEST['m_operation_pay_date'],
                $_REQUEST['m_shop'],
                $_REQUEST['m_orderid'],
                $_REQUEST['m_amount'],
                $_REQUEST['m_curr'],
                $_REQUEST['m_desc'],
                $_REQUEST['m_status'],
                $m_key);
            $sign_hash = strtoupper(hash('sha256', implode(':', $arHash)));
            if ($_REQUEST['m_sign'] == $sign_hash && $_REQUEST['m_status'] == 'success') {
                $flashMessage = Yii::t('site', 'Payment is successfully done !');
                Yii::app()->user->setFlash('sucess', $flashMessage);
                $usercash = new Usercash();
                $user = Yii::app()->user;
                $user_id = $user->id;
                /** @var Usercash $usercash */
                $usercash = $usercash->findByAttributes(array('userId' => $user_id));
                $usercash->realMoney += $_REQUEST['m_amount'];
                $usercash->save();
                exit;
            }
            $flashMessage = Yii::t('site', $_REQUEST['m_orderid'] . '|error');
            Yii::app()->user->setFlash('error', $flashMessage);
            echo $_REQUEST['m_orderid'] . '|error';
        } else if (isset($_REQUEST['system']) && $_REQUEST['system'] == 'payeer') {
            $m_shop = Yii::app()->params['payeerShop'];
            $m_amount = number_format($_REQUEST['amount'], 2, '.', '');
            $m_curr = 'USD';
            $m_desc = base64_encode('deposit');
            $m_key = Yii::app()->params['payeerSecretKey'];
            /** @var IncomeDepositHistory $incomeDepositHistory */
            $incomeDepositHistory = $this->createIncomeDepositeHistoryRecord();
            $m_orderid = $incomeDepositHistory->id;
            $arHash = array(
                $m_shop,
                $m_orderid,
                $m_amount,
                $m_curr,
                $m_desc,
                $m_key
            );
            $sign = strtoupper(hash('sha256', implode(':', $arHash)));

            $curlfields = array(
                'm_shop' => $m_shop,
                'm_orderid' => $m_orderid,
                'm_amount' => $m_amount,
                'm_curr' => $m_curr,
                'm_desc' => $m_desc,
                'm_sign' => $sign,

            );
            $data = http_build_query($curlfields);
            $url = 'http://payeer.com/merchant' . '?' . $data;
            $this->redirect($url);
        } else if (isset($_REQUEST['system']) && $_REQUEST['system'] == 'webmoney') {
            $curlfields = array(
                'LMI_PAYMENT_AMOUNT' => $_REQUEST['LMI_PAYMENT_AMOUNT'],
                'LMI_PAYMENT_DESC' => 'deposit',
                'LMI_PAYMENT_NO' => '123',
                'LMI_PAYEE_PURSE' => Yii::app()->params['webmoneyWalletNumber'],
                'LMI_SIM_MODE' => '0',
            );
            $url = 'https://merchant.webmoney.ru/lmi/payment.asp';
            $postfieds = http_build_query($curlfields);
            if ($curl = curl_init()) {

                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $postfieds);
                curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($curl, CURLOPT_AUTOREFERER, true);
                curl_setopt($curl, CURLOPT_REFERER, $url);
                curl_setopt($curl, CURLOPT_USERAGENT, "Opera/10.00 (Windows NT 5.1; U; ru) Presto/2.2.0");
                $out = curl_exec($curl);
                echo $out;
                curl_close($curl);
                exit();
            }

        }

        $webmoneyWalletNumber = Yii::app()->params['webmoneyWalletNumber'];
        return $this->render('cash', array(
            'webmoneyWalletNumber' => $webmoneyWalletNumber
        ));
    }

    protected function createIncomeDepositeHistoryRecord($m_sign = 0)
    {
        if (isset($_REQUEST['amount']) && isset($_REQUEST['system'])) {
            $incomeDepositHistory = new IncomeDepositHistory();
            $user = Yii::app()->user;
            $user_id = $user->id;
            $incomeDepositHistory->status = 0;
            $incomeDepositHistory->userId = $user_id;
            $incomeDepositHistory->system = $_REQUEST['system'];
            $incomeDepositHistory->summ = $_REQUEST['amount'];
            $incomeDepositHistory->time = time();
            $incomeDepositHistory->operationType = 3;
            $incomeDepositHistory->sign = $m_sign;
            if (isset($_REQUEST['accountIdent'])) {
                $incomeDepositHistory->accountIdent = $_REQUEST['accountIdent'];
            }
            if (isset($_REQUEST['secureId'])) {
                $incomeDepositHistory->secureId = $_REQUEST['secureId'];
            }
            $result = $incomeDepositHistory->save();
            return $result ? $incomeDepositHistory : false;
        }
        return false;
    }
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='payout-form-payoutForm-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
} 