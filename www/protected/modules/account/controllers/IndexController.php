<?php

class IndexController extends CybController
{
    public $layout = '//layouts/account';

    public function actionBets()
    {
        $simpleBetsModel = new Userbets('search');

        $expressBetsModel = new Expressbets('search');

        $tournamebtBetsModel = new Tournamentsbets('search');

        $simpleBetsModel->userId = $expressBetsModel->userId = $tournamebtBetsModel->userId = Yii::app()->user->id;
        $params = Yii::app()->params;
        $this->render('bets', array(
            'simpleBetsModel' => $simpleBetsModel,
            'expressBetsModel' => $expressBetsModel,
            'tournamebtBetsModel' => $tournamebtBetsModel,
            'params' => $params,
        ));
    }

    public function actionBonuses()
    {
        /** @var User $user */
        $user = Yii::app()->getUser();
        $userId = $user->id;

        $dataForRender = array();

        if (!empty($_REQUEST['promoCodeActivateForm'])) {
            /** @var PromoCode $promoCode */
            $promoCode = new PromoCode();
            $promoActResult = $promoCode->activatePromoCode($_REQUEST['code']);
            $dataForRender['promoCodeResult'] = $promoActResult;
        }
        /** @var RefCode $refCode */
        $refCode = new RefCode();
        /** @var RefCode $refCode */
        $refCode = $refCode->getRefCode($userId);
        $regCount = $refCode->getRegCount();
        $regUsers = array();
        if ($refCode->getRegUsers()) {
            $regUsersArr = $refCode->getRegUsers();
            foreach ($regUsersArr as $regUser) {
                /** @var User $user */
                $user = $regUser['user'];
                $key = $user->userName;
                $bonusSum = $regUser['sum'];
                $regUsers[$key] = $bonusSum;
            }
        } else {
            $regUsers = array(array());
        }
        $refCodeString = $refCode->code;
        $dataForRender['refCode'] = array(
            'refCodeString' => $refCodeString,
            'regUsers' => $regUsers,
            'regCount' => $regCount,
        );

        $bonus = new Bonus();
        $bonuses = $bonus->getActiveBonusesByUserId($userId);
        $bonuses = array_reverse($bonuses);
        $dataForRender['bonuses'] = $bonuses;

        // getting bonus balance
        /** @var BonusBalance $bonusBalance */
        $bonusBalance = new BonusBalance();
        $bonusBalance = $bonusBalance->getBalanceByUserId($userId);
        $dataForRender['bonusBalance'] = $bonusBalance;


        $this->render('bonuses', $dataForRender);
    }

    public function actionCash()
    {
        /**Todo: check this: */
        if (isset($_POST['summ']) && isset($_POST['accountIdent']) && isset($_POST['secureId']) && isset($_POST['system'])) {
//            $userCashOperationsHistory = new Payments();
//            $user = Yii::app()->getUser();
//            $user_id = $user->id;
//            $result = $userCashOperationsHistory->addOperation(
//                $user_id,
//                Payments::CASH_HISTORY_INCOME,
//                $_REQUEST['summ'],
//                $_REQUEST['accountIdent'],
//                $_REQUEST['secureId'],
//                $_REQUEST['system']);
            $incomeDepositHistory = new IncomeDepositHistory();
            $user = Yii::app()->getUser();
            $user_id = $user->id;
            $incomeDepositHistory->status = 0;
            $incomeDepositHistory->userId = $user_id;
            $incomeDepositHistory->system = $_REQUEST['system'];
            $incomeDepositHistory->summ = $_REQUEST['summ'];
            $incomeDepositHistory->time = time();
            $incomeDepositHistory->operationType = 3;
            if (isset($_POST['accountIdent'])) {
                $incomeDepositHistory->accountIdent = $_POST['accountIdent'];
            }
            if (isset($_POST['secureId'])) {
                $incomeDepositHistory->secureId = $_POST['secureId'];
            }
            $result = $incomeDepositHistory->save();
            if ($result) {
                // activating group bonuses for incoming deposit
                $groupBonusActivator = new GroupBonusActivator();
                $groupBonusActivator->ActivateGroupBonuses(4, Yii::app()->getUser()->id);

                $flashMessage = Yii::t('site', 'Real money succesfully added.');
                Yii::app()->user->setFlash('success', $flashMessage);
            } else {
                $flashMessage = Yii::t('site', 'Error adding money !');
                Yii::app()->user->setFlash('error', $flashMessage);
            }
//        } // moved to siteController
//        else if (isset($_Get['m_operation_id']) && isset($_GET['m_shop'])) {
//            if ($_REQUEST['m_status'] == 'success') {
//                $flashMessage = Yii::t('site', 'Payment is successfully done !');
//                Yii::app()->user->setFlash('sucess', $flashMessage);
//                $usercash = new Usercash();
//                $user = Yii::app()->user;
//                $user_id = $user->id;
//                /** @var Usercash $usercash */
//                $usercash = $usercash->findByAttributes(array('userId' => $user_id));
//                $usercash->realMoney += $_POST['m_amount'];
//                $usercash->save();
//                exit;
//            }
        } else if (isset($_REQUEST['m_operation_id']) && isset($_REQUEST['m_sign'])) {
            $m_key = Yii::app()->params['payeerSecretKey'];;
            $arHash = array(
                $_REQUEST['m_operation_id'],
                $_REQUEST['m_operation_ps'],
                $_REQUEST['m_operation_date'],
                $_REQUEST['m_operation_pay_date'],
                $_REQUEST['m_shop'],
                $_REQUEST['m_orderid'],
                $_REQUEST['m_amount'],
                $_REQUEST['m_curr'],
                $_REQUEST['m_desc'],
                $_REQUEST['m_status'],
                $m_key);
            $sign_hash = strtoupper(hash('sha256', implode(':', $arHash)));
            if ($_REQUEST['m_sign'] == $sign_hash && $_REQUEST['m_status'] == 'success') {
                $flashMessage = Yii::t('site', 'Payment is successfully done !');
                Yii::app()->user->setFlash('sucess', $flashMessage);
                $usercash = new Usercash();
                $user = Yii::app()->user;
                $user_id = $user->id;
                /** @var Usercash $usercash */
                $usercash = $usercash->findByAttributes(array('userId' => $user_id));
                $usercash->realMoney += $_REQUEST['m_amount'];
                $usercash->save();
                exit;
            }
            $flashMessage = Yii::t('site', $_REQUEST['m_orderid'] . '|error');
            Yii::app()->user->setFlash('error', $flashMessage);
            echo $_REQUEST['m_orderid'] . '|error';
        } else if (isset($_REQUEST['system']) && $_REQUEST['system'] == 'payeer') {
            $m_shop = Yii::app()->params['payeerShop'];
            $m_amount = number_format($_REQUEST['amount'], 2, '.', '');
            $m_curr = 'USD';
            $m_desc = base64_encode('deposit');
            $m_key = Yii::app()->params['payeerSecretKey'];
            /** @var IncomeDepositHistory $incomeDepositHistory */
            $incomeDepositHistory = $this->createIncomeDepositeHistoryRecord();
            $m_orderid = $incomeDepositHistory->id;
            $arHash = array(
                $m_shop,
                $m_orderid,
                $m_amount,
                $m_curr,
                $m_desc,
                $m_key
            );
            $sign = strtoupper(hash('sha256', implode(':', $arHash)));

            $curlfields = array(
                'm_shop' => $m_shop,
                'm_orderid' => $m_orderid,
                'm_amount' => $m_amount,
                'm_curr' => $m_curr,
                'm_desc' => $m_desc,
                'm_sign' => $sign,

            );
            $data = http_build_query($curlfields);
            $url = 'http://payeer.com/merchant' . '?' . $data;
            $this->redirect($url);
        } else if (isset($_REQUEST['system']) && $_REQUEST['system'] == 'webmoney') {
            $curlfields = array(
                'LMI_PAYMENT_AMOUNT' => $_REQUEST['LMI_PAYMENT_AMOUNT'],
                'LMI_PAYMENT_DESC' => 'deposit',
                'LMI_PAYMENT_NO' => '123',
                'LMI_PAYEE_PURSE' => Yii::app()->params['webmoneyWalletNumber'],
                'LMI_SIM_MODE' => '0',
            );
            $url = 'https://merchant.webmoney.ru/lmi/payment.asp';
            $postfieds = http_build_query($curlfields);
            if ($curl = curl_init()) {

                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $postfieds);
                curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($curl, CURLOPT_AUTOREFERER, true);
                curl_setopt($curl, CURLOPT_REFERER, $url);
                curl_setopt($curl, CURLOPT_USERAGENT, "Opera/10.00 (Windows NT 5.1; U; ru) Presto/2.2.0");
                $out = curl_exec($curl);
                echo $out;
                curl_close($curl);
                exit();
            }

        }


        $this->render('cash', array());
    }

    public function actionMessages()
    {

        $personalMessage = new PersonalMessage();
        $user = Yii::app()->getUser();
        $userId = $user->id;
        /** @var PersonalMessage $incomingMessages */
        $incomingMessages = $dataProvider = $personalMessage->searchByRecipientId($userId)->getData();
        /** @var PersonalMessage $outgoingMessages */
        $outgoingMessages = $dataProvider = $personalMessage->searchBySenderId($userId)->getData();
        /** @var PersonalMessage $messages */
        $messagesData = array_merge($incomingMessages, $outgoingMessages);
        /** @var personalMessage $messages */
        $messages = array();
        foreach ($messagesData as $key => $message) {
            $id = $message->id;
            $messages[$id] = $message;
        }
        ksort($messages, SORT_NUMERIC);
        $messages = array_reverse($messages);
        $this->render('messages', array('messages' => $messages, 'userId' => $userId));
    }

    protected function createIncomeDepositeHistoryRecord($m_sign = 0)
    {
        if (isset($_REQUEST['amount']) && isset($_REQUEST['system'])) {
            $incomeDepositHistory = new IncomeDepositHistory();
            $user = Yii::app()->user;
            $user_id = $user->id;
            $incomeDepositHistory->status = 0;
            $incomeDepositHistory->userId = $user_id;
            $incomeDepositHistory->system = $_REQUEST['system'];
            $incomeDepositHistory->summ = $_REQUEST['amount'];
            $incomeDepositHistory->time = time();
            $incomeDepositHistory->operationType = 3;
            $incomeDepositHistory->sign = $m_sign;
            if (isset($_REQUEST['accountIdent'])) {
                $incomeDepositHistory->accountIdent = $_REQUEST['accountIdent'];
            }
            if (isset($_REQUEST['secureId'])) {
                $incomeDepositHistory->secureId = $_REQUEST['secureId'];
            }
            $result = $incomeDepositHistory->save();
            return $result ? $incomeDepositHistory : false;
        }
        return false;
    }

}