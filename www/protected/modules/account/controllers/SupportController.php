<?php

class SupportController extends CybController
{
    public $layout = '//layouts/account';

    public function actionIndex()
    {
        $tickets = Ticket::model()->findAllByAttributes(array('user_id' => Yii::app()->user->id));
        $tickets = array_reverse($tickets);
        $this->render('index', array(
            'tickets' => $tickets
        ));
    }

    public function actionTicket($id)
    {
        /** @var TicketPersonalMessages $ticketPersonalMessages */
        $ticketPersonalMessages = TicketPersonalMessages::model()->findAllByAttributes(
            array(
                'ticket_id' => $id
            )
        );
        $ticketPersonalMessages = array_reverse($ticketPersonalMessages);

        /** @var Ticket[] $ticket */
        $ticket= Ticket::model()->findByPk($id);

        /** @var PersonalMessage $model */
        $model = new PersonalMessage();
        return $this->render('ticket',
            array(
                'ticketPersonalMessages' => $ticketPersonalMessages,
                'ticket' => $ticket,
                'model' => $model
            )
        );
    }
    public function actionSendMessage($id){

        //Todo: refactor model names
        /**@var PersonalMessage */
        $personalMessage = new PersonalMessage();
        /**@var TicketPersonalMessages $ticketPersonalMessages */
        $ticketPersonalMessages = new TicketPersonalMessages();
        if(!empty($_POST['PersonalMessage'])) {
            $personalMessage->attributes =$_POST['PersonalMessage'];
            $personalMessage->sender_id = Yii::app()->user->id;
            $result = $personalMessage->save();

            $ticketPersonalMessages->personal_message_id = $personalMessage->id;
            $ticketPersonalMessages->ticket_id = $id;
            $ticketPersonalMessages->save();
        }
        return $this->actionTicket($id);
    }

    public function actionCreateTicket(){
        $model = new Ticket;

        // uncomment the following code to enable ajax-based validation

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'tickets-ticketForm-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }


        if (isset($_POST['Ticket'])) {
            $model->attributes = $_POST['Ticket'];
            $model->user_id = intval(Yii::app()->user->id);
            $model->time = time();
            $model->status_id = TicketStatus::OPEN;
            if ($model->validate()) {
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', Yii::t('site', 'Your ticket successfully created'));
                } else {
                    Yii::app()->user->setFlash('error', Yii::t('site', 'Your ticket not created'));
                }
                /** @var PersonalMessage $massage */
                $massage = new PersonalMessage();
                $massage->subject = $model->title;
                $massage->text = $model->text;
                $massage->send(Yii::app()->user->id);

                /** @var TicketPersonalMessages $ticketPersonalMessage */
                $ticketPersonalMessage = new TicketPersonalMessages();
                $ticketPersonalMessage->ticket_id= $model->id;
                $ticketPersonalMessage->personal_message_id = $massage->id;
                $ticketPersonalMessage->save();
            }
        }
         $this->redirect(Yii::app()->request->redirect($_SERVER['HTTP_REFERER']));
    }

    // Uncomment the following methods and override them if needed
    /*
    public function filters()
    {
        // return the filter configuration for this controller, e.g.:
        return array(
            'inlineFilterName',
            array(
                'class'=>'path.to.FilterClass',
                'propertyName'=>'propertyValue',
            ),
        );
    }

    public function actions()
    {
        // return external action classes, e.g.:
        return array(
            'action1'=>'path.to.ActionClass',
            'action2'=>array(
                'class'=>'path.to.AnotherActionClass',
                'propertyName'=>'propertyValue',
            ),
        );
    }
    */
}