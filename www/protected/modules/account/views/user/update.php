<?php
$baseUrl = Yii::app()->theme->baseUrl;
Yii::app()->clientScript->registerScriptFile($baseUrl.'/js/jquery.form.js');

?>
    <h1 class="page-title"><?php echo Yii::t('site','Edit User Account') ?></h1>
<div class="form">
	
	<?php
	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'user-form',
		'enableAjaxValidation' => true,
		'clientOptions' => array(
			'validateOnSubmit' => true,
			'validateOnChange' => true,
		),
		'htmlOptions' => array('class' => 'reg-form g-clearfix')
	));
	?>
    <p class="note"><?php echo  Yii::t('edit','Fields with')?> <span class="required">*</span><?php echo  Yii::t('edit','are required.')?></p>

    <?php echo $form->errorSummary($model); ?>
	<div class="row">
        <div class="label">
            <?php echo Helper::fixLabelAsterisk($form->labelEx($user, 'firstName')); ?>
        </div>

        <?php echo $form->textField($model, 'firstName', array('size' => 45, 'maxlength' => 45, 'class' => 'w2')); ?>
        <?php echo $form->error($model, 'firstName'); ?>
	</div>
    <div class="row">
        <div class="label">
            <?php echo Helper::fixLabelAsterisk($form->labelEx($user, 'lastName')); ?>
        </div>

        <?php echo $form->textField($model, 'lastName', array('size' => 45, 'maxlength' => 45, 'class' => 'w2')); ?>
        <?php echo $form->error($model, 'lastName'); ?>
    </div>
    <div class="row">
        <div class="label">
            <?php echo $form->labelEx($user, 'userName'); ?>
        </div>

        <?php echo $form->textField($model, 'userName', array('size' => 45, 'maxlength' => 45, 'class' => 'w2', 'disabled' => 'true')); ?>
        <?php echo $form->error($model, 'lastName'); ?>
    </div>
    <div class="row">
        <div class="label">
            <?php echo CHtml::label(Yii::t('account', 'Change password?'), 'change_password'); ?>
        </div>
        <?php echo CHTML::activeCheckBox($model, 'change_password', false); ?>
    </div>
    <div class="row">
        <div class="password_container">
            <div class="label">
                <?php echo Helper::fixLabelAsterisk($form->labelEx($model, 'old_password')); ?>
            </div>

            <?php echo $form->passwordField($model, 'old_password', array('size' => 45, 'maxlength' => 45, 'class' => 'w2')); ?>
            <?php echo $form->error($model, 'old_password'); ?>
        </div>
    </div>

    <div class="row password_container">
            <div class="label">
                <?php echo Helper::fixLabelAsterisk($form->labelEx($model, 'password')); ?>
            </div>

            <?php echo $form->passwordField($model, 'password', array('size' => 45, 'maxlength' => 45, 'class' => 'w2')); ?>
            <?php echo $form->error($model, 'password'); ?>
    </div>
    <div class="row password_container">
            <div class="label">
                <?php echo Helper::fixLabelAsterisk($form->labelEx($model, 'repeat_password')); ?>
            </div>

            <?php echo $form->passwordField($model, 'repeat_password', array('size' => 45, 'maxlength' => 45, 'class' => 'w2')); ?>
            <?php echo $form->error($model, 'repeat_password'); ?>
    </div>
    <div class="row">
        <div class="label">
            <?php echo $form->labelEx($model, 'timezone'); ?>
        </div>
        <?php echo $form->dropDownList($model, 'timezone', User::getTimeZoneSelect()); ?>
    </div>
    <div class="row">
        <div class="label">
            <?php echo $form->labelEx($model, 'language'); ?>
        </div>

        <?php echo CHtml::dropDownList('language', $model->language, CHtml::listData(Languages::model()->findAll(array('order'=>'name')), 'id', 'name'), array(
            'class' => 'user-field')); ?>
    </div>
    <div class="row buttons">
        <?php echo CHtml::submitButton(Yii::t('edit','Save'), array('class' => 'btn-reg btn-style2')); ?>
    </div>
	<?php $this->endWidget(); ?>
	
</div>
<?php
Yii::app()->clientScript->registerScript(uniqid(), "
	jQuery('#AccountEditForm_change_password').click(function (e) {
		jQuery(this).is(':checked') ? jQuery('.password_container').show() : jQuery('.password_container').hide();
	});
", CClientScript::POS_END);