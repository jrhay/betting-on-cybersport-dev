<?php
/** @var string $productRevenue
 * @var string $productName
 * @var string $userId
 */
?>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter27897090 = new Ya.Metrika({id:27897090});
            } catch(e) { }
        });
        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n[removed].insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";
        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/27897090" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<script>
    ga('ecommerce:addTransaction', {
        'id': '<?php echo $userId?>',                     // Transaction ID. Required.
        'revenue': '<?php echo $productRevenue?>'               // Grand Total.
    });
    ga('ecommerce:addItem', {
        'id': '<?php echo $userId?>',                     // Transaction ID. Required.
        'name': '<?php echo $productName?>'    // Product name. Required.
    });
</script>