<div class="message item-list col-inner style6" xmlns="http://www.w3.org/1999/html">

<h1 class="page-title"><?php echo  Yii::t('admin', 'Пополнить баланс') ?></h1>


<form style="display: block">
    <div class="pay-sys style4">
        <div id="radioWBButton" class="select-styled webmoney jq-radio " onclick="selectSys('webmoney')"
             value="webmoney" name="system"
            ></div>
        <div id="radioPayeerButton" class="select-styled payeer jq-radio " onclick="selectSys('payeer')"
             value="payeer" name="system"
            ></div>
        <div id="" class="select-styled  jq-radio " onclick="alert('toto')"
             value="webmoney" name="system"
            ></div>

    </div>

</form>
<?php
echo CHtml::ajaxLink(Yii::t('cash', 'Payout'), 'payout',
    array('update' => '#simple-div'),
    array('id' => 'simple-link-' . uniqid(),
        'class' => 'btn btn-style2 btn-payout')
);
?>
<div class="ui-dialog ui-widget ui-widget-content ui-corner-all ui-draggable ui-dialog-buttons"
     style=" border-top:none;margin-left: 25%;margin-top:0px" id="simple-div">

</div>

<script type="text/javascript">
    function selectSys(sysname) {
        switch (sysname) {
            case 'webmoney':
                $('#radioWBButton').addClass('checked');
                $('#radioPayeerButton').removeClass('checked');
                document.getElementById('webmoneyForm').style.display = "block";
                document.getElementById('payeerForm').style.display = "none";
                break
            case 'payeer':
                $('#radioPayeerButton').addClass('checked');
                $('#radioWBButton').removeClass('checked');
                document.getElementById('webmoneyForm').style.display = "none";
                document.getElementById('payeerForm').style.display = "block";
                break;
        }
    }
</script>

<form style="display: none" name="payeerForm" id="payeerForm" method="POST"
      action="<?php echo  Yii::app()->createUrl('account/usercash/cash') ?>">
    <h3> <?php echo  Yii::t('admin', 'Внести депозит через Payeer') ?></h3><br>

    <input type='hidden' required value="payeer" name="system"/>

    <!--        <div class="pay-sys style4">-->
    <!--            <!--            <input class="select-styled webmoney" onclick="" required value="webmoney" name="system"-->
    <!--            <!--                   type="radio" checked="checked"/>-->
    <!--            <!--            <input class="select-styled payeer" required value="payeer" name="system" type="radio"/>-->
    <!--            <!--            <input class="select-styled bitcoin" required value="" name="system" type="radio"/>-->
    <!--        </div>-->

    <div class="inputs">
        <div class="input">
            <input type="text" name="amount" value="100">
            <label for="amount"><?php echo  Yii::t('admin', 'Сумма') ?> ( $ ) </label>
        </div>
        <div class="btn-pay btn-style1">
            <input class="btn-pay btn-style1 " type="submit" value="<?php echo  Yii::t('admin', 'Оплатить') ?>">
        </div>
    </div>

    <div class="payment-system-text payment-form">
        <?php  if (Yii::app()->language === 'en'): ?>
            Payeer.com  include 150+ ways to help you with deposit.
            <div class="payment-icons">
                <img src="<?php echo  Yii::app()->baseUrl . '/../images/payment_systems/visa.png' ?>">
                 <img src="<?php echo  Yii::app()->baseUrl . '/../images/payment_systems/master_card.png' ?>">
                 <img src="<?php echo  Yii::app()->baseUrl . '/../images/payment_systems/w1.png' ?>">
                 <img src="<?php echo  Yii::app()->baseUrl . '/../images/payment_systems/yandex_money.png' ?>">
                 <img src="<?php echo  Yii::app()->baseUrl . '/../images/payment_systems/wwt.png' ?>">
            </div>
            To make a deposit from your Payeer account:
            Simply enter the amount you would like to deposit in the field above and Click  ‘DEPOSIT’. Then choose the most convenient way to finish it.

            A secure connection to the Payeer website will be opened in order to complete the transaction.
            After your payment has been confirmed, the funds will be credited to your Cybbet account instantly.

            Please note, if you did not find right way for you, try to change country inside Payeer.com system. They will offer you different ways, according to country you choose.

            For use Credit Cards by Payeer. It necessary to have their account. Just choose this way and follow the Payeer instructions.

            *Please note that the minimum withdrawal amount is 15 USD
        <?php  else : ?>
            Система Payeer включает в себя более 150 способов пополнения счета.
            <div class="payment-icons2">
                <img src="<?php echo  Yii::app()->baseUrl . '/../images/payment_systems/visa.png' ?>">
                <img src="<?php echo  Yii::app()->baseUrl . '/../images/payment_systems/master_card.png' ?>">
                <img src="<?php echo  Yii::app()->baseUrl . '/../images/payment_systems/qiwi_wallet.png' ?>">
                <img src="<?php echo  Yii::app()->baseUrl . '/../images/payment_systems/yandex_money.png' ?>">
                <img src="<?php echo  Yii::app()->baseUrl . '/../images/payment_systems/liqpay.png' ?>">
                <img src="<?php echo  Yii::app()->baseUrl . '/../images/payment_systems/mts.png' ?>">
                <img src="<?php echo  Yii::app()->baseUrl . '/../images/payment_systems/megafon.jpeg' ?>">
                <img src="<?php echo  Yii::app()->baseUrl . '/../images/payment_systems/wwt.png' ?>">
            </div>
            Для пополнения счета - укажите сумму денег, нажмите "Внести", далее вас перенаправит на безопасное соединение с Payeer.com
            Далее выберите удобный для вас способ оплаты из всех перечисленных.

            Обратите внимание, если вы не нашли подходящий вам способ, попробуйте поменять страну в payeer.com. Сервис подберет вам другие, доступные в другой стране, инструменты оплаты.

            Для оплаты кредитными картами через Payeer - вам понадобится, сначала создать там аккаунт. Просто выберите данный метод и следуйте инструкциям.

            *Пожалуйста, помните, что минимальная сумма на вывод составляет 15 USD.
        <?php endif ?>
    </div>

</form>

<form class="" id='webmoneyForm' name='webmoneyForm' method="POST"
      action="https://merchant.webmoney.ru/lmi/payment.asp">
    <h3> <?php echo  Yii::t('admin', 'Внести депозит через WebMoney') ?></h3><br>
    <input type="hidden" required value="webmoney" name="system"/>
    <input type="hidden" required value="<?php echo  Yii::app()->user->id ?>" name="USER_ID"/>
    <!--        <input type="hidden" required-->
    <!--               value="-->
    <?php  //= Yii :: app()->createUrl('account/index/cash', array('payment_status' => 'success')) ?><!--"-->
    <!--               name="SuccessUrl"/>-->
    <!--        <input type="hidden" required-->
    <!--               value="-->
    <?php  //= Yii :: app()->createUrl('account/index/cash', array('payment_status' => 'failed')) ?><!--"-->
    <!--               name="FailedUrl"/>-->

    <div class="inputs">
        <div class="input">
            <input type="text" name="LMI_PAYMENT_AMOUNT" value="100">
            <label for="summ"><?php echo  Yii::t('admin', 'Сумма') ?> ( $ )</label>
        </div>
        <div class="input">
            <input type="hidden" name="LMI_PAYMENT_DESC" value="deposit">
        </div>
        <div class="input">
            <input type="hidden" name="LMI_PAYMENT_NO" value="<?php echo  rand(1000, 10000) ?>">
        </div>
        <div class="input">
            <?php  /** @var string $webmoneyWalletNumber */ ?>
            <input type="hidden" name="LMI_PAYEE_PURSE" value="<?php echo  $webmoneyWalletNumber ?>">
        </div>
        <div class="input">
            <input type="hidden" name="LMI_SIM_MODE" value="0">
        </div>
        <div class="btn-pay btn-style1 ">
            <input class="btn-pay btn-style1 " type="submit" value="<?php echo  Yii::t('admin', 'Оплатить') ?>">
        </div>
    </div>

    <div class="payment-system-text payment-form">
        <?php  if (Yii::app()->language === 'en'): ?>
            Cybbet accepts transactions from WebMoney accounts.

            To make a deposit from your WebMoney account:
            Simply enter the amount you would like to deposit in the field above and Click ‘DEPOSIT’.

            A secure connection to the Webmoney website will be opened in order to complete the transaction.
            After your payment has been confirmed, the funds will be credited to your Cybbet account instantly.

            Important! If any problems occur while making a deposit with your WebMoney Keeper Classic account please clear Cache and Cookies. If you need further instructions about your deposit, please contact our support team for immediate assistance, by clicking on the online support icon.

            If you do not have a WebMoney account, please click on the button below.
            *Please note that the minimum withdrawal amount is USD15

        <?php  else : ?>
            Мы принимаем переводы через систему Webmoney.
            Для совершения депозита денег,  просто введите нужную сумму и нажмите "Внести".

            Будет создано безопасное соединение в системой Webmoney для завершения платежа.
            После того, как платеж будет завершен, деньги моментально зачислятся на ваш счет в Cybbet.com

            Важно! Если у вас возникли проблемы во время депозита через  WebMoney Keeper Classic, пожалуйста, почистите Cache и Cookies .  Если вам понадобят дополнительные инструкции, свяжитесь с нашей командой поддержки.

            Если у вас нету аккаунта в webmoney, нажмите на кнопку ниже.
            *Пожалуйста, помните, что минимальная сумма на вывод составляет 15 USD
        <?php endif ?>
        <br>

    </div>


</form>


<form style="display: none" method="POST" name="addRealMoneyForm" action="account/index/cash">
    <div class="inputs">
        <div class="input">
            <input required name="summ" type="text"/>
            <label for="summ"><?php echo  Yii::t('admin', 'Сумма') ?> </label>
        </div>
        <div class="input">
            <input required name="accountIdent" type="text"/>
            <label for="accountIdent">Account ID or e-mail</label>
        </div>
        <div class="input">
            <input required name="secureId" type="text"/>
            <label for="secureId">Secure ID</label>
        </div>
        <input class="btn-pay btn-style2" type="submit" value=" <?php echo  Yii::t('admin', 'Оплатить') ?> "/>
    </div>

</form>

<!--    <a class="btn btn-style1" style="width: 30%;margin-left: 35%" href="-->
<?php  //= Yii::app()->createUrl('games/index') ?><!--" title="">--><?php  //= Yii::t('site','Вывести деньги')?><!--</a>-->
<br>
<br>

</div>
<a class="btn btn-style1" href="<?php echo  Yii::app()->createUrl('games') ?>" title=""><?php echo
    Yii::t('site', 'Сделать ставку
                        прямо сейчас')?></a>

