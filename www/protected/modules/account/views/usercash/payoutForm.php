<?php
/* @var $this PayoutFormController */
/* @var $model PayoutForm */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'payout-form-payoutForm-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// See class documentation of CActiveForm for details on this,
	// you need to use the performAjaxValidation()-method described there.
//	'enableAjaxValidation'=>true,
//    'enableClientValidation' => true,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>
    <br>
	<?php echo $form->errorSummary($model); ?>
	<div class="row selectBox">
		<?php echo $form->labelEx($model,'system'); ?>
		<?php
        /** @var PaymentSystem $paymentSystems[] */
        echo $form->dropDownList($model,'system',
            CHtml::listData($paymentSystems,'id','name'),
        array(
            'class' => 'btn-style1',
            'style' => 'max-width: 100%'
        )); ?>
		<?php echo $form->error($model,'system'); ?>
	</div>
    <br>
	<div class="row">
		<?php echo $form->labelEx($model,'eWalletNumber'); ?>
        <br>
		<?php echo $form->textField($model,'eWalletNumber', array('required' => true)); ?>
		<?php echo $form->error($model,'eWalletNumber'); ?>
	</div>
    <br>
	<div class="row">
		<?php echo $form->labelEx($model,'amount'); ?>
        <br>
		<?php echo $form->textField($model,'amount', array('required' => true)); ?>
		<?php echo $form->error($model,'amount'); ?>
	</div>
    <br>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Submit', array('class' => 'btn btn-style1')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->