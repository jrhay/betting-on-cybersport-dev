<?php
echo "

<div class=\"message item-list col-inner style6\">
    <table  cellspacing=\"5\" cellpadding=\"10\" border=\"1\" width=\"100%\" class=\"\">";

echo "
                <tr valign='top' align='center'>
                <td>";
echo "          <br><br>
                <h1 class=\"\">" . Yii::t('admin', 'Бонусный счет') . "</h1>
                <br>
                <br>
                <div style='width: 60%;background: #212121;color: #CBCBCB;border-width: 0' class=\"btn-style1 \"><h3>$bonusBalance (BP)</h3></div>
                <br>
                <br>";

echo "
                     <form class='form-actions' name = 'promoCodeActivateForm' action=\"" . Yii::app()->createUrl('account/index/bonuses') . "\" method=\"post\" enctype=\"multipart/form-data\">
                        <input type='hidden' name=\"promoCodeActivateForm\" value='true'>
                        <h1 class=\"\">" . Yii::t('admin', 'Промокод') . "</h1><br>
                        <input  required type='text' name=\"code\">
                        <br><br>
                        <input style='width: 60%' class=\"btn-style1\" type=\"submit\"  value=\"" . Yii::t('admin', 'Активировать') . "\" />
                     <br>
                     </form>";
if (isset($promoCodeResult)) {
    if ($promoCodeResult) {
        echo "<br><div style=\"color: green\">" . Yii::t('admin', 'Промо код активирован') . "</div>";
    } else {
        echo "<br><div style=\"color: red\">" . Yii::t('admin', 'Промо код не активирован!') . "</div>";
    }
}

echo "
                </td>";
echo " <td>";
echo "
                <br>
                <br>
                <h1>" . Yii::t('admin', 'Пригласить друга') . "</h1>
                <br>
                <span>" . Yii::t('admin', 'Ссылка') . "</span>
                <br>
                <br>
                <textarea style=\"height: 100%; width: 70%;overflow: auto\" rows=\"3\">http://{$refCode['refCodeString']}</textarea>
                <br>
                <br>
                <span>" . Yii::t('admin', 'Зарегистрировано по вашему приглашению : ') . ' ' . $refCode['regCount'] . " </span>
                <br>
                <br>
                <select class=\"btn-style1\">
                    <option >" . Yii::t('admin', 'Приглашенные пользователи') . "</option>";

foreach ($refCode['regUsers'] as $username => $bonusSum) {
    echo "          <option>  $username  + $bonusSum на бонусный счет</option>
                <br>
                <br>
                ";
}
echo "

                </select>";
echo " </td>";
echo "
            </tr>";
echo "
    </table>
    <br>
    <br>
    <h1 class=\"page-title\">" . Yii::t('admin', 'Активные бонусы') . "</h1>
    <ul>";

/** @var Bonus $bonus */
foreach ($bonuses as $bonus) {

    echo "
        <li>
            <div class=\"item toggle style4\">
                $bonus->title
                <span></span>
            </div>
            <div class=\"hidden style5\">
                $bonus->text
            </div>
        </li>";
}
echo '
    </ul>
</div>';