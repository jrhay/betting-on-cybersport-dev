<?php
echo '
<div class="message item-list col-inner style6">
    <h1 class="page-title">' . Yii::t('admin','Сообщения') . '</h1>
    <ul>';
/** @var PersonalMessage $message */
foreach ($messages as $message) {
//    $created = date('l jS \of F Y h:i:s A', $message->created);
    $created = $message->created;
    $markOutgoing = '';
    if($message->sender_id == $userId) {
        $color = Yii::app()->params['outgoingMessageMarkerColor'];
        $markOutgoing = "<div style=\"color: $color\">" . Yii::t('admin','(исходящее сообщение)') . "</div>";
    }
    echo "
        <li>
            <div class=\"item toggle style4\">
                $message->subject &nbsp $markOutgoing
                <span></span>
            </div>
            <div class=\"hidden style5\">
                $message->text
                <hr>
                $created
            </div>
        </li>";
}
echo '
    </ul>
</div>';