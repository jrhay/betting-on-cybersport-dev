<?php $this

    ->widget('application.widgets.grid.GGridView', array(
        'id' => 'simplebet-grid',
        'itemsCssClass' => 'res-tab',
        'dataProvider' => $model->search(),
        'pager' => array(
            'header' => '',
        ),
        'template' => '{items}{pager}',
        'columns' => array(
            'Time' => array('header' => Yii::t('general', 'Time'), 'name' => 'Time', 'value' => function ($data) {
                    $fontcolor = '';
                    switch ($data->isWin) {
                        case 0:
                            $fontcolor = '';
                            break;
                        case 1:
                            $fontcolor = 'red';
                            break;
                        case 2:
                            $fontcolor = 'green';
                            break;
                    };
                    echo '<font color=' . $fontcolor . '>' . gmdate('Y-m-d H:i:s', $data->game->game_start_at) . '</div>';
                }),
            'Event' => array('header' => Yii::t('general', 'Event'), 'name' => 'Event', 'value' => function ($data) {
                    $fontcolor = '';
                    switch ($data->isWin) {
                        case 0:
                            $fontcolor = '';
                            break;
                        case 1:
                            $fontcolor = 'red';
                            break;
                        case 2:
                            $fontcolor = 'green';
                            break;
                    };
                    echo '
                    <div style="color: ' . $fontcolor . '" class="mybets-sport">
                    <img class="game_icon_bets_list" width="20" height="20" src="/images/games/' . $data->game->category->image . '"
                           alt=""/>'
                        . $data->game->category->title . '</div>';
                    echo '<div style="color: ' . $fontcolor . '">' . $data->game->team1->name . ' - ' . $data->game->team2->name . '</div>';
                }),
            'Result' => array('name' => 'Result', 'header' => Yii::t('general', 'Result'), 'value' => function ($data) {


                    switch ($data->isWin) {
                        case 0:
                            echo Yii::t('games', 'This game is not finished yet');
                            break;
                        case 1:
                            echo '<div class="bet red">-' . $data->summ . ' $</div>';
                            break;
                        case 2:
                            echo '<div class="bet green">+' . $data->koef * $data->summ . ' $</div>';
                            break;
                    }
                    if ($data->isWin && !empty($data->game->betvideo)) {
                        echo
                        '<a class="link" onclick="PopUpShow'. $data->id.'()" href="#popup'.$data->id.'"  title="">'.Yii::t('admin', 'смотреть видео').'</a>
                        <script>
                            $(document).ready(function(){
                                PopUpHide'. $data->id.'();

                             });
                            function PopUpShow'. $data->id.'(){
                                var content'. $data->id .' = '. json_encode($data->game->betvideo).';
                                var x = document.getElementById("content-popup-'. $data->id .'");
                                x.innerHTML = content'. $data->id .';
                                $("#popup'. $data->id.'").show();
                            }
                            function PopUpHide'. $data->id.'(){
                                $("#popup'. $data->id.'").hide();
                            }
                         </script>';
                        echo '
                   <div class="b-popup" style="position: relative; z-index: 999;align: center;display: none" id="popup'. $data->id.'">
                       <div class="b-popup-content" style="padding: 0">
                           <div id = "content-popup-'. $data->id .'"></div>
                           <a href="javascript:PopUpHide'. $data->id.'()">Спрятать  видео</a>
                       </div>
                   </div>';

                    }
                }),
        ),
    )); ?>


