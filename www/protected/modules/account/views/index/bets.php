<div class="results style6">
    <?php
    $this->widget('zii.widgets.jui.CJuiTabs', array(
        'cssFile' => null,
        'id' => 'mybets-table',
        'tabs' => array(
            Yii::t('general', 'Simple Bets') => array(
                'content' => $this->renderPartial('_simple_bets_results', array(
                        'model' => $simpleBetsModel
                    ), true),
                'id' => 'tab_1',
            ),
            Yii::t('general', 'Express Bets') => array(
                'content' => $this->renderPartial('_express_bets_results', array(
                        'model' => $expressBetsModel
                    ), true),
                'id' => 'tab_2',
            ),
            Yii::t('general', 'Tournaments') => array(
                'content' => $this->renderPartial('_tournaments_bets_results', array(
                        'model' => $tournamebtBetsModel
                    ), true),
                'id' => 'tab_3',
            ),
        ),
        'options' => array(
            'collapsible' => false,
            'class' => 'tabs'
        ),
    ));
    ?>
</div>
