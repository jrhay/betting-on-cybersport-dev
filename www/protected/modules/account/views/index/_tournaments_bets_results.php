<?php $this->widget('application.widgets.grid.GGridView', array(
    'id' => 'tournaments-grid',
    'itemsCssClass' => 'res-tab',
    'dataProvider' => $model->search(),
    'pager' => array(
        'header' => '',
    ),
    'template' => '{items}{pager}',
    'columns' => array(
        'Time' => array('header' => Yii::t('general', 'Time'), 'name' => 'Time', 'value' => function ($data) {
                $fontcolor = '';
                switch ($data->isWin) {
                    case 0:
                        $fontcolor = '';
                        break;
                    case 1:
                        $fontcolor = 'red';
                        break;
                    case 2:
                        $fontcolor = 'green';
                        break;
                };
                echo "<font color = \"$fontcolor\">" . gmdate('Y-m-d H:i:s', $data->tournament->start_date) . '</font>';
            }),
        'Event' => array('header' => Yii::t('general', 'Event'), 'name' => 'Event', 'value' => function ($data) {
                $fontcolor = '';
                switch ($data->isWin) {
                    case 0:
                        $fontcolor = '';
                        break;
                    case 1:
                        $fontcolor = 'red';
                        break;
                    case 2:
                        $fontcolor = 'green';
                        break;
                };
                echo '<div style="color: ' . $fontcolor . '" class="userbets-expressGameRow">';
                echo '<img class="game_icon_bets_list" width="20" height="20" src="/images/tournaments/logo/' . $data->tournament->logo . '"
                           alt=""/>';
                echo $data->tournament->name . ' (' . $data->team->name . ')';
                echo '</div>';
            }),
        'Result' => array('name' => 'Result', 'header' => Yii::t('general', 'Result'), 'value' => function ($data) {

                switch ($data->isWin) {
                    case 0:
                        echo Yii::t('games', 'This game is not finished yet');
                        break;
                    case 1:
                        echo '<div class="bet red">-' . $data->summ . ' $</div>';
                        break;
                    case 2:
                        echo '<div class="bet green">+' . $data->koef * $data->summ . ' $</div>';
                        break;
                }
            }),
    ),
)); ?>
