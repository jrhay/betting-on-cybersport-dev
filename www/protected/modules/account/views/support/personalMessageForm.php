<?php
/* @var $this PersonalMessageController */
/* @var $model PersonalMessage */
/* @var $form CActiveForm */
/* @var Ticket $ticket*/
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'personal-message-personalMessageForm-form',
    'action' => Yii::app()->createUrl('account/support/sendMessage').'?id='.$ticket->id,
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// See class documentation of CActiveForm for details on this,
	// you need to use the performAjaxValidation()-method described there.
	'enableAjaxValidation'=>false,
)); ?>
    <br>
	<p class="note">Fields with <span class="required">*</span> are required.</p>
    <br>
	<?php echo $form->errorSummary($model); ?>

    <?php  echo $form->hiddenField($model,'recipient_id',array(
        'value' => $ticket->user->id,
    ))?>
    <?php  echo $form->hiddenField($model,'subject',array(
        'value' => $ticket->title,
    ))?>

	<div class="row ticket-message">
		<?php echo $form->labelEx($model,'text'); ?>
		<?php echo $form->textArea($model,'text', array(
        )); ?>
		<?php echo $form->error($model,'text'); ?>
	</div>
    <br>
	<div class="row buttons">
		<?php echo CHtml::submitButton(Yii::t('site','Send Message'), array(
            'class' => 'btn style1',
            'style' => 'margin-left: 1.5%',

        )); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->