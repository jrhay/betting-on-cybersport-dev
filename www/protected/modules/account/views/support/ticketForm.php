<?php
/* @var $this TicketsController */
/* @var $model Ticket */
/* @var $form CActiveForm */
?>

<div class="form" >

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'tickets-ticketForm-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// See class documentation of CActiveForm for details on this,
	// you need to use the performAjaxValidation()-method described there.
	'enableAjaxValidation'=>false,
    'action' => Yii::app()->createUrl('account/support/createTicket'),
)); ?>

<!--	<p class="note">Fields with <span class="required">*</span> are required.</p>-->

	<?php echo $form->errorSummary($model); ?>


	<div class="row">
        <?php
        /** @var TicketCategories[] $ticketsCArr */
        $ticketsCArr = array();
        foreach($ticketCategories as $category) {
            $ticketsCArr[$category->id] = Yii::t('ticket Category', $category->name);
        }
        ?>
        <table>
            <tr>
                <td>
		<?php echo $form->labelEx($model,'category_id'); ?>
                </td>
                <td>
		<?php echo $form->dropDownList($model,'category_id',$ticketsCArr, array(
            'style' => 'max-width: 100%',
            'class' => 'btn-style2'
        )) ?>
                </td>
            </tr>
        </table>
		<?php echo $form->error($model,'category_id'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array(
            'class' => 'form-input',
            'required' => 'required'
        )); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'text'); ?>
		<?php echo $form->textArea($model,'text',array(
            'class' => 'form-input',
            'required' => 'required',
            'style' => 'z-index: 9999'
        )); ?>
		<?php echo $form->error($model,'text'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton( Yii::t('account','Submit'), array(
            'class' => 'btn-style2',
            'style' => 'width: 50%'
        ));?>
        <br>
        <?php echo CHtml::link(Yii::t('site','My tickets'),Yii::app()->createUrl('account/support/index'))?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->