<?php
/* @var $this SupportController */

$this->breadcrumbs = array(
    'Support',
);
?>
<div class="message item-list col-inner style6">
    <h1><?php echo  Yii::t('site', 'My tickets') ?></h1>
    <br>
    <div class="ticket-list">
        <ul>

            <?php
            /** @var Ticket[] $tickets */
            foreach ($tickets as $ticket): ?>
                <li>
                    <a href="<?php echo  Yii::app()->createUrl('account/support/ticket').'?id='.$ticket->id?>">
                    <?php echo  $ticket->id ?>
                    <?php echo  $ticket->title ?>
                    </a>
                </li>
            <?php  endforeach ?>
        </ul>
    </div>
</div>
