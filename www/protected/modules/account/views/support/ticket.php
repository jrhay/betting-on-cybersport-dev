<?php
/* @var $this SupportController */
/** @var TicketPersonalMessages[] $ticketPersonalMessages
 *  @var Ticket $ticket
 */

$this->breadcrumbs = array(
    'Support' => array('/account/support'),
    'Ticket',
);
?>
<div class="message item-list col-inner style6">
    <h1><?php echo  Yii::t('site', 'Ticket Correspondence') ?></h1>
    <br>
    <h3><?php echo  Yii::t('site', 'Ticket') ?> № <?php echo $ticket->id?> <?php echo  $ticket->title?></h3>
    <br>
    <div class="correspondence">
        <?php  foreach ($ticketPersonalMessages as $tpm): ?>
            <div class="message">
                <div class="info">
                    <div class="sender">
                        <?php echo  $tpm->personalMessage->sender->userName ?>
                    </div>
                    <div class="created">
                        <?php echo  $tpm->personalMessage->created ?>
                    </div>
                </div>
                <div class="text">
                    <?php echo  $tpm->personalMessage->text ?>
                </div>
            </div>
        <?php  endforeach ?>
        <br>
    </div>
    <h4  style="margin-top: 10%;margin-bottom: 3%;margin-left: 0%"><?php echo Yii::t('site', 'New Message '); ?></h4>
    <?php  include('personalMessageForm.php')?>
</div>
