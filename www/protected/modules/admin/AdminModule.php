<?php

class AdminModule extends MasterModule
{

    public $theme = 'admin';


	public function init()
	{

        Yii::app()->user->setStateKeyPrefix('_admin_');
        Yii::app()->user->loginUrl = 'admin/user/login';
        Yii::app()->user->allowAutoLogin = true;
        Yii::app()->user->autoUpdateFlash = false;

        if (!Yii::app()->request->isAjaxRequest) {
            Yii::app()->clientScript->registerCoreScript('jquery.ui');
        }

        Yii::app()->theme = $this->theme;
        // Set theme url
        Yii::app()->themeManager->setBaseUrl(Yii::app()->theme->baseUrl);
        Yii::app()->themeManager->setBasePath(Yii::app()->theme->basePath);

		$this->setImport(array(
			'admin.models.*',
			'admin.components.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}

    static function missingTranslation($event) {
        $attributes = array('category' => $event->category, 'message' => $event->message);
        if (($model = MessageSource::model()->find('message=:message AND category=:category', $attributes)) === null) {
            $model = new MessageSource();
            $model->attributes = $attributes;
            if (!$model->save())
                return Yii::log('Message ' . $event->message . ' could not be added to messageSource table');
        }
        return $event;
    }
}
