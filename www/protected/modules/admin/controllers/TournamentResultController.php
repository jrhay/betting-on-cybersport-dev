<?php

class TournamentResultController extends AdminBaseController
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
//	public $layout='//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'admin','statistics'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new TournamentResult;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['TournamentResult'])) {
            $model->attributes = $_POST['TournamentResult'];
            $existedResults = TournamentResult::model()->findAllByAttributes(array(
                'tournament_id' => $model->tournament_id,
            ));
            if (!empty($existedResults)) {
                return $this->actionShowMessage(Yii::t('site', 'Result for this tournament is already exists.Update it or delete!'));
            }
            $model->winner_id = floatval($model->winner_id);
            $model->winner_koef = floatval($model->winner_koef);
            if ($model->validate()) {
                $model->handleTournamentBets();
                if ($model->logicError) {
                    return $this->actionShowMessage($model->logicError);
                }
                $model->save();
            }
            $this->redirect(array('view', 'id' => $model->id));
        }

        /** @var Teams[] $teams */
        $teams = Teams::model()->findAll();

        /** @var CDbCriteria $criteria */
        $criteria = new CDbCriteria();
//        $criteria->addCondition('start_date > :now');
//        $criteria->params[':now'] = time();

        /** @var Tournaments[] $tournaments */
        $tournaments = Tournaments::model()->findAll($criteria);

        $tournamentsModel = new Tournaments();

        $this->render('create', array(
            'model' => $model,
            'teams' => $teams,
            'tournaments' => $tournaments,
            'tournamentsModel' => $tournamentsModel,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['TournamentResult'])) {

            $model->attributes = $_POST['TournamentResult'];
            $model->time = DateTime::createFromFormat('Y-m-d H:i:s', $_POST['TournamentResult']['time'])->getTimestamp();
            if ($model->validate()) {
                $model->handleTournamentBets();
                if ($model->logicError) {
                    return $this->actionShowMessage($model->logicError);
                }
                $model->save();
            }
            $this->redirect(array('view', 'id' => $model->id));
        }

        /** @var Teams[] $teams */
        $teams = Teams::model()->findAll();

        /** @var CDbCriteria $criteria */
        $criteria = new CDbCriteria();
        $criteria->addCondition('start_date > :now');
        $criteria->params[':now'] = time();

        /** @var Tournaments[] $tournaments */
        $tournaments = Tournaments::model()->findAll($criteria);

        $this->render('update', array(
            'model' => $model,
            'teams' => $teams,
            'tournaments' => $tournaments
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('TournamentResult');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new TournamentResult('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['TournamentResult']))
            $model->attributes = $_GET['TournamentResult'];

        /** @var Teams[] $teams */
        $teams = Teams::model()->findAll();

        /** @var CDbCriteria $criteria */
        $criteria = new CDbCriteria();
        $criteria->addCondition('start_date > :now');
        $criteria->params[':now'] = time();

        /** @var Tournaments[] $tournaments */
        $tournaments = Tournaments::model()->findAll($criteria);
        $this->render('admin', array(
            'model' => $model,
            'teams' => $teams,
            'tournaments' => $tournaments
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return TournamentResult the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = TournamentResult::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param TournamentResult $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'tournament-result-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /** renders error paget with message */
    /** @param string $message */
    protected function actionShowMessage($message)
    {
        return $this->render('error', array(
            'message' => $message));
    }

    /**
     * ajax tournament statistics
     * @param integer $id
     * */
    public function actionStatistics($id)
    {
        if(!$this->isAjax())
            exit();
        /** @var Tournaments $tournament */
        $tournament = Tournaments::model()->findByPk($id);

        $countAllBets = count($tournament->bets);
        $countAllMoney = 0;
        foreach($tournament->bets as $bet) {
            $countAllMoney += $bet->summ;
        }
        /** @var integer[] $countBets */
        $countBets = array();
        /** @var integer[] $countMoney */
        $countMoney = array();
        foreach($tournament->tournamentsdetails as $detail) {
            foreach ($tournament->bets as $bet) {
                if($bet->teamId === $detail->teamId) {
                    $countBets[$detail->teamId]++;
                    $countMoney[$detail->teamId] += $bet->summ;
                }
            }
            if(!isset($countBets[$detail->teamId]))
                $countBets[$detail->teamId] = 0;
            if(!isset($countMoney[$detail->teamId]))
                $countMoney[$detail->teamId] = 0;
        }

        $this->renderPartial('_statistics', array(
            'tournament' => $tournament,
            'countAllBets' => $countAllBets,
            'countAllMoney' => $countAllMoney,
            'countBets' => $countBets,
            'countMoney' => $countMoney,
        ));
    }
    protected function isAjax() {
        return isset($_SERVER['HTTP_X_REQUESTED_WITH'])
        && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
    }
}
