<?php

class GameCategoriesController extends AdminBaseController
{

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
//			'postOnly + delete', // we only allow deletion via POST request
		);
	}


	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new GameCategories;

		// Uncomment the following line if AJAX validation is needed
		 $this->performAjaxValidation($model);

		if(isset($_POST['GameCategories']))
		{
			$model->attributes=$_POST['GameCategories'];
            $uploadedFile=CUploadedFile::getInstance($model,'image');
            $fileName = crypt(time(), rand(0,10000)).$uploadedFile;
            $model->image = $fileName;
			if($model->save()) {
                $uploadedFile->saveAs(Yii::app()->basePath.'/../images/games/'.$fileName);
                $this->redirect(array('index'));
            }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		 $this->performAjaxValidation($model);

		if(isset($_POST['GameCategories']))
		{
			$model->attributes=$_POST['GameCategories'];

            $uploadedFile=CUploadedFile::getInstance($model,'image');
            if (!empty($uploadedFile)) {
                $fileName = crypt(time(), rand(0,10000)).$uploadedFile;
                $model->image = $fileName;
            }
			if($model->save()) {
                if(!empty($uploadedFile))
                {
                    $uploadedFile->saveAs(Yii::app()->basePath.'/../images/games/'.$fileName);
                }
                $this->redirect(array('index'));
            }

		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
        if ($id) {
            $gameCategory = GameCategories::model()->findByPk($id);
            $gameCategory->setAttribute('status', CybController::DELETED_STATUS);
            $gameCategory->saveAttributes(array('status' => CybController::DELETED_STATUS));
            $gameCategory->delete();
        }
        $this->redirect(array('index'));

    }

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$model=new GameCategories('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['GameCategories']))
			$model->attributes=$_GET['GameCategories'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return GameCategories the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=GameCategories::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param GameCategories $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='game-categories-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
