<?php

/**
 * Class UsercashController
 */
class UsercashController extends AdminBaseController {

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Usercash the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Usercash::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, Yii::t('admin', 'The requested page does not exist.'));
        return $model;
    }

    public function actionIndex() {

        $model=new Usercash('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Usercash']))
            $model->attributes=$_GET['Usercash'];

        $this->render('index',array(
            'model'=>$model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);
        // Uncomment the following line if AJAX validation is needed
         $this->performAjaxValidation($model,'usercash-form');

        if(isset($_POST['Usercash']))
        {
            $model->attributes=$_POST['Usercash'];
            if($model->save()) {
                $this->redirect(array('index'));
            }
        }

        $this->render('update',array(
            'model'=>$model,
        ));
    }

    /**
     * Performs the AJAX validation.
     * @param Usercash $model the model to be validated
     */
    protected function performAjaxValidation($model, $form) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === $form) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}