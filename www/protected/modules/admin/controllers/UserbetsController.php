<?php

/**
 * Class UserbetsController
 */
class UserbetsController extends AdminBaseController {

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Userbets the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Userbets::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, Yii::t('admin', 'The requested page does not exist.'));
        return $model;
    }

    public function actionIndex() {

        $model=new Userbets('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Userbets']))
            $model->attributes=$_GET['Userbets'];

        $this->render('index',array(
            'model'=>$model,
        ));
    }

    public function actionDelete($id) {
        if ($id) {
            $currentUser = Userbets::model()->findByPk($id);
            $currentUser->setAttribute('status', CybController::DELETED_STATUS);
            $currentUser->saveAttributes(array('status' => CybController::DELETED_STATUS));
        }
        $this->redirect(array('index'));
    }

    /**
     * Cretes a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionCreate()
    {
        $model=  new Userbets();
        $model->betType = Userbets::TYPE_BET_SIMPLE;
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model,'userbets-form');

        if(isset($_POST['Userbets']))
        {
            $model->attributes=$_POST['Userbets'];
            if($model->save()) {
                $this->redirect(array('index'));
            }
        }

        $this->render('update',array(
            'model'=>$model,
        ));
    }
    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);
        // Uncomment the following line if AJAX validation is needed
         $this->performAjaxValidation($model,'userbets-form');

        if(isset($_POST['Userbets']))
        {
            $model->attributes=$_POST['Userbets'];
            if($model->save()) {
                $this->redirect(array('index'));
            }
        }

        $this->render('update',array(
            'model'=>$model,
        ));
    }

    /**
     * Performs the AJAX validation.
     * @param Userbets $model the model to be validated
     */
    protected function performAjaxValidation($model, $form) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === $form) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}