<?php

class TournamentsController extends AdminBaseController
{

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }


    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Tournaments the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Tournaments::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {

        /** @var Tournaments $model */
        $model = new Tournaments;
        $model->start_date = time();


        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Tournaments'])) {
            $_POST['Tournaments']['start_date'] = User::model()->getTimestampFromDate($_POST['Tournaments']['start_date']);
            $model->attributes = $_POST['Tournaments'];
            $model->logo = CUploadedFile::getInstance($model, 'logo');
            if($model->logo) {
                $filename = $model->logo->getName() . rand();
                $path = Yii::app()->theme->basePath . '/img/' . $filename;
                $model->logo->saveAs($path, true);
                $model->logo = $filename;
            }
            if ($model->save()) {
                $this->redirect(array('index'));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Performs the AJAX validation.
     * @param Tournaments $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'tournaments-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {

        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Tournaments'])) {
            $_POST['Tournaments']['start_date'] = User::model()->getTimestampFromDate($_POST['Tournaments']['start_date']);

            $model->attributes = $_POST['Tournaments'];

            if (CUploadedFile::getInstance($model, 'logo')) {
                $model->logo = CUploadedFile::getInstance($model, 'logo');
                $pathParts = pathinfo($model->logo->getName());
                $filename = $pathParts['filename'].'--'.rand().'.'.$pathParts['extension'];
                $path = Yii::app()->basePath.'/../images/tournaments/logo/' . $filename;
                $model->logo->saveAs($path, true);
                $model ->logo =  $filename;
            }
            if ($model->save()) {
                $this->redirect(array('index'));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $model = new Tournaments('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Tournaments']))
            $model->attributes = $_GET['Tournaments'];

        $this->render('index', array(
            'model' => $model,
        ));
    }
}
