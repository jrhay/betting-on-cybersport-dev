<?php

/**
 * Class EmailController
 */
class EmailController extends AdminBaseController {

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $languages = Languages::model()->findAll();

        $model = new Email;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Email'])) {
            $model->attributes = $_POST['Email'];
            if ($model->save())
                $this->redirect(array('index'));
        }

        $this->render('update', array(
            'model' => $model,
            'lang' => $languages,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id, $lang_id) {
        $languages = Languages::model()->findAll();

        $model = $this->loadModel($id, isset($_POST['Email']['lang_id']) ? $_POST['Email']['lang_id'] : $lang_id);

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Email'])) {
            $model->attributes = $_POST['Email'];
            $model->id = $id;

            if ($model->save())
                $this->redirect(array('index'));
        }

        $this->render('update', array(
            'model' => $model,
            'lang' => $languages
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {

        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $model = new Email('search');

        $cache = Yii::app()->cache->get('email');
        $cacheData = array();
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Email'])) {
            $model->attributes = $_GET['Email'];
        } else {
            if ($cache) {
                foreach ($model->attributes as $key => $attr) {
                    if (isset($cache[$key])) {
                        $model->$key = $cache[$key];
                    }
                }
            }
        }

        if (Yii::app()->request->isAjaxRequest) {
            foreach ($model->attributes as $key => $attr) {
                $cacheData[$key] = $attr;
            }
            Yii::app()->cache->set('Email', $cacheData, 3600);
        }

        $this->render('index', array(
            'model' => $model,
        ));
    }

    /**
     * Manages all models.
     */

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Email the loaded model
     * @throws CHttpException
     */
    public function loadModel($id, $lang_id = null) {
        if (!$lang_id) {
            $lang_id = Yii::app()->params['default_lang_id'];
        }
        $model = Email::model()->findByPk(array('id' => $id, 'lang_id' => $lang_id));
        if ($model === null) {
            $model = new Email;
            //throw new CHttpException(404,Yii::t('admin_home','The requested page does not exist.'));
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Email $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'email-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
