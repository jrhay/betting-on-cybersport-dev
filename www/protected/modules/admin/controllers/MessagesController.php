<?php

class MessagesController extends AdminBaseController
{

//    /**
//     * @return array action filters
//     */
//    public function filters()
//    {
//        return array(
//            'accessControl', // perform access control for CRUD operations
//            'postOnly + delete', // we only allow deletion via POST request
//        );
//    }


//    /**
//     * Displays a particular model.
//     * @param integer $id the ID of the model to be displayed
//     */
//    public function actionView($id)
//    {
//        $this->render('view',array(
//            'model'=>$this->loadModel($id),
//        ));
//    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionSend()
    {
        $userModel = new User;
        $users = $userModel->findAll();
        $sendResult = array();
        $model=new PersonalMessage('search');

        if (isset($_REQUEST['PersonalMessage']) && $model->attributes = $_REQUEST['PersonalMessage']) {
            $model->sender_id = Yii::app()->getUser()->id;
            $recipientModel = User::model()->findByAttributes(array('userName' => $_REQUEST['userName']));
            $recipient_id = $recipientModel->id;
            $sendResult['result'] = $model->send($recipient_id);
        }

        $this->render('send',array(
            'model'=>$model,
            'users'=> $users,
            'sendResult' => $sendResult,
        ));
    }

    public function actionUserAutoComplete(){
        if (isset($_GET['q'])) {

            $criteria = new CDbCriteria;
            $criteria->condition = 'userName LIKE :userName';
            $criteria->params = array(':userName'=>$_GET['q'].'%');

            if (isset($_GET['limit']) && is_numeric($_GET['limit'])) {
                $criteria->limit = $_GET['limit'];
            }

            $users = User::model()->findAll($criteria);

            $resStr = '';
            foreach ($users as $user) {
                $resStr .= $user->userName."\n";
            }
            echo $resStr;
        }
    }
    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
       //Todo: implement update message
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {

    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Games the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=PersonalMessage::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Games $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='messages-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
