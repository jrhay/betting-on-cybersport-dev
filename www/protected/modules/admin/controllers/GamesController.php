<?php

class GamesController extends AdminBaseController
{

    public function actionTeamAutoComplete()
    {
        if (isset($_GET['q'])) {

            $criteria = new CDbCriteria;

            $criteria->condition = 'name LIKE :name';
            $criteria->params = array(':name' => $_GET['q'] . '%');

            if(isset($_REQUEST['category_id'])) {
                $criteria->addCondition('category_id = :category_id');
                $criteria->params[':category_id'] = $_REQUEST['category_id'];
            }

            if (isset($_GET['limit']) && is_numeric($_GET['limit'])) {
                $criteria->limit = $_GET['limit'];
            }

            $gameCategories = Teams::model()->findAll($criteria);

            $resStr = '';
            foreach ($gameCategories as $category) {
                $resStr .= $category->name . "\n";
            }
            echo $resStr;
        }
    }
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }


    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {

        $model = new Games;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Games'])) {
            $_POST['Games']['game_start_at'] = User::model()->getTimestampFromDate($_POST['Games']['game_start_at']);
            $model->attributes = $_POST['Games'];
            $team1 =  Teams::model()->findByAttributes(array('name' => $_POST['Games']['team1_title']));
            $model->team1_id = $team1->id;
            $team2 =  Teams::model()->findByAttributes(array('name' => $_POST['Games']['team2_title']));
            $model->team2_id = $team2->id;

            $uploadedFile = CUploadedFile::getInstance($model, 'game_ico');
            if (!empty($uploadedFile)) {
                $fileName = crypt(time(), rand(0, 10000)) . $uploadedFile;
                $model->game_ico = $fileName;

            }

            $model->create_at = time();

            if ($model->save()) {

                $gameInital = new Gamesinitial();
                $gameInital->attributes = $model->attributes;
                $gameInital->gameId = $model->id;
                $gameInital->save();

                if (!empty($uploadedFile)) {
                    $uploadedFile->saveAs(Yii::app()->basePath . '/../images/gameIcons/' . $fileName);
                }
                $this->redirect(array('index'));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Games'])) {

            $_POST['Games']['game_start_at'] = User::model()->getTimestampFromDate($_POST['Games']['game_start_at']);
//            $_POST['Games']['game_end'] = User::model()->getTimestampFromDate($_POST['Games']['game_end']);
            $model->attributes = $_POST['Games'];

            $team1 =  Teams::model()->findByAttributes(array('name' => $_POST['Games']['team1_title']));
            $model->team1_id = $team1->id;
            $team2 =  Teams::model()->findByAttributes(array('name' => $_POST['Games']['team2_title']));
            $model->team2_id = $team2->id;

            $uploadedFile = CUploadedFile::getInstance($model, 'game_ico');
            if (!empty($uploadedFile)) {
                $fileName = crypt(time(), rand(0, 10000)) . $uploadedFile;
                $model->game_ico = $fileName;
            }

            if ($model->save()) {

                $gameInital = Gamesinitial::model()->findByAttributes(array('gameId' => $model->id));
                if ($gameInital) {
                    $gameInital->attributes = $model->attributes;
                    $gameInital->gameId = $model->id;
                    $gameInital->save();
                }
                if (!empty($uploadedFile)) {
                    $uploadedFile->saveAs(Yii::app()->basePath . '/../images/gameIcons/' . $fileName);
                }
                $this->redirect(array('index'));
            }

        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $model = new Games('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Games']))
            $model->attributes = $_GET['Games'];

        $this->render('index', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Games the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Games::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Games $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'games-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
