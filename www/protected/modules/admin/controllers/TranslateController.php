<?php

/**
 * Class TranslateController
 */
class TranslateController extends AdminBaseController {

    /**
     * @var string
     */
    public $defaultAction = 'admin';
    /**
     * @var array
     */
    private $_cache = array();

    /**
     *
     */
    const ID = 'mp-translate';

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionCreate($id, $language) {
        $model = new Message('create');
        $model->id = $id;
        $model->language = $language;
        $model->translation = $model->source->message;
        
        $this->performAjaxValidation($model, array('message-form'));

        if (isset($_POST['Message'])) {
            $model->attributes = $_POST['Message'];
            if ($model->save()) {
                if (Yii::app()->getRequest()->getIsAjaxRequest()) {
                    echo Yii::t('admin', 'Translation added successfully');
                    Yii::app()->end();
                }
                else
                    $this->redirect(array('admin/translate/missing'));
            }
        }

        Yii::app()->clientScript->scriptMap = array(
            'jquery.js' => false,
            'jquery.min.js' => false,
        );

        $this->renderpartial('create', array('model' => $model), false, true);
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id, $language) {
        $models = $this->translateLoadModels($id);

        $this->performAjaxValidationTabular($models, array('message-form'));

        if (isset($_POST['Message'])) {
            foreach($models as $i=>$model){
                $model->attributes = $_POST['Message'][$i];
                if ($model->translation) {
                    $model->save();
                }                
            }
            if (true) {
                if (Yii::app()->getRequest()->getIsAjaxRequest()) {
                    echo Yii::t('admin', 'Messages updated successfully');
                    Yii::app()->end();
                }
                else
                    $this->redirect(array('admin/translate/index'));
            }
        }

        Yii::app()->clientScript->scriptMap = array(
            'jquery.js' => false,
            'jquery.min.js' => false,
        );

        $this->renderpartial('update', array('models' => $models), false, true);
    }

    /**
     * Deletes a record
     * @param integer $id the ID of the model to be deleted
     * @param string $language the language of the model to de deleted
     */
    public function actionDelete($id, $language) {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            $model = $this->translateLoadModel($id, $language);
            if ($model->delete()) {
                if (Yii::app()->getRequest()->getIsAjaxRequest()) {
                    echo Yii::t('admin', 'Message deleted successfully');
                    Yii::app()->end();
                }
                else
                    $this->redirect(Yii::app()->getRequest()->getUrlReferrer());
            }
        }
        else
            throw new CHttpException(400);
    }

    /**
     * Manages all models.
     */
    public function actionIndex() {
        $model = new MessageSource('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['MessageSource']))
            $model->attributes = $_GET['MessageSource'];

        if (isset($_GET['missing'])) {
            Yii::app()->user->setState('missing', (int) $_GET['missing']);
            unset($_GET['missing']);
        }
        if (isset($_GET['language_filter'])) {
            Yii::app()->user->setState('language_filter', $_GET['language_filter']);
            unset($_GET['language_filter']);
        }

        $this->render('index', array(
            'model' => $model,
        ));
    }

    /**
     * 
     */
    public function actionMissing() {
        $model = new MessageSource('search');
        $model->unsetAttributes();  // clear any default values

        if (isset($_GET['MessageSource']))
            $model->attributes = $_GET['MessageSource'];

        $model->language = $this->getLanguage();
               
        $this->render('missing', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a record
     * @param integer $id the ID of the model to be deleted
     * @param string $language the language of the model to de deleted
     */
    public function actionMissingdelete($id) {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            $model = MessageSource::model()->findByPk($id);
            if ($model->delete()) {
                if (Yii::app()->getRequest()->getIsAjaxRequest()) {
                    echo Yii::t('admin', 'Message deleted successfully');
                    Yii::app()->end();
                }
                else
                    $this->redirect(Yii::app()->getRequest()->getUrlReferrer());
            }
        }
        else
            throw new CHttpException(400);
    }

    /**
     * @throws CHttpException
     */
    function actionSet() {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            $this->setLanguage($_POST[self::ID]);
            $this->redirect(Yii::app()->getRequest()->getUrlReferrer());
        }
        else
            throw new CHttpException(400);
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function translateLoadModel($id, $language) {
        $model = Message::model()->findByPk(array('id' => $id, 'language' => $language));
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * @param $id
     * @return array
     * @throws CHttpException
     */
    public function translateLoadModels($id) {
        $models = Message::model()->with(array('lang', 'source'))->findAllByAttributes(array('id' => $id));        
        if ($models === null){
            throw new CHttpException(404, 'The requested page does not exist.');
        } else {
            $languages = Languages::model()->findAll();            
            foreach($languages as $language){
                $present = false;
                foreach($models as $model){
                    if ($language->dialect == $model->language) {
                        $present = true;
                    }
                }
                if (!$present) {
                    $message = new Message('create');
                    $message->id = $id;
                    $message->language = $language->dialect;
                    $message->lang = $language;
                    if ($language->id == Yii::app()->params['default_lang_id']) {
                        $message->translation = $message->source->message;
                    }
                    $models[] = $message;
                    
                }
            }
        }
        return $models;
    }

    /**
     * @return mixed|string
     */
    public function getLanguage() {
        $key = self::ID;
        $acceptedLanguages = CHtml::listData(Languages::model()->findAll(), 'dialect', 'name');

        if (($language = @$this->_cache['language']) !== null)
            return $language;
        elseif (Yii::app()->getSession()->contains($key))
            $language = Yii::app()->getSession()->get($key);
        elseif (isset($_POST[$key]) && !empty($_POST[$key]))
            $language = $_POST[$key];
        elseif (isset($_GET[$key]) && !empty($_GET[$key]))
            $language = $_GET[$key];
        else
            $language = Yii::app()->getRequest()->getPreferredLanguage();

        if (!key_exists($language, $acceptedLanguages)) {
            if ($language === Yii::app()->sourceLanguage)
                $language = $this->defaultLanguage;
            elseif (strpos($language, "_") !== false) {
                $language = substr($language, 0, 2);
                if (!key_exists($language, $acceptedLanguages))
                    $language = $this->defaultLanguage;
            }
        }
        return $language;
    }

    /**
     * @param null $language
     * @return mixed|null|string
     */
    function setLanguage($language = null) {
        if ($language === null)
            $language = $this->getLanguage();

        $this->_cache['language'] = $language;

        Yii::app()->getSession()->add(self::ID, $language);
        Yii::app()->setLanguage($language);
        return $language;
    }

    /**
     * @param $model
     * @param $forms
     */
    public function performAjaxValidationTabular($model, $forms) {
        if (isset($_POST['ajax']) && in_array($_POST['ajax'], $forms)) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * @param $models
     * @param $forms
     */
    public function performAjaxValidation($models, $forms) {
        if (isset($_POST['ajax']) && in_array($_POST['ajax'], $forms)) {
            echo CActiveForm::validateTabular($models);
            Yii::app()->end();
        }
    }

}