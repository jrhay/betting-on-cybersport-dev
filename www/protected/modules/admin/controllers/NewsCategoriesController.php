<?php

/**
 * Class NewsCategoriesController
 */
class NewsCategoriesController extends AdminBaseController {

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return NewsCategoriesController the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = NewsCategories::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, Yii::t('admin', 'The requested page does not exist.'));
        return $model;
    }
    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        if ($id) {
           $newsCategories = NewsCategories::model()->deleteByPk($id);
        }
        $this->redirect(array('index'));

    }
    public function actionCreate()
    {
        $model=new NewsCategories();

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model,'news-categories-form');

        if(isset($_POST['NewsCategories']))
        {
            $model->attributes=$_POST['NewsCategories'];
            if($model->save()) {
                $this->redirect(array('index'));
            }
        }

        $this->render('create',array(
            'model'=>$model,
        ));
    }

    public function actionIndex() {

        $model=new NewsCategories('search');
//        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['NewsCategoriesController']))
            $model->attributes=$_GET['NewsCategoriesController'];

        $this->render('index',array(
            'model'=>$model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model,'news-categories-form');

        if(isset($_POST['NewsCategories']))
        {
            $model->attributes=$_POST['NewsCategories'];
            if($model->save()) {
                $this->redirect(array('index'));
            }
        }

        $this->render('update',array(
            'model'=>$model,
        ));
    }

    /**
     * Performs the AJAX validation.
     * @param NewsCategoriesController $model the model to be validated
     */
    protected function performAjaxValidation($model, $form) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === $form) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}