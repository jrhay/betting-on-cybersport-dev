<?php

/**
 * Class UserController
 */
class UserController extends AdminBaseController {

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return User the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = User::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, Yii::t('admin', 'The requested page does not exist.'));
        return $model;
    }

    public function actionIndex() {

        $model=new User('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['User']))
            $model->attributes=$_GET['User'];

        $this->render('index',array(
            'model'=>$model,
        ));
    }
    public function actionCreate()
    {
        $model=new User('create');

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model,'user-form');

        if(isset($_POST['User']))
        {
            $model->attributes=$_POST['User'];

            if($model->save()) {
                $this->_changeUserLanguage($model->id,$_POST['User']['language'], true);
                $this->redirect(array('index'));
            }

        }

        $this->render('create',array(
            'model'=>$model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);
        $model->language = Userlanguage::model()->findByAttributes(array('user_id'=>$id))->Languages_id;
        // Uncomment the following line if AJAX validation is needed
         $this->performAjaxValidation($model,'user-form');

        if(isset($_POST['User']))
        {

            $model->attributes=$_POST['User'];
            if($model->save()) {


                $this->_changeUserLanguage($model->id,$_POST['User']['language']);
                $this->redirect(array('index'));
            }
        }

        $this->render('update',array(
            'model'=>$model,
        ));
    }

    private function _changeUserLanguage($user_id,$language_id,$isNew = false) {
        if (!$isNew) {
            $userLang = Userlanguage::model()->findByAttributes(array('user_id'=>$user_id));

            Yii::app()->db->createCommand("UPDATE userlanguage SET Languages_id=".$language_id." WHERE id=".$userLang->id)->query();
        } else {
            $userLang = new Userlanguage();
            $userLang->user_id = $user_id;
            $userLang->Languages_id = $language_id;

            $userLang->save();
        }
//        var_dump($userLang->save());die;
        Helper::changeUserLanguage($user_id,$language_id);
        return true;
    }
    /**
     *
     */
    public function actionLogin() {

        $this->layout='//layouts/login';

        $model = new LoginForm;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login()) {
                $this->redirect(array("/admin/index/index"));
                //Todo:check it:
                if (Yii::app()->user->usertypeId!=User::ADMIN_USER) {
                    Yii::app()->user->logout();
                    Yii::app()->user->setFlash('error', Yii::t('admin','Only administrators can access administration area.'));
                } else {
                    $this->redirect(array("/admin/index/index"));
                }
            }

        }


        // display the login form
        if (!Yii::app()->user->isGuest) {
            $this->redirect(array("/admin/"));
        } else {
            $this->render('login', array('model' => $model));
        }
    }

    public function actionDelete($id) {
        if ($id) {
            $currentUser = User::model()->findByPk($id);
            $currentUser->setAttribute('status', User::DELETED_USER_STATUS);
            $currentUser->saveAttributes(array('status' => User::DELETED_USER_STATUS));
        }
        $this->redirect(array('index'));
    }

    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect(array('/admin/user/login'));
    }

    /**
     * Performs the AJAX validation.
     * @param User $model the model to be validated
     */
    protected function performAjaxValidation($model, $form) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === $form) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}