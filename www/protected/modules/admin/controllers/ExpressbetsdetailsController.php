<?php

class ExpressbetsdetailsController extends AdminBaseController
{

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
//			'postOnly + delete', // we only allow deletion via POST request
		);
	}


	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
    public function actionCreate($expressBetId)
    {

        $model=new Expressbetsdetails;
        $model->expressBetId = $expressBetId;
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if(isset($_POST['Expressbetsdetails']))
        {

            $model->attributes=$_POST['Expressbetsdetails'];

            if($model->save()) {

                $this->redirect(array('index','expressBetId'=>$model->expressBetId));
            }
        }

        $this->render('create',array(
            'model'=>$model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {


        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if(isset($_POST['Expressbetsdetails']))
        {
            $model->attributes=$_POST['Expressbetsdetails'];


            if($model->save()) {
                $this->redirect(array('index','expressBetId'=>$model->expressBetId));
            }

        }

        $this->render('update',array(
            'model'=>$model,
        ));
    }

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($expressBetId)
	{

        $this->loadModel($expressBetId)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(array('expressbets/index'));
	}

	/**
	 * Manages all models.
	 */
	public function actionIndex($expressBetId)
	{
		$model=new Expressbetsdetails('search');
		$model->unsetAttributes();  // clear any default values
		$model->expressBetId = $expressBetId;  // clear any default values
		if(isset($_GET['Expressbetsdetails']))
			$model->attributes=$_GET['Expressbetsdetails'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Expressbetsdetails the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Expressbetsdetails::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Expressbetsdetails $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='expressbetsdetails-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
