<?php

/**
 * Class IndexController
 */
class IndexController extends AdminBaseController
{

    /**
     *
     */


    public function init()
    {
        /* Run init */
        parent::init();
    }

    /**
     *
     */
    public function actionIndex()
    {
//        $this->redirect(array("/admin/index/index"));

// получаем список пользователей
        $user = new User();
        $users = $user->searchActiveUsers()->getData();
        $dataForRender['users'] = $users;

        /** @var BonusCategory $bonusCategories */
        $bonusCategories = new BonusCategory();
        $bonusCategories = $bonusCategories->search()->getData();
        /** @var BonusCategory $bonusCategory */
        foreach ($bonusCategories as $key => $bonusCategory) {
            if ($bonusCategory->category_name === 'bonus_market') {
                unset($bonusCategories[$key]);
            }
        }
        $dataForRender['bonusCategories'] = $bonusCategories;

        if (!empty($_REQUEST['sendMessageForm'])) {
            $personalMssage= new PersonalMessage();
            $personalMssage->sender_id = Yii::app()->getUser()->id;
            $personalMssage->subject = $_REQUEST['subject'];
            $personalMssage->text = $_REQUEST['text'];
            $recipient_id = $_REQUEST['recipient_id'];
            $sendResult = $personalMssage->send($recipient_id);
            $dataForRender['sendMessageResult'] = $sendResult;
        }
        if (!empty($_REQUEST['addBonusForm'])) {
            /** @var Bonus $promoCode */
            $promoCode = new Bonus();
            $promoCode->title = $_REQUEST['title'];
            $promoCode->text = $_REQUEST['text'];
            $endDate = ($_REQUEST['endDate']) ? $_REQUEST['endDate'] : 0;

            $addResult = $promoCode->addBonus(
                $_REQUEST['userId'],
                intval($_REQUEST['value']),
                $_REQUEST['bonusCategoryId'],
                $endDate);
            $dataForRender['addBonusResult'] = $addResult;
        }
        if (!empty($_REQUEST['addGroupBonusForm'])) {
            /** @var GroupBonus $promoCode */
            $promoCode = new GroupBonus();
            $promoCode->title = $_REQUEST['title'];
            $promoCode->text = $_REQUEST['text'];
            $startDate = $_REQUEST['startDate'];
            $endDate = $_REQUEST['endDate'];
            $inPercents = 0;
            $percentTo = null;
            if ($_REQUEST['percent_to'] != 0) {
                $inPercents = true;
                switch (intval($_REQUEST['percent_to'])) {
                    case 1 :
                        $percentTo = GroupBonus::PERCENT_TO_DEPOSIT;
                        break;
                    case 2 :
                        $percentTo = GroupBonus::PERCENT_T0_BET;
                        break;
                    case 3 :
                        $percentTo = GroupBonus::PERCENT_TO_INCOME_DEPOSIT;
                        break;
                    default:
                        $percentTo = 0;
                }
            }
            $addResult = $promoCode->addGroupBonus(
                intval($_REQUEST['value']),
                $inPercents,
                $percentTo,
                $_REQUEST['bonusCategoryId'],
                $startDate,
                $endDate);
            $dataForRender['addGroupBonusResult'] = $addResult;
        }
        if (!empty($_REQUEST['addCodeForm'])) {
            /** @var PromoCode $promoCode */
            $promoCode = new PromoCode();
            $promoCodeResult = $promoCode->addNewPromoCode($_REQUEST['type'],$_REQUEST['code'], $_REQUEST['value']);
            $dataForRender['promoCodeResult'] = $promoCodeResult;
        }
        if (!empty($_REQUEST['changeJackpotForm'])) {
            /** @var Jackpot $jackpot */
            $jackpot = new Jackpot();
            $jackpot= $jackpot->getCurrentJackpot();
            $changeJackpotResult = $jackpot->setJackpot($_REQUEST['koef'], $_REQUEST['percent']);
            $dataForRender['changeJackpotResult'] = $changeJackpotResult;
        }
        if (!empty($_REQUEST['resetJackpotForm'])) {
            /** @var Jackpot $jackpot */
            $jackpot = new Jackpot();
            $jackpot= $jackpot->getCurrentJackpot();
            $resetJackpotResult = $jackpot->resetJackpot();
            $dataForRender['resetJackpotResult'] = $resetJackpotResult;
        }
        if (!empty($_REQUEST['issueJackpotForm'])) {
            /** @var Jackpot $jackpot */
            $jackpot = new Jackpot();
            $jackpot= $jackpot->getCurrentJackpot();
            $issueJackpotResult = $jackpot->issueMoney($_REQUEST['userId']);
            $dataForRender['issueJackpotResult'] = $issueJackpotResult;
        }

        /** @var Jackpot $jackpot */
        $jackpot = new Jackpot();
        $jackpot= $jackpot->getCurrentJackpot();
        $contendersList = $jackpot->getContendersList();
        $dataForRender['contendersList'] = $contendersList;
        $dataForRender['currentJackpot'] = $jackpot;

        $this->render('index', $dataForRender);
    }

    public function actionTest()
    {
//        $this->redirect(array("/admin/index/index"));
        $this->render('index');
    }

}