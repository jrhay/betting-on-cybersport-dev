<?php

/**
 * Class ContentController
 */
class ContentController extends AdminBaseController {

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $languages = Languages::model()->findAll();

        $model = new Content;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Content'])) {
            $model->attributes = $_POST['Content'];

            if ($model->save())
                $this->redirect(array('index'));
        }

        $this->render('create', array(
            'model' => $model,
            'lang' => $languages,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id, $lang_id) {
        $languages = Languages::model()->findAll();

        $model = $this->loadModel($id, isset($_POST['Content']['lang_id']) ? $_POST['Content']['lang_id'] : $lang_id);

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Content'])) {

            $model->attributes = $_POST['Content'];
            $model->id = $id;

            if ($model->save())
                $this->redirect(array('index'));
        }

        $this->render('update', array(
            'model' => $model,
            'lang' => $languages
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {

        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $model = new Content('search');

        $cache = Yii::app()->cache->get('content');
        $cacheData = array();
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Content'])) {
            $model->attributes = $_GET['Content'];
        } else {
            if ($cache) {
                foreach ($model->attributes as $key => $attr) {
                    if (isset($cache[$key])) {
                        $model->$key = $cache[$key];
                    }
                }
            }
        }

        if (Yii::app()->request->isAjaxRequest) {
            foreach ($model->attributes as $key => $attr) {
                $cacheData[$key] = $attr;
            }
            Yii::app()->cache->set('content', $cacheData, 3600);
        }

        $this->render('index', array(
            'model' => $model,
        ));
    }

    /**
     * Manages all models.
     */

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Content the loaded model
     * @throws CHttpException
     */
    public function loadModel($id, $lang_id = null) {
        if (!$lang_id) {
            $lang_id = Yii::app()->params['default_lang_id'];
        }
        $model = Content::model()->findByPk(array('id' => $id, 'lang_id' => $lang_id));
        if ($model === null) {
            $model = new Content;
            //throw new CHttpException(404,Yii::t('admin_home','The requested page does not exist.'));
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Content $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'content-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
