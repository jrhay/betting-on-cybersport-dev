<?php

class StatisticsController extends AdminBaseController
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
{
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('index','view'),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('game','games','bets','cancel','site','users','moderators','deposit','incomeDeposit','user','userautocomplete','userGames','userBonuses','userGameBets'),
                'users'=>array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('admin','delete'),
                'users'=>array('admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }
    public function actionUserAutoComplete()
    {
        if (isset($_GET['q'])) {

            $criteria = new CDbCriteria;
            $criteria->condition = 'userName LIKE :userName';
            $criteria->params = array(':userName' => $_GET['q'] . '%');

            if (isset($_GET['limit']) && is_numeric($_GET['limit'])) {
                $criteria->limit = $_GET['limit'];
            }

            $users = User::model()->findAll($criteria);

            $resStr = '';
            foreach ($users as $user) {
                $resStr .= $user->userName . "\n";
            }
            echo $resStr;
        }
    }

    public function actionTournamentResults(){
//        $model =
        return $this->render('tournamentResults', array(

        ));
    }

    public function actionIncomeDeposit()
    {
        $timeRangeFormModel = new TimeRangeForm();
        $criteria = new CDbCriteria();
        $timeRangeForm = array('', '');
        if ($timeRangeForm = Yii::app()->request->getParam('TimeRangeForm', false)) {
            $start_time = DateTime::createFromFormat('Y-m-d H:i:s', $timeRangeForm['start_time'])->getTimestamp();
            $end_time = DateTime::createFromFormat('Y-m-d H:i:s', $timeRangeForm['end_time'])->getTimestamp();
            $criteria->addCondition('time > :start_time AND time < :end_time');
            $criteria->params['start_time'] = $start_time;
            $criteria->params[':end_time'] = $end_time;
        }
        $criteria->addCondition('status = :payed');
        $criteria->params[':payed'] = IncomeDepositHistory::STATUS_PAID;
        /** @var IncomeDepositHistory[] $incomeDepositOperations */
        $incomeDepositOperations = IncomeDepositHistory::model()->findAll($criteria);
        $sumDeposit = 0;
        $averageDeposit = 0;
        foreach ($incomeDepositOperations as $income) {
            $sumDeposit += $income->summ;
        }
        if ($sumDeposit) {
            $averageDeposit = intval($sumDeposit / count($incomeDepositOperations));
        } else {
            $averageDeposit = 0;
        }
//        $start_time = 0;
//        if ($timeParam = Yii::app()->request->getParam('start_time', false)) {
//            $start_time = DateTime::createFromFormat('Y-m-d H:i:s', $timeParam);
//            $start_time = $start_time->getTimestamp();
//        }
//        $end_time = 0;
//        if ($timeParam = Yii::app()->request->getParam('end_time', false)) {
//            $end_time = DateTime::createFromFormat('Y-m-d H:i:s', $timeParam);
//            $end_time = $end_time->getTimestamp();
//        }
        return $this->render('incomeDeposit', array(
            'timeRangeForm' => $timeRangeForm,
            'incomeDepositOperations' => $incomeDepositOperations,
            'timeRangeFormModel' => $timeRangeFormModel,
            'sumDeposit' => $sumDeposit,
            'averageDeposit' => $averageDeposit,
        ));
    }

    public function actionDeposit()
    {
        $criteria = new CDbCriteria();
        $userName = '';
        $realMoney = false;
        $cyberMoney = false;
        if ($userName = Yii::app()->request->getParam('userName', false)) {
            $criteria->addCondition('userName = :userName');
            $criteria->params[':userName'] = $userName;
        }
        $users = User::model()->findAll($criteria);

        if ($money = Yii::app()->request->getParam('MoneyForm', false)) {
            $realMoney = isset($money['realMoney']) ? $money['realMoney'] : false;
            $cyberMoney = isset($money['cyberMoney']) ? $money['cyberMoney'] : false;

            foreach ($users as $key => $user) {
                if ($realMoney && !$user->usercash->realMoney) {
                    unset($users[$key]);
                }
                if ($cyberMoney && !$user->usercash->cyberMoney) {
                    unset($users[$key]);
                }
            }
        }

        $moneyFormModel = new MoneyForm();
        return $this->render('deposit', array(
            'users' => $users,
            'moneyFormModel' => $moneyFormModel,
            'userName' => $userName,
            'realMoney' => $realMoney,
            'cyberMoney' => $cyberMoney,
        ));
    }

    public function actionCancel($id){
        /** @var Games $game */
        $game = Games::model()->findByPk($id);
        $result = new Result();
        $result->cancelGame($id);
        $this->actionGame($id);
    }
    public function actionGame($id)
    {
        /** @var Games $game */
        $game = Games::model()->findByPk($id);
        //result for this game
        /** @var Result $result */
        $result = Result::model()->findByAttributes(array('game_id' => $id));

        /** @var GameResultForm $model */
        $model=new GameResultForm;
        $model->videoFrameCode  = $game->betvideo;
        if($result) {
            $model->result = $result->result;
            $model->score = $result->score;
        }
        if(isset($_POST['GameResultForm']))
        {
            $model->attributes=$_POST['GameResultForm'];
            $model->videoFrameCode = $_POST['GameResultForm']['videoFrameCode'];
            if($model->validate())
            {
                if(!$result) {
                    $result = new Result;
                    $result->game_id = $game->id;
                    $result-> add_result_time = time();

                }
                $result->result = $model->result;
                $result->score = $model->score;
                $result->handleBets();
                $result->save();

//                if(!empty($model->gameEndTime)) {
//                    $end_time = DateTime::createFromFormat('Y-m-d H:i:s', $model->gameEndTime)->getTimestamp();
//                    $game->game_end = $end_time;
//                    $game->save();
//                }
                if(!empty($model->videoFrameCode)) {
                    $game->betvideo = $model->videoFrameCode;
                    $game->save();
                }
            }
            $this->actionGames();
        }
        //team bet sum ( for all bets)
        /** @var Userbets[] $userbets */
        $userbets = $game->userbets;
        $team1Sum = 0;
        foreach ($userbets as $bet) {
            if (($bet->result == 1) && ($bet->status !== Userbets::CANCELED) && ($bet->moneyType == Usercash::REAL_MONEY)) {
                $team1Sum += $bet->summ;
            }
        }
        $team2Sum = 0;
        foreach ($userbets as $bet) {
            if (($bet->result == 2) && ($bet->status !== Userbets::CANCELED) && ($bet->moneyType == Usercash::REAL_MONEY)) {
                $team2Sum += $bet->summ;
            }
        }
        $drawSum = 0;
        foreach ($userbets as $bet) {
            if (($bet->result == 0) && ($bet->status !== Userbets::CANCELED) && ($bet->moneyType == Usercash::REAL_MONEY)) {
                $drawSum += $bet->summ;
            }
        }

        $this->render('games/game', array(
            'game' => $game,
            'team1Sum' => $team1Sum,
            'team2Sum' => $team2Sum,
            'drawSum' => $drawSum,
            'result' => $result,
            'model' => $model,
        ));
    }

    public function actionGames()
    {
        $games = Games::model()->findAll();
        $games = array_reverse($games);
        $this->render('games', array(
            'games' => $games,
        ));
    }

    public function actionBets($game_id)
    {
        /** @var Games $game */
        $game = Games::model()->findByPk($game_id);

        /** @var CDbCriteria $criteria */
        $criteria = new CDbCriteria();

        $criteria->addColumnCondition(array('gameId' => $game->id));

        if ($userName = Yii::app()->request->getParam('userName', false)) {
            $user = User::model()->findByAttributes(array('userName' => $userName));
            $criteria->addCondition('userId = :userId');
            $criteria->params[':userId'] = $user->id;
        }


        /** @var Userbets[] $bets */
        $bets = Userbets::model()->findAll($criteria);
        foreach ($bets as $key => $bet) {
            if ($start_time = Yii::app()->request->getParam('start_time', false)) {
                $start_time = DateTime::createFromFormat('Y-m-d H:i:s', $start_time);
                $start_time = $start_time->getTimestamp();
                if ($bet->game->create_at < $start_time) {
                    unset ($bets[$key]);
                    continue;
                }
            }
            if ($end_time = Yii::app()->request->getParam('end_time', false)) {
                $end_time = DateTime::createFromFormat('Y-m-d H:i:s', $end_time);
                $end_time = $end_time->getTimestamp();
                if ($bet->game->create_at > $end_time) {
                    unset ($bets[$key]);
                    continue;
                }
            }
            if ($category_id = Yii::app()->request->getParam('Games', false)) {
                $category_id = $category_id['category_id'];
                if ($bet->game->category_id != $category_id) {
                    unset ($bets[$key]);
                    continue;
                }
            }
        }
        /** @var GameCategories $gameCategories */
        $gameCategories = GameCategories::model()->findAll();

        $modelAttr = array('category_id' => false);
        $model = new Games();
        return $this->render('bets', array(
            'game' => $game,
            'bets' => $bets,
            'gameCategories' => $gameCategories,
            'model' => $model,
        ));
    }

    public function actionModerators()
    {
        $criteria = new CDbCriteria();
        $in = array(1, 3);
        $criteria->addInCondition('usertypeId', $in);
        $moderators = User::model()->findAll($criteria);
        $this->render('moderators', array(
            'moderators' => $moderators,
        ));
    }

    public function actionSite()
    {
        $start_time = 0;
        if ($timeParam = Yii::app()->request->getParam('start_time', false)) {
            $start_time = DateTime::createFromFormat('Y-m-d H:i:s', $timeParam);
            $start_time = $start_time->getTimestamp();
        }
        $end_time = 0;
        if ($timeParam = Yii::app()->request->getParam('end_time', false)) {
            $end_time = DateTime::createFromFormat('Y-m-d H:i:s', $timeParam);
            $end_time = $end_time->getTimestamp();
        }
        //getting average margin
        /** @var Games[] $games */
        $criteria = new CDbCriteria();
        if ($start_time) {
            $criteria->addCondition('create_at > :start_time');
            $criteria->params[':start_time'] = $start_time;
        }
        if ($end_time) {
            $criteria->addCondition('create_at < :end_time');
            $criteria->params[':end_time'] = $end_time;
        }
        $games = Games::model()->findAll($criteria);
        $countGames = count($games);
        $averageMargin = 0;
        $summMargin = 0;
        foreach ($games as $game) {
            $summMargin += $game->betPercents;
        }
        if ($countGames) {
            $averageMargin = $summMargin / count($games);
        } else {
            $averageMargin = 0;
        }

        //Todo: implement getting balance
        $balance = 0;

        //getting active games and count of active games
        /** @var Games $activeGames */
        $now = time();
        /** @var CDbCriteria $criteria */
        $criteria = new CDbCriteria;
        $criteria->addCondition('game_end > :now');
        $criteria->params[':now'] = $now;
        if ($start_time) {
            $criteria->addCondition('create_at > :start_time');
            $criteria->params[':start_time'] = $start_time;
        }
        if ($end_time) {
            $criteria->addCondition('create_at < :end_time');
            $criteria->params[':end_time'] = $end_time;
        }

        $activeGames = Games::model()->findAll($criteria);
        $countActiveGames = count($activeGames);

        //Todo: emplement profit
        $profit = 0;
        $balance = 0;

        //count users
        $criteria = new CDbCriteria();
        if ($start_time) {
            $criteria->addCondition('createTime > :start_time');
            $criteria->params[':start_time'] = $start_time;
        }
        if ($end_time) {
            $criteria->addCondition('createTime < :end_time');
            $criteria->params[':end_time'] = $end_time;
        }
        $users = User::model()->findAll($criteria);
        $countUsers = count($users);

        //count active users
        $criteria = new CDbCriteria();
        $criteria->addCondition('status = :activeUserStatus');
        $criteria->params[':activeUserStatus'] = User::ACTIVE_USER_STATUS;
        if ($start_time) {
            $criteria->addCondition('createTime > :start_time');
            $criteria->params[':start_time'] = $start_time;
        }
        if ($end_time) {
            $criteria->addCondition('createTime < :end_time');
            $criteria->params[':end_time'] = $end_time;
        }
        $users = User::model()->findAll($criteria);
        $countActiveUsers = count($users);

//        //count bets
//        $betsCount = Gamesbetshistory::model()->count();

        //count Events
        $eventsCount = Events::model()->count();

        //getting userbetsCount

//        $userBetsCount = Userbets::model()->count();
        $userBets = array();
        foreach ($games as $game) {
            $userBets = array_merge($userBets, $game->userbets);
        }
        $userBetsCount = count($userBets);

        //count express bets
//        $expressBetsCount = Expressbets::model()->count();
        //todo: refactor for more fast
        /** @var Expressbets[] $expressBets */
        $expressBets = Expressbets::model()->findAll();
        foreach ($expressBets as $key => $eb) {
            $in_time_range = false;
            foreach ($eb->expressbetsdetails as $dt) {
                if ($this->in_array_field($dt->game->id, 'id', $games)) {
                    $in_time_range = true;
                    break;
                }
            }
            if (!$in_time_range) {
                unset($expressBets[$key]);
            }
        }
        $expressBetsCount = count($expressBets);

        //tournament bets
//        $tournamentBetsCount = Tournamentsbets::model()->count();
        $criteria = new CDbCriteria();
        if ($start_time) {
            $criteria->addCondition('start_date > :start_time');
            $criteria->params[':start_time'] = $start_time;
        }
        if ($end_time) {
            $criteria->addCondition('start_date < :end_time');
            $criteria->params[':end_time'] = $end_time;
        }
        /** @var Tournaments[] $tournaments */
        $tournaments = Tournaments::model()->findAll($criteria);
        /** @var Tournamentsbets[] $tournamentsBets */
        $tournamentsBets = Tournamentsbets::model()->findAll();

        foreach ($tournamentsBets as $key => $bet) {
            if (!$this->in_array_field($bet->tournamentId, 'id', $tournaments)) {
                unset($tournamentsBets[$key]);
            };
        }
        $tournamentBetsCount = count($tournamentsBets);

        $model = new CFormModel();
        $model->attributes = array('time');

        $this->render('site', array(
            'countGames' => $countGames,
            'averageMargin' => $averageMargin,
            'balance' => $balance,
            'activeGames' => $activeGames,
            'countActiveGames' => $countActiveGames,
            'profit' => $profit,
            'countUsers' => $countUsers,
            'countActiveUsers' => $countActiveUsers,
//            'betsCount' => $betsCount,
            'eventsCount' => $eventsCount,
            'userBetsCount' => $userBetsCount,
            'expressBetsCount' => $expressBetsCount,
            'tournamentBetsCount' => $tournamentBetsCount,
            'model' => $model,
        ));
    }

    private function in_array_field($needle, $needle_field, $haystack, $strict = false)
    {
        if ($strict) {
            foreach ($haystack as $item)
                if (isset($item->$needle_field) && $item->$needle_field === $needle)
                    return true;
        } else {
            foreach ($haystack as $item)
                if (isset($item->$needle_field) && $item->$needle_field == $needle)
                    return true;
        }
        return false;
    }

    public function actionUser($id)
    {
        $user = User::model()->findByPk($id);
        // average bet sum
        /** @var Userbets[] $userbets */
        $userbets = Userbets::model()->findAllByAttributes(array(
            'userId' => $id,
        ));
        // average bet summ
        $averageBetSum = 0;
        $sum = 0;
        //max bet summ
        $maxBetSum = 0;
        $averageBetTime = 0;

        foreach ($userbets as $key => $bet) {
            $sum += $bet->summ;
            if ($bet->summ >= $maxBetSum) {
                $maxBetSum = $bet->summ;
            }
        }
        if ($count = count($userbets)) {
            $averageBetSum = intval($sum / $count);
        } else {
            $averageBetSum = 0;
        }

        //user favorite category
        $favoriteCategory = $this->getUserFavoriteCategory($id);

        return $this->render('users/user', array(
            'averageBetSum' => $averageBetSum,
            'user' => $user,
            'maxBetSum' => $maxBetSum,
            'favoriteCategory' => $favoriteCategory,
        ));
    }

    /** @return GameCategories */
    protected function getUserFavoriteCategory($id)
    {
        /** @var Games[] $games */
        $games = $this->getUserGames($id);
        $gameCategories = array();
        foreach ($games as $game) {
            if (empty($gameCategories[$game->category_id])) {
                $gameCategories[$game->category_id] = 0;
            }
            $gameCategories[$game->category_id]++;
        }
        $favoriteCategoryId = 0;
        $max = 0;
        foreach ($gameCategories as $key => $value) {
            if ($value >= $max) {
                $max = $value;
                $favoriteCategoryId = $key;
            }
        }
        /** @var GameCategories $favoriteCategory */
        $favoriteCategory = GameCategories::model()->findByPk($favoriteCategoryId);
        return $favoriteCategory;
    }

    protected function getUserGames($id)
    {
        /** @var Games[] $games */
        $games = array();

        /** @var Userbets[] $userbets */
        $userbets = Userbets::model()->findAllByAttributes(array('userId' => $id));
        foreach ($userbets as $key=> $bet) {
            if(!array_key_exists($bet->game->id, $games)) {
                $games[] = $bet->game;
            }
        }

        $games = array_reverse($games);
        return $games;
    }

    public function actionUsers()
    {
        $criteria = new CDbCriteria();
        if ($userName = Yii::app()->request->getParam('userName', false)) {
            $criteria->addCondition('userName = :userName');
            $criteria->params[':userName'] = $userName;
        }

        $users = User::model()->findAll($criteria);

        //Todo: use better\faster algorithm
        if ($favoriteCategoryId = Yii::app()->request->getParam('Games', false)) {
            $favoriteCategoryId = $favoriteCategoryId['category_id'];
            foreach ($users as $key => $user) {
                $userFC = $this->getUserFavoriteCategory($user->id);
                if (!$userFC or $userFC->id != $favoriteCategoryId) {
                    unset($users[$key]);
                }
            }
        }
        if ($AverageBetSum = Yii::app()->request->getParam('AverageBetSum', false)) {
            $minAverageSum = $AverageBetSum['min_average_sum'];
            $maxAverageSum = $AverageBetSum['max_average_sum'];;
            foreach ($users as $key => $user) {
                $userAverageBetSum = $this->getUserAverageBetSum($user->id);
                if ($userAverageBetSum < $minAverageSum or $userAverageBetSum > $maxAverageSum) {
                    unset($users[$key]);
                }
            }
        }
        /** @var GameCategories[] $gameCategories */
        $gameCategories = GameCategories::model()->findAll();
        /** @var Games $model */
        $model = new Games();
        $model->category_id = $favoriteCategoryId;
        /** @var AverageBetSum $model2 */
        $model2 = new AverageBetSum();
        return $this->render('users', array(
            'users' => $users,
            'gameCategories' => $gameCategories,
            'model' => $model,
            'model2' => $model2,
            'favoriteCategoryId' => $favoriteCategoryId,
        ));
    }

    protected function getUserAverageBetSum($id)
    {
        // average bet summ
        $averageBetSum = 0;
        $sum = 0;
        //max bet summ
        $maxBetSum = 0;
        $averageBetTime = 0;
        $userbets = $this->getUserBets($id);
        foreach ($userbets as $key => $bet) {
            $sum += $bet->summ;
            if ($bet->summ >= $maxBetSum) {
                $maxBetSum = $bet->summ;
            }
        }
        if ($count = count($userbets)) {
            $averageBetSum = intval($sum / $count);
        } else {
            $averageBetSum = 0;
        }
        return $averageBetSum;
    }

    protected function getUserBets($id)
    {
        $userbets = Userbets::model()->findAllByAttributes(array(
            'userId' => $id,
        ));
        $userbets = array_reverse($userbets);
        return $userbets;
    }

    public function actionUserGames($id)
    {
        /**@var Games[] $games */
        $games = $this->getUserGames($id);
        return $this->render('users/userGames', array(
            'games' => $games,
            'userId' => $id,
        ));
    }

    public function actionUserBonuses($id)
    {
        /** @var Bonus[] $bonuses */
        $bonuses = Bonus::model()->findAllByAttributes(array(
            'user_id' => $id,
            'active' => 1,
        ));
        $bonuses = array_reverse($bonuses);
        return $this->render('users/userBonuses', array(
            'bonuses' => $bonuses));
    }

    public function actionUserGameBets($gameId, $userId)
    {
        /** @var Userbets $userbets */
        $userbets = Userbets::model()->findAllByAttributes(array(
            'userId' => $userId,
            'gameId' => $gameId,
        ));
        $userbets = array_reverse($userbets);
        return $this->render('users/userGamesBets', array(
            'bets' => $userbets,
            'gameId' => $gameId,
        ));
    }

    /** @return Teams */
    protected function getUserFavoriteTeam($id)
    {
        /** @var Games[] $bets */
        //Todo::emplement
    }
    // Uncomment the following methods and override them if needed
    /*
    public function filters()
    {
        // return the filter configuration for this controller, e.g.:
        return array(
            'inlineFilterName',
            array(
                'class'=>'path.to.FilterClass',
                'propertyName'=>'propertyValue',
            ),
        );
    }

    public function actions()
    {
        // return external action classes, e.g.:
        return array(
            'action1'=>'path.to.ActionClass',
            'action2'=>array(
                'class'=>'path.to.AnotherActionClass',
                'propertyName'=>'propertyValue',
            ),
        );
    }
    */
}