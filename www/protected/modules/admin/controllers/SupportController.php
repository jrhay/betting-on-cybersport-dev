<?php

class SupportController extends AdminBaseController
{
    public function actionTicket()
    {
        /** @var Ticket $ticket */
        $ticket = Ticket::model()->findByPk(Yii::app()->request->getParam('id'));
        $ticketStatuses = TicketStatus::model()->findAll();
        $employee = null;
        if ($ticket->employee_id) {
            $employee = User::model()->findByPk($ticket->employee_id);
        }
        /** @var TicketPersonalMessages[] $ticketPersonalMessages */
        $ticketPersonalMessages = TicketPersonalMessages::model()->findAllByAttributes(array('ticket_id' => $ticket->id));
        /** @var PersonalMessage $personalMessage */
        $model = new PersonalMessage();
        return $this->render('ticket', array(
            'ticket' => $ticket,
            'ticketStatuses' => $ticketStatuses,
            'employee' => $employee,
            'ticketPersonalMessages' => $ticketPersonalMessages,
            'model' => $model,
        ));
    }

    public function actionCreateTab()
    {
        $category = Yii::app()->request->getParam('category');
        $tickets = Ticket::model()->findAllByAttributes(array('category_id' => $category));
        return $this->render('tab', array(
            'tickets' => $tickets,
        ));
    }

    public function actionSendMessage($id){

        //Todo: refactor model names
        /**@var PersonalMessage */
        $personalMessage = new PersonalMessage();
        /**@var TicketPersonalMessages $ticketPersonalMessages */
        $ticketPersonalMessages = new TicketPersonalMessages();
        if(!empty($_POST['PersonalMessage'])) {
            $personalMessage->attributes =$_POST['PersonalMessage'];
            $personalMessage->sender_id = Yii::app()->user->id;
            $result = $personalMessage->save();

            $ticketPersonalMessages->personal_message_id = $personalMessage->id;
            $ticketPersonalMessages->ticket_id = $id;
            $ticketPersonalMessages->save();
        }
        return $this->actionTicket($id);
    }
    /**
     * Attaches ticket to user who clicked "attach" button*/
    public function actionAttach()
    {
        /** @var Ticket $ticket */
        $ticketId = Yii::app()->request->getParam('id');
        $ticket = Ticket::model()->findByPk($ticketId);
        if ($ticket) {
            $ticket->status_id = TicketStatus::ATTACHED;
            $ticket->employee_id = Yii::app()->user->id;
            $ticket->save();
        }
        return $this->actionTicket($ticketId);
    }

    public function actionAttachToEmployee()
    {
        $ticketId = Yii::app()->request->getParam('id');
        $ticket = Ticket::model()->findByPk($ticketId);
        if ($ticket) {
            $ticket->status_id = TicketStatus::ATTACHED;
            $ticket->employee_id = Yii::app()->user->id;
            if (Yii::app()->request->getParam('userName', false)) {
                /** @var User $user */
                $user = User::model()->findByAttributes(array(
                    'userName' => Yii::app()->request->getParam('userName'),
                ));
                $ticket->employee_id = $user->id;
            }
            $ticket->save();
        }
        return $this->actionTicket($ticketId);
    }

    public function actionStatistics()
    {
        $attributes = array(
            'status_id' => TicketStatus::CLOSED,
        );
        $employeeId = false;
        $userName = '';
        if ($userName = Yii::app()->request->getParam('userName', false)) {
            $user = User::model()->findByAttributes(array(
                'userName' => $userName
            ));
            $employeeId = $user->id;
            $attributes['employee_id'] = $user->id;
        }
        /** @var Ticket[] $tickets */
        $tickets = Ticket::model()->findAllByAttributes($attributes);
        $tickets = array_reverse($tickets);
        /**@var User[] $users */
        $users = User::model()->findAll();
        return $this->render('statistics', array(
            'tickets' => $tickets,
            'users' => $users,
            'employeeId' => $employeeId,
            'userName' => $userName,
        ));
    }

    public function actionClose()
    {
        $ticketId = Yii::app()->request->getParam('id');
        /** @var Ticket $ticket */
        $ticket = Ticket::model()->findByPk($ticketId);
        if ($ticket) {
            $ticket->status_id = TicketStatus::CLOSED;
            $ticket->employee_id = Yii::app()->user->id;
            if ($result = $ticket->save()) {
                $personalMessage = new PersonalMessage();
                $personalMessage->sender_id = Yii::app()->getUser()->id;
                $personalMessage->subject = Yii::t('site', 'Тикет №' . $ticket->id . '(' . $ticket->title . ' ) ');
                $personalMessage->text = Yii::t('site', 'Ticket is closed.');
                $recipient_id = $ticket->user_id;
                $sendResult = $personalMessage->send($recipient_id);
            }

        }
        $this->actionIndex();
    }

    public function actionDelete()
    {
        $ticketId = Yii::app()->request->getParam('id');
        $ticket = Ticket::model()->deleteByPk($ticketId);
        $this->actionIndex();
    }


    public function actionIndex()
    {
        $tickets = array();
        /** @var TicketCategories $ticketCategories */
        $ticketCategories = TicketCategories::model()->findAll();

        foreach ($ticketCategories as $ct) {
            $tickets[$ct->id] = Ticket::model()->findAllByAttributes(array(
                'category_id' => $ct->id,
            ));
            $tickets[$ct->id] = array_reverse($tickets[$ct->id]);

        }

        /** @var TicketStatus[] $ticketStatuses */
        $ticketStatuses = TicketStatus::model()->findAll();
        /** @var User[] $users */
        $users = User::model()->findAll();

        $this->render('index', array(
            'ticketCategories' => $ticketCategories,
            'tickets' => $tickets,
            'ticketStatuses' => $ticketStatuses,
            'users' => $users,
        ));
    }

    // Uncomment the following methods and override them if needed
    /*
    public function filters()
    {
        // return the filter configuration for this controller, e.g.:
        return array(
            'inlineFilterName',
            array(
                'class'=>'path.to.FilterClass',
                'propertyName'=>'propertyValue',
            ),
        );
    }

    public function actions()
    {
        // return external action classes, e.g.:
        return array(
            'action1'=>'path.to.ActionClass',
            'action2'=>array(
                'class'=>'path.to.AnotherActionClass',
                'propertyName'=>'propertyValue',
            ),
        );
    }
    */
}