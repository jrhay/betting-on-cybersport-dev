<?php

class BonusesController extends AdminBaseController
{
    public function actionUserAutoComplete()
    {
        if (isset($_GET['q'])) {

            $criteria = new CDbCriteria;
            $criteria->condition = 'userName LIKE :userName';
            $criteria->params = array(':userName' => $_GET['q'] . '%');

            if (isset($_GET['limit']) && is_numeric($_GET['limit'])) {
                $criteria->limit = $_GET['limit'];
            }

            $users = User::model()->findAll($criteria);

            $resStr = '';
            foreach ($users as $user) {
                $resStr .= $user->userName . "\n";
            }
            echo $resStr;
        }
    }

    public function actionCategoriesAutoComplete()
    {
        if (isset($_GET['q'])) {

            $criteria = new CDbCriteria;
            $criteria->condition = 'category_name LIKE :category_name';
            $criteria->params = array(':category_name' => $_GET['q'] . '%');

            if (isset($_GET['limit']) && is_numeric($_GET['limit'])) {
                $criteria->limit = $_GET['limit'];
            }

            $categories = BonusCategory::model()->findAll($criteria);

            $resStr = '';
            foreach ($categories as $category) {
                $resStr .= $category->category_name . "\n";
            }
            echo $resStr;
        }
    }

    public function actionCreatePromoCode()
    {

        $model = new PromoCode();
        $promoCodeCategories = new PromoCodeCategories();
        $promoCodeCategories = $promoCodeCategories->findAll();
        $createPromoCodeResult = array();

        if (isset($_REQUEST['PromoCode']) && $model->attributes = $_REQUEST['PromoCode']) {
            if ($model->save()) {
                $createPromoCodeResult['result'] = true;
            } else {
                $createPromoCodeResult['result'] = false;
            }
        }
        return $this->render('createPromoCode', array(
            'model' => $model,
            'promoCodeCategories' => $promoCodeCategories,
            'createPromoCodeResult' => $createPromoCodeResult,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionAddPersonalBonus()
    {
        $model = new Bonus();
        $addBonusResult = array();
        $bonusCategories = BonusCategory::model()->findAll();
        if (!empty($_REQUEST['Bonus']) && $model->attributes = $_REQUEST['Bonus']) {
            /** @var User $bonusRecipient */
            $bonusRecipient = User::model()->findByAttributes(array('userName' => $_REQUEST['userName']));
            if (!$bonusRecipient) {
                $addBonusResult['result'] = false;
                return $this->render('addPersonalBonus', array(
                    'model' => $model,
                    'addBonusResult' => $addBonusResult,
                    'bonusCategories' => $bonusCategories
                ));
            }
            $userId = $bonusRecipient->id;

            /** @var BonusCategory $bonusCategory */
//            $bonusCategory = BonusCategory::model()->findByAttributes(array('category_name' => $_REQUEST['category_name']));
//            if(!$bonusCategory) {
//                $addBonusResult['result'] = false;
//                return $this->render('addPersonalBonus', array(
//                    'model' => $model,
//                    'addBonusResult' => $addBonusResult,
//                    'bonusCategories' => $bonusCategories
//                ));
//            }
//            $bonusCategoryId = $bonusCategory->id;
            $bonusCategoryId = $_REQUEST['Bonus']['bonus_category_id'];
            $endDate = $_REQUEST['Bonus']['end_date'];
            $addResult = $model->addBonus(
                $userId,
                $model->value,
                $bonusCategoryId,
                $endDate);
            $addBonusResult['result'] = $addResult;
        }

        return $this->render('addPersonalBonus', array(
            'model' => $model,
            'addBonusResult' => $addBonusResult,
            'bonusCategories' => $bonusCategories
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionAddGroupBonus()
    {
        /** @var GroupBonus $model */
        $model = new GroupBonus;

        /** @var GroupBonusCategories $groupBonusCategories */
        $groupBonusCategories = new GroupBonusCategories();
        $groupBonusCategories = $groupBonusCategories->findAll();

        /** @var BonusCategory $bonusCategories
         */
        $bonusCategories = new BonusCategory();
        $bonusCategories = $bonusCategories->findAll();
        // uncomment the following code to enable ajax-based validation

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'group-bonus-addGroupBonus-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        $addGroupBonusResult = array();
        if (isset($_POST['GroupBonus'])) {
            $model->attributes = $_POST['GroupBonus'];
            $model->in_percents = true;
            if($model->percent_to == 4) {
                $model->percent_to = 0;
                $model->in_percents = false;
            }
            $model->start_date = $_REQUEST['GroupBonus']['start_date'];
            $model->end_date = $_REQUEST['GroupBonus']['end_date'];
            if ($model->validate()) {
                if ($model->save()) {
                    $addGroupBonusResult['result'] = true;
                } else {
                    $addGroupBonusResult['result'] = false;
                }
            }
        }
        $this->render('addGroupBonus', array(
            'model' => $model,
            'addGroupBonusResult' => $addGroupBonusResult,
            'groupBonusCategories' => $groupBonusCategories,
            'bonusCategories' => $bonusCategories,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {

    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Games the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = PersonalMessage::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Games $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'bonuses-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
