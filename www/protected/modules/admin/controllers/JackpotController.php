<?php

class JackpotController extends AdminBaseController
{
    public function actionManage()
    {
        $model = Jackpot::model()->getCurrentJackpot();
        // uncomment the following code to enable ajax-based validation
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'jackpot-manage-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        $manageJackpotResult = array();
        if (isset($_POST['Jackpot'])) {
            $model->attributes = $_POST['Jackpot'];
            if ($model->validate()) {
                $manageJackpotResult['result'] = $model->save();
            }
        }
        return $this->render('manage', array(
            'model' => $model,
            'manageJackpotResult' => $manageJackpotResult,
        ));
    }

    public function actionPay()
    {
        $payJackpotResult = array();
        if (!empty($_REQUEST['payJackpotForm'])) {
            /** @var Jackpot $jackpot */
            $jackpot = new Jackpot();
            $jackpot = $jackpot->getCurrentJackpot();
            $user = User::model()->findByAttributes(array('userName' => $_REQUEST['userName']));
            $payJackpotResult['result'] = $jackpot->issueMoney($user->id);
        }
        return $this->render('pay', array(
            'payJackpotResult' => $payJackpotResult
        ));
    }

    public function actionReset()
    {
        $resetJackpotResult = array();
        if (isset($_POST['resetJackpotForm'])) {
            $jackpot = new Jackpot();
            $jackpot = $jackpot->getCurrentJackpot();
            $resetJackpotResult['result'] = $jackpot->resetJackpot();
        }
        return $this->render('reset', array(
            'resetJackpotResult' => $resetJackpotResult
        ));
    }

    public function actionUserAutoComplete()
    {
        if (isset($_GET['q'])) {

            $criteria = new CDbCriteria;
            $criteria->condition = 'userName LIKE :userName';
            $criteria->params = array(':userName' => $_GET['q'] . '%');

            if (isset($_GET['limit']) && is_numeric($_GET['limit'])) {
                $criteria->limit = $_GET['limit'];
            }

            $users = User::model()->findAll($criteria);

            $resStr = '';
            foreach ($users as $user) {
                $resStr .= $user->userName . "\n";
            }
            echo $resStr;
        }
    }


    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Games the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = PersonalMessage::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Games $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'bonuses-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
