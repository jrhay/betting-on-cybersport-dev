<?php

class AdminCButtonColumn extends CButtonColumn {

    public $filter;

    protected function renderFilterCellContent() {
        if (is_string($this->filter))
            echo $this->filter;
        else
            echo $this->grid->blankDisplay;
    }

}