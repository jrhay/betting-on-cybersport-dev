<?php

/**
 * Admin base controller class
 */

class AdminBaseController extends CybController {

    public $layout = '//layouts/admin';

    public function init() {
        /* Run init */
        $this->pageTitle = Yii::t('admin','Administration area');
        parent::init();
    }
}