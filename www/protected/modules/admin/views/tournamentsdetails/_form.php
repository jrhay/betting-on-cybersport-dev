<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */

$baseUrl = Yii::app()->theme->baseUrl;
//Yii::app()->clientScript->registerCssFile($baseUrl.'/assets/css/jquery/jquery.dataTables.css');
Yii::app()->clientScript->registerScriptFile($baseUrl.'/js/jquery.form.js');
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'tournamentsdetails-form',
	'enableAjaxValidation'=>true,
    'clientOptions'        => array(
        'validateOnSubmit' => true,
        'validateOnChange' => true,
    ),
    'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
    ),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'teamId'); ?>
        <?php echo $form->dropDownList($model, 'teamId', CHtml::listData(Teams::model()->findAll(), 'id', 'name')); ?>
        <?php echo $form->error($model,'teamId'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'teamKoef'); ?>
        <?php echo $form->textField($model,'teamKoef'); ?>
        <?php echo $form->error($model,'teamKoef'); ?>
    </div>

<!--    <div class="row">-->
<!--        --><?php //echo $form->labelEx($model,'baseKoef'); ?>
<!--        --><?php //echo $form->textField($model,'baseKoef'); ?>
<!--        --><?php //echo $form->error($model,'baseKoef'); ?>
<!--    </div>-->
    <div class="row buttons">

		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
        <?php echo CHtml::link(Yii::t('admin', 'Cancel'), array('/admin/tournamentsdetails/index','tournamentId'=>$model->tournamentId), array('class' => 'btn btn-common',)); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->