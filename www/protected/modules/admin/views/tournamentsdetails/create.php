<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
    Yii::t('admin','Express Bets Item')=>array('index'),
    Yii::t('admin','Create'),
);
?>

<h1><?php echo Yii::t('admin','Create Express Bets Item') ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>