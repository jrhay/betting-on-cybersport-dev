<?php
/* @var $this UserController */
/* @var $model User */
$this->breadcrumbs=array(
    Yii::t('admin', 'Manage Tournament Details')=>array('index'),
    Yii::t('admin', 'Manage'),
);
?>

<h1><?php echo Yii::t('admin','Manage Tournament Details') ?></h1>
<ul class="ui-buttons-panel">
    <li>
        <?php echo CHtml::link(Yii::t('admin', '+ Add a New Team'), array('create','tournamentId'=>$model->tournamentId), array('class' => "btn btn-primary")); ?>
    </li>
    <li>
        <?php echo CHtml::link(Yii::t('admin', 'Tournaments List'), array('/admin/tournaments/index'), array('class' => "btn btn-primary")); ?>
    </li>
</ul>
<?php $this->widget('application.widgets.grid.GGridView', array(
    'id'=>'user-grid',
    'itemsCssClass'=> 'table',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'pager' => array(
        'header' => '',
    ),
    'template' => '{items}{pager}',
    'columns'=>array(
        'id',
        'teamId'=>array('name'=>'teamId','value'=>'$data->team->name'),
        'teamKoef',
//        'baseKoef',
            array(
                'header' => Yii::t('admin','Actions'),
                'class' => 'CButtonColumn',
                'headerHtmlOptions' => array('width' => '30%'),
                'buttons' => array(
                    'delete' => array(
                        'visible'=>'true',
                        'imageUrl' => null,
                        'options' => array(
                            'class' => 'btn-delete btn',
                        ),
                        'click' => "function(){
                            if (confirm('" . Yii::t('admin', 'Do you really want to delete this team from the tournament?') . "')){
                                return true;
                            } else {
                                return false;
                            }
                        }",
                        'url' => 'array("/admin/tournamentsdetails/delete","id"=>$data->id)'
                    ),
                    'view' => array(
                        'visible' => 'false',

                    ),
                'update' => array(

                    'imageUrl' => null,
                    'options' => array(
                        'class' => 'btn btn-primary'
                    ),
                    'url' => 'array("/admin/tournamentsdetails/update","id"=>$data->id)'))
        ),

    ),
    'theadCssClass' => 'ContentAdmin'
)); ?>
