<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm */
$baseUrl = Yii::app()->theme->baseUrl;
Yii::app()->clientScript->registerScriptFile($baseUrl.'/js/jquery.form.js');
?>

<h1><?php echo Yii::t('admin', 'Login') ?></h1>
<?php $form = $this->beginWidget('CActiveForm', array(
    'id'                     => 'login-form',
    'htmlOptions'            => array('class' => 'login'),
    'enableClientValidation' => true,

    'clientOptions'          => array(
        'validateOnSubmit' => true,
        'validateOnChange' => true,
    ),
)); ?>
<fieldset>
    <div class="row">
        <label><?php echo $form->labelEx($model, 'username'); ?></label>


        <?php echo $form->textField($model, 'username', array('class' => 'w2')); ?>
        <?php echo $form->error($model, 'username'); ?>
    </div>
    <div class="row"><label><?php echo $form->labelEx($model, 'password'); ?></label>
        <?php echo $form->passwordField($model, 'password', array('class' => 'w2')); ?>
        <?php echo $form->error($model, 'password'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'rememberMe'); ?>
        <?php echo $form->checkBox($model, 'rememberMe'); ?>
        <?php echo $form->error($model, 'rememberMe'); ?>

    </div>
</fieldset>

<fieldset>
    <div class="form-field">
        <?php echo CHtml::submitButton(Yii::t('admin', 'Login'), array('class' => 'btn-common-med')); ?>
    </div>
</fieldset>

<?php
echo CHtml::hiddenField('browser_tz_offset', '');
$this->endWidget(); ?>

<script>
    var d = new Date();
    jQuery('#browser_tz_offset').val(d.getTimezoneOffset() / -60);
</script>
