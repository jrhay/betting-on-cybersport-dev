<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */

$baseUrl = Yii::app()->theme->baseUrl;
//Yii::app()->clientScript->registerCssFile($baseUrl.'/assets/css/jquery/jquery.dataTables.css');
Yii::app()->clientScript->registerScriptFile($baseUrl.'/js/jquery.form.js');
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	'enableAjaxValidation'=>true,
    'clientOptions'        => array(
        'validateOnSubmit' => true,
        'validateOnChange' => true,
    ),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
    <div class="row">
        <?php echo $form->labelEx($model,'userName'); ?>
        <?php echo $form->textField($model,'userName',array('size'=>45,'maxlength'=>45)); ?>
        <?php echo $form->error($model,'userName'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'firstName'); ?>
        <?php echo $form->textField($model,'firstName',array('size'=>45,'maxlength'=>45)); ?>
        <?php echo $form->error($model,'firstName'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'lastName'); ?>
        <?php echo $form->textField($model,'lastName',array('size'=>45,'maxlength'=>45)); ?>
        <?php echo $form->error($model,'lastName'); ?>
    </div>


    <?php if ($this->action->id =='update') { ?>
    <div class="row">
            <?php echo CHtml::label(Yii::t('admin', 'Change password?'), 'change_password'); ?>
            <?php echo $form->checkBox($model, 'change_password', false); ?>
    </div>
    <?php } ?>


        <div id="password_container" <?php echo ($this->action->id =='update')?' style="display: none"':'' ?>>
            <div class="row">
                <?php echo $form->labelEx($model, 'password'); ?>
                <?php echo $form->passwordField($model, 'password', array('size' => 45, 'maxlength' => 45, 'class' => 'w2', 'autocomplete'=>'off')); ?>
                <?php echo $form->error($model, 'password'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model, 'repeat_password'); ?>
                <?php echo $form->passwordField($model, 'repeat_password', array('size' => 45, 'maxlength' => 45, 'class' => 'w2', 'autocomplete'=>'off')); ?>
                <?php echo $form->error($model, 'repeat_password'); ?>
            </div>
        </div>
        <script>
            jQuery('#User_change_password').click(function (e) {
                jQuery(this).is(':checked') ? jQuery("#password_container").show() : jQuery("#password_container").hide();
            });
        </script>


    <div class="row">
        <?php echo $form->labelEx($model,'usertypeId'); ?>
<!--        --><?php //echo $form->textField($model,'usertypeId',array('size'=>10,'maxlength'=>10)); ?>
        <?php echo $form->dropDownList($model, 'usertypeId', CHtml::listData(Usertype::model()->findAll(), 'id', 'type')); ?>
        <?php echo $form->error($model,'usertypeId'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'status'); ?>
        <?php echo $form->dropDownList($model, 'status', User::getStatusFilter()); ?>
        <?php echo $form->error($model,'status'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'timezone'); ?>
        <?php echo $form->dropDownList($model, 'timezone', User::getTimeZoneSelect()); ?>
        <?php echo $form->error($model, 'timezone'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'language'); ?>
        <?php echo $form->dropDownList($model, 'language', CHtml::listData(Languages::model()->findAll(), 'id', 'name')); ?>
        <?php echo $form->error($model,'language'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'email'); ?>
        <?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>100)); ?>
        <?php echo $form->error($model,'email'); ?>
    </div>





    <div class="row buttons">

		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
        <?php echo CHtml::link(Yii::t('admin', 'Cancel'), array('/admin/user/index'), array('class' => 'btn btn-common',)); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->