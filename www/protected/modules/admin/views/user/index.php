<?php
/* @var $this UserController */
/* @var $model User */
$this->breadcrumbs=array(
    'Users'=>array('index'),
    'Manage',
);
?>

<h1>Manage Users</h1>
<ul class="ui-buttons-panel">
    <li>
        <?php echo CHtml::link(Yii::t('admin', '+ Add a New User'), array('create'), array('class' => "btn btn-primary")); ?>
    </li>
</ul>
<?php $this->widget('application.widgets.grid.GGridView', array(
    'id'=>'user-grid',
    'itemsCssClass'=> 'table',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'pager' => array(
        'header' => '',
    ),
    'template' => '{items}{pager}',
    'columns'=>array(
        'id',
        'firstName',
        'lastName',
        'usertypeId'=>array('name'=>'usertypeId', 'value'=>'$data->usertype->type', 'filter'=>CHtml::listData(Usertype::model()->findAll(),'id','type')),
        'userName',
        'email',
        'status' => array('name' => 'status', 'filter' => User::getStatusFilter(),
                              'value' => 'User::alias("Status",$data->status)'),
            array(
                'header' => Yii::t('admin','Actions'),
                'class' => 'CButtonColumn',
                'headerHtmlOptions' => array('width' => '20%'),
                'buttons' => array(
                'delete' => array(
                    'visible'=>(function($i, $user){if ($user->status!=User::DELETED_USER_STATUS) {return true;} else {return false;}}),
                    'imageUrl' => null,
                    'options' => array(
                        'class' => 'btn-delete btn',
                    ),
                    'click' => "function(){
                            if (confirm('" . Yii::t('admin', 'Do you really want to delete this user?') . "')){
                                return true;
                            } else {
                                return false;
                            }
                        }",
                    'url' => 'array("/admin/user/delete","id"=>$data->id)'
                ),
                'view' => array(
                    'visible' => 'false',
                ),
                'update' => array(

                    'imageUrl' => null,
                    'options' => array(
                        'class' => 'btn btn-primary'
                    ),
                    'url' => 'array("/admin/user/update","id"=>$data->id)'))
        ),

    ),
    'theadCssClass' => 'ContentAdmin'
)); ?>
