<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */

$baseUrl = Yii::app()->theme->baseUrl;
//Yii::app()->clientScript->registerCssFile($baseUrl.'/assets/css/jquery/jquery.dataTables.css');
Yii::app()->clientScript->registerScriptFile($baseUrl.'/js/jquery.form.js');
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'usercash-form',
	'enableAjaxValidation'=>true,
    'clientOptions'        => array(
        'validateOnSubmit' => true,
        'validateOnChange' => true,
    ),
)); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'userId'); ?>
        <?php echo $form->textField($model,'userId',array('size'=>20,'maxlength'=>20,'disabled'=>true)); ?>
        <?php echo $form->error($model,'userId'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'realMoney'); ?>
        <?php echo $form->textField($model,'realMoney'); ?>
        <?php echo $form->error($model,'realMoney'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'cyberMoney'); ?>
        <?php echo $form->textField($model,'cyberMoney'); ?>
        <?php echo $form->error($model,'cyberMoney'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'isBlocked'); ?>
        <?php echo $form->dropDownList($model, 'isBlocked', Usercash::getStatusFilter()); ?>
        <?php echo $form->error($model,'isBlocked'); ?>
    </div>

    <div class="row buttons">

		<?php echo CHtml::submitButton('Save'); ?>
        <?php echo CHtml::link(Yii::t('admin', 'Cancel'), array('/admin/usercash/index'), array('class' => 'btn btn-common',)); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->