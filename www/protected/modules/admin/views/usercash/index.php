<?php
/* @var $this UserController */
/* @var $model User */
$this->breadcrumbs=array(
    'Usercash'=>array('index'),
    'Manage',
);
?>

<h1><?php echo Yii::t('admin','Manage Users Cash') ?></h1>
<?php $this->widget('application.widgets.grid.GGridView', array(
    'id'=>'usercash-grid',
    'itemsCssClass'=> 'table',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'pager' => array(
        'header' => '',
    ),
    'template' => '{items}{pager}',
    'columns'=>array(
        'id',
        'userId'=>array('name'=>'userId','filter'=>CHtml::listData(User::model()->findAll(),'id','userName'),
                        'value'=>'$data->user->userName'),
        'realMoney',
        'cyberMoney',
        'isBlocked' => array('name' => 'isBlocked', 'filter' => Usercash::getStatusFilter(),
                              'value' => 'Usercash::alias("Status",$data->isBlocked)'),
            array(
                'header' => Yii::t('admin','Actions'),
                'class' => 'CButtonColumn',
                'headerHtmlOptions' => array('width' => '20%'),
                'buttons' => array(
                'delete' => array(
                    'visible'=>'false',
                ),
                'view' => array(
                    'visible' => 'false',
                ),
                'update' => array(

                    'imageUrl' => null,
                    'options' => array(
                        'class' => 'btn btn-primary'
                    ),
                    'url' => 'array("/admin/usercash/update","id"=>$data->id)'))
        ),

    ),
    'theadCssClass' => 'ContentAdmin'
)); ?>
