<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Users cash'=>array('index'),
    $model->userId=>array('index'),
	'Update',
);

$this->menu=array(
	array('label'=>'List User', 'url'=>array('index')),
	array('label'=>'Create User', 'url'=>array('create')),
	array('label'=>'View User', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage User', 'url'=>array('admin')),
);
?>

<h1>Update Cash of user #<?php echo $model->userId; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>