<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */

$baseUrl = Yii::app()->theme->baseUrl;
//Yii::app()->clientScript->registerCssFile($baseUrl.'/assets/css/jquery/jquery.dataTables.css');
Yii::app()->clientScript->registerScriptFile($baseUrl.'/js/jquery.form.js');
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'game-categories-form',
	'enableAjaxValidation'=>true,
    'clientOptions'        => array(
        'validateOnSubmit' => true,
        'validateOnChange' => true,
    ),
    'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
    ),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
    <div class="row">
        <?php echo $form->labelEx($model,'title'); ?>
        <?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>255)); ?>
        <?php echo $form->error($model,'title'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'alias'); ?>
        <?php echo $form->textField($model,'alias',array('size'=>60,'maxlength'=>255)); ?>
        <?php echo $form->error($model,'alias'); ?>
    </div>

<!--    <div class="row">-->
<!--        --><?php //echo $form->labelEx($model,'image'); ?>
<!--        --><?php //echo $form->textField($model,'image',array('size'=>60,'maxlength'=>255)); ?>
<!--        --><?php //echo $form->error($model,'image'); ?>
<!--    </div>-->

    <div class="row">
        <?php echo $form->labelEx($model,'image'); ?>
        <?php echo CHtml::activeFileField($model, 'image'); ?>
        <?php echo $form->error($model,'image'); ?>
    </div>
    <?php if($model->isNewRecord!='1'){ ?>
    <div class="row image">
        <?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/games/'.$model->image,"image",array("width"=>200)); ?>
    </div>
    <?php } ?>

    <div class="row">
        <?php echo $form->labelEx($model,'status'); ?>
        <?php echo $form->dropDownList($model, 'status', User::getStatusFilter()); ?>
        <?php echo $form->error($model,'status'); ?>
    </div>

    <div class="row buttons">

		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
        <?php echo CHtml::link(Yii::t('admin', 'Cancel'), array('/admin/gameCategories/index'), array('class' => 'btn btn-common',)); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->