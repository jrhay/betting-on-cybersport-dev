<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */

$baseUrl = Yii::app()->theme->baseUrl;
//Yii::app()->clientScript->registerCssFile($baseUrl.'/assets/css/jquery/jquery.dataTables.css');
Yii::app()->clientScript->registerScriptFile($baseUrl.'/js/jquery.form.js');
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'teams-form',
	'enableAjaxValidation'=>true,
    'clientOptions'        => array(
        'validateOnSubmit' => true,
        'validateOnChange' => true,
    ),
    'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
    ),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
    <div class="row">
        <?php echo $form->labelEx($model,'name'); ?>
        <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
        <?php echo $form->error($model,'name'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'country_id'); ?>
        <?php echo $form->dropDownList($model, 'country_id', CHtml::listData(Countries::model()->findAll(array('order'=>'t.name')),'id','name')); ?>
        <?php echo $form->error($model,'country_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'teamLogo'); ?>
        <?php echo CHtml::activeFileField($model, 'teamLogo'); ?>
        <?php echo $form->error($model,'teamLogo'); ?>
    </div>
    <?php if($model->isNewRecord!='1'){ ?>
        <div class="row image">
            <?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/teamsLogos/'.$model->teamLogo,"image",array("width"=>200)); ?>
        </div>
    <?php } ?>

    <div class="row">
        <?php echo $form->labelEx($model,'category_id'); ?>
        <?php echo $form->dropDownList($model, 'category_id', CHtml::listData(GameCategories::model()->findAll(),'id','title')); ?>
        <?php echo $form->error($model,'category_id'); ?>
    </div>

<!--    <div class="row">-->
<!--        --><?php //echo $form->labelEx($model,'type'); ?>
<!--        --><?php //echo $form->textField($model,'type');?>
<!--        --><?php //echo $form->error($model,'type'); ?>
<!--    </div>-->

    <div class="row">
        <?php echo $form->labelEx($model,'status'); ?>
        <?php echo $form->dropDownList($model, 'status', User::getStatusFilter()); ?>
        <?php echo $form->error($model,'status'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'register_date'); ?>
        <?php echo $form->textField($model,'register_date',array('disabled'=>true)); ?>
        <?php echo $form->error($model,'register_date'); ?>
    </div>

    <div class="row buttons">

		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
        <?php echo CHtml::link(Yii::t('admin', 'Cancel'), array('/admin/teams/index'), array('class' => 'btn btn-common',)); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->