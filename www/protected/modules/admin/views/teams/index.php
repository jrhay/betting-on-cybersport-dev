<?php
/* @var $this UserController */
/* @var $model User */
$this->breadcrumbs=array(
    Yii::t('admin', 'Manage Teams')=>array('index'),
    Yii::t('admin', 'Manage'),
);
?>

<h1><?php echo Yii::t('admin','Manage Teams') ?></h1>
<ul class="ui-buttons-panel">
    <li>
        <?php echo CHtml::link(Yii::t('admin', '+ Add a New Team'), array('create'), array('class' => "btn btn-primary")); ?>
    </li>
</ul>
<?php $this->widget('application.widgets.grid.GGridView', array(
    'id'=>'user-grid',
    'itemsCssClass'=> 'table',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'pager' => array(
        'header' => '',
    ),
    'template' => '{items}{pager}',
    'columns'=>array(
        'id',
        'name',
        'teamLogo',
        'country_id'=>array('name'=>'country_id','filter'=>CHtml::listData(Countries::model()->findAll(),'id','name'),
                    'value'=>'$data->country->name'),
        'category_id'=>array('name'=>'category_id','filter'=>CHtml::listData(GameCategories::model()->findAll(),'id','title'),
                    'value'=>'$data->category->title'),
        'status' => array('name' => 'status', 'filter' => User::getStatusFilter(),
            'value' => 'User::alias("Status",$data->status)'),
            array(
                'header' => Yii::t('admin','Actions'),
                'class' => 'CButtonColumn',
                'headerHtmlOptions' => array('width' => '20%'),
                'buttons' => array(
                'delete' => array(
                    'visible'=>(function($i, $game){if ($game->status!=User::DELETED_USER_STATUS) {return true;} else {return false;}}),
                    'imageUrl' => null,
                    'options' => array(
                        'class' => 'btn-delete btn',
                    ),
                    'click' => "function(){
                            if (confirm('" . Yii::t('admin', 'Do you really want to delete this user?') . "')){
                                return true;
                            } else {
                                return false;
                            }
                        }",
                    'url' => 'array("/admin/teams/delete","id"=>$data->id)'
                ),
                'view' => array(
                    'visible' => 'false',
                ),
                'update' => array(

                    'imageUrl' => null,
                    'options' => array(
                        'class' => 'btn btn-primary'
                    ),
                    'url' => 'array("/admin/teams/update","id"=>$data->id)'))
        ),

    ),
    'theadCssClass' => 'ContentAdmin'
)); ?>
<?php //Todo: fix pager size selector?>
<script>
    $('.display-number').remove();
</script>
