<?php
/* @var $this UserController */
/* @var $model User */
$this->breadcrumbs=array(
    'Game Categories'=>array('index'),
    'Manage',
);
?>

<h1><?php echo Yii::t('admin','Manage Game Categories') ?></h1>
<ul class="ui-buttons-panel">
    <li>
        <?php echo CHtml::link(Yii::t('admin', '+ Add a New Banner'), array('create'), array('class' => "btn btn-primary")); ?>
    </li>
</ul>
<?php $this->widget('application.widgets.grid.GGridView', array(
    'id'=>'banners-grid',
    'itemsCssClass'=> 'table',
    'dataProvider'=>$dataProvider,
    'filter'=>$model,
    'pager' => array(
        'header' => '',
    ),
    'template' => '{items}{pager}',
    'columns'=>array(
        'id',
        'image',
        'width',
        'height',
        'url',
        'lang' => array('name' => 'lang', 'filter' => Banners::getLangFilter(),
            'value' => 'Banners ::alias("Lang",$data->lang)'),
            array(
                'header' => Yii::t('admin','Actions'),
                'class' => 'CButtonColumn',
                'headerHtmlOptions' => array('width' => '20%'),
                'buttons' => array(
                'delete' => array(
                    'visible'=>"true",
                    'imageUrl' => null,
                    'options' => array(
                        'class' => 'btn-delete btn',
                    ),
                    'click' => "function(){
                            if (confirm('" . Yii::t('admin', 'Do you really want to delete this user?') . "')){
                                return true;
                            } else {
                                return false;
                            }
                        }",
                    'url' => 'array("/admin/banners/delete","id"=>$data->id)'
                ),
                'view' => array(
                    'visible' => 'false',
                ),
                'update' => array(

                    'imageUrl' => null,
                    'options' => array(
                        'class' => 'btn btn-primary'
                    ),
                    'url' => 'array("/admin/banners/update","id"=>$data->id)'))
        ),

    ),
    'theadCssClass' => 'ContentAdmin'
)); ?>
