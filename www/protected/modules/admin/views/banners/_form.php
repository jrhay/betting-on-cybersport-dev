<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */

$baseUrl = Yii::app()->theme->baseUrl;
//Yii::app()->clientScript->registerCssFile($baseUrl.'/assets/css/jquery/jquery.dataTables.css');
Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/jquery.form.js');
?>

<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'banners-form',
        'enableAjaxValidation' => true,
        'clientOptions' => array(
//        'validateOnSubmit' => true,
            'validateOnChange' => true,
        ),
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
        ),
    )); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>
    <div class="row">
        <?php echo $form->labelEx($model, 'width'); ?>
        <?php echo $form->textField($model, 'width'); ?>
        <?php echo $form->error($model, 'width'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'height'); ?>
        <?php echo $form->textField($model, 'height'); ?>
        <?php echo $form->error($model, 'height'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'image'); ?>
        <?php echo CHtml::activeFileField($model, 'image'); ?>
        <?php echo $form->error($model, 'image'); ?>
    </div>
    <?php if ($model->isNewRecord != '1') { ?>
        <div class="row image">
            <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/banners/' . $model->image, "image", array("width" => 200)); ?>
        </div>
    <?php } ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'url'); ?>
        <?php echo $form->textArea($model, 'url', array('rows' => 6, 'cols' => 50)); ?>
        <?php echo $form->error($model, 'url'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'lang'); ?>
        <?php echo $form->dropDownList($model, 'lang',
            array(
                Banners::LANG_RU => Yii::t('banners', 'ru'),
                Banners::LANG_ENG => Yii::t('banners', 'en'),
            )) ?>
        <?php echo $form->error($model, 'lang'); ?>
    </div>

    <div class="row buttons">

        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
        <?php echo CHtml::link(Yii::t('admin', 'Cancel'), array('/admin/banners/index'), array('class' => 'btn btn-common',)); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->