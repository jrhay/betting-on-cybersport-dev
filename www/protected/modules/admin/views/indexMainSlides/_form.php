<?php
/* @var $this IndexMainSlidesController */
/* @var $model IndexMainSlides */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'index-main-slides-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation'=>false,
        'htmlOptions'=>array(
            'enctype' => 'multipart/form-data',
        ),

    )); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'slide'); ?>
        <?php
        $htmlOptions= array('');
        if(Yii::app()->controller->action->id == 'create') {
            $htmlOptions['required'] = true;
        }
        ?>
        <?php echo $form->fileField($model, 'slide' ,$htmlOptions); ?>
        <?php echo $form->error($model,'slide'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'slide_eng'); ?>
        <?php
        $htmlOptions= array('');
        if(Yii::app()->controller->action->id == 'create') {
            $htmlOptions['required'] = true;
        }
        ?>
        <?php echo $form->fileField($model, 'slide_eng' ,$htmlOptions); ?>
        <?php echo $form->error($model,'slide_eng'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'title'); ?>
        <?php echo $form->textArea($model,'title',array('rows'=>1, 'cols'=>50)); ?>
        <?php echo $form->error($model,'title'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'title_eng'); ?>
        <?php echo $form->textArea($model,'title_eng',array('rows'=>1, 'cols'=>50)); ?>
        <?php echo $form->error($model,'title_eng'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'text'); ?>
        <?php echo $form->textArea($model,'text',array('rows'=>5, 'cols'=>50)); ?>
        <?php echo $form->error($model,'text'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'text_eng'); ?>
        <?php echo $form->textArea($model,'text_eng',array('rows'=>5, 'cols'=>50)); ?>
        <?php echo $form->error($model,'text_eng'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'ref'); ?>
        <?php echo $form->textArea($model,'ref',array('rows'=>5, 'cols'=>50)); ?>
        <?php echo $form->error($model,'ref'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->