<?php
/* @var $this IndexMainSlidesController */
/* @var $model IndexMainSlides */

$this->breadcrumbs=array(
	'Index Main Slides'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List IndexMainSlides', 'url'=>array('index')),
	array('label'=>'Create IndexMainSlides', 'url'=>array('create')),
	array('label'=>'View IndexMainSlides', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage IndexMainSlides', 'url'=>array('admin')),
);
?>

<h1>Update IndexMainSlides <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>