<?php
/* @var $this IndexMainSlidesController */
/* @var $model IndexMainSlides */

$this->breadcrumbs=array(
	'Index Main Slides'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List IndexMainSlides', 'url'=>array('index')),
	array('label'=>'Manage IndexMainSlides', 'url'=>array('admin')),
);
?>

<h1>Create IndexMainSlides</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>