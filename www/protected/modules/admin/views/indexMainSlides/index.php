<?php
/* @var $this IndexMainSlidesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Index Main Slides',
);

$this->menu=array(
	array('label'=>'Create IndexMainSlides', 'url'=>array('create')),
	array('label'=>'Manage IndexMainSlides', 'url'=>array('admin')),
);
?>

<h1>Index Main Slides</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
