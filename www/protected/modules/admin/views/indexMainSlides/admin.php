<?php
/* @var $this IndexMainSlidesController */
/* @var $model IndexMainSlides */

$this->breadcrumbs=array(
	'Index Main Slides'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List IndexMainSlides', 'url'=>array('index')),
	array('label'=>'Create IndexMainSlides', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#index-main-slides-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<h1>Manage Index Main Slides</h1>
<hr>
<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
<br>
<a href="<?php echo Yii::app()->createUrl('admin/IndexMainSlides/create')?>" class="btn btn-primary" style="float: right;margin: 1%"> Create New Slide</a>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'index-main-slides-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'slide',
        'slide_eng',
		'title',
        'title_eng',
		'text',
        'text_eng',
		'ref',
		array(
			'class'=>'CButtonColumn',
		)

	),
)); ?>
