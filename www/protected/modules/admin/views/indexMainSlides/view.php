<?php
/* @var $this IndexMainSlidesController */
/* @var $model IndexMainSlides */

$this->breadcrumbs=array(
	'Index Main Slides'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List IndexMainSlides', 'url'=>array('index')),
	array('label'=>'Create IndexMainSlides', 'url'=>array('create')),
	array('label'=>'Update IndexMainSlides', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete IndexMainSlides', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage IndexMainSlides', 'url'=>array('admin')),
);
?>

<h1>View IndexMainSlides #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'slide',
		'title',
        'title_eng',
		'text',
        'text_eng',
		'ref'
	),
)); ?>
