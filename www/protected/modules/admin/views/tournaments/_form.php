    <?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */

$baseUrl = Yii::app()->theme->baseUrl;
//Yii::app()->clientScript->registerCssFile($baseUrl.'/assets/css/jquery/jquery.dataTables.css');
Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/jquery.form.js');
?>

<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'tournaments-form',
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
        ),
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
        ),
    )); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>
    <div class="row">
        <?php echo $form->labelEx($model, 'name'); ?>
        <?php echo $form->textField($model, 'name'); ?>
        <?php echo $form->error($model, 'name'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'start_date'); ?>
        <?php $this->widget('application.widgets.CJuiDateTimePicker.CJuiDateTimePicker', array(
            'attribute' => 'start_date',
            'mode' => 'datetime',
            'language' => 'en-GB',
//            'cssFile'     => null,
            'model' => $model,

            'htmlOptions' => array(
                'class' => 'w2',
                'value' => date("Y-m-d H:i:s", $this->action->id == "create" ? time() : $model->start_date),
            ),
            'options' => array(
                'changeMonth' => 'false',
                'changeYear' => 'false',
                'timeFormat' => 'hh:mm:ss',
                'dateFormat' => 'yy-mm-dd',
                'showButtonPanel' => true,
                'currentText' => Yii::t('general', 'Current Time'),
                'hourMax' => 23,
                'stepMinute' => 1,
                'showSecond' => false

            ),
        ));?>
        <?php echo $form->error($model, 'start_date'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'status'); ?>
        <?php echo $form->dropDownList($model, 'status', Tournaments::getStatusFilter()); ?>
        <?php echo $form->error($model, 'status'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'activeStatus'); ?>
        <?php echo $form->dropDownList($model, 'activeStatus', Tournaments::getActiveStatusFilter()); ?>
        <?php echo $form->error($model, 'activeStatus'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'isFloatingKoef'); ?>
        <?php echo $form->dropDownList($model, 'isFloatingKoef', Tournaments::getYesNoFilter()); ?>
        <?php echo $form->error($model, 'isFloatingKoef'); ?>
    </div>
    <div class="row">
        <?php
        //Todo:get it out of here
        $gameCategoriesList = array();
        foreach(GameCategories::model()->findAll() as $ctg) {
            $gameCategoriesList[$ctg->id] = $ctg->title;
        }
        ?>
        <?php echo $form->labelEx($model, 'gameCategory'); ?>
        <?php echo $form->dropDownList($model, 'gameCategory', $gameCategoriesList); ?>
        <?php echo $form->error($model, 'gameCategory'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'logo'); ?>
        <?php echo $form->fileField($model, 'logo'); ?>
        <?php echo $form->error($model, 'logo'); ?>
    </div>
    <!--    <div class="row">-->
    <!--        --><?php //echo $form->labelEx($model,'winnerId'); ?>
    <!--        --><?php //echo $form->textField($model,'winnerId'); ?>
    <!--        --><?php //echo $form->error($model,'winnerId'); ?>
    <!--    </div>-->
    <!---->
    <!--    <div class="row">-->
    <!--        --><?php //echo $form->labelEx($model,'winnerKoef'); ?>
    <!--        --><?php //echo $form->textField($model,'winnerKoef'); ?>
    <!--        --><?php //echo $form->error($model,'winnerKoef'); ?>
    <!--    </div>-->
</div>


<div class="row buttons">

    <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    <?php echo CHtml::link(Yii::t('admin', 'Cancel'), array('/admin/tournaments/index'), array('class' => 'btn btn-common',)); ?>
</div>

<?php $this->endWidget(); ?>

</div><!-- form -->