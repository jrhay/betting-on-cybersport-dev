<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	Yii::t('admin','Tournaments')=>array('index'),
    $model->id=>array('index'),
    Yii::t('admin','Update'),
);
?>

<h1><?php echo Yii::t('admin','Update Tournament') ?> - <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>