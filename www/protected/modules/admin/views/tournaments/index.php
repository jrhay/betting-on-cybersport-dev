<?php
/* @var $this UserController */
/* @var $model User */
$this->breadcrumbs = array(
    Yii::t('admin', 'Manage Tournaments') => array('index'),
    Yii::t('admin', 'Manage'),
);
?>

<h1><?php echo Yii::t('admin', 'Manage Tournaments') ?></h1>
<ul class="ui-buttons-panel">
    <li>
        <?php echo CHtml::link(Yii::t('admin', '+ Add a New Tournament'), array('create'), array('class' => "btn btn-primary")); ?>
    </li>
</ul>
<?php $this->widget('application.widgets.grid.GGridView', array(
    'id' => 'user-grid',
    'itemsCssClass' => 'table',
    'dataProvider' => $model->search(),
//    'filter' => $model,
    'pager' => array(
        'header' => '',
    ),
    'template' => '{items}{pager}',
    'columns' => array(
        'id',
        'name' => array(
            'name' => 'name',
            'value' => '$data->name',
            'htmlOptions' => array('style' => 'max-width: 6%')
        ),
        array(
            'name' => 'Game Category',
            'value' => '$data->gameCategory',
        ),
        'start_date' => array('name' => 'start_date', 'value' => function ($data) {
                echo Helper::getDateFormatted(Yii::t('information', 'Date format 3 ::yyyy-MM-dd HH:mm:ss'), $data->start_date, '');
            }),
//        'status',
        'isFloatingKoef' => array('name' => 'isFloatingKoef', 'filter' => Tournaments::getYesNoFilter(),
            'value' => 'Tournaments::alias("YesNo",$data->isFloatingKoef)'),
        array(
            'name' => 'winner',
            'value' => 'isset($data->winner->name)?$data->winner->name:""',
        ),
        'winnerKoef',
        'logo' => array(
            'name' => 'logo',
            'value' => '$data->logo',
            'htmlOptions' => array('style' => 'max-width: 6%')
        ),
        'status' => array('name' => 'status', 'filter' => Tournaments::getStatusFilter(),
            'value' => 'Tournaments::alias("Status",$data->status)'),
        array('name' => 'activeStatus', 'filter' => Tournaments::getActiveStatusFilter(),
            'value' => 'Tournaments::alias("ActiveStatus",$data->activeStatus)'),
        array(
            'header' => Yii::t('admin', 'Actions'),
            'class' => 'CButtonColumn',
            'headerHtmlOptions' => array('width' => '20%'),
            'buttons' => array(
                'delete' => array(
                    'visible' => 'false',
                ),
                'view' => array(
                    'visible' => 'true',
                    'label' => 'Teams List',
                    'imageUrl' => null,
                    'options' => array(
                        'class' => 'btn btn-primary'
                    ),
                    'url' => 'array("/admin/tournamentsdetails/index","tournamentId"=>$data->id)'
                ),
                'update' => array(

                    'imageUrl' => null,
                    'options' => array(
                        'class' => 'btn btn-primary'
                    ),
                    'url' => 'array("/admin/tournaments/update","id"=>$data->id)'))
        ),

    ),
    'theadCssClass' => 'ContentAdmin'
)); ?>
