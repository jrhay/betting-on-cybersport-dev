<form class='form-actions' name='resetJackpotForm'
      action="<?php echo  Yii::app()->createUrl('admin/jackpot/reset') ?>" method="post" enctype="multipart/form-data">
    <input type='hidden' name="resetJackpotForm" value='true'>
    <h5><?php echo  Yii::t('admin', 'Обнулить Джекпот') ?>"</h5>
    <input type="submit" value="<?php echo  Yii::t('admin', 'Сброс') ?>"/>
    <br>
</form>
<?php  if (!empty($resetJackpotResult)) {
    if ($resetJackpotResult['result']) {
        echo "<br><div style=\"color: green\">" . Yii::t('admin', 'Jackpot successfully reseted') . "</div>";
    } else {
        echo "<br><div style=\"color: red\">" . Yii::t('admin', 'Error Jackpot is not reseted') . "</div>";
    }
}?>