<?php
/* @var $this JackpotController */
/* @var $model Jackpot */
/* @var $form CActiveForm */
?>
<h4><?php echo Yii::t('admin', 'Manage Jackpot')?></h4>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'jackpot-manage-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// See class documentation of CActiveForm for details on this,
	// you need to use the performAjaxValidation()-method described there.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'koef'); ?>
		<?php echo $form->textField($model,'koef'); ?>
		<?php echo $form->error($model,'koef'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'percent'); ?>
		<?php echo $form->textField($model,'percent'); ?>
		<?php echo $form->error($model,'percent'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'current_sum'); ?>
		<?php echo $form->textField($model,'current_sum'); ?>
		<?php echo $form->error($model,'current_sum'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'last_reset_time'); ?>
		<?php echo $form->textField($model,'last_reset_time', array(
            'readonly' => true,
        )); ?>
		<?php echo $form->error($model,'last_reset_time'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton(Yii::t('site','Save Changes')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<?php  if (!empty($manageJackpotResult)) {
    if ($manageJackpotResult['result']) {
        echo "<br><div style=\"color: green\">" . Yii::t('admin', 'Jackpot successfully reseted') . "</div>";
    } else {
        echo "<br><div style=\"color: red\">" . Yii::t('admin', 'Error Jackpot is not reseted') . "</div>";
    }
}?>