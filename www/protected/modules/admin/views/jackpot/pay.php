<form class='form-actions' name = 'issueJackpotForm' action="<?php echo Yii::app()->createUrl('admin/jackpot/pay')?>" method="post" enctype="multipart/form-data">
<input type='hidden' name="payJackpotForm" value='true'>
<h5><?php echo Yii::t('admin', 'Pay jackpot to user')?></h5><br>
    <span><?php echo Yii::t('admin', 'jackpot resipient')?></span><br>
    <div class="row">
        <?php $this->widget('CAutoComplete',
            array(
                'model' => 'user',
                'name' => 'userName',
                'url' => array('jackpot/userautocomplete'),
                'minChars' => 2,
                'htmlOptions' => array('required' => 'required'),
            )
        );?>
    </div>
    <input type="submit"  value="<?php echo Yii::t('admin', 'Начислить')?>" />
    <br>
    </form>
<?php  if (!empty($payJackpotResult)) {
    if ($payJackpotResult['result']) {
        echo "<br><div style=\"color: green\">" . Yii::t('admin', '     Jackpot successfully paid') . "</div>";
    } else {
        echo "<br><div style=\"color: red\">" . Yii::t('admin', 'Error Jackpot is not paid') . "</div>";
    }
}?>