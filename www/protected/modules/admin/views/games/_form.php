    <?php
/* @var $this UserController */
/* @var $model Games */
/* @var $form CActiveForm */

$baseUrl = Yii::app()->theme->baseUrl;
//Yii::app()->clientScript->registerCssFile($baseUrl.'/assets/css/jquery/jquery.dataTables.css');
Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/jquery.form.js');
?>

    <div id="create-game-form" class="form">

        <?php $form = $this->beginWidget('CActiveForm', array(
            'id' => 'games-form',
            'enableAjaxValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
                'validateOnChange' => true,
            ),
            'htmlOptions' => array(
                'enctype' => 'multipart/form-data',
            ),
        )); ?>

        <p class="note">Fields with <span class="required">*</span> are required.</p>

        <?php echo $form->errorSummary($model); ?>

        <div class="row">
            <?php echo $form->labelEx($model, 'name'); ?>
            <?php echo $form->textField($model, 'name'); ?>
            <?php echo $form->error($model, 'name'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model, 'category_id'); ?>
            <?php echo $form->dropDownList($model, 'category_id', CHtml::listData(GameCategories::model()->findAll(), 'id', 'title')); ?>
            <?php echo $form->error($model, 'category_id'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model, 'map'); ?>
            <?php echo $form->textField($model, 'map'); ?>
            <?php echo $form->error($model, 'map'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model, 'betPercents'); ?>
            <?php echo $form->textField($model, 'betPercents'); ?>
            <?php echo $form->error($model, 'betPercents'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model, 'team1_id'); ?>
            <?php
            $value = '';
            if (Yii::app()->controller->action->id === 'update') {
                /** @var Teams $team1 */
                $team1 = Teams :: model()->findByPk($model->team1_id);
                $value = $team1->name;
            }
            ?>
            <?php $this->widget('application.widgets.TeamsAutoComplete.TeamsAutoComplete',
                array(
                    'model' => 'gameCategories',
                    'name' => 'Games[team1_title]',
                    'url' => array('games/teamAutocomplete'),
                    'minChars' => 2,
                    'value' => $value,
                    'htmlOptions' => array(
                        'required' => 'required',
                    )
                )
            );?>
            <?php echo $form->error($model, 'team1_id'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model, 'team2_id'); ?>
            <?php
            $value = '';
            if (Yii::app()->controller->action->id === 'update') {
                /** @var Teams $team1 */
                $team2 = Teams :: model()->findByPk($model->team2_id);
                $value = $team2->name;
            }
            ?>
            <?php $this->widget('application.widgets.TeamsAutoComplete.TeamsAutoComplete',
                array(
                    'model' => 'gameCategories',
                    'name' => 'Games[team2_title]',
                    'url' => array('games/teamAutocomplete'),
                    'minChars' => 2,
                    'value' => $value,
                    'htmlOptions' => array(
                        'required' => 'required',
                    )
                )
            );?>
            <?php echo $form->error($model, 'team2_id'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model, 'draw_koef'); ?>
            <?php echo $form->textField($model, 'draw_koef'); ?>
            <?php echo $form->error($model, 'draw_koef'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model, 'team1_koef'); ?>
            <?php echo $form->textField($model, 'team1_koef'); ?>
            <?php echo $form->error($model, 'team1_koef'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model, 'team2_koef'); ?>
            <?php echo $form->textField($model, 'team2_koef'); ?>
            <?php echo $form->error($model, 'team2_koef'); ?>
        </div>


        <div class="row">
            <?php echo $form->labelEx($model, 'game_ico'); ?>
            <?php echo CHtml::activeFileField($model, 'game_ico'); ?>
            <?php echo $form->error($model, 'game_ico'); ?>
        </div>
        <?php if ($model->isNewRecord != '1') { ?>
            <div class="row image">
                <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/gameIcons/' . $model->game_ico, "image", array("width" => 200)); ?>
            </div>
        <?php } ?>

        <div class="row">
            <?php echo $form->labelEx($model, 'event_id'); ?>
            <?php echo $form->dropDownList($model, 'event_id', CHtml::listData(Events::model()->findAll(), 'id', 'text')); ?>
            <?php echo $form->error($model, 'event_id'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model, 'game_start_at'); ?>
            <?php $this->widget('application.widgets.CJuiDateTimePicker.CJuiDateTimePicker', array(
                'attribute' => 'game_start_at',
                'mode' => 'datetime',
                'language' => 'en-GB',
//            'cssFile'     => null,
                'model' => $model,
                'htmlOptions' => array(
                    'class' => 'w2',
                    'value' => gmdate('Y-m-d H:i:s', $model->game_start_at),
                ),
                'options' => array(
                    'changeMonth' => 'false',
                    'changeYear' => 'false',
                    'timeFormat' => 'hh:mm:ss',
                    'dateFormat' => 'yy-mm-dd',
                    'showButtonPanel' => true,
                    'currentText' => Yii::t('doctor_home', 'Current Time'),
                    'hourMax' => 23,
                    'stepMinute' => 1,
                    'showSecond' => false

                ),
            ));?>
            <?php echo $form->error($model, 'game_start_at'); ?>
        </div>

        <!--    <div class="row">-->
        <!--        --><?php //echo $form->labelEx($model,'game_end'); ?>
        <!--        --><?php //$this->widget('application.widgets.CJuiDateTimePicker.CJuiDateTimePicker', array(
        //            'attribute'   => 'game_end',
        //            'mode'        => 'datetime',
        //            'language'    => 'en-GB',
        ////            'cssFile'     => null,
        //            'model'       => $model,
        //            'htmlOptions' => array(
        //                'class' => 'w2',
        //                'value'=>Helper::getDateFormatted(Yii::t('information','Date format 3 ::yyyy-MM-dd HH:mm:ss'), $model->game_end,''),
        //            ),
        //            'options'     => array(
        //                'changeMonth' => 'false',
        //                'changeYear'  => 'false',
        //                'timeFormat'  => 'hh:mm:ss',
        //                'dateFormat'  => 'yy-mm-dd',
        //                'showButtonPanel'=>true,
        //                'currentText'=>Yii::t('doctor_home','Current Time'),
        //                'hourMax' =>23,
        //                'stepMinute'=>1,
        //                'showSecond'=>false
        //
        //            ),
        //        ));
        ?>
        <!--        --><?php //echo $form->error($model,'game_end'); ?>
        <!--    </div>-->

        <div class="row">
            <?php echo $form->labelEx($model, 'status'); ?>
            <?php echo $form->dropDownList($model, 'status', Games::getStatusFilter()); ?>
            <?php echo $form->error($model, 'status'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model, 'manager_id'); ?>
            <?php echo $form->dropDownList($model, 'manager_id', CHtml::listData(User::model()->OnlyActive()->onlyManagers()->findAll(), 'id', 'userName')); ?>
            <?php echo $form->error($model, 'manager_id'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model, 'max_sum'); ?>
            <?php echo $form->textField($model, 'max_sum'); ?>
            <?php echo $form->error($model, 'max_sum'); ?>
        </div>


        <div class="row buttons">

            <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
            <?php echo CHtml::link(Yii::t('admin', 'Cancel'), array('/admin/games/index'), array('class' => 'btn btn-common',)); ?>
        </div>

        <?php $this->endWidget(); ?>

    </div><!-- form -->
<?php  Yii::app()->clientScript->registerScript(uniqid(), "
 jQuery('#create-game-form').on('input','#Games_team1_koef', function() {
            changeTeam2KoefTournaments();
        });

 jQuery('#create-game-form').on('input','#Games_team2_koef', function() {
            changeTeam1KoefTournaments();
        });
 jQuery('#create-game-form').on('input','#Games_draw_koef', function() {
            changeTeam2KoefTournaments();
        });


        function changeTeam2KoefTournaments() {

             if (jQuery('#Games_betPercents').val() == '')   {
                return;
            }

            margin = jQuery('#Games_betPercents').val();
            margin = parseFloat(margin  );
            fullPercents = margin + 100;

            team1_koef = jQuery('#Games_team1_koef').val();
            team2_koef = jQuery('#Games_team2_koef').val();
            draw_koef = jQuery('#Games_draw_koef').val();

            if(draw_koef == '' ) {
                p1 = 100/team1_koef;
                p2 = margin + 100 - p1;
                team2_koef = 100 / p2;
            } else {
                p1 = 100/team1_koef;
                pd = 100/draw_koef;
                p2 =  margin  + 100 - (p1 + pd)
                team2_koef = 100 / ( ( margin + 100 ) - (p1 + pd));
            }

            jQuery('#Games_team2_koef').val(team2_koef.toFixed(3));
        }
        function changeTeam1KoefTournaments() {

             if (jQuery('#Games_betPercents').val() == '')   {
                return;
            }

            margin = jQuery('#Games_betPercents').val();
            margin = parseFloat(margin);
            fullPercents = margin + 100;

            team1_koef = jQuery('#Games_team1_koef').val();
            team2_koef = jQuery('#Games_team2_koef').val();
            draw_koef = jQuery('#Games_draw_koef').val();

            if(draw_koef == '') {
                p2 = 100/team2_koef;
                p1 = margin + 100 - p2;
                team1_koef = 100 / p1;
            } else {
                p2 = 100/team2_koef;
                pd = 100/draw_koef;
                p1 =  margin  + 100 - (p2 + pd)
                team1_koef = 100 / p1;
            }

            jQuery('#Games_team1_koef').val(team1_koef.toFixed(3));
        }
        ")?>