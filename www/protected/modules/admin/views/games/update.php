<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	Yii::t('admin','Games')=>array('index'),
    $model->name=>array('index'),
    Yii::t('admin','Update'),
);
?>

<h1><?php echo Yii::t('admin','Update Team') ?> - <?php echo $model->name; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>