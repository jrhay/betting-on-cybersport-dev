<?php
/* @var $this UserController */
/* @var $model User */
$this->breadcrumbs = array(
    Yii::t('admin', 'Manage Games') => array('index'),
    Yii::t('admin', 'Manage'),
);
?>

<h1><?php echo Yii::t('admin', 'Manage Games') ?></h1>
<ul class="ui-buttons-panel">
    <li>
        <?php echo CHtml::link(Yii::t('admin', '+ Add a New Game'), array('create'), array('class' => "btn btn-primary")); ?>
    </li>
</ul>
<?php $this->widget('application.widgets.grid.GGridView', array(
    'id' => 'user-grid',
    'itemsCssClass' => 'table',
    'dataProvider' => new CActiveDataProvider('Games', array(
        'criteria' => array(
            'order'  => 'id DESC',
        ))),
    'filter' => $model,
//    'pager' => array(
//        'header' => '',
//    ),
    'template' => '{items}{pager}',
    'columns' => array(
        'category_id' => array('name' => 'category_id', 'filter' => CHtml::listData(GameCategories::model()->findAll(), 'id', 'title'),
            'value' => '$data->category->title'),
        'team1_id' => array('name' => 'team1_id', 'filter' => CHtml::listData(Teams::model()->findAll(), 'id', 'name'),
            'value' => '$data->team1->name'),
        'team2_id' => array('name' => 'team2_id', 'filter' => CHtml::listData(Teams::model()->findAll(), 'id', 'name'),
            'value' => '$data->team2->name'),
        'team1_koef',
        'team2_koef',
//        'game_ico',
        'draw_koef',
        'map',
        'game_start_at' => array('name' => 'game_start_at', 'value' => function ($data) {
                echo Helper::getDateFormatted(Yii::t('information', 'Date format 3 ::yyyy-MM-dd HH:mm:ss'), $data->game_start_at, '');
            }),
//        'game_end'=>array('name'=>'game_end', 'value'=>function($data) {
//                echo Helper::getDateFormatted(Yii::t('information','Date format 3 ::yyyy-MM-dd HH:mm:ss'), $data->game_end,'');
//            }),
        'manager_id' => array('name' => 'manager_id', 'value' => '$data->manager->userName'),
        'max_sum',

        'status' => array('name' => 'status', 'filter' => Games::getStatusFilter(),
            'value' => 'Games::alias("Status",$data->status)'),
        array(
            'header' => Yii::t('admin', 'Actions'),
            'class' => 'CButtonColumn',
            'headerHtmlOptions' => array('width' => '20%'),
            'buttons' => array(
                'update' => array(

                    'imageUrl' => null,
                    'options' => array(
                        'class' => 'btn no_delete'
                    ),
                    'url' => 'array("/admin/games/update","id"=>$data->id)'),
                'delete' => array(
                    'visible' => 'true',
                    'options' => array(
                        'class' => 'btn btn-primary'
                    ),
                    'url' => 'array("/admin/games/delete","id"=>$data->id)'
                ),
                'view' => array(
                    'visible' => 'false',
                ),

            )
        ),

    ),
    'theadCssClass' => 'ContentAdmin'
)); ?>
<script>
    $('.display-number').remove();
</script>