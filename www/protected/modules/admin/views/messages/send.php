<?php
/* @var $this PersonalMessageController */
/* @var $model PersonalMessage */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'personal-message-send-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // See class documentation of CActiveForm for details on this,
        // you need to use the performAjaxValidation()-method described there.
        'enableAjaxValidation' => false,
    )); ?>

    <br>
    <h4><?php echo  Yii::t('admin', 'Send message to user : ') ?></h4>
    <br>

    <p class="note"><?php echo  Yii::t('site', 'Fields with') ?> <span
            class="required">*</span><?php echo  Yii::t('site', 'are required') ?></p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'recipient_id'); ?>
        <?php $this->widget('CAutoComplete',
            array(
                'model' => 'user',
                'name' => 'userName',
                'url' => array('messages/userautocomplete'),
                'minChars' => 2,
                'htmlOptions' => array('required' => 'required')
            )
        );?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'subject'); ?>
        <?php echo $form->textField($model, 'subject'); ?>
        <?php echo $form->error($model, 'subject'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'text'); ?>
        <?php echo $form->textArea($model, 'text'); ?>
        <?php echo $form->error($model, 'text'); ?>
    </div>
    <div class="row buttons">
        <?php echo CHtml::submitButton(Yii::t('site','submit')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
<?php  if (!empty($sendResult)) : ?>
    <?php  if ($sendResult['result']) : ?>
        <br>
        <div style="color: green"><?php echo  Yii::t('admin', 'Message is successfully sent!') ?></div>
    <?php  else : ?>
        <br>
        <div style="color: red"><?php echo  Yii::t('admin', 'Error message is not sent!') ?></div>
    <?php  endif ?>
<?php  endif ?>