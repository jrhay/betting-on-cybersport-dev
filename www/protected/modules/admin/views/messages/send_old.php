<h4><?php echo  Yii::t('admin', 'Отправить сообщение пользователю : ') ?></h4>
<form id="massage-form" class='form-actions' name='sendMessageForm' action="<?php echo  Yii::app()->createUrl('admin/messages/send') ?>"
      method="post" enctype="multipart/form-data">
    <input type='hidden' name="sendMessageForm" value='true'>
    <span><?php echo  Yii::t('admin', 'Адресат:') ?></span><br>
    <select name="recipient_id">";
        <?php  foreach ($users as $user) : ?>
            <option value="<?php echo $user->id?>"><?php echo  $user->userName ?></option>
        <?php  endforeach ?>
    </select>
    <br><br><br>
    <span><?php echo  Yii::t('admin', 'Тема сообщения:') ?></span>
    <br>
    <input type='text' name="subject">
    <br><br><br>
    <span><?php echo  Yii::t('admin', 'Текст сообщения') ?></span>
    <br>
    <textarea name="text"></textarea>
    <br>
    <input type="submit" value="<?php echo  Yii::t('admin', 'Отправить') ?>"/>
    <br>
</form>
<?php  if (!empty($sendResult)) : ?>
    <?php  if ($sendResult['result']) : ?>
        <br>
        <div style="color: green"><?php echo  Yii::t('admin', 'Сообщение усешно отправлено') ?></div>
    <?php  else : ?>
        <br>
        <div style="color: red"><?php echo  Yii::t('admin', 'Сообщение не отпрвлено!') ?></div>
    <?php  endif ?>
<?php  endif ?>
