<?php
/* @var $this EgbGamesController */
/* @var $model EgbGames */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'egb-games-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'egb_id'); ?>
		<?php echo $form->textField($model,'egb_id',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'egb_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cyb_id'); ?>
		<?php echo $form->textField($model,'cyb_id',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'cyb_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'egb_result'); ?>
		<?php echo $form->textField($model,'egb_result'); ?>
		<?php echo $form->error($model,'egb_result'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'add_egb_result_time'); ?>
		<?php echo $form->textField($model,'add_egb_result_time',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'add_egb_result_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'add_time'); ?>
		<?php echo $form->textField($model,'add_time',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'add_time'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->