<?php
/* @var $this EgbGamesController */
/* @var $model EgbGames */

$this->breadcrumbs=array(
	'Egb Games'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List EgbGames', 'url'=>array('index')),
	array('label'=>'Manage EgbGames', 'url'=>array('admin')),
);
?>

<h1>Create EgbGames</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>