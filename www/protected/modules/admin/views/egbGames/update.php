<?php
/* @var $this EgbGamesController */
/* @var $model EgbGames */

$this->breadcrumbs=array(
	'Egb Games'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List EgbGames', 'url'=>array('index')),
	array('label'=>'Create EgbGames', 'url'=>array('create')),
	array('label'=>'View EgbGames', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage EgbGames', 'url'=>array('admin')),
);
?>

<h1>Update EgbGames <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>