<?php
/* @var $this EgbGamesController */
/* @var $model EgbGames */

$this->breadcrumbs=array(
	'Egb Games'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List EgbGames', 'url'=>array('index')),
	array('label'=>'Create EgbGames', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#egb-games-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Egb Games</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'egb-games-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'egb_id',
		'cyb_id',
		'egb_result',
		array(
            'name' => 'add_egb_result_time',
            'value' => 'gmdate(\'Y-m-d H:i:s\', $data->add_egb_result_time)'
        ),
        array(
            'name' => 'add_time',
            'value' => 'gmdate(\'Y-m-d H:i:s\', $data->add_time)'
        ),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
