<?php
/* @var $this EgbGamesController */
/* @var $model EgbGames */

$this->breadcrumbs=array(
	'Egb Games'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List EgbGames', 'url'=>array('index')),
	array('label'=>'Create EgbGames', 'url'=>array('create')),
	array('label'=>'Update EgbGames', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete EgbGames', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage EgbGames', 'url'=>array('admin')),
);
?>

<h1>View EgbGames #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'egb_id',
		'cyb_id',
		'egb_result',
		'add_egb_result_time',
		'add_time',
	),
)); ?>
