<?php
/* @var $this EgbGamesController */
/* @var $data EgbGames */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('egb_id')); ?>:</b>
	<?php echo CHtml::encode($data->egb_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cyb_id')); ?>:</b>
	<?php echo CHtml::encode($data->cyb_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('egb_result')); ?>:</b>
	<?php echo CHtml::encode($data->egb_result); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('add_egb_result_time')); ?>:</b>
	<?php echo CHtml::encode($data->add_egb_result_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('add_time')); ?>:</b>
	<?php echo CHtml::encode($data->add_time); ?>
	<br />


</div>