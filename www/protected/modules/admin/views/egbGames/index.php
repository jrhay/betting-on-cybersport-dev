<?php
/* @var $this EgbGamesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Egb Games',
);

$this->menu=array(
	array('label'=>'Create EgbGames', 'url'=>array('create')),
	array('label'=>'Manage EgbGames', 'url'=>array('admin')),
);
?>

<h1>Egb Games</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
