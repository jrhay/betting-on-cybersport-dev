<?php
/* @var $this PayoutController */
/* @var $model Payout */

$this->breadcrumbs=array(
	'Payouts'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Payout', 'url'=>array('index')),
	array('label'=>'Manage Payout', 'url'=>array('admin')),
);
?>

<h1>Create Payout</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>