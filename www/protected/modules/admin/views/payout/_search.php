<?php
/* @var $this PayoutController */
/* @var $model Payout */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id',array('size'=>20,'maxlength'=>20)); ?>
	</div>

<!--	<div class="row">-->
<!--		--><?php //echo $form->label($model,'date'); ?>
<!--		--><?php //echo $form->textField($model,'date',array('size'=>20,'maxlength'=>20)); ?>
<!--	</div>-->

<!--	<div class="row">-->
<!--		--><?php //echo $form->label($model,'system_id'); ?>
<!--		--><?php //echo $form->textField($model,'system_id',array('size'=>20,'maxlength'=>20)); ?>
<!--	</div>-->

	<div class="row">
		<?php echo $form->label($model,'summ'); ?>
		<?php echo $form->textField($model,'summ'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'e_wallet'); ?>
		<?php echo $form->textField($model,'e_wallet',array('size'=>60,'maxlength'=>255)); ?>
	</div>

<!--	<div class="row">-->
<!--		--><?php //echo $form->label($model,'user_id'); ?>
<!--		--><?php //echo $form->textField($model,'user_id',array('size'=>20,'maxlength'=>20)); ?>
<!--	</div>-->

<!--	<div class="row">-->
<!--		--><?php //echo $form->label($model,'status'); ?>
<!--		--><?php //echo $form->textField($model,'status'); ?>
<!--	</div>-->

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->