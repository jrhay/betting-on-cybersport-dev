<?php
/* @var $this PayoutController */
/* @var $model Payout */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'payout-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    )); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'date'); ?>
        <?php echo $form->textField($model, 'date', array(
            'size' => 20,
            'maxlength' => 20,
            'disabled' => true,
            'value' => gmdate('Y-m-d H:i:s', $model->date))); ?>
        <?php echo $form->error($model, 'date'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'system_id'); ?>
        <?php echo $form->dropDownList($model, 'system_id', CHtml::listData(PaymentSystem::model()->findAll(), 'id', 'name'), array()); ?>
        <?php echo $form->error($model, 'system_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'summ'); ?>
        <?php echo $form->textField($model, 'summ'); ?>
        <?php echo $form->error($model, 'summ'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'e_wallet'); ?>
        <?php echo $form->textField($model, 'e_wallet', array('size' => 60, 'maxlength' => 255)); ?>
        <?php echo $form->error($model, 'e_wallet'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'user_id'); ?>
        <?php $this->widget('CAutoComplete',
            array(
                'model' => 'Payout',
                'name' => 'Payout[user_id]',
                'url' => array('payout/userautocomplete'),
                'minChars' => 2,
                'htmlOptions' => array('required' => 'required','required' => true),
                'value' => User::model()->findByPk($model->user_id)->userName
            )
        );?>
        <?php echo $form->error($model, 'user_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'status'); ?>
        <?php echo $form->dropDownList($model, 'status', Payout::getStatusFilter()); ?>
        <?php echo $form->error($model, 'status'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->