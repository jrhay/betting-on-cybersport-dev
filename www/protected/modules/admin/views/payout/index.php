<?php
/* @var $this PayoutController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Payouts',
);

$this->menu=array(
	array('label'=>'Create Payout', 'url'=>array('create')),
	array('label'=>'Manage Payout', 'url'=>array('admin')),
);
?>

<h1>Payouts</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
