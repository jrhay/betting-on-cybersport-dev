<?php
/* @var $this PayoutController */
/* @var $model Payout */

$this->breadcrumbs=array(
	'Payouts'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Payout', 'url'=>array('index')),
	array('label'=>'Create Payout', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#payout-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Payouts</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'payout-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		array(
            'name' => 'date',
            'value' => 'gmdate("Y-m-d H:i:s",$data->date)'
        ),
        array(
            'name' => 'user_id',
            'value' => 'PaymentSystem::model()->findByPk($data->system_id)->name'
        ),
		'summ',
		'e_wallet',
        array(
            'name' => 'user_id',
            'value' => 'User::model()->findByPk($data->user_id)->userName'
        ),
        'details',
        array(
            'name' => 'status',
            'filter' => Payout::getStatusFilter(),
            'value' => 'Payout::alias("Status",$data->status)'
        ),
        array(
            'header' => Yii::t('admin', 'Actions'),
            'class' => 'CButtonColumn',
            'template' => '{view} {update} {delete} {make payout}',
            'buttons'=>array(
                'make payout' => array(
                    'label'=>'payout', // text label of the button
                    'url'=>"CHtml::normalizeUrl(array('makePayout', 'id'=>\$data->id))",
                    'imageUrl'=>'/path/to/copy.gif',  // image URL of the button. If not set or false, a text link is used
                    'options' => array('class'=>'copy'), // HTML options for the button
                ),
            ),
        ),
    )

));

?>
