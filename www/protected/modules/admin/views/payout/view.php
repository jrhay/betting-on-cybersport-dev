<?php
/* @var $this PayoutController */
/* @var $model Payout */

$this->breadcrumbs=array(
	'Payouts'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Payout', 'url'=>array('index')),
	array('label'=>'Create Payout', 'url'=>array('create')),
	array('label'=>'Update Payout', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Payout', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Payout', 'url'=>array('admin')),
);
?>

<h1>View Payout #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'date',
		'system_id',
		'summ',
		'e_wallet',
		'user_id',
		'status',
	),
)); ?>
