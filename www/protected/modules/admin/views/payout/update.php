<?php
/* @var $this PayoutController */
/* @var $model Payout */

$this->breadcrumbs=array(
	'Payouts'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Payout', 'url'=>array('index')),
	array('label'=>'Create Payout', 'url'=>array('create')),
	array('label'=>'View Payout', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Payout', 'url'=>array('admin')),
);
?>

<h1>Update Payout <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>