<?php
/* @var $this SupportController */
/** @var TicketStatus[] $ticketStatuses */
/** @var User[] $users */

$this->breadcrumbs = array(
    'Support',
);
?>
    <h1><?php echo Yii::t('site','Support Tickets')?></h1>

    <!--create tabs for widget-->
<?php
$tabs = array();
/** @var TicketCategories[] $ticketCategories */
/** @var Ticket[] $tickets */
foreach ($ticketCategories as $ct) {
    $content = '';
    $tabs[] = array(
        'title' => $ct->name,
        'view' => 'tab',
        'data' => array(
            'tickets' => $tickets[$ct->id],
            'ticketStatuses' => $ticketStatuses,
            'users' => $users,
        )
    );
}
?>
<?php
$this->widget('CTabView', array(
    'tabs' => $tabs
));
?>