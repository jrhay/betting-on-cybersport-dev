<h4><?php echo  Yii::t('site', 'Get statistics for employee'); ?></h4>
<h5><?php echo  Yii::t('site', 'select employee:'); ?></h5>
<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'statistics-employee-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // See class documentation of CActiveForm for details on this,
    // you need to use the performAjaxValidation()-method described there.
    'enableAjaxValidation' => false,
    'action' => Yii::app()->createUrl('admin/support/statistics')
)); ?>
<div class="row">
    <?php $this->widget('CAutoComplete',
        array(
            'model' => 'user',
            'name' => 'userName',
            'url' => array('jackpot/userautocomplete'),
            'minChars' => 2,
            'value' => isset($userName) ? $userName: '',
            'htmlOptions' => array(
                'required' => 'required',
                'style' => 'margin-left: 1.1em'
            ),
        )
    );?>
</div>
<?php echo CHtml::submitButton(Yii::t('site', 'O.K.'), array(
    'class' => 'btn btn-primary',
    'style' => 'margin-left: 0'
)); ?>
<br>
<br>
<?php  if (!empty($employeeId)) : ?>
    <?php
    $selectedEmployee = User::model()->findByPk($employeeId);
    ?>
    <div style="color: green">
        <h5><?php echo  Yii::t('site', "tickets closed by $selectedEmployee->userName : "); ?><?php echo  count($tickets) ?></h5>
    </div>
<?php  endif ?>

<br>
<?php $this->endWidget(); ?>
<table class="statistics-table">
    <thead>
    <tr>
        <td><?php echo  Yii::t('site', '№') ?></td>
        <td><?php echo  Yii::t('site', 'Subject') ?></td>
        <td><?php echo  Yii::t('site', 'text') ?></td>
        <td><?php echo  Yii::t('site', 'closed by') ?></td>
    </tr>
    </thead>
    <?php  /** @var Ticket[] $tickets */ ?>
    <?php  foreach ($tickets as $ticket): ?>

            <tr>

                <td class="grey number">
                    <a href="<?php echo  Yii::app()->createUrl('admin/support/ticket').'?id='.$ticket->id?>">
                    <?php echo  $ticket->id ?>
                    </a>
                </td>
                <td class="grey subject">
                    <?php echo  $ticket->title ?>
                </td>
                <td class="grey text">
                <span>
                <?php echo  substr($ticket->text, 0, 250) ?>
                </span>
                </td>
                <td class="green by">
                    <?php  $employee = User::model()->findByAttributes(array('id' => $ticket->employee_id));
                    echo $employee->userName;?>
                </td>
            </tr>

    <?php  endforeach ?>

    </thead>
</table>