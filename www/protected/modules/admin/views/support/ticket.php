<?php  /** @var Ticket $ticket */
/** @var TicketStatus[] $ticketStatuses */
/** @var User $employee */
/** @var CActiveForm $form */
/** @var TicketPersonalMessages[] $ticketPersonalMessages */
/** @var PersonalMessage $model */
?>

<h1><?php echo  Yii::t('site', 'Ticket №') ?> <?php echo  $ticket->id ?></h1>
<h3> <?php echo  $ticket->title ?></h3>
<?php
$status = '';
foreach ($ticketStatuses as $ts) {
    if ($ts->id === $ticket->status_id) {
        $status = $ts->name;
    }
}
?>
<h5> <?php echo  Yii::t('site', 'status') ?> : <?php echo  $status ?></h5>
<?php  if ($employee): ?>
    <h5> <?php echo  Yii::t('site', 'Employee') ?> : <?php echo  $employee->userName ?></h5>
<?php  endif ?>

<span class="span-whites" style="margin-bottom: 5%">
<?php echo  $ticket->text ?>
</span>
<a class="btn btn-primary"
   href="<?php echo  Yii::app()->createUrl('admin/support/attach') . '?id=' . $ticket->id ?>"><?php echo  Yii::t('site', 'Attach') ?></a>
<a class="btn btn-primary"
   href="<?php echo  Yii::app()->createUrl('admin/support/close') . '?id=' . $ticket->id ?>"><?php echo  Yii::t('site', 'Close') ?></a>
<a class="btn btn-primary"
   href="<?php echo  Yii::app()->createUrl('admin/support/delete') . '?id=' . $ticket->id ?>"><?php echo  Yii::t('site', 'Delete') ?></a>
<div style="margin-left: 8em">
    <h4><?php echo  Yii::t('site', 'or'); ?></h4>
</div>
<h4><?php echo  Yii::t('site', 'Attach to another employee'); ?></h4>
<h5><?php echo  Yii::t('site', 'select employee:'); ?></h5>
<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'attach-ticket-to-employee-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // See class documentation of CActiveForm for details on this,
    // you need to use the performAjaxValidation()-method described there.
    'enableAjaxValidation' => false,
    'action' => Yii::app()->createUrl('admin/support/attachToEmployee')
)); ?>
<div class="row">
    <?php $this->widget('CAutoComplete',
        array(
            'model' => 'user',
            'name' => 'userName',
            'url' => array('jackpot/userautocomplete'),
            'minChars' => 2,
            'htmlOptions' => array(
                'required' => 'required',
                'style' => 'margin-left: 1.1em'
            ),
        )
    );?>
</div>
<input type="hidden" name="id" value="<?php echo  $ticket->id ?>">
<?php echo CHtml::submitButton(Yii::t('site', 'Attach to employee'), array(
    'class' => 'btn btn-primary',
    'style' => 'margin-left: 0'
)); ?>
<br>
<?php $this->endWidget(); ?>
<h4 class="margin-tb-5"><?php echo  Yii::t('site', 'Correspondence'); ?></h4>

<div class="correspondence">
    <?php  foreach ($ticketPersonalMessages as $tpm): ?>
        <div class="message">
            <div class="info">
                <div class="sender">
                    <?php echo  $tpm->personalMessage->sender->userName ?>
                </div>
                <div class="created">
                    <?php echo  $tpm->personalMessage->created ?>
                </div>
            </div>
            <div class="text">
                <?php echo  $tpm->personalMessage->text ?>
            </div>
        </div>
    <?php  endforeach ?>
    <br>
</div>
<br>
<br>
<h4  style="margin-top: 10%;margin-bottom: 3%;margin-left: 0%"><?php echo Yii::t('site', 'New Message '); ?></h4>
<?php  include('personalMessageForm.php')?>


