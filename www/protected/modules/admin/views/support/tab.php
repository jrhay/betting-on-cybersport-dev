<ul class="ticket-list">
    <?php  /** @var Ticket[] $tickets */ ?>
    <?php  /** @var TicketStatus[] $ticketStatuses */ ?>
    <?php  foreach ($tickets as $ticket): ?>
        <li>
            <?php
            $status = '';
            foreach ($ticketStatuses as $ts) {
                if ($ts->id === $ticket->status_id) {
                    $status = $ts->name;
                }
            }
            $color = '';
            switch ($ticket->status_id) {
                case TicketStatus::OPEN :
                    $color = 'red';
                    break;
                case TicketStatus::ATTACHED :
                    $color = 'blue';
                    break;
                case TicketStatus::CLOSED :
                    $color = 'green';
                    break;
            }
            ?>
            <a style="color: <?php echo  $color ?>"
               href="<?php echo  Yii::app()->createUrl('admin/support/ticket') . '?id=', $ticket->id ?>">
                <!--                --><?php  // echo Yii::t('site', ' № ') ?>
                <span>№</span>
                <?php echo  $ticket->id ?>
                <!--                --><?php  // echo Yii::t('site', 'subject') ?>
                <?php echo  $ticket->title ?>
                <!--                --><?php  // echo Yii::t('site', 'status') ?>
                <span class="status">(<?php echo  $status ?>
                    <?php
                    $employeerUserName = '';
                    if (!empty($ticket->employee_id)) {
                        foreach ($users as $user) {
                            if ($user->id === $ticket->employee_id) {
                                $employeerUserName = $user->userName;
                                echo "<span>by $employeerUserName</span>";
                            }
                        }

                    }
                    ?>
                    )</span>


            </a>
        </li>
    <?php  endforeach ?>
</ul>