<?php
/* @var $this IndexSlidesController */
/* @var $model IndexSlides */

$this->breadcrumbs=array(
	'Index Slides'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List IndexSlides', 'url'=>array('index')),
	array('label'=>'Manage IndexSlides', 'url'=>array('admin')),
);
?>

<h1>Create IndexSlides</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>