<?php
/* @var $this IndexSlidesController */
/* @var $model IndexSlides */

$this->breadcrumbs=array(
	'Index Slides'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List IndexSlides', 'url'=>array('index')),
	array('label'=>'Create IndexSlides', 'url'=>array('create')),
	array('label'=>'View IndexSlides', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage IndexSlides', 'url'=>array('admin')),
);
?>

<h1>Update IndexSlides <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>