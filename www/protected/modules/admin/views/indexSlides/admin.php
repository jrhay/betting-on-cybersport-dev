<?php
/* @var $this IndexSlidesController */
/* @var $model IndexSlides */

$this->breadcrumbs=array(
	'Index Slides'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List IndexSlides', 'url'=>array('index')),
	array('label'=>'Create IndexSlides', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#index-slides-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Index Slides</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
<br>
<a href="<?php echo Yii::app()->createUrl('admin/IndexSlides/create')?>" class="btn btn-primary" style="float: right;margin: 1%"> Create New Slide</a>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'index-slides-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'slide',
        'slide_eng',
		'title',
        'title_eng',
		'text',
        'text_eng',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
<br>
<br>
<iframe frameBorder="0"  height="2000px" style="font-size: 10px; width:100%" src="<?php echo  Yii::app()->createUrl('/admin/IndexMainSlides/admin')?>">

</iframe>
