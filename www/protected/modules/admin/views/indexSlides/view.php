<?php
/* @var $this IndexSlidesController */
/* @var $model IndexSlides */

$this->breadcrumbs=array(
	'Index Slides'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List IndexSlides', 'url'=>array('index')),
	array('label'=>'Create IndexSlides', 'url'=>array('create')),
	array('label'=>'Update IndexSlides', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete IndexSlides', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage IndexSlides', 'url'=>array('admin')),
);
?>

<h1>View IndexSlides #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'slide',
		'title',
        'title_eng',
		'text',
        'text_eng'
	),
)); ?>
