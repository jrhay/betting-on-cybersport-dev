<?php
/* @var $this TournamentResultController */
/* @var $data TournamentResult */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tournament_id')); ?>:</b>
	<?php echo CHtml::encode($data->tournament_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('time')); ?>:</b>
	<?php echo CHtml::encode($data->time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('winner_id')); ?>:</b>
	<?php echo CHtml::encode($data->winner_id); ?>
	<br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('winner_koef')); ?>:</b>
    <?php echo CHtml::encode($data->winner_koef); ?>
    <br />


</div>