<?php
/* @var $this TournamentResultController */
/* @var $model TournamentResult */

$this->breadcrumbs = array(
    'Tournament Results' => array('index'),
    'Create',
);

$this->menu = array(
    array('label' => 'List TournamentResult', 'url' => array('index')),
    array('label' => 'Manage TournamentResult', 'url' => array('admin')),
);
?>

    <h1>Create TournamentResult</h1>

<?php $this->renderPartial('_form', array(
    'model' => $model,
    'teams' => $teams,
    'tournaments' => $tournaments)); ?>
    <h1>Статистика</h1>
<?php echo
CHtml::activeDropDownList($tournamentsModel, 'name', CHtml::listData(Tournaments::model()->findAll(), 'id', 'name')); ?>
    <br>
    <div id="statistics"></div>
<?php  Yii::app()->clientScript->registerScript(uniqid(), '
$("#Tournaments_name").change(function(){
    var selectedValue = this.value;
    $.ajax({
        url: "statistics",
        data: "id=" + selectedValue,
        success: function(data){
        var containerDiv = document.getElementById("statistics");
        containerDiv.innerHTML = data;
  }
});
    return false;
});
'); ?>