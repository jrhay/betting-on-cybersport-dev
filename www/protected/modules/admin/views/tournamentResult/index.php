<?php
/* @var $this TournamentResultController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tournament Results',
);

$this->menu=array(
	array('label'=>'Create TournamentResult', 'url'=>array('create')),
	array('label'=>'Manage TournamentResult', 'url'=>array('admin')),
);
?>

<h1>Tournament Results</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
