<?php
/**
 * Created by PhpStorm.
 * User: lldl
 * Date: 23.12.14
 * Time: 0:37
 */
/** @var Tournaments $tournament */
/** @var integer $countAllBets */
/** @var integer $countAllMoney */
/** @var integer[] $countBets */
/** @var integer[] $countMoney */
?>

<?php
// всего ставок
// всего денег


// имя турнира
// таблица
// команда /сколько всего ставок/ /всего денег поставлено/ /процент от общего числа денег/ /базовый коэф/ /текущий коэф/


?>
<h1> <?php echo  Yii::t('admin', 'Статистика по турниру') ?> <?php echo  $tournament->name ?></h1>
<h5> <?php echo  Yii::t('admin', 'Всего ставок') ?> : <?php echo  $countAllBets ?></h5>
<h5><?php echo  Yii::t('admin', 'Всего поставлено') ?> : <?php echo  $countAllMoney ?></h5>
<hr>
<h1 align="center"> <?php echo  Yii::t('admin', 'Статистика по командам турнира') ?> <?php echo  $tournament->name ?></h1>
<table class="statistics-table">
    <tr>
        <th>
            <?php echo  Yii::t('admin', 'Название команды') ?>
        </th>
        <th>
            <?php echo  Yii::t('admin', 'Колличество ставок') ?>
        </th>
        <th>
            <?php echo  Yii::t('admin', 'На команду поставлено') ?>
        </th>
        <th>
            <?php echo  Yii::t('admin', '% от общей суммы ставок') ?>
        </th>
<!--        <th>-->
<!--            --><?php //= Yii::t('admin', 'Начальный коэф') ?>
<!--        </th>-->
        <th>
            <?php echo  Yii::t('admin', 'Текущий коэф') ?>
        </th>
    </tr>
    <?php foreach($tournament->tournamentsdetails as $detail):?>
    <tr>
        <td><?php echo $detail->team->name?></td>
        <td><?php echo $countBets[$detail->teamId]?></td>
        <td><?php echo $countMoney[$detail->teamId]?></td>
        <td><?php
            if($countAllMoney) {
                echo $countBets[$detail->teamId] * 100 / $countAllMoney;
            } else {
                echo '0';
            }
            ?>%</td>
<!--        <td>--><?php //$detail-> ?><!--</td>-->
        <td><?php echo $detail->teamKoef?></td>
    </tr>
    <?php endforeach?>
</table>

