<?php
/* @var $this TournamentResultController */
/* @var $model TournamentResult */

$this->breadcrumbs=array(
	'Tournament Results'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TournamentResult', 'url'=>array('index')),
	array('label'=>'Create TournamentResult', 'url'=>array('create')),
	array('label'=>'View TournamentResult', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage TournamentResult', 'url'=>array('admin')),
);
?>

<h1>Update TournamentResult <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array(
    'model'=>$model,
    'teams' => $teams,
    'tournaments' => $tournaments)); ?>
<form>
	<label for="changeTournamentSelect"></label>
	<select id="changeTournamentSelect">
		<option value='1'> Первое</option>
		<option value='2'> Второе</option>
	</select>
</form>