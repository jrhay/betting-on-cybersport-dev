<?php
/* @var $this TournamentResultController */
/* @var $model TournamentResult */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'tournament-result-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    )); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>
    <?php
    $tournamentsForSelect = array();
    /** @var Tournaments[] $tournaments */
    foreach ($tournaments as $tournament) {
        $tournamentsForSelect[$tournament->id] = $tournament->name;
    }
    ?>
    <div class="row">
        <?php echo $form->labelEx($model, 'tournament_id'); ?>
        <?php echo $form->dropDownList($model, 'tournament_id', $tournamentsForSelect); ?>
        <?php echo $form->error($model, 'tournament_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'start_time'); ?>
        <?php
        Yii::import('application.extensions.yiiDateTimePicker.CJuiDateTimePicker');
        $this->widget('CJuiDateTimePicker',
            array(
                'name' => 'TournamentResult[time]',
                'id' => 'recommendTimeInput',
                'mode' => 'datetime',
                'value' => date("Y-m-d H:i:s", $this->action->id == "create" ? time() : $model->time),
                'options' => array(
                    'dateFormat' => 'yy-mm-dd',
                    'timeFormat' => 'hh:mm:ss',
//                'minDate' => 'Today'
                ),
                'htmlOptions' => array(
                    'autocomplete' => 'off',
                    'required' => true,

                ),
                'language' => 'ru'
            ));
        ?>

        <?php $teamsForSelect = array();
        /** @var Tournaments[] $tournaments */
        foreach ($teams as $team) {
            $teamsForSelect[$team->id] = $team->name;
        }?>
        <div class="row" style="margin-left: 0.1%">
            <?php echo $form->label($model, 'winner_id'); ?>
            <?php echo $form->dropDownList($model, 'winner_id', $teamsForSelect, array(
                'required' => true,
            )); ?>
        </div>

        <?php if(!isset($model->tournament) or $model->tournament->isFloatingKoef):?>
        <div class="row" style="margin-left: 0.1%">
            <?php echo $form->labelEx($model, 'winner_koef'); ?>
            <?php echo $form->textField($model, 'winner_koef',array(
                'required' => true,
                'value' => ' '
            )); ?>
            <?php echo $form->error($model, 'winner_koef'); ?>
        </div>
        <?php  endif?>

        <div class="row buttons">
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
        </div>

        <?php $this->endWidget(); ?>

    </div>
    <!-- form -->