<?php
/* @var $this TournamentResultController */
/* @var $model TournamentResult */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    )); ?>

<!--    <div class="row">-->
<!--        --><?php //echo $form->label($model, 'id'); ?>
<!--        --><?php //echo $form->textField($model, 'id', array('size' => 20, 'maxlength' => 20)); ?>
<!--    </div>-->

    <?php
    $tournamentsForSelect = array();
    /** @var Tournaments[] $tournaments */
    foreach ($tournaments as $tournament) {
        $tournamentsForSelect[$tournament->id] = $tournament->name;
    }
    ?>
    <div class="row">
        <?php echo $form->label($model, 'tournament_id'); ?>
        <?php echo $form->dropDownList($model, 'tournament_id', $tournamentsForSelect); ?>
    </div>

<!--    <div class="row">-->
<!--        --><?php //echo $form->label($model, 'time'); ?>
<!--        --><?php //echo $form->textField($model, 'time', array('size' => 20, 'maxlength' => 20)); ?>
<!--    </div>-->
    <?php $teamsForSelect = array();
    /** @var Tournaments[] $tournaments */
    foreach ($teams as $team) {
        $teamsForSelect[$team->id] = $team->name;
    }?>
    <div class="row">
        <?php echo $form->label($model, 'winner_id'); ?>
        <?php echo $form->dropDownList($model, 'winner_id',$teamsForSelect); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->