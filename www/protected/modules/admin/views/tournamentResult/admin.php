<?php
/* @var $this TournamentResultController */
/* @var $model TournamentResult */

$this->breadcrumbs = array(
    'Tournament Results' => array('index'),
    'Manage',
);

$this->menu = array(
    array('label' => 'List TournamentResult', 'url' => array('index')),
    array('label' => 'Create TournamentResult', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#tournament-result-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Tournament Results</h1>

<?php echo CHtml::link('Advanced Search', '#', array('class' => 'search-button')); ?>
<div class="search-form" style="display:none">
    <?php $this->renderPartial('_search', array(
        'model' => $model,
        'tournaments' => $tournaments,
        'teams' => $teams,
    )); ?>
</div><!-- search-form -->
<?php  $dataProvider = new CActiveDataProvider('TournamentResult') ?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'tournament-result-grid',
    'dataProvider' => $model->search(),
//    'filter' => $model,
    'columns' => array(
        'id',
        array(
            'name' => 'tournament',
            'value' => '$data->tournament->name'),
        array(
            'name' => 'time',
            'value' => 'date("Y-m-d H:i:s", $data->time)'),
        array(
            'name' => 'winner',
            'value' => '$data->winner->name'),
        array(
            'name' => 'winner koef',
            'value' => '$data->winner_koef'),
        array(
            'class' => 'CButtonColumn',
        ),
    ),
)); ?>
