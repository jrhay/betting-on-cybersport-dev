<?php
/* @var $this TournamentResultController */
/* @var $model TournamentResult */

$this->breadcrumbs=array(
	'Tournament Results'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List TournamentResult', 'url'=>array('index')),
	array('label'=>'Create TournamentResult', 'url'=>array('create')),
	array('label'=>'Update TournamentResult', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TournamentResult', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TournamentResult', 'url'=>array('admin')),
);
?>

<h1>View TournamentResult #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		array(
            'name' => 'tournament',
            'value' => $model->tournament->name),
		array(
            'name' => 'time',
            'value' => gmdate("Y-m-d H:i:s",$model->time)
        ),
		array(
            'name' => 'winner',
            'value' => $model->winner->name,
        ),
        array(
            'name' => 'winner koef',
            'value' => $model->winner_koef,
        ),
	),
)); ?>
