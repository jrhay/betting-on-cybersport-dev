<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
    Yii::t('admin','Team Players')=>array('index'),
    Yii::t('admin','Create'),
);
?>

<h1><?php echo Yii::t('admin','Create Team Player') ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>