<?php
/* @var $this UserController */
/* @var $model User */
$this->breadcrumbs=array(
    Yii::t('admin', 'Manage Teams')=>array('index'),
    Yii::t('admin', 'Manage'),
);
?>

<h1><?php echo Yii::t('admin','Manage Team Players') ?></h1>
<ul class="ui-buttons-panel">
    <li>
        <?php echo CHtml::link(Yii::t('admin', '+ Add a New Team'), array('create'), array('class' => "btn btn-primary")); ?>
    </li>
</ul>
<?php $this->widget('application.widgets.grid.GGridView', array(
    'id'=>'user-grid',
    'itemsCssClass'=> 'table',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'pager' => array(
        'header' => '',
    ),
    'template' => '{items}{pager}',
    'columns'=>array(
        'id',
        'team_id'=>array('name'=>'team_id','filter'=>CHtml::listData(Teams::model()->findAll(),'id','name'),
            'value'=>'$data->team->name'),
        'user_id'=>array('name'=>'user_id','filter'=>CHtml::listData(User::model()->findAll(),'id','userName'),
        'value'=>'$data->user->userName'),
            array(
                'header' => Yii::t('admin','Actions'),
                'class' => 'CButtonColumn',
                'headerHtmlOptions' => array('width' => '20%'),
                'buttons' => array(
                'delete' => array(
                    'visible'=>'true',
                    'imageUrl' => null,
                    'options' => array(
                        'class' => 'btn-delete btn',
                    ),
                    'click' => "function(){
                            if (confirm('" . Yii::t('admin', 'Do you really want to delete this team player?') . "')){
                                return true;
                            } else {
                                return false;
                            }
                        }",
//                    'url' => 'array("/admin/teamPlayers/delete","id"=>$data->id)'
                ),
                'view' => array(
                    'visible' => 'false',
                ),
                'update' => array(

                    'imageUrl' => null,
                    'options' => array(
                        'class' => 'btn btn-primary'
                    ),
                    'url' => 'array("/admin/teamPlayers/update","id"=>$data->id)'))
        ),

    ),
    'theadCssClass' => 'ContentAdmin'
)); ?>
