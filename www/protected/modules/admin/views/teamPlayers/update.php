<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	Yii::t('admin','Team Players')=>array('index'),
    $model->user_id=>array('index'),
    Yii::t('admin','Update'),
);
?>

<h1><?php echo Yii::t('admin','Update Team Player') ?> - <?php echo $model->user_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>