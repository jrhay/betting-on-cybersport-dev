<?php
/* @var $this EmailController */
/* @var $model Email */
$this->breadcrumbs = array(
    Yii::t('admin_home', 'Email templates') => array('index'),
    Yii::t('admin_home', 'Manage'),
);
$baseUrl = Yii::app()->theme->baseUrl;
Yii::app()->clientScript->registerCssFile($baseUrl . '/assets/css/jquery/jquery.dataTables.css');
?>
<h1><?php echo Yii::t('admin_home', 'Email template manager'); ?></h1>

<?php
$this->widget('application.widgets.grid.GGridView', array(
    'id' => 'admin_wrapper',
    'dataProvider' => $model->with('lang_name')->search(),
    'filter' => $model,
    'template' => '{summary}{items}{pager}',
    'columns' => array(
        array(
            'name' => 'code',
            'headerHtmlOptions' => array('width' => '10%'),
        ),
        'subject',
        'language_search' => array(
            'name' => 'language_search',
            'value' => '$data->lang_name->name',
            'filter' => CHtml::listData(Languages::model()->findAll(), 'name', 'name')
        ),
        array(
            'header' => Yii::t('admin_home','Actions'),
            'class' => 'CButtonColumn',
            'headerHtmlOptions' => array('width' => '20%'),
            'buttons' => array(
                'delete' => array(
                    'visible' => 'false',
                ),
                'view' => array(
                    'visible' => 'false',
                ),
                'update' => array(
                    'imageUrl' => null,
                    'options' => array(
                        'class' => 'btn btn-primary'
                    ),
                    'url' => 'array("/admin/email/update","id"=>$data->id,"lang_id"=>$data->lang_id)'))
        ),
    ),
    'itemsCssClass' => 'table',
    'theadCssClass' => 'ContentAdmin'
));
?>