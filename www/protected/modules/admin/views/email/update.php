<?php
/* @var $this ContentController */
/* @var $model Content */

$this->breadcrumbs=array(
    Yii::t('admin_home', 'Email templates')=>array('index'),
    Yii::t('admin_home', 'Update').' '.$model->subject,
);
?>
<h1><?php echo Yii::t('admin_home', 'Update email') ?> - <?php echo $model->subject; ?></h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'lang'=>$lang)); ?>