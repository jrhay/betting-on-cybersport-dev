<?php
/* @var $this EmailController */
/* @var $model Email */
/* @var $form CActiveForm */
$baseUrl = Yii::app()->theme->baseUrl;
Yii::app()->clientScript->registerCssFile($baseUrl . '/assets/css/forms.css');
?>

<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'email-form',
    'enableAjaxValidation' => true,
    'htmlOptions' => array('class' => 'form'),
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'validateOnChange' => true,
        'beforeValidate' => 'js:function(){tinyEditor_Email_body.post(); return true;}',
    ),
        ));
?>

<p class="note"><?php echo Yii::t('admin_home','Fields with <span class="required">*</span> are required.') ?></p>
<fieldset>
    <?php //echo $form->errorSummary($model);   ?>
    <div class="row"><?php echo $form->labelEx($model, 'code'); ?>
        <?php echo $model->code; ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'lang_id'); ?>

        <?php echo $form->dropDownList($model, 'lang_id', CHtml::listData($lang, 'id', 'name')); ?>
        <?php echo $form->error($model, 'lang_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'subject'); ?>
        <?php echo $form->textField($model, 'subject', array('size' => 60, 'maxlength' => 255, 'class' => 'w2')); ?>
        <?php echo $form->error($model, 'subject'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'body'); ?>
        <?php
        $this->widget('application.widgets.tinyeditor.ETinyEditor', array(
            'model' => $model,
            'attribute' => 'body',
            'options' => array(
                'width' => '100%',
                'height' => '450px',
                'class' => "w2",
                'cssclass' => 'tinyeditor',
                'controlclass' => 'tinyeditor-control',
                'controls' => array('bold', 'italic', 'underline', 'strikethrough', '|', 'subscript', 'superscript', '|', 'orderedlist', 'unorderedlist', '|', 'outdent', 'indent', '|', 'leftalign', 'centeralign', 'rightalign', 'blockjustify', '|', 'unformat', '|', 'undo', 'redo', 'n', 'font', 'size', 'style', '|', 'image', 'hr', 'link', 'unlink', '|', 'print'),
                'toggle' => array('text' => 'source', 'activetext' => 'wysiwyg', 'cssclass' => 'toggle'),
                'footer' => 'true',
            ),
        ));
        ?>
        <?php echo $form->error($model, 'body'); ?>
        <br/>
        <?php echo Yii::t('admin_home', $model->hint); ?>
    </div>

    
</fieldset>
<fieldset>
    <div class="row">
        <?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('admin_home', 'Create') : Yii::t('admin_home', 'Save'), array('class' => 'btn btn-primary')); ?>
        <?php echo CHtml::link(Yii::t('admin_home', 'Cancel'), array('/admin/email/index'), array('class' => 'btn',)); ?>
    </div>
</fieldset>
<?php echo $form->hiddenField($model, 'code'); ?>
<?php echo $form->hiddenField($model, 'hint'); ?>
<?php $this->endWidget(); ?>

