<?php
/* @var $this ContentController */
/* @var $model Content */

$this->breadcrumbs=array(
    Yii::t('admin_home','Contents')=>array('index'),
    Yii::t('admin_home','Create '),
);
?>
<h1><?php echo Yii::t('admin_home','Create Content') ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model, 'lang'=>$lang)); ?>