<?php
/* @var $this TournamentsbetsController */
/* @var $model Tournamentsbets */

$this->breadcrumbs=array(
	'Tournamentsbets'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Tournamentsbets', 'url'=>array('index')),
	array('label'=>'Manage Tournamentsbets', 'url'=>array('admin')),
);
?>

<h1>Create Tournamentsbets</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>