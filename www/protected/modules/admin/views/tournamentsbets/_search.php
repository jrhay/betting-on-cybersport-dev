<?php
/* @var $this TournamentsbetsController */
/* @var $model Tournamentsbets */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>


	<div class="row">
		<?php echo $form->label($model,'tournamentId'); ?>
		<?php echo $form->textField($model,'tournamentId'); ?>
	</div>



	<div class="row">
		<?php echo $form->label($model,'koef'); ?>
		<?php echo $form->textField($model,'koef'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'summ'); ?>
		<?php echo $form->textField($model,'summ'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'status'); ?>
		<?php echo $form->textField($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'isWin'); ?>
		<?php echo $form->textField($model,'isWin'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->