<?php
/* @var $this TournamentsbetsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tournamentsbets',
);

$this->menu=array(
	array('label'=>'Create Tournamentsbets', 'url'=>array('create')),
	array('label'=>'Manage Tournamentsbets', 'url'=>array('admin')),
);
?>

<h1>Tournamentsbets</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
