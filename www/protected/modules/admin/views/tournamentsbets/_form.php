<?php
/* @var $this TournamentsbetsController */
/* @var $model Tournamentsbets */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'tournamentsbets-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    )); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
            <?php echo $form->label($model, 'userId'); ?>
            <?php  $this->widget('CAutoComplete',
                array(
                    'model' => 'user',
                    'value' => isset($model->user->userName) ? $model->user->userName : '',
                    'name' => 'Tournamentsbets[user_id]',
                    'url' => array('tournamentsbets/userautocomplete'),
                    'minChars' => 2,
                    'htmlOptions' => array('required' => 'required'),
                )
            );?>
        <?php echo $form->error($model, 'userId'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'tournamentId'); ?>
        <?php echo $form->textField($model, 'tournamentId'); ?>
        <?php echo $form->error($model, 'tournamentId'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'teamId'); ?>
        <?php  $this->widget('CAutoComplete',
            array(
                'model' => 'Team',
                'value' => isset($model->team->name) ? $model->team->name : '',
                'name' => 'Tournamentsbets[team_id]',
                'url' => array('tournamentsbets/teamsautocomplete'),
                'minChars' => 2,
                'htmlOptions' => array('required' => 'required'),
            )
        );?>
        <?php echo $form->error($model, 'teamId'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'koef'); ?>
        <?php echo $form->textField($model, 'koef'); ?>
        <?php echo $form->error($model, 'koef'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'summ'); ?>
        <?php echo $form->textField($model, 'summ'); ?>
        <?php echo $form->error($model, 'summ'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'status'); ?>
        <?php echo $form->textField($model, 'status'); ?>
        <?php echo $form->error($model, 'status'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'isWin'); ?>
        <?php echo $form->textField($model, 'isWin'); ?>
        <?php echo $form->error($model, 'isWin'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->