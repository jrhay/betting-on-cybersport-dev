<?php
/* @var $this TournamentsbetsController */
/* @var $model Tournamentsbets */

$this->breadcrumbs=array(
	'Tournamentsbets'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Tournamentsbets', 'url'=>array('index')),
	array('label'=>'Create Tournamentsbets', 'url'=>array('create')),
	array('label'=>'Update Tournamentsbets', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Tournamentsbets', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Tournamentsbets', 'url'=>array('admin')),
);
?>

<h1>View Tournamentsbets #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'userId',
		'tournamentId',
		'teamId',
		'koef',
		'summ',
		'status',
		'isWin',
	),
)); ?>
