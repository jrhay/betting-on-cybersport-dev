<?php
/* @var $this TournamentsbetsController */
/* @var $model Tournamentsbets */

$this->breadcrumbs=array(
	'Tournamentsbets'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Tournamentsbets', 'url'=>array('index')),
	array('label'=>'Create Tournamentsbets', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#tournamentsbets-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Tournamentsbets</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
<ul class="ui-buttons-panel">
    <li>
        <?php echo CHtml::link(Yii::t('admin', '+ Add a New tournaments'), array('create'), array('class' => "btn btn-primary")); ?>
    </li>
</ul>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'tournamentsbets-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		array(
            'header' => 'userName',
            'value' => '$data->user->userName'
        ),
		'tournamentId',
        array(
            'header' => 'team',
            'value' => '$data->team->name'
        ),
		'koef',
		'summ',
		/*
		'status',
		'isWin',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
