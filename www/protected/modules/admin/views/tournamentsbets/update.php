<?php
/* @var $this TournamentsbetsController */
/* @var $model Tournamentsbets */

$this->breadcrumbs=array(
	'Tournamentsbets'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Tournamentsbets', 'url'=>array('index')),
	array('label'=>'Create Tournamentsbets', 'url'=>array('create')),
	array('label'=>'View Tournamentsbets', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Tournamentsbets', 'url'=>array('admin')),
);
?>

<h1>Update Tournamentsbets <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>