<?php 
$this->breadcrumbs = array(
    Yii::t('admin', 'Translations') => array('index'),
    Yii::t('admin', 'Manage Messages'),
);
?>

<h1><?php echo Yii::t('admin','Manage i18n content') ?></h1>

<?php
$baseUrl = Yii::app()->theme->baseUrl;
Yii::app()->clientScript->registerScriptFile($baseUrl.'/js/jquery.form.js');
Yii::app()->clientScript->registerCssFile($baseUrl . '/css/modal-custom.css');


$language_count = Languages::model()->enabled()->count();
if (Yii::app()->user->getState('language_filter', '')) {
    $language_count = 1;
}
$source = MessageSource::model()->findAll();
$languages = array('') + CHtml::listData(Languages::model()->findAll(), 'dialect', 'name');
$this->widget('application.widgets.grid.GGridView', array(
    'id' => 'message-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'itemsCssClass'=> 'table',
    'theadCssClass' => 'ContentAdmin',
    'template' => '{summary}{items}{pager}',
    'rowCssClassExpression' => function ($row, $data) use($language_count) {
        $rowCssClass = array('odd', 'even');
        return $rowCssClass[$row % 2] . ($language_count != $data->translation ? ' incompleted' : '');
    },
    'columns' => array( 
        array(
            'name' => 'category',
            'filter' => CHtml::listData($source, 'category', 'category'),
            'headerHtmlOptions' => array('width'=>'10%'),
        ),
        array(
            'name' => 'message',            
            'headerHtmlOptions' => array('width'=>'32%'),
        ), 
        /*
        array(
            'name' => 'translation',
            'headerHtmlOptions' => array('width'=>'32%'),
        ),
        array(
            'name' => 'language',
            'filter' => CHtml::listData(Languages::model()->findAll(), 'dialect', 'name'),      
            'headerHtmlOptions' => array('width'=>'5%'),
        ),
        */          
        array(
            'filter' => CHtml::dropDownList('missing', Yii::app()->user->getState('missing', 0), array(0=>Yii::t('admin','All'), 1=>Yii::t('admin','Missing translations')), array('id' => 'missing')).
                        CHtml::dropDownList('language_filter', Yii::app()->user->getState('language_filter', ''), $languages, array('id' => 'language_filter')),
            'class' => 'AdminCButtonColumn',
            'headerHtmlOptions' => array('width'=>'16%'),
            'template' => '{update}',//{delete}
            'buttons' => array(
                /*
                'delete' => array(                    
                    'imageUrl' => null,
                    'options' => array(
                        'class' => 'btn-common-sm yellow',
                    ),
                    'click' => "function(){
                            if (confirm('" . Yii::t('admin', 'Do you really want to delete this message?') . "')){
                                return true;
                            } else {
                                return false;
                            }
                        }",
                    'url' => 'Yii::app()->getController()->createUrl("delete",array("id"=>$data->id,"language"=>$data->language))'
                ),                
                */
                'update' => array(
                    'imageUrl' => null,
                    'options' => array(
                        'class' => 'btn btn-primary update-message'
                    ),
                    'url' => 'Yii::app()->getController()->createUrl("update",array("id"=>$data->id,"language"=>$data->language))'
                )
            ),            
        )
    ),
));


Yii::app()->clientScript->registerScript(uniqid(),"
$('a.update-message').live('click', function () {
    $.post(
        this.href,
        {},
        function (data) {
            $('#update-dialog-container').html(data);
            $('#update-dialog').dialog('open');
        },
        'html'
    );
    return false;
});

$('#message-form').live('submit', function () {
    $(this).ajaxSubmit({
        success:function () {            
            jQuery('#message-grid').yiiGridView('update');
            return false;
        },
        complete:function () {
            $('#update-dialog').dialog('close');
        },
        clearForm:true,
        resetForm:true
    });
    return false;
});
",CClientScript::POS_END
);

$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'update-dialog',
    'cssFile' => null,
    'options' => array(
        'title' => Yii::t('admin', 'Update message'),
        'autoOpen' => false,
        'modal' => true,
        'hide' => 'drop',
        'show' => 'drop',
        'width' => 700,
        'resizable' => false,
        'buttons' => array(
            Yii::t('admin', 'Save') => 'js:function(){ $("#message-form").submit();}',
            Yii::t('admin', 'Cancel') => 'js:function(){ $(this).dialog("close");}',
        ),
    ),
));
?>
<div id="update-dialog-container"></div>
<?php
$this->endWidget('application.widgets.jui.MGHJuiDialog');
?>
