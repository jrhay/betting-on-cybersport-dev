<?php 
$this->breadcrumbs = array(
    Yii::t('admin', 'Translations') => array('index'),
    Yii::t('admin', 'Missing Translations'),
);
?>

<h1><?php echo Yii::t('admin', 'Missing Translations')?></h1>

<?php
$baseUrl = Yii::app()->theme->baseUrl;
Yii::app()->clientScript->registerCssFile($baseUrl . '/assets/css/jquery/jquery.dataTables.css');
Yii::app()->clientScript->registerScriptFile($baseUrl.'/assets/js/jquery.form.js');
Yii::app()->clientScript->registerCssFile($baseUrl.'/assets/css/jquery/jquery-ui-custom.css');

$source = MessageSource::model()->findAll();
$this->widget('application.widgets.grid.GGridView', array(
    'id' => 'message-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'itemsCssClass' => 'dataTable',
    'theadCssClass' => 'ContentAdmin',
    'template' => '{summary}{items}{pager}',
    'columns' => array(
        array(
            'name' => 'message',
            //'filter' => CHtml::listData($source, 'message', 'message'),
            'headerHtmlOptions' => array('width' => '70%'),
        ),
        array(
            'name' => 'category',
            'filter' => CHtml::listData($source, 'category', 'category'),
            'headerHtmlOptions' => array('width' => '14%'),
        ),
        array(
            'header' => CHtml::dropDownList('mp-translate', $this->getLanguage(), CHtml::listData(Languages::model()->findAll(), 'dialect', 'name'), array('id' => 'mp-translate')),
            'class' => 'CButtonColumn',
            'headerHtmlOptions' => array('width' => '16%'),
            'template' => '{create}{delete}',
            'buttons' => array(
                'delete' => array(
                    'imageUrl' => null,
                    'options' => array(
                        'class' => 'btn-common-sm yellow',
                    ),
                    'click' => "function(){
                            if (confirm('" . Yii::t('admin', 'Do you really want to delete this translation?') . "')){
                                return true;
                            } else {
                                return false;
                            }
                        }",
                    'url' => 'Yii::app()->getController()->createUrl("missingdelete",array("id"=>$data->id))'
                ),
                'create' => array(
                    'label' => Yii::t('admin', 'Create'),
                    'options' => array(
                        'class' => 'btn-common-sm create-message'
                    ),
                    'url' => 'Yii::app()->getController()->createUrl("create",array("id"=>$data->id,"language"=>Yii::app()->controller->getLanguage()))'
                )
            ),
        )
    ),
));

Yii::app()->getClientScript()->registerScript('mp-translate-dropdown', '
    $("#mp-translate").change(function(){
         $.post(
             "' . Yii::app()->getController()->createUrl("/admin/translate/set") . '",
             {"mp-translate":$(this).val()},
             function(){window.top.location.reload();}
     )});
 ');

Yii::app()->clientScript->registerScript(uniqid(),"
$('a.create-message').live('click', function () {
    $.post(
        this.href,
        {},
        function (data) {
            $('#create-dialog-container').html(data);
            $('#create-dialog').dialog('open');
        },
        'html'
    );
    return false;
});

$('#message-form').live('submit', function () {
    $(this).ajaxSubmit({
        success:function () {            
            jQuery('#message-grid').yiiGridView('update');            
            return false;
        },
        complete:function () {
            $('#create-dialog').dialog('close');
        },
        clearForm:true,
        resetForm:true
    });
    return false;
});
",CClientScript::POS_END
);

$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'create-dialog',
    'cssFile' => null,
    'options' => array(
        'title' => Yii::t('admin', 'Create translation'),
        'autoOpen' => false,
        'modal' => true,
        'hide' => 'drop',
        'show' => 'drop',
        'width' => 700,
        'resizable' => false,
        'buttons' => array(
            Yii::t('admin', 'Save') => 'js:function(){ $("#message-form").submit();}',
            Yii::t('admin', 'Cancel') => 'js:function(){ $(this).dialog("close");}',
        ),
    ),
));
?>
<div id="create-dialog-container"></div>
<?php
$this->endWidget('application.widgets.jui.MGHJuiDialog');
?>