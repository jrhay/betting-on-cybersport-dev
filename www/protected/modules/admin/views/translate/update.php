<div class="form">
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'message-form',
    'enableAjaxValidation' => true,
    'clientOptions'        => array(
        'validateOnSubmit' => true,
        'validateOnChange' => false,
    ),
    'htmlOptions'          => array('class' => 'form-inline no-tabs', 'enctype'=>'multipart/form-data',)
));
foreach($models as $i=>$model) { ?>
    <?php echo $form->hiddenField($model, '['.$i.']id', array('size' => 10, 'maxlength' => 10)); ?>
    <?php echo $form->hiddenField($model, '['.$i.']language', array('size' => 16, 'maxlength' => 16)); ?>
    <?php if (!$i) { ?>
    <div class="row">
        <label ><?php echo MessageSource::model()->getAttributeLabel('category').' '. $model->source->category; ?></label>
        <br/>
    </div>
    <?php } ?>
    <div class="row">
        <label class="required" for="Message_translation"><?php echo $model->lang->name;?> </label><br/>
        <?php //echo $form->labelEx($model, 'translation'); ?>
        <?php echo $form->textArea($model, '['.$i.']translation', array('rows' => 2, 'cols' => 80, 'style'=>'height: 50px;')); ?>
        <?php echo $form->error($model, '['.$i.']translation'); ?>
    </div>

    <?php 
}
$this->endWidget(); ?>
</div>