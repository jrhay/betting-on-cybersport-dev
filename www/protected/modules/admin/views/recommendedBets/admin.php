<?php
/* @var $this RecommendedBetsController */
/* @var $model RecommendedBets */

$this->breadcrumbs=array(
	'Recommended Bets'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List RecommendedBets', 'url'=>array('index')),
	array('label'=>'Create RecommendedBets', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#recommended-bets-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Recommended Bets</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'recommended-bets-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		array(
            'name' => 'date',
            'value' => 'date(\'d.m.y\',$data->date)'
        ),
		'text',
		'koef',
		'summ',
        array(
            'header' => 'Name',
            'value' => '$data->game->name'
        ),
		array(
			'name' => 'game_id',
			'value' => '$data->game->id'
		),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
