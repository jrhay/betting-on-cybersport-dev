<?php
/* @var $this RecommendedBetsController */
/* @var $model RecommendedBets */

$this->breadcrumbs=array(
	'Recommended Bets'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List RecommendedBets', 'url'=>array('index')),
	array('label'=>'Manage RecommendedBets', 'url'=>array('admin')),
);
?>

<h1>Create RecommendedBets</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>