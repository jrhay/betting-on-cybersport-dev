<?php
/* @var $this RecommendedBetsController */
/* @var $model RecommendedBets */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'recommended-bets-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'koef'); ?>
        <?php echo $form->textField($model,'koef'); ?>
        <?php echo $form->error($model,'koef'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'summ'); ?>
        <?php echo $form->textField($model,'summ'); ?>
        <?php echo $form->error($model,'summ'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'game_id'); ?>
        <?php
        $gamesModels = Games::model()->findAllByAttributes(array('status' => Games::GAME_STATUS_NOT_STARTED));
        $games = array();
        /** @var Games $gamesModels */
        foreach($gamesModels as $gm) {
            $games[$gm->id] = $gm->name;
        }
        ?>
        <?php echo $form->dropDownList($model,'game_id', $games, array(
            'required'=> true,
        )); ?>
        <?php echo $form->error($model,'game_id'); ?>
    </div>

<!--	<div class="row">-->
<!--		--><?php //echo $form->labelEx($model,'date'); ?>
<!--        --><?php
//        Yii::import('application.extensions.yiiDateTimePicker.CJuiDateTimePicker');
//        $this->widget('CJuiDateTimePicker',
//            array(
//                'name' => 'RecommendedBets[date]',
//                'id' => 'recommendTimeInput',
//                'mode' => 'datetime',
//                'value' => date("Y-m-d H:i:s", $model->date),
//                'options' => array(
//                    'dateFormat' => 'yy-mm-dd',
//                    'timeFormat' => 'hh:mm:ss',
////                'minDate' => 'Today'
//                ),
//                'htmlOptions' => array(
//                    'autocomplete' => 'off',
//                    'required' => true,
//
//                ),
//                'language' => 'ru'
//            ));
//        ?>
<!--		--><?php //echo $form->error($model,'date'); ?>
<!--	</div>-->

	<div class="row">
		<?php echo $form->labelEx($model,'text'); ?>
		<?php echo $form->textArea($model,'text',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'text'); ?>
	</div>



	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->