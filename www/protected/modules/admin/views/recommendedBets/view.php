<?php
/* @var $this RecommendedBetsController */
/* @var $model RecommendedBets */

$this->breadcrumbs = array(
    'Recommended Bets' => array('index'),
    $model->id,
);

$this->menu = array(
    array('label' => 'List RecommendedBets', 'url' => array('index')),
    array('label' => 'Create RecommendedBets', 'url' => array('create')),
    array('label' => 'Update RecommendedBets', 'url' => array('update', 'id' => $model->id)),
    array('label' => 'Delete RecommendedBets', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Are you sure you want to delete this item?')),
    array('label' => 'Manage RecommendedBets', 'url' => array('admin')),
);
?>

<h1>View RecommendedBets #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'id',
        array(
            'name' => 'date',
            'value' => date('d.m.y',$model->date)
        ),
        'text',
        'koef',
        'summ',
        array(
            'name' => 'game',
            'value' => $model->game->name
        )
    ),
)); ?>
