<?php
/* @var $this RecommendedBetsController */
/* @var $model RecommendedBets */

$this->breadcrumbs=array(
	'Recommended Bets'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List RecommendedBets', 'url'=>array('index')),
	array('label'=>'Create RecommendedBets', 'url'=>array('create')),
	array('label'=>'View RecommendedBets', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage RecommendedBets', 'url'=>array('admin')),
);
?>

<h1>Update RecommendedBets <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>