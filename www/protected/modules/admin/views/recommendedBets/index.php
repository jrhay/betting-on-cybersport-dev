<?php
/* @var $this RecommendedBetsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Recommended Bets',
);

$this->menu=array(
	array('label'=>'Create RecommendedBets', 'url'=>array('create')),
	array('label'=>'Manage RecommendedBets', 'url'=>array('admin')),
);
?>

<h1>Recommended Bets</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
