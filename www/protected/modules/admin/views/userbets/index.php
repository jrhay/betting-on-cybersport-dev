<?php
/* @var $this UserController */
/* @var $model User */
$this->breadcrumbs = array(
    'User Bets' => array('index'),
    'Manage',
);
/** @var Userbets $model */
?>

<h1><?php echo Yii::t('admin', 'Manage Users Bets') ?></h1>
<ul class="ui-buttons-panel">
    <li>
        <?php echo CHtml::link(Yii::t('admin', '+ Add a New Userbets'), array('create'), array('class' => "btn btn-primary")); ?>
    </li>
</ul>
<?php $this->widget('application.widgets.grid.GGridView', array(
    'id' => 'usercash-grid',
    'itemsCssClass' => 'table',
    'dataProvider' => $model->searchOrderDesc(),
    'filter' => $model,
    'pager' => array(
        'header' => '',
    ),
    'template' => '{items}{pager}',
    'columns' => array(
        'id',
        'betType',
        'User Name' => array('name' => 'userName', 'value' => '$data->user->userName'),
        'Game Name' => array('name' => 'gameName', 'value' => '$data->game->name'),
        'koef',
        'moneyType' => array('name' => 'moneyType', 'filter' => Usercash::getMoneyTypeFilter(), 'value' => 'Usercash::alias("MoneyType",$data->moneyType)'),
        'result' => array('name' => 'result', 'filter' => Userbets::getResultFilter(), 'value' => 'Userbets::alias("Result",$data->result)'),
        'isWin' => array('name' => 'isWin', 'filter' => Userbets::getisWinFilter(), 'value' => 'Userbets::alias("isWin",$data->isWin)'),
        'status' => array('name' => 'status', 'filter' => Userbets::getStatusFilter(),
            'value' => 'Userbets::alias("Status",$data->status)'),
        'summ',

        array(
            'header' => Yii::t('admin', 'Actions'),
            'class' => 'CButtonColumn',
            'headerHtmlOptions' => array('width' => '20%'),
            'buttons' => array(
                'delete' => array(
                    'visible' => (function ($i, $bet) {
                        if ($bet->status != CybController::DELETED_STATUS) {
                            return true;
                        } else {
                            return false;
                        }
                    }),
                    'imageUrl' => null,
                    'options' => array(
                        'class' => 'btn-delete btn',
                    ),
                    'click' => "function(){
                            if (confirm('" . Yii::t('admin', 'Do you really want to delete this user?') . "')){
                                return true;
                            } else {
                                return false;
                            }
                        }",
                    'url' => 'array("/admin/userbets/delete","id"=>$data->id)'
                ),
                'view' => array(
                    'visible' => 'false',
                ),
                'update' => array(

                    'imageUrl' => null,
                    'options' => array(
                        'class' => 'btn btn-primary'
                    ),
                    'url' => 'array("/admin/userbets/update","id"=>$data->id)'))
        ),

    ),
    'theadCssClass' => 'ContentAdmin'
)); ?>
