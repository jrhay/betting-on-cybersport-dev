<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Users bets'=>array('index'),
    $model->userId=>array('index'),
	'Update',
);

?>

<h1>Update Bet of user #<?php echo $model->userId; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>