<?php
/* @var $this BonusController */
/* @var $model Bonus */
/* @var $form CActiveForm */
?>

    <div class="form">

        <?php $form = $this->beginWidget('CActiveForm', array(
            'id' => 'bonus-addPersonalBonus-form',
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // See class documentation of CActiveForm for details on this,
            // you need to use the performAjaxValidation()-method described there.
            'enableAjaxValidation' => false,
        )); ?>

        <p class="note">Fields with <span class="required">*</span> are required.</p>

        <?php echo $form->errorSummary($model); ?>

        <div class="row">
            <?php echo $form->labelEx($model, 'user_id'); ?>
            <?php $this->widget('CAutoComplete',
                array(
                    'model' => 'user',
                    'name' => 'userName',
                    'url' => array('bonuses/userautocomplete'),
                    'minChars' => 2,
                    'htmlOptions' => array('required' => 'required'),
                )
            );?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model, 'value'); ?>
            <?php echo $form->textField($model, 'value'); ?>
            <?php echo $form->error($model, 'value'); ?>
        </div>

        <?php
        /** @var BonusCategory[] $ct */
        $ct = array();
        /** @var BonusCategory[] $bonusCategories */
        foreach($bonusCategories as $ctg) {
            $ct[$ctg->id] = $ctg->category_name;
        }
        ?>
        <div class="row">
            <?php echo $form->labelEx($model, 'bonus_category_id'); ?>
            <?php echo $form->dropDownList($model, 'bonus_category_id',$ct, array(
                'required' => true
            )); ?>
            <?php echo $form->error($model, 'bonus_catetory_id'); ?>
        </div>

<!--        <div class="row">-->
<!--            --><?php //echo $form->labelEx($model, 'bonus_category_id'); ?>
<!--            --><?php //$this->widget('CAutoComplete',
//                array(
//                    'model' => 'BonusCategories',
//                    'name' => 'category_name',
//                    'url' => array('bonuses/categoriesautocomplete'),
//                    'minChars' => 2,
//                    'htmlOptions' => array('required' => 'required')
//                )
//            );?>
<!--        </div>-->

        <div class="row">
            <?php echo $form->labelEx($model, 'end_date'); ?>
            <?php
            Yii::import('application.extensions.yiiDateTimePicker.CJuiDateTimePicker');
            $this->widget('CJuiDateTimePicker',
                array(
                    'name' => 'Bonus[end_date]',
                    'id' => 'recommendTimeInput',
                    'mode' => 'datetime',
                    'options' => array(
                        'dateFormat' => 'yy-mm-dd',
                        'timeFormat' => 'hh:mm:ss',
                        'minDate' => 'Today'),
                    'htmlOptions' => array(
                        'autocomplete' => 'off',
                        'required' => true,
                    ),
                    'language' => 'ru'
                ));
            ?>
            <?php echo $form->error($model, 'end_date'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model, 'title'); ?>
            <?php echo $form->textField($model, 'title'); ?>
            <?php echo $form->error($model, 'title'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model, 'text'); ?>
            <?php echo $form->textArea($model, 'text'); ?>
            <?php echo $form->error($model, 'text'); ?>
        </div>

        <div class="row buttons">
            <?php echo CHtml::submitButton(Yii::t('site', 'submit')); ?>
        </div>

        <?php $this->endWidget(); ?>

    </div><!-- form -->
<?php  if (!empty($addBonusResult)) : ?>
    <?php  if ($addBonusResult['result']) : ?>
        <br>
        <div style="color: green"><?php echo  Yii::t('admin', 'Message is successfully sent!') ?></div>
    <?php  else : ?>
        <br>
        <div style="color: red"><?php echo  Yii::t('admin', 'Error message is not sent!') ?></div>
    <?php  endif ?>
<?php  endif ?>