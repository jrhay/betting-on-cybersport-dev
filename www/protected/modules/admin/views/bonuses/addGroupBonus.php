<?php
/* @var $this GroupBonusController */
/* @var $model GroupBonus */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'group-bonus-addGroupBonus-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// See class documentation of CActiveForm for details on this,
	// you need to use the performAjaxValidation()-method described there.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'value'); ?>
		<?php echo $form->textField($model,'value'); ?>
		<?php echo $form->error($model,'value'); ?>
	</div>

    <?php
    $gBCategories = array();
    /** @var BonusCategory[] $bonusCategories */
    foreach($groupBonusCategories as $ctg) {
        $gBCategories[$ctg->id] = $ctg->name;
    }
    ?>
    <div class="row">
        <?php echo $form->labelEx($model,'percent_to'); ?>
        <?php echo $form->dropDownList($model,'percent_to', $gBCategories); ?>
        <?php echo $form->error($model,'percent_to'); ?>
    </div>

    <?php
    $bCategories = array();
    /** @var BonusCategory[] $bonusCategories */
    foreach($bonusCategories as $ctg) {
        $bCategories[$ctg->id] = $ctg->category_name;
    }
    ?>
	<div class="row">
		<?php echo $form->labelEx($model,'bonus_category_id'); ?>
		<?php echo $form->dropDownList($model,'bonus_category_id', $bCategories); ?>
		<?php echo $form->error($model,'bonus_category_id'); ?>
	</div>

    <div class="row">
        <?php echo $form->labelEx($model, 'start_date'); ?>
        <?php
        Yii::import('application.extensions.yiiDateTimePicker.CJuiDateTimePicker');
        $this->widget('CJuiDateTimePicker',
            array(
                'name' => 'GroupBonus[start_date]',
                'id' => 'recommendTimeInput',
                'mode' => 'datetime',
                'options' => array(
                    'dateFormat' => 'yy-mm-dd',
                    'timeFormat' => 'hh:mm:ss',
                    'minDate' => 'Today'),
                'htmlOptions' => array(
                    'autocomplete' => 'off',
                    'required' => true,
                ),
                'language' => 'ru'
            ));
        ?>
        <?php echo $form->error($model, 'start_date'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'end_date'); ?>
        <?php
        $this->widget('CJuiDateTimePicker',
            array(
                'name' => 'GroupBonus[end_date]',
                'id' => 'recommendTimeInput2',
                'mode' => 'datetime',
                'options' => array(
                    'dateFormat' => 'yy-mm-dd',
                    'timeFormat' => 'hh:mm:ss',
                    'minDate' => 'Today'),
                'htmlOptions' => array(
                    'autocomplete' => 'off',
                    'required' => true,
                ),
                'language' => 'ru'
            ));
        ?>
        <?php echo $form->error($model, 'end_date'); ?>
    </div>

	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title'); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'text'); ?>
		<?php echo $form->textArea($model,'text'); ?>
		<?php echo $form->error($model,'text'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton('Submit'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<?php  if (!empty($addGroupBonusResult)) : ?>
    <?php  if ($addGroupBonusResult['result']) : ?>
        <br>
        <div style="color: green"><?php echo  Yii::t('admin', 'Message is successfully sent!') ?></div>
    <?php  else : ?>
        <br>
        <div style="color: red"><?php echo  Yii::t('admin', 'Error message is not sent!') ?></div>
    <?php  endif ?>
<?php  endif ?>