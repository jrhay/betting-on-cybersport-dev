<?php
/* @var $this PromoCodeController */
/* @var $model PromoCode */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'promo-code-createPromoCod-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // See class documentation of CActiveForm for details on this,
        // you need to use the performAjaxValidation()-method described there.
        'enableAjaxValidation' => false,
    )); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <?php  $categories = array();
        /** @var PromoCodeCategories[] $promoCodeCategories */
        foreach($promoCodeCategories as $ct) {
            $categories[$ct->id] = $ct->name;
        }
    ?>
    <div class="row">
        <?php echo $form->labelEx($model, 'type'); ?>
        <?php echo $form->dropDownList($model, 'type', $categories, array(
            'required'=>true,
        )); ?>
        <?php echo $form->error($model, 'type'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'code'); ?>
        <?php echo $form->textField($model, 'code'); ?>
        <?php echo $form->error($model, 'code'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'value'); ?>
        <?php echo $form->textField($model, 'value'); ?>
        <?php echo $form->error($model, 'value'); ?>
    </div>


    <div class="row buttons">
        <?php echo CHtml::submitButton('Submit'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
<?php  if (!empty($createPromoCodeResult)) : ?>
    <?php  if ($createPromoCodeResult['result']) : ?>
        <br>
        <div style="color: green"><?php echo  Yii::t('admin', 'Promo code is successfully created!') ?></div>
    <?php  else : ?>
        <br>
        <div style="color: red"><?php echo  Yii::t('admin', 'Error , promo code is not created!') ?></div>
    <?php  endif ?>
<?php  endif ?>