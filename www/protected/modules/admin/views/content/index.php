<?php
/* @var $this ContentController */
/* @var $model Content */
$this->breadcrumbs = array(
    Yii::t('admin', 'Content') => array('index'),
    Yii::t('admin', 'Manage'),
);
$baseUrl = Yii::app()->theme->baseUrl;
Yii::app()->clientScript->registerCssFile($baseUrl . '/css/jquery/jquery.dataTables.css');
?>
<h1><?php echo Yii::t('admin','Content manager');?></h1>

<!--    Control buttons -->
<ul class="btn-common inline right">
    <li>
        <?php //echo CHtml::link('+ add new page',array('create'), array('class'=>"btn-common-sm")); ?>
    </li>
</ul>
<!--    Control buttons END -->
<?php
$this->widget('application.widgets.grid.GGridView', array(
    'id' => 'admin_wrapper',
    'itemsCssClass'=> 'table',
    'dataProvider' => $model->with('lang_name')->search(),
    'filter' => $model,
    'template' => '{summary}{items}{pager}',
    'columns' => array(
        array('name'=>'id', 'headerHtmlOptions' => array('width'=>'5%'),),
        'title',
        'language_name' => array(
            'name' => 'language_search', 'value' => '$data->lang_name->name', 'filter' => CHtml::listData(Languages::model()->findAll(), 'name', 'name')),
        array(
            'header' => Yii::t('admin','Actions'),
            'class' => 'CButtonColumn',
            'headerHtmlOptions' => array('width' => '20%'),
            'buttons' => array(
                'delete' => array(
                    'visible' => 'false',
                    'imageUrl' => null,
                    'options' => array(
                        'class' => 'btn-common-sm yellow',
                    ),
                    'click' => "function(){
                            if (confirm('Do you really want to delete this admin?')){
                                return true;
                            } else {
                                return false;
                            }
                        }",
                ),
                'view' => array(
                    'visible' => 'false',
                ),
                'update' => array(
                    'imageUrl' => null,
                    'options' => array(
                        'class' => 'btn btn-primary'
                    ),
                    'url' => 'array("/admin/content/update","id"=>$data->id,"lang_id"=>$data->lang_id)'))
        ),
    ),
    'theadCssClass' => 'ContentAdmin'
));
?>