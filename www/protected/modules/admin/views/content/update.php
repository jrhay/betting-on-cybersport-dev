<?php
/* @var $this ContentController */
/* @var $model Content */

$this->breadcrumbs=array(
    Yii::t('admin', 'Contents')=>array('index'),
    Yii::t('admin', 'Update').' '.$model->title,
);
?>
<h1><?php echo Yii::t('admin', 'Update Content') ?> - <?php echo $model->title; ?></h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'lang'=>$lang)); ?>