<?php
/* @var $this ContentController */
/* @var $model Content */

$this->breadcrumbs=array(
    Yii::t('admin','Contents')=>array('index'),
    Yii::t('admin','Create '),
);
?>
<h1><?php echo Yii::t('admin','Create Content') ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model, 'lang'=>$lang)); ?>