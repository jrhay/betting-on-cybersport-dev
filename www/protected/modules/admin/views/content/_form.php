<?php
/* @var $this ContentController */
/* @var $model Content */
/* @var $form CActiveForm */
$baseUrl = Yii::app()->theme->baseUrl;
Yii::app()->clientScript->registerCssFile($baseUrl . '/css/forms.css');

?>

<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'content-form',
    'enableAjaxValidation' => true,
    'htmlOptions' => array('class' => 'form-inline no-tabs'),
    'clientOptions'        => array(
        'validateOnSubmit' => true,
        'validateOnChange' => true,
        'beforeValidate'=>'js:function(){tinyEditor_Content_content.post(); return true;}',

    ),
        ));
?>

<p class="note"><?php echo Yii::t('admin','Fields with <span class="required">*</span> are required.') ?></p>
<fieldset>
        <?php //echo $form->errorSummary($model);  ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'title'); ?>
        <?php echo $form->textField($model, 'title', array('size' => 60, 'maxlength' => 255, 'class' => 'w2')); ?>
        <?php echo $form->error($model, 'title'); ?>
    </div>
    <div class="row wysiwyg-content">
        <?php echo $form->labelEx($model, 'lang_id'); ?>
        <?php echo $form->dropDownList($model, 'lang_id', CHtml::listData($lang, 'id', 'name')); ?>
        <?php echo $form->error($model, 'lang_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'content'); ?>
        <?php
        $this->widget('application.widgets.tinyeditor.ETinyEditor', array(
            'model' => $model,
            'attribute' => 'content',
            'options' => array(
                'width' => '100%',
                'height' => '450px',
                'class' => "w2",
                'cssclass' => 'tinyeditor',
                'controlclass' => 'tinyeditor-control',
                'controls' => array('bold', 'italic', 'underline', 'strikethrough', '|', 'subscript', 'superscript', '|', 'orderedlist', 'unorderedlist', '|', 'outdent', 'indent', '|', 'leftalign', 'centeralign', 'rightalign', 'blockjustify', '|', 'unformat', '|', 'undo', 'redo', 'n', 'font', 'size', 'style', '|', 'image', 'hr', 'link', 'unlink', '|', 'print'),
                'toggle' => array('text' => 'source', 'activetext' => 'wysiwyg', 'cssclass' => 'toggle'),
                'footer' => 'true',
            ),
        ));
        ?>
    <?php echo $form->error($model, 'content'); ?>
    </div>


</fieldset>
<fieldset>
    <div class="row content-field-buttons">
<?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('admin','Create') : Yii::t('admin','Save'), array()); ?>
<?php echo CHtml::link(Yii::t('admin','Cancel'), array('index'), array('class' => 'btn btn-common',)); ?>
    </div>
</fieldset>

<?php $this->endWidget(); ?>

