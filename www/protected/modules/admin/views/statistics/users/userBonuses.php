<?php
/**@var Bonus[] $bonuses */
?>
<h1><?php echo  Yii::t('site', 'User Bonuses') ?></h1>
<div class="statistics-t-scroll">
    <table class="statistics-bets-t" style="overflow-y: scroll">
        <tr>
            <th><?php echo  Yii::t('site', 'ID') ?></th>
            <th><?php echo  Yii::t('site', 'Category') ?></th>
            <th><?php echo  Yii::t('site', 'Create Date') ?></th>
            <th><?php echo  Yii::t('site', 'End Date') ?></th>
            <th><?php echo  Yii::t('site', 'value') ?></th>
        </tr>
        <?php
        foreach ($bonuses as $bonus):
            $active = '';
            $now = time();
            $endDate = DateTime::createFromFormat('Y-m-d H:i:s', $bonus->end_date)->getTimestamp();
            if($endDate > $now or !$endDate) {
                $active = 'active';
            } else {
                $active = 'inactive';
            }
            ?>
            <tr class="<?php echo $active?>">
                <td><?php echo  $bonus->id ?></td>
                <td><?php echo  $bonus->bonus_category->category_name ?></td>
                <td><?php echo  $bonus->create_date ?></td>
                <td><?php echo  $bonus->end_date ?></td>
                <td><?php echo  $bonus->value ?>$</td>

            </tr>

        <?php  endforeach ?>
    </table>
</div>