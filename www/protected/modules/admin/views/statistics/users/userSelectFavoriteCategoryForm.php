<?php /** @var CActiveForm $form */?>
<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'statistics-category-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // See class documentation of CActiveForm for details on this,
    // you need to use the performAjaxValidation()-method described there.
    'enableAjaxValidation' => false,
    'action' => Yii::app()->createUrl('admin/statistics/users')
)); ?>
<?php
$categories = array();
/** @var GameCategories $gameCategories */
foreach ($gameCategories as $ctg) {
    $categories[$ctg->id] = $ctg->title;
}
?>
    <div class="row">
        <?php echo $form->labelEx($model, 'category_id') ?>
        <?php echo $form->dropDownList($model, 'category_id', $categories) ?>
    </div>
<?php echo CHtml::submitButton(Yii::t('site', 'O.K.'), array(
    'class' => 'btn btn-primary',
    'style' => 'margin-left: 0'
)); ?>
    <br>
    <br>
<?php $this->endWidget(); ?>