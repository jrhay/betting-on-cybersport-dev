<?php
/**@var Games[] $games */
?>
<h1><?php echo  Yii::t('site', 'User Bets on №'.$gameId.' Game') ?></h1>
<div class="statistics-t-scroll">
    <table class="statistics-bets-t" style="overflow-y: scroll">
        <tr>
            <th><?php echo  Yii::t('site', 'ID') ?></th>
            <th><?php echo  Yii::t('site', 'koef') ?></th>
            <th><?php echo  Yii::t('site', 'summ') ?></th>
            <th><?php echo  Yii::t('site', 'result') ?></th>
        </tr>
        <?php
        foreach ($bets as $bet):
            ?>
            <tr>
                <td><?php echo  $bet->id ?></td>
                <td><?php echo  $bet->koef ?></td>
                <td><?php echo  $bet->summ ?>$</td>
                <td><?php  switch($bet->result){
                        case 0: echo 'draw';break;
                        case 1: echo 'team1';break;
                        case 2: echo 'team2';break;
                    }
                    ?></td>

            </tr>

        <?php  endforeach ?>
    </table>
</div>