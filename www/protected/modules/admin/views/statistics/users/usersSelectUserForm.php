<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'statistics-user-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // See class documentation of CActiveForm for details on this,
    // you need to use the performAjaxValidation()-method described there.
    'enableAjaxValidation' => false,
    'action' => Yii::app()->createUrl('admin/statistics/users')
)); ?>
    <div class="row">
        <?php $this->widget('CAutoComplete',
            array(
                'model' => 'user',
                'name' => 'userName',
                'url' => array('statistics/userautocomplete'),
                'minChars' => 2,
                'htmlOptions' => array(
                    'required' => 'required',
                    'style' => 'margin-left: 1.1em'
                ),
            )
        );?>
    </div>
<?php echo CHtml::submitButton(Yii::t('site', 'O.K.'), array(
    'class' => 'btn btn-primary',
    'style' => 'margin-left: 0'
)); ?>
    <br>
    <br>
<?php $this->endWidget(); ?>