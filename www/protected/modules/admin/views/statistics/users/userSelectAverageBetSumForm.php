<?php
/* @var $this PersonalMessageController */
/* @var $model2 PersonalMessage */
/* @var $form CActiveForm */
/* @var Ticket $ticket*/
?>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'personal-message-personalMessageForm-form',
        'action' => Yii::app()->createUrl('admin/statistics/users'),
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // See class documentation of CActiveForm for details on this,
        // you need to use the performAjaxValidation()-method described there.
        'enableAjaxValidation'=>false,
    )); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model2); ?>

    <div class="row">
        <?php echo $form->labelEx($model2,'min_average_sum'); ?>
        <?php echo $form->telField($model2,'min_average_sum'); ?>
        <?php echo $form->error($model2,'min_average_sum'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model2,'max_average_sum'); ?>
        <?php echo $form->textField($model2,'max_average_sum'); ?>
        <?php echo $form->error($model2,'max_average_sum'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton(Yii::t('site','O.K.'), array(
            'class' => 'btn-primary',
            'style' => 'margin-left: 1.5%',
        )); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->