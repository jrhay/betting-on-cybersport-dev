<?php
/** @var User $user */?>
<h1><?php echo Yii::t('site', 'User Statistics')?></h1>
<h4><?php echo Yii::t('site', 'User Info')?></h4>
<br>
<?php echo Yii::t('site', 'ID')?> : <?php echo  $user->id?>
<br>
<?php echo Yii::t('site', 'UserName')?> : <?php echo  $user->userName?>
<br>
<?php echo Yii::t('site', 'First Name')?> : <?php echo  $user->firstName?>
<br>
<?php echo Yii::t('site', 'Last Name')?> : <?php echo  $user->lastName?>
<br>
<?php echo Yii::t('site', 'Email')?> : <?php echo  $user->email?>
<br>
<?php echo Yii::t('site', 'Role')?> : <?php echo  $user->usertype->type?>
<br>
<?php echo Yii::t('site', 'Time Zone')?> : <?php echo  $user->timezone?>
<br>
<?php echo Yii::t('site', 'Balance ( Real Money ) ')?> : <?php echo  $user->usercash->realMoney ?> $
<br>
<?php echo Yii::t('site', 'Balance ( Cyber Money ) ')?> : <?php echo  $user->usercash->cyberMoney ?>
<br>
<?php echo Yii::t('site', 'Bonus Balance')?> : <?php echo  $user->bonus_balance? $user->bonus_balance->balance : 0?> $
<br>
<?php echo Yii::t('site', 'Average Bet Sum')?> : <?php echo  $averageBetSum ?> $
<br>
<?php echo Yii::t('site', 'Max Bet Sum')?> : <?php echo  $maxBetSum ?> $
<br>
<?php echo Yii::t('site', 'Favorite Category')?> : <?php echo isset($favoriteCategory) ?  $favoriteCategory->title : '-' ?> $
<hr>
<h4><?php echo Yii::t('site', 'User footprints')?></h4>
<a href="<?php echo Yii::app()->createUrl('admin/statistics/userGames').'?id='.$user->id ?>"><?php echo Yii::t('site', 'User Games and Bets')?></a>
<br>
<a href="<?php echo Yii::app()->createUrl('admin/statistics/userBonuses').'?id='.$user->id ?>"><?php echo Yii::t('site', 'Current Bonuses')?></a>