<?php
/**@var Games[] $games */
?>
<h1><?php echo  Yii::t('site', 'User Games') ?></h1>
<div class="statistics-t-scroll">
    <table class="statistics-bets-t" style="overflow-y: scroll">
        <tr>
            <th><?php echo  Yii::t('site', 'ID') ?></th>
            <th><?php echo  Yii::t('site', 'Name') ?></th>
            <th><?php echo  Yii::t('site', 'Category') ?></th>
            <th><?php echo  Yii::t('site', 'Start') ?></th>
            <th><?php echo  Yii::t('site', 'End') ?></th>
            <th><?php echo  Yii::t('site', 'Bets') ?></th>
        </tr>
        <?php
        foreach ($games as $game):
            ?>
            <tr>
                <td>
                    <a href="<?php echo  Yii::app()->createUrl('admin/statistics/game') . '?id=' . $game->id ?>"><?php echo  $game->id ?></a>
                </td>
                <td><?php echo  $game->name ?></td>
                <td><?php echo  $game->category->title ?></td>
                <td><?php echo  gmdate("Y-m-d H:i:s ", $game->game_start_at) ?></td>
                <?php  if (isset($game->game_end)): ?>
                    <td><?php echo  gmdate("Y-m-d H:i:s ", $game->game_end) ?>></td>
                <?php  endif ?>
                <td>
                    <a href="<?php echo  Yii::app()->createUrl('admin/statistics/userGameBets') . '?gameId=' . $game->id.'&userId=' . $userId ?>"><?php echo  Yii::t('site','bets') ?></a>
                </td>
            </tr>

        <?php  endforeach ?>
    </table>
</div>