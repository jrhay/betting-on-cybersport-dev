<?php
/** @var Games $game */
/** @var Userbets[] $bets */
?>
<h1><?php echo  Yii::t('site', 'User Deposit List') ?></h1>


<br>
<h4><?php echo Yii::t('site', 'Select User')?></h4>
<?php  include('deposit/depositSelectUserForm.php');?>
<h4><?php echo Yii::t('site', 'Select if user has:')?></h4>
<?php  include('deposit/depositMoneyForm.php');?>
<br>
<?php echo Yii::t('site', 'Count : ') . count($users)?>
<br>
<br>
<div class="statistics-t-scroll">
    <table class="statistics-bets-t" style="overflow-y: scroll">
        <tr>
            <th><?php echo  Yii::t('site', 'ID') ?></th>
            <th><?php echo  Yii::t('site', 'Username') ?></th>
            <th><?php echo  Yii::t('site', 'realMoney') ?></th>
            <th><?php echo  Yii::t('site', 'cyberMoney') ?></th>
        </tr>
        <?php
        foreach ($users as $user):
            ?>
            <tr>
                <td><a href="<?php echo Yii::app()->createUrl('admin/statistics/user').'?id='.$user->id ?>"><?php echo  $user->id ?></a></td>
                <td><?php echo  $user->userName ?></td>
                <td><?php echo  $user->usercash->realMoney ?></td>
                <td><?php echo  $user->usercash->cyberMoney ?></td>
            </tr>

        <?php  endforeach ?>
    </table>
</div>