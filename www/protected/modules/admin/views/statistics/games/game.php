<?php
/** @var Games $game */
?>
<h1>Game № <?php echo  $game->id ?>  <?php echo  $game->name ?> (<?php echo  $game->category->title ?> <?php echo  $game->showStatusTitle() ?>) </h1>
<h4><?php echo  Yii::t('site', 'Game Info') ?>: </h4>
<?php echo  Yii::t('site', 'Game ID ') ?> :
<?php echo  $game->id ?>
<br>
<?php echo  Yii::t('site', 'Game Name ') ?> :
<?php echo  $game->name ?>
<br>
<?php echo  Yii::t('site', 'Game Category ') ?> :
<?php echo  $game->category->title ?>
<br>
<?php echo  Yii::t('site', 'Game start ') ?> :
<?php echo  gmdate("Y-m-d H:i:s ", $game->game_start_at) ?>
<br>
<?php echo  Yii::t('site', 'Game end ') ?> :
<?php echo  gmdate("Y-m-d H:i:s ", $game->game_end) ?>
<br>
<?php echo  Yii::t('site', 'Game created ad ') ?> :
<?php echo  gmdate("Y-m-d H:i:s ", $game->create_at) ?>
<br>
<?php echo  Yii::t('site', 'Margin') ?> : <?php echo  $game->betPercents?> %
<br>
<a href="<?php echo  Yii::app()->createUrl('admin/statistics/bets') . '?game_id=' . $game->id ?>"><?php echo  Yii::t('site', 'Bets info') ?></a>
<br>
<hr>
<h4><?php echo  Yii::t('site', 'Bet') ?>: </h4>

<?php echo  Yii::t('site', 'Total Bet Sum : ') ?> = <?php echo  $team1Sum ?>$ + <?php echo  $team2Sum ?>$ + <?php echo  $drawSum ?>$ = <?php echo  $team1Sum + $team2Sum + $drawSum ?>$
<br>
<?php echo  Yii::t('site', 'Margin') ?> : <?php echo  $game->betPercents?> %
<br>
=> <?php echo  Yii::t('site', 'Profit') ?> : <?php echo  ($team1Sum + $team2Sum + $drawSum) * ($game->betPercents) * 0.01 ?> $
<hr>
<h4><?php echo  Yii::t('site', 'Result') ?>: </h4>

<?php echo  Yii::t('site', 'Who Win') ?> =
<?php  if ($result) {
    switch ($result->result) {
        case Userbets::DRAW_WIN:
            echo('Draw');
            break;
        case Userbets::TEAM1_WIN:
            echo $game->team1->name;
            break;
        case Userbets::TEAM2_WIN:
            echo $game->team2->name;
            break;
    }
} else {
    echo Yii::t('site', 'Game is not finished yet');
}
?>

<?php
    if(!$game->isCanceled())
        include('resultForm.php')
?>


<table class="result-table">
    <tr>
        <td>
            <h4><?php echo  Yii::t('site', 'Team 1') ?>: </h4>
            <?php echo  Yii::t('site', ' Team') ?> : <?php echo  $game->team1->name ?>
            <br>
            <?php echo  Yii::t('site', 'current koef') ?> : <?php echo  $game->team1_koef ?>
            <br>
            <?php echo  Yii::t('site', 'Total bet sum') ?> : <?php echo  $team1Sum ?> $
            <br>
        </td>
        <td>
            <h4><?php echo  Yii::t('site', 'Team 2') ?>: </h4>
            <?php echo  Yii::t('site', ' Team') ?> : <?php echo  $game->team2->name ?>
            <br>
            <?php echo  Yii::t('site', 'current koef') ?> : <?php echo  $game->team2_koef ?>
            <br>
            <?php echo  Yii::t('site', 'Total bet sum') ?> : <?php echo  $team2Sum ?> $
            <br>
        </td>
        <td>
            <h4><?php echo  Yii::t('site', 'Draw') ?>: </h4>
            <br>
            <?php echo  Yii::t('site', 'current koef') ?> : <?php echo  $game->draw_koef ?>
            <br>
            <?php echo  Yii::t('site', 'Total bet sum') ?> : <?php echo  $drawSum ?> $
        </td>
    </tr>
</table>
<hr>
<hr>
<h4><?php echo  Yii::t('site', 'Cancel Game') ?>: </h4>
<br>
    <a href="<?php echo Yii::app()->createUrl('/admin/statistics/cancel',array('id'=>$game->id))?>"><?php echo  Yii::t('game' ,'Cancel Game')?></a>
<hr>