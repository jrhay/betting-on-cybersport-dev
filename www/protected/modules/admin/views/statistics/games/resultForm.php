<?php
/* @var $this GameResultFormController */
/* @var $model GameResultForm */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'game-result-form-resultForm-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// See class documentation of CActiveForm for details on this,
	// you need to use the performAjaxValidation()-method described there.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

    <?php
    $aviableResults = array();
    $aviableResults[Userbets::DRAW_WIN] = 'Draw';
    $aviableResults[Userbets::TEAM1_WIN] = 'Team1';
    $aviableResults[Userbets::TEAM2_WIN] = 'Team2';
    ?>
	<div class="row">
		<?php echo $form->labelEx($model,'result'); ?>
		<?php echo $form->dropDownList($model,'result', $aviableResults); ?>
		<?php echo $form->error($model,'result'); ?>
	</div>
    <div class="row">
        <?php echo $form->labelEx($model,'score'); ?>
        <?php echo $form->textField($model,'score'); ?>
        <?php echo $form->error($model,'score'); ?>
    </div>

<!--	<div class="row">-->
<!--		--><?php //echo $form->labelEx($model,'gameEndTime'); ?>
<!--        --><?php
//        Yii::import('application.extensions.yiiDateTimePicker.CJuiDateTimePicker');
//        $this->widget('CJuiDateTimePicker',
//            array(
//                'name' => 'GameResultForm[gameEndTime]',
//                'value' => $model['gameEndTime'],
//                'id' => 'recommendTimeInput2',
//                'mode' => 'datetime',
//                'options' => array(
//                    'dateFormat' => 'yy-mm-dd',
//                    'timeFormat' => 'hh:mm:ss',
////                'minDate' => 'Today'
//                ),
//                'htmlOptions' => array(
//                    'autocomplete' => 'off',
//                    'required' => true,
//                ),
//                'language' => 'ru'
//            ));
//        ?>
<!--		--><?php //echo $form->error($model,'gameEndTime'); ?>

        <div class="row" style="">
            <?php echo $form->labelEx($model,'videoFrameCode'); ?>
            <?php echo $form->textArea($model,'videoFrameCode', array(
            )); ?>
            <?php echo $form->error($model,'videoFrameCode'); ?>
        </div>
<!--	</div>-->


	<div class="row buttons">
		<?php echo CHtml::submitButton('Submit'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->