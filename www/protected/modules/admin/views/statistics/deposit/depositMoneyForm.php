<?php
/* @var $this PersonalMessageController */
/* @var $moneyFormModel PersonalMessage */
/* @var $form CActiveForm */
/* @var Ticket $ticket*/
?>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'personal-message-personalMessageForm-form',
        'action' => Yii::app()->createUrl('admin/statistics/userDeposit'),
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // See class documentation of CActiveForm for details on this,
        // you need to use the performAjaxValidation()-method described there.
        'enableAjaxValidation'=>false,
    )); ?>


    <?php echo $form->errorSummary($moneyFormModel); ?>

    <div class="row">
        <?php echo $form->labelEx($moneyFormModel,'realMoney'); ?>
        <?php echo $form->checkBox($moneyFormModel,'realMoney'); ?>
        <?php echo $form->error($moneyFormModel,'realMoney'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($moneyFormModel,'cyberMoney'); ?>
        <?php echo $form->checkBox($moneyFormModel,'cyberMoney'); ?>
        <?php echo $form->error($moneyFormModel,'cyberMoney'); ?>
    </div>
    <br>
    <div class="row buttons">
        <?php echo CHtml::submitButton(Yii::t('site','O.K.'), array(
            'class' => 'btn-primary',
            'style' => 'margin-left: 1.5%',
        )); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->