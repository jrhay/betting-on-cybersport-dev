<?php /** @var IncomeDepositeOperations[] $incomeDepositOperations */
?>
<h4><?php echo Yii::t('site', 'Income Deposit Statistics:')?></h4>
<h4><?php echo Yii::t('site', 'Select Time Range')?></h4>
<?php  include('incomeDeposit/incomeDepositSelectTimeForm.php');?>
<h5><?php echo Yii::t('site', 'Sum income Deposit')?>:</h5>
<?php echo $sumDeposit?> $
<h5><?php echo Yii::t('site', 'Average income Deposit')?>:</h5>
<?php echo  $averageDeposit?> $
<h5><?php echo Yii::t('site', 'Deposit operations')?>:</h5>
<?php echo  count($incomeDepositOperations)?>

<div class="statistics-t-scroll">
    <table class="statistics-bets-t" style="overflow-y: scroll">
        <tr>
            <th><?php echo  Yii::t('site', 'ID') ?></th>
            <th><?php echo  Yii::t('site', 'Username') ?></th>
            <th><?php echo  Yii::t('site', 'summ') ?></th>
            <th><?php echo  Yii::t('site', 'time') ?></th>
        </tr>
        <?php foreach ($incomeDepositOperations as $operation):  ?>
            <tr>
                <td><?php echo  $operation->id ?></td>
                <td><a href="<?php echo  Yii::app()->createUrl('admin/statistics/user').'?id='.$operation->user->id?>"><?php echo  $operation->user->userName ?></a></td>
                <td><?php echo  $operation->summ ?></td>
                <td><?php echo  date('d-m-Y H:i:s',$operation->time) ?></td>
            </tr>

        <?php  endforeach ?>
    </table>
</div>