<?php
/**@var User[] $moderators */
?>
<h1><?php echo  Yii::t('site', 'Moderators List') ?></h1>
<div class="statistics-t-scroll">
    <table class="statistics-bets-t" style="overflow-y: scroll">
        <tr>
            <th><?php echo  Yii::t('site', 'ID') ?></th>
            <th><?php echo  Yii::t('site', 'Closed tickets') ?></th>
            <th><?php echo  Yii::t('site', 'userName') ?></th>
            <th><?php echo  Yii::t('site', 'First Name') ?></th>
            <th><?php echo  Yii::t('site', 'Last Name') ?></th>
            <th><?php echo  Yii::t('site', 'Email') ?></th>
        </tr>
        <?php
        foreach ($moderators as $moderator):
            ?>
            <tr>
                <td><a href="<?php echo Yii::app()->createUrl('admin/statistics/user').'?id='.$moderator->id ?>"><?php echo  $moderator->id ?></a></td>
                <td><a href="<?php echo Yii::app()->createUrl('admin/support/statistics').'?userName='.$moderator->userName ?>"><?php echo Yii::t('site','view statistics')?></a></td>
                <td><?php echo  $moderator->userName ?></td>
                <td><?php echo  $moderator->firstName ?></td>
                <td><?php echo  $moderator->lastName ?></td>
                <td><?php echo  $moderator->email ?></td>
            </tr>

        <?php  endforeach ?>
    </table>
</div>