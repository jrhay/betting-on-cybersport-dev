<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'bonus-addPersonalBonus-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // See class documentation of CActiveForm for details on this,
    // you need to use the performAjaxValidation()-method described there.
    'enableAjaxValidation' => false,
)); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'start_time'); ?>
        <?php
        Yii::import('application.extensions.yiiDateTimePicker.CJuiDateTimePicker');
        $this->widget('CJuiDateTimePicker',
            array(
                'name' => 'start_time',
                'id' => 'recommendTimeInput',
                'mode' => 'datetime',
                'options' => array(
                    'dateFormat' => 'yy-mm-dd',
                    'timeFormat' => 'hh:mm:ss',
//                'minDate' => 'Today'
                ),
                'htmlOptions' => array(
                    'autocomplete' => 'off',
                    'required' => true,
                ),
                'language' => 'ru'
            ));
        ?>
        <?php echo $form->labelEx($model, 'end_time'); ?>
        <?php
        $this->widget('CJuiDateTimePicker',
            array(
                'name' => 'end_time',
                'id' => 'recommendTimeInput2',
                'mode' => 'datetime',
                'options' => array(
                    'dateFormat' => 'yy-mm-dd',
                    'timeFormat' => 'hh:mm:ss',
//                'minDate' => 'Today'
                ),
                'htmlOptions' => array(
                    'autocomplete' => 'off',
                    'required' => true,
                ),
                'language' => 'ru'
            ));
        ?>
        <?php echo $form->error($model, 'end_date'); ?>
    </div>
    <div class="row buttons">
        <?php echo CHtml::submitButton(Yii::t('site', 'submit'), array(
            'class' => 'btn btn-primary'
        )); ?>
    </div>

<?php $this->endWidget(); ?>