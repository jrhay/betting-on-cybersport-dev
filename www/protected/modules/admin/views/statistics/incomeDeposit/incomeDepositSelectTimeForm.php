<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'admin-statistics-income-deposit-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // See class documentation of CActiveForm for details on this,
    // you need to use the performAjaxValidation()-method described there.
    'enableAjaxValidation' => false,
)); ?>

    <div class="row">
        <?php echo $form->labelEx($timeRangeFormModel, 'start_time'); ?>
        <?php
        Yii::import('application.extensions.yiiDateTimePicker.CJuiDateTimePicker');
        $this->widget('CJuiDateTimePicker',
            array(
                'name' => 'TimeRangeForm[start_time]',
                'id' => 'recommendTimeInput',
                'value' => $timeRangeForm['start_time'],
                'mode' => 'datetime',
                'options' => array(
                    'dateFormat' => 'yy-mm-dd',
                    'timeFormat' => 'hh:mm:ss',
//                'minDate' => 'Today'
                ),
                'htmlOptions' => array(
                    'autocomplete' => 'off',
                    'required' => true,
                ),
                'language' => 'ru'
            ));
        ?>
        <br><br>
        <?php echo $form->labelEx($timeRangeFormModel, 'end_time'); ?>
        <?php
        $this->widget('CJuiDateTimePicker',
            array(
                'name' => 'TimeRangeForm[end_time]',
                'value' => $timeRangeForm['end_time'],
                'id' => 'recommendTimeInput2',
                'mode' => 'datetime',
                'options' => array(
                    'dateFormat' => 'yy-mm-dd',
                    'timeFormat' => 'hh:mm:ss',
//                'minDate' => 'Today'
                ),
                'htmlOptions' => array(
                    'autocomplete' => 'off',
                    'required' => true,
                ),
                'language' => 'ru'
            ));
        ?>
        <?php echo $form->error($timeRangeFormModel, 'end_date'); ?>
    </div>
    <div class="row buttons">
        <?php echo CHtml::submitButton(Yii::t('site', 'submit'), array(
            'class' => 'btn btn-primary'
        )); ?>
    </div>

<?php $this->endWidget(); ?>