<?php
/** @var Games $game */
/** @var Userbets[] $bets */
?>
<h1 xmlns="http://www.w3.org/1999/html">Bets on Game № <?php echo  $game->id ?>  <?php echo  $game->name ?>
    (<?php echo  $game->category->title ?>) </h1>
<h4><?php echo Yii::t('site','Select user')?></h4>
<?php  include('bets/betsSelectUserForm.php') ?>
<!--<h4>--><?php //=Yii::t('site','Select Category')?><!--</h4>-->
<?php // include('bets/betsSelectCategoryForm.php') ?>
<?php  include('bets/betsSelectTimeForm.php') ?>

<br>
<table class="statistics-bets-t">
    <tr>
        <th><?php echo  Yii::t('site','ID')?></th>
        <th><?php echo  Yii::t('site','UserName')?></th>
        <th><?php echo  Yii::t('site','result')?></th>
        <th><?php echo  Yii::t('site','koef')?></th>
        <th><?php echo  Yii::t('site','sum')?></th>
    </tr>
    <?php
    foreach ($bets as $bet):
        ?>
        <tr>
            <td><?php echo  $bet->id?></td>
            <td><?php echo  $bet->user->userName?></td>
            <td><?php  switch($bet->result){
                    case 0: echo 'draw';break;
                    case 1: echo 'team1';break;
                    case 2: echo 'team2';break;
                }
                ?></td>
            <td><?php echo  $bet->koef?></td>
            <td><?php echo  $bet->summ?>$</td>

            <?php  $bet ?>
        </tr>
    <?php  endforeach ?>
</table>