<?php
/* @var $this StatisticsController
 * @var integer $balance
 */

$this->breadcrumbs = array(
    'Statistics' => array('/admin/statistics'),
    'Site',
);
?>
<h4><?php echo  Yii::t('site', 'Select time') ?></h4>
<?php  include('site/siteSelectTimeForm.php') ?>
<h1><?php echo  Yii::t('site', 'Site Statistics') ?></h1>
<br>
<h3><?php echo Yii::t('site', 'static info')?>:</h3>
<br>
<!--<h4>--><?php //= Yii::t('site', 'Balance') ?><!-- : --><?php //= $balance ?><!--</h4>-->
<!--<h4>--><?php //= Yii::t('site', 'Profit') ?><!-- : --><?php //= $profit ?><!-- $ </h4>-->

<h4><?php echo  Yii::t('site', 'Events') ?> : <?php echo  $eventsCount ?> </h4>
<br>
<h3><?php echo Yii::t('site', 'time-dependent')?>:</h3>
<br>
<h4><?php echo  Yii::t('site', 'Users registered on site') ?> : <?php echo  $countUsers ?> </h4>
<h4><?php echo  Yii::t('site', 'Active Users') ?> : <?php echo  $countActiveUsers ?> </h4>
<h4><?php echo  Yii::t('site', 'Average Margin') ?> : <?php echo  $averageMargin ?> %</h4>
<h4><?php echo  Yii::t('site', 'Games') ?> : <?php echo  $countGames ?></h4>
<h4><?php echo  Yii::t('site', 'Active Games') ?> : <?php echo  $countActiveGames ?> </h4>

<!--<h4>--><?php  //= Yii::t('site', 'Bets')?><!-- : --><?php  //= $betsCount?><!-- </h4>-->
<h4><?php echo  Yii::t('site', 'Ordinars') ?> : <?php echo  $userBetsCount ?> </h4>
<h4><?php echo  Yii::t('site', 'Express Bets') ?> : <?php echo  $expressBetsCount ?> </h4>
<h4><?php echo  Yii::t('site', 'Tournament Bets ') ?> : <?php echo  $tournamentBetsCount ?> </h4>




