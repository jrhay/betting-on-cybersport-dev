<?php
/* @var $this StatisticsController
 * @var Games[] $games*/

$this->breadcrumbs=array(
	'Statistics'=>array('/admin/statistics'),
	'Games',
);
?>
<h1><?php echo Yii::t('site', 'Aviable Games')?></h1>
<div class="ticket-list">
    <h3><?php echo  Yii::t('site','Waiting for results')?></h3>
    <ul>
        <?php
        /** @var Ticket[] $tickets */
        foreach ($games as $game): ?>
            <?php if(!empty($game->result) or $game->status == Games::GAME_STATUS_CANCELED){continue;}?>
            <li>
                <a href="<?php echo  Yii::app()->createUrl('admin/statistics/game').'?id='.$game->id?>">
                    <?php echo  $game->id ?>
                    <?php echo  $game->name ?>
                    <!--                    --><?php //=Yii::t('site','begin:')?>
                    ( <?php echo  $game->category->title ?> <?php echo Yii::t('site','at')?>
                    <?php echo  gmdate("Y-m-d H:i:s ", $game->game_start_at) ?>
                    "<?php echo  $game->team1->name ?>" vs "<?php echo  $game->team2->name?>"
                    )</a>
                <!--                --><?php //=Yii::t('site','Bet on ')?>
            </li>
        <?php  endforeach ?>
    </ul>
    <h3><?php echo  Yii::t('site','Finished Games')?></h3>
    <ul>
        <?php
        /** @var Ticket[] $tickets */
        foreach ($games as $game): ?>
            <?php if(empty($game->result)){continue;}?>
            <li>
                <a href="<?php echo  Yii::app()->createUrl('admin/statistics/game').'?id='.$game->id?>">
                    <?php echo  $game->id ?>
                    <?php echo  $game->name ?>
<!--                    --><?php //=Yii::t('site','begin:')?>
                    ( <?php echo  $game->category->title ?> <?php echo Yii::t('site','at')?>
                    <?php echo  gmdate("Y-m-d H:i:s ", $game->game_start_at) ?>
                    "<?php echo  $game->team1->name ?>" vs "<?php echo  $game->team2->name?>"
                )</a>
<!--                --><?php //=Yii::t('site','Bet on ')?>
            </li>
        <?php  endforeach ?>
    </ul>
    <h3><?php echo  Yii::t('site','Canceled Games')?></h3>
    <ul>
        <?php
        /** @var Ticket[] $tickets */
        foreach ($games as $game): ?>
            <?php if(($game->status != Games::GAME_STATUS_CANCELED)){continue;}?>
            <li>
                <a href="<?php echo  Yii::app()->createUrl('admin/statistics/game').'?id='.$game->id?>">
                    <?php echo  $game->id ?>
                    <?php echo  $game->name ?>
                    <!--                    --><?php //=Yii::t('site','begin:')?>
                    ( <?php echo  $game->category->title ?> <?php echo Yii::t('site','at')?>
                    <?php echo  gmdate("Y-m-d H:i:s ", $game->game_start_at) ?>
                    "<?php echo  $game->team1->name ?>" vs "<?php echo  $game->team2->name?>"
                    )</a>
                <!--                --><?php //=Yii::t('site','Bet on ')?>
            </li>
        <?php  endforeach ?>
    </ul>
</div>

