<h4><?php echo  Yii::t('site', 'Select time') ?></h4>
<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'bonus-addPersonalBonus-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // See class documentation of CActiveForm for details on this,
    // you need to use the performAjaxValidation()-method described there.
    'enableAjaxValidation' => false,
)); ?>

<div class="row">
    <?php echo $form->labelEx($model, 'start_time'); ?>
    <?php
    Yii::import('application.extensions.yiiDateTimePicker.CJuiDateTimePicker');
    $this->widget('CJuiDateTimePicker',
        array(
            'name' => 'start_time',
            'id' => 'recommendTimeInput',
            'mode' => 'datetime',
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'timeFormat' => 'hh:mm:ss',
//                'minDate' => 'Today'
            ),
            'htmlOptions' => array(
                'autocomplete' => 'off',
                'required' => true,
            ),
            'language' => 'ru'
        ));
    ?>
    <br>
    <br>
    <?php echo $form->labelEx($model, 'end_time'); ?>
    <?php
    $this->widget('CJuiDateTimePicker',
        array(
            'name' => 'end_time',
            'id' => 'recommendTimeInput2',
            'mode' => 'datetime',
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'timeFormat' => 'hh:mm:ss',
//                'minDate' => 'Today'
            ),
            'htmlOptions' => array(
                'autocomplete' => 'off',
                'required' => true,
            ),
            'language' => 'ru'
        ));
    ?>
    <?php echo $form->error($model, 'end_date'); ?>
</div>
<div class="row buttons">
    <?php echo CHtml::submitButton(Yii::t('site', 'submit'), array(
        'class' => 'btn btn-primary',
        'style' => 'margin-left: 1.6%'
    )); ?>
</div>

<?php $this->endWidget(); ?>
<?php
/**
 * Created by PhpStorm.
 * User: lldl
 * Date: 10/28/14
 * Time: 10:46 PM
 */ 