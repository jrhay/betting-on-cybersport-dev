<?php
/** @var Games $game */
/** @var Userbets[] $bets */
?>
<h1><?php echo  Yii::t('site', 'User list') ?></h1>


<br>
<h4><?php echo Yii::t('site', 'Select User')?></h4>
<?php  include('users/usersSelectUserForm.php');?>
<h4><?php echo Yii::t('site', 'Select favorite category')?></h4>
<?php  include('users/userSelectFavoriteCategoryForm.php');?>
<h4><?php echo Yii::t('site', 'Select average bet sum')?></h4>
<?php  include('users/userSelectAverageBetSumForm.php');?>
<br>
<div class="statistics-t-scroll">
    <table class="statistics-bets-t" style="overflow-y: scroll">
        <tr>
            <th><?php echo  Yii::t('site', 'ID') ?></th>
            <th><?php echo  Yii::t('site', 'Username') ?></th>
            <th><?php echo  Yii::t('site', 'First Name') ?></th>
            <th><?php echo  Yii::t('site', 'Last Name') ?></th>
            <th><?php echo  Yii::t('site', 'Email') ?></th>
            <th><?php echo  Yii::t('site', 'Registered') ?></th>
        </tr>
        <?php
        foreach ($users as $user):
            ?>
            <tr>
                <td><a href="<?php echo Yii::app()->createUrl('admin/statistics/user').'?id='.$user->id ?>"><?php echo  $user->id ?></a></td>
                <td><?php echo  $user->userName ?></td>
                <td><?php echo  $user->firstName ?></td>
                <td><?php echo  $user->lastName ?></td>
                <td><?php echo  $user->email ?></td>
                <td><?php echo  date('d-m-Y H:i:s',$user->createTime) ?></td>
            </tr>

        <?php  endforeach ?>
    </table>
</div>