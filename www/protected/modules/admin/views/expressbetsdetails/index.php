<?php
/* @var $this UserController */
/* @var $model User */
$this->breadcrumbs=array(
    Yii::t('admin', 'Manage Express Bet Details')=>array('index'),
    Yii::t('admin', 'Manage'),
);
?>

<h1><?php echo Yii::t('admin','Manage Express Bet Details') ?></h1>
<ul class="ui-buttons-panel">
    <li>
        <?php echo CHtml::link(Yii::t('admin', '+ Add a New Express Bet Item'), array('create','expressBetId'=>$model->expressBetId), array('class' => "btn btn-primary")); ?>
    </li>
    <li>
        <?php echo CHtml::link(Yii::t('admin', 'Express Bets List'), array('/admin/expressbets/index'), array('class' => "btn btn-primary")); ?>
    </li>
</ul>
<?php $this->widget('application.widgets.grid.GGridView', array(
    'id'=>'user-grid',
    'itemsCssClass'=> 'table',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'pager' => array(
        'header' => '',
    ),
    'template' => '{items}{pager}',
    'columns'=>array(
        'gameId'=>array('name'=>'gameId','filter'=>CHtml::listData(Games::model()->findAll(),'id','name'),
            'value'=>'$data->game->name'),
        'koef',
        'rateResult' => array('name' => 'rateResult', 'filter' => CybController::getResultFilter(),'value' => 'Controller::alias("Result",$data->rateResult)'),
            array(
                'header' => Yii::t('admin','Actions'),
                'class' => 'CButtonColumn',
                'headerHtmlOptions' => array('width' => '30%'),
                'buttons' => array(
                    'delete' => array(
                        'visible'=>'true',
                        'imageUrl' => null,
                        'options' => array(
                            'class' => 'btn-delete btn',
                        ),
                        'click' => "function(){
                            if (confirm('" . Yii::t('admin', 'Do you really want to delete this express bet item?') . "')){
                                return true;
                            } else {
                                return false;
                            }
                        }",
                        'url' => 'array("/admin/expressbetsdetails/delete","expressBetId"=>$data->id)'
                    ),
                    'view' => array(
                        'visible' => 'false',

                    ),
                'update' => array(

                    'imageUrl' => null,
                    'options' => array(
                        'class' => 'btn btn-primary'
                    ),
                    'url' => 'array("/admin/expressbetsdetails/update","id"=>$data->id)'))
        ),

    ),
    'theadCssClass' => 'ContentAdmin'
)); ?>
