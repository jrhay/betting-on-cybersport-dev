<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */

$baseUrl = Yii::app()->theme->baseUrl;
//Yii::app()->clientScript->registerCssFile($baseUrl.'/assets/css/jquery/jquery.dataTables.css');
Yii::app()->clientScript->registerScriptFile($baseUrl.'/js/jquery.form.js');
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'expressbetsdetails-form',
	'enableAjaxValidation'=>true,
    'clientOptions'        => array(
        'validateOnSubmit' => true,
        'validateOnChange' => true,
    ),
    'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
    ),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'gameId'); ?>
        <?php echo $form->dropDownList($model, 'gameId', CHtml::listData(Games::model()->findAll(),'id','name')); ?>
        <?php echo $form->error($model,'gameId'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'rateResult'); ?>
                <?php echo $form->dropDownList($model, 'rateResult', CybController::getResultFilter()); ?>
        <?php echo $form->error($model,'rateResult'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'koef'); ?>
        <?php echo $form->textField($model,'koef'); ?>
        <?php echo $form->error($model,'koef'); ?>
    </div>
    <div class="row buttons">

		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
        <?php echo CHtml::link(Yii::t('admin', 'Cancel'), array('/admin/expressbetsdetails/index','expressBetId'=>$model->expressBetId), array('class' => 'btn btn-common',)); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->