<?php
/* @var $this TicketCategories */
/* @var $model TicketCategories*/

$this->breadcrumbs=array(
	'Ticket Categories'=>array('index'),
    $model->id=>array('index'),
	'Update',
);

$this->menu=array(
	array('label'=>'List TicketCategoriesController', 'url'=>array('index')),
	array('label'=>'Create TicketCategoriesController', 'url'=>array('create')),
	array('label'=>'View TicketCategoriesController', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage TicketCategoriesController', 'url'=>array('admin')),
);
?>

<h1>Update Category #<?php echo $model->name; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>