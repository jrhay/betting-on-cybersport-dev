<?php
/* @var $this TicketCategoriesController */
/* @var $model TicketCategories */

$this->breadcrumbs=array(
//    'Users'=>array('index'),
    'Create',
);
?>

    <h1>Create TicketCategory</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>