<?php
/* @var $this ReviewsController */
/* @var $model Reviews */

$this->breadcrumbs=array(
	'Reviews'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Reviews', 'url'=>array('index')),
	array('label'=>'Create Reviews', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#reviews-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Reviews</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
<a href="<?php echo Yii::app()->createUrl('admin/reviews/create')?>" class="btn btn-primary" style="float: right;margin: 1%"> Create New Review</a>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'reviews-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'name',
        'name_eng',
//		'title',
//		'title_eng',
		'text',
		'text_eng',
		'team',
        array(
            'name' => 'date',
            'value' => function($data) {
                echo Helper::getDateFormatted(Yii::t('information','Date format 3 ::yyyy-MM-dd HH:mm:ss'), $data->date,'');
            }),
		/*
		'date',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
