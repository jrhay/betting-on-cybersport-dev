<?php
/* @var $this ReviewsController */
/* @var $model Reviews */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'reviews-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

    <div class="row">
        <?php echo $form->labelEx($model,'name_eng'); ?>
        <?php echo $form->textField($model,'name_eng',array('size'=>60,'maxlength'=>255)); ?>
        <?php echo $form->error($model,'name_eng'); ?>
    </div>

<!--	<div class="row">-->
<!--		--><?php //echo $form->labelEx($model,'title'); ?>
<!--		--><?php //echo $form->textArea($model,'title',array('rows'=>6, 'cols'=>50)); ?>
<!--		--><?php //echo $form->error($model,'title'); ?>
<!--	</div>-->
<!---->
<!--	<div class="row">-->
<!--		--><?php //echo $form->labelEx($model,'title_eng'); ?>
<!--		--><?php //echo $form->textArea($model,'title_eng',array('rows'=>6, 'cols'=>50)); ?>
<!--		--><?php //echo $form->error($model,'title_eng'); ?>
<!--	</div>-->

	<div class="row">
		<?php echo $form->labelEx($model,'text'); ?>
		<?php echo $form->textArea($model,'text',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'text'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'text_eng'); ?>
		<?php echo $form->textArea($model,'text_eng',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'text_eng'); ?>
	</div>

    <div class="row">
        <?php echo $form->labelEx($model,'team'); ?>
        <?php echo $form->textField($model,'team'); ?>
        <?php echo $form->error($model,'team'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'date'); ?>
        <?php $this->widget('application.widgets.CJuiDateTimePicker.CJuiDateTimePicker', array(
            'attribute' => 'date',
            'mode' => 'datetime',
            'language' => 'en-GB',
//            'cssFile'     => null,
            'model' => $model,
            'htmlOptions' => array(
                'class' => 'w2',
                'value' => gmdate('Y-m-d H:i:s', $model->date),
            ),
            'options' => array(
                'changeMonth' => 'false',
                'changeYear' => 'false',
                'timeFormat' => 'hh:mm:ss',
                'dateFormat' => 'yy-mm-dd',
                'showButtonPanel' => true,
                'currentText' => Yii::t('doctor_home', 'Current Time'),
                'hourMax' => 23,
                'stepMinute' => 1,
                'showSecond' => false

            ),
        ));?>
        <?php echo $form->error($model, 'game_start_at'); ?>
    </div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->