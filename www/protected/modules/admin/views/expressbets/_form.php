<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */

$baseUrl = Yii::app()->theme->baseUrl;
//Yii::app()->clientScript->registerCssFile($baseUrl.'/assets/css/jquery/jquery.dataTables.css');
Yii::app()->clientScript->registerScriptFile($baseUrl.'/js/jquery.form.js');
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'expressbets-form',
	'enableAjaxValidation'=>true,
    'clientOptions'        => array(
        'validateOnSubmit' => true,
        'validateOnChange' => true,
    ),
    'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
    ),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'userId'); ?>
        <?php echo $form->dropDownList($model, 'userId', CHtml::listData(User::model()->findAll(),'id','userName')); ?>
        <?php echo $form->error($model,'userId'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'summ'); ?>
        <?php echo $form->textField($model,'summ'); ?>
        <?php echo $form->error($model,'summ'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'status'); ?>
        <?php echo $form->dropDownList($model, 'status', CybController::getStatusFilter()); ?>
        <?php echo $form->error($model,'status'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'isWin'); ?>
        <?php echo $form->dropDownList($model, 'isWin', CybController::getisWinFilter()); ?>
        <?php echo $form->error($model,'isWin'); ?>
    </div>


    <div class="row buttons">

		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
        <?php echo CHtml::link(Yii::t('admin', 'Cancel'), array('/admin/expressbets/index'), array('class' => 'btn btn-common',)); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->