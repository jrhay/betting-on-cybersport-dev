<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	Yii::t('admin','Express Bets')=>array('index'),
    $model->id=>array('index'),
    Yii::t('admin','Update'),
);
?>

<h1><?php echo Yii::t('admin','Update express bet #') ?> - <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>