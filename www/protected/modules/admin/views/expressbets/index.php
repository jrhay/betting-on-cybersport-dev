<?php
/* @var $this UserController */
/* @var $model User */
$this->breadcrumbs=array(
    Yii::t('admin', 'Manage Express Bets')=>array('index'),
    Yii::t('admin', 'Manage'),
);
?>

<h1><?php echo Yii::t('admin','Manage Express Bets') ?></h1>
<ul class="ui-buttons-panel">
    <li>
        <?php echo CHtml::link(Yii::t('admin', '+ Add a New Expressbets'), array('create'), array('class' => "btn btn-primary")); ?>
    </li>
</ul>
<?php $this->widget('application.widgets.grid.GGridView', array(
    'id'=>'user-grid',
    'itemsCssClass'=> 'table',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
//    'pager' => array(
//        'header' => '',
//    ),
    'template' => '{items}{pager}',
    'columns'=>array(
        'userId'=>array('name'=>'userId','filter'=>CHtml::listData(User::model()->findAll(),'id','userName'),
            'value'=>'$data->user->userName'),
        'summ',
        'time'=>array('name'=>'time', 'value'=>function($data) {
                echo Helper::getDateFormatted(Yii::t('information','Date format 3 ::yyyy-MM-dd HH:mm:ss'), $data->time,'');
            }),
        'isWin'=> array('name' => 'isWin', 'filter' => CybController::getisWinFilter(),'value' => 'CybController::alias("isWin",$data->isWin)'),
        'status' => array('name' => 'status', 'filter' => CybController::getStatusFilter(),
            'value' => 'CybController::alias("Status",$data->status)'),
            array(
                'header' => Yii::t('admin','Actions'),
                'class' => 'CButtonColumn',
                'headerHtmlOptions' => array('width' => '30%'),
                'buttons' => array(
                    'delete' => array(
                        'visible'=>'true',
                        'imageUrl' => null,
                        'options' => array(
                            'class' => 'btn-delete btn',
                        ),
                        'click' => "function(){
                            if (confirm('" . Yii::t('admin', 'Do you really want to delete this express bet?') . "')){
                                return true;
                            } else {
                                return false;
                            }
                        }",
                        'url' => 'array("/admin/expressbets/delete","id"=>$data->id)'
                    ),
                    'view' => array(
                        'visible' => 'true',
                        'label'=>'Koef List',
                        'imageUrl' => null,
                        'options' => array(
                            'class' => 'btn btn-primary'
                        ),
                        'url' => 'array("/admin/expressbetsdetails/index","expressBetId"=>$data->id)'
                    ),
                'update' => array(

                    'imageUrl' => null,
                    'options' => array(
                        'class' => 'btn btn-primary'
                    ),
                    'url' => 'array("/admin/expressbets/update","id"=>$data->id)'))
        ),

    ),
    'theadCssClass' => 'ContentAdmin'
)); ?>
