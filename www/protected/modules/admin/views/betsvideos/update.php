<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Bets Videos'=>array('index'),
    $model->title=>array('index'),
	'Update',
);
?>

<h1>Update Bet Video - <?php echo $model->title; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>