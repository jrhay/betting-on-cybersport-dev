<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Bets Videos'=>array('index'),
	'Create',
);
?>

<h1>Create Bet Video</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>