<?php
/* @var $this UserController */
/* @var $model User */
$this->breadcrumbs=array(
    'Game Categories'=>array('index'),
    'Manage',
);
?>

<h1><?php echo Yii::t('admin','Manage Game Categories') ?></h1>
<ul class="ui-buttons-panel">
    <li>
        <?php echo CHtml::link(Yii::t('admin', '+ Add a New Game'), array('create'), array('class' => "btn btn-primary")); ?>
    </li>
</ul>
<?php $this->widget('application.widgets.grid.GGridView', array(
    'id'=>'user-grid',
    'itemsCssClass'=> 'table',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
//    'pager' => array(
//        'header' => '',
//    ),
    'template' => '{items}{pager}',
    'columns'=>array(
        'id',
        'title',
        'link',
        'status' => array('name' => 'status', 'filter' => Betsvideos::getStatusFilter(),
            'value' => 'Betsvideos::alias("Status",$data->status)'),
        'lang' => array('name' => 'lang', 'filter' => Betsvideos::getLangFilter(),
            'value' => 'Betsvideos::alias("Lang",$data->lang)'),
        'parse_inter' => array('name' => 'parse_inter', 'filter' => Betsvideos::getParseInterFilter(),
            'value' => 'Betsvideos::alias("ParseInter",$data->parse_inter)'),
            array(
                'header' => Yii::t('admin','Actions'),
                'class' => 'CButtonColumn',
                'headerHtmlOptions' => array('width' => '20%'),
                'buttons' => array(
                'delete' => array(
                    'visible'=>"true",
                    'imageUrl' => null,
                    'options' => array(
                        'class' => 'btn-delete btn',
                    ),
                    'click' => "function(){
                            if (confirm('" . Yii::t('admin', 'Do you really want to delete this user?') . "')){
                                return true;
                            } else {
                                return false;
                            }
                        }",
                    'url' => 'array("/admin/betsvideos/delete","id"=>$data->id)'
                ),
                'view' => array(
                    'visible' => 'false',
                ),
                'update' => array(

                    'imageUrl' => null,
                    'options' => array(
                        'class' => 'btn btn-primary'
                    ),
                    'url' => 'array("/admin/betsvideos/update","id"=>$data->id)'))
        ),

    ),
    'theadCssClass' => 'ContentAdmin'
)); ?>
