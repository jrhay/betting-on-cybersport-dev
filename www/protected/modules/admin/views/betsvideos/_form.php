<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */

$baseUrl = Yii::app()->theme->baseUrl;
//Yii::app()->clientScript->registerCssFile($baseUrl.'/assets/css/jquery/jquery.dataTables.css');
Yii::app()->clientScript->registerScriptFile($baseUrl.'/js/jquery.form.js');
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'betsvideos-form',
	'enableAjaxValidation'=>true,
    'clientOptions'        => array(
        'validateOnSubmit' => true,
        'validateOnChange' => true,
    ),
    'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
    ),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
    <div class="row">
        <?php echo $form->labelEx($model,'title'); ?>
        <?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>255)); ?>
        <?php echo $form->error($model,'title'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'link'); ?>
        <?php echo $form->textField($model,'link'); ?>
        <?php echo $form->error($model,'link'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'status'); ?>
        <?php echo $form->dropDownList($model, 'status', Betsvideos::getStatusFilter()); ?>
        <?php echo $form->error($model, 'status'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'lang'); ?>
        <?php echo $form->dropDownList($model, 'lang', Betsvideos::getLangFilter()); ?>
        <?php echo $form->error($model, 'lang'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'parse_inter'); ?>
        <?php echo $form->dropDownList($model, 'parse_inter', Betsvideos::getParseInterFilter()); ?>
        <?php echo $form->error($model, 'parse_inter'); ?>
    </div>

    <div class="row buttons">

		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
        <?php echo CHtml::link(Yii::t('admin', 'Cancel'), array('/admin/betsvideos/index'), array('class' => 'btn btn-common',)); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->