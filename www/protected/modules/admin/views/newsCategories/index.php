<?php
/* @var $this NewsCategoriesController */
/* @var $model NewsCategories */
$this->breadcrumbs = array(
    'NewsCategoriesController' => array('index'),
    'Manage',
);
?>

<h1><?php echo Yii::t('admin', 'Manage News Categories') ?></h1>
<ul class="ui-buttons-panel">
    <li>
        <?php echo CHtml::link(Yii::t('admin', '+ Add a new Category'), array('create'), array('class' => "btn btn-primary")); ?>
    </li>
</ul>
<?php $this->widget('application.widgets.grid.GGridView', array(
    'id' => 'news-categories-grid',
    'itemsCssClass' => 'table',
    'dataProvider' => $model->search(),
//    'pager' => array(
//        'header' => '',
//    ),
    'template' => '{items}{pager}',
    'columns' => array(
        'id',
        'name',
        array(
            'header' => Yii::t('admin', 'Actions'),
            'class' => 'CButtonColumn',
            'headerHtmlOptions' => array('width' => '20%'),
            'buttons' => array(
                'delete' => array(
                    'visible' => (function ($i, $game) {
                            return true;
                        }),
                    'imageUrl' => null,
                    'options' => array(
                        'class' => 'btn-delete btn',
                    ),
                    'click' => "function(){
                            if (confirm('" . Yii::t('admin', 'Do you really want to delete category?') . "')){
                                return true;
                            } else {
                                return false;
                            }
                        }",
                    'url' => 'array("/admin/newsCategories/delete","id"=>$data->id)'
                ),
                'view' => array(
                    'visible' => 'false',
                ),
                'update' => array(

                    'imageUrl' => null,
                    'options' => array(
                        'class' => 'btn btn-primary'
                    ),
                    'url' => 'array("/admin/newsCategories/update","id"=>$data->id)'))
        ),

    ),
    'theadCssClass' => 'ContentAdmin'
)); ?>
