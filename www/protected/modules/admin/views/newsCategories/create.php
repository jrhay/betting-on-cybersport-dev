<?php
/* @var $this NewsCategoriesController */
/* @var $model NewsCategories */

$this->breadcrumbs=array(
//    'Users'=>array('index'),
    'Create',
);
?>

    <h1>Create NewsCategory</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>