<?php
/* @var $this NewsCategories */
/* @var $model User */

$this->breadcrumbs=array(
	'News Categories'=>array('index'),
    $model->id=>array('index'),
	'Update',
);

$this->menu=array(
	array('label'=>'List NewsCategoriesController', 'url'=>array('index')),
	array('label'=>'Create NewsCategoriesController ', 'url'=>array('create')),
	array('label'=>'View NewsCategoriesController', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage NewsCategoriesController', 'url'=>array('admin')),
);
?>

<h1>Update Category #<?php echo $model->name; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>