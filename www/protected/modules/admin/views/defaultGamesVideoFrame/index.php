<?php
/* @var $this DefaultGamesVideoFrameController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Default Games Video Frames',
);

$this->menu=array(
	array('label'=>'Create DefaultGamesVideoFrame', 'url'=>array('create')),
	array('label'=>'Manage DefaultGamesVideoFrame', 'url'=>array('admin')),
);
?>

<h1>Default Games Video Frames</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
