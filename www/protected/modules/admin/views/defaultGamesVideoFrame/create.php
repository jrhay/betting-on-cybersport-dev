<?php
/* @var $this DefaultGamesVideoFrameController */
/* @var $model DefaultGamesVideoFrame */

$this->breadcrumbs=array(
	'Default Games Video Frames'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List DefaultGamesVideoFrame', 'url'=>array('index')),
	array('label'=>'Manage DefaultGamesVideoFrame', 'url'=>array('admin')),
);
?>

<h1>Create DefaultGamesVideoFrame</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>