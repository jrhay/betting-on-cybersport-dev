<?php
/* @var $this DefaultGamesVideoFrameController */
/* @var $data DefaultGamesVideoFrame */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('frame')); ?>:</b>
	<?php echo CHtml::encode($data->frame); ?>
	<br />


</div>