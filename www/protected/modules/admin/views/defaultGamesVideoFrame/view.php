<?php
/* @var $this DefaultGamesVideoFrameController */
/* @var $model DefaultGamesVideoFrame */

$this->breadcrumbs=array(
	'Default Games Video Frames'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List DefaultGamesVideoFrame', 'url'=>array('index')),
	array('label'=>'Create DefaultGamesVideoFrame', 'url'=>array('create')),
	array('label'=>'Update DefaultGamesVideoFrame', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete DefaultGamesVideoFrame', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DefaultGamesVideoFrame', 'url'=>array('admin')),
);
?>

<h1>View DefaultGamesVideoFrame #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'frame',
	),
)); ?>
