<?php
/* @var $this DefaultGamesVideoFrameController */
/* @var $model DefaultGamesVideoFrame */

$this->breadcrumbs=array(
	'Default Games Video Frames'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List DefaultGamesVideoFrame', 'url'=>array('index')),
	array('label'=>'Create DefaultGamesVideoFrame', 'url'=>array('create')),
	array('label'=>'View DefaultGamesVideoFrame', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage DefaultGamesVideoFrame', 'url'=>array('admin')),
);
?>

<h1>Update DefaultGamesVideoFrame <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>