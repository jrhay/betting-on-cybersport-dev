<?php
/**
 * Created by PhpStorm.
 * User: lldl
 * Date: 10/2/14
 * Time: 12:58 PM
 */

/** checking group bonuses and add bonus if need */
class GroupBonusActivator extends CModel
{

    /**
     * Returns the list of attribute names of the model.
     * @return array list of attribute names.
     */
    public function attributeNames()
    {
        return array();
    }

    /**
     *activate group bonuses
     * @param integer $category_id
     * @param integer $userId
     * @return bool
     */
    public function ActivateGroupBonuses($category_id, $user_id, $percentToSum = null){
        $commonSumBonus =0;
        $groupBonus = new GroupBonus();
        $activeGroupBonuses = $groupBonus->getActiveBonusesByCategoryId($category_id);
        /** @var GroupBonus $actGrBonus */
        foreach($activeGroupBonuses as $actGrBonus){
            /** @var Bonus $bonus */
            $bonus = new Bonus();
            if(isset($actGrBonus->title)) {
                $bonus->title = $actGrBonus->title;
            }
            if(isset($actGrBonus->text)) {
                $bonus->text = $actGrBonus->text;
            }
            $bonusSum = $actGrBonus->getResultValue($user_id, $percentToSum);
            $commonSumBonus += $bonusSum;
            $bonus->addBonus($user_id, $bonusSum, $category_id, $actGrBonus->end_date);
        }
        return $commonSumBonus;
    }
}