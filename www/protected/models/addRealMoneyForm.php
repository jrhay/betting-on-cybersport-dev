<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class addRealMoneyForm extends CFormModel
{
    public $summ;
    public $accountIdent;
    public $secureId;


    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules()
    {
        return array(

            array('summ, accountIdent, secureId', 'required'),
            array('summ', 'numerical','max'=>100),
//            array('summ', 'checkSum'),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array(
            'summ'=>Yii::t('site','Summ ($)'),
            'accountIdent'=>Yii::t('site','Account Id or Email ($)'),
            'secureId'=>Yii::t('site','SecureId ($)'),
        );
    }
}
