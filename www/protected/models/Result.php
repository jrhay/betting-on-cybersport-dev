<?php

/**
 * This is the model class for table "result".
 *
 * The followings are the available columns in table 'result':
 * @property string $id
 * @property string $game_id
 * @property string $result
 * @property string $add_result_time
 * @property string $score
 */
class Result extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'result';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('game_id, result, add_result_time', 'required'),
            array('game_id, add_result_time', 'length', 'max' => 20),
            array('result', 'length', 'max' => 5),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, game_id, result, add_result_time', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'game' => array(self::BELONGS_TO, 'Games', 'game_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('site', 'ID'),
            'game_id' => Yii::t('site', 'Game'),
            'result' => Yii::t('site', 'Result'),
            'add_result_time' => Yii::t('site', 'Add Result Time'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('game_id', $this->game_id, true);
        $criteria->compare('result', $this->result, true);
        $criteria->compare('add_result_time', $this->add_result_time, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Result the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /** Cansel game with recalculating payments
     * @param integer $id
     */
    public function cancelGame($id)
    {
        $game = Games::model()->findByPk($id);
        //no double cancel ;)
        if ($game->status === Games::GAME_STATUS_CANCELED) {
            return;
        }
        $this->cancelUserbets($game);
        $this->cancelExpressbets($game);
        $this->handleCanceledGame($game);
    }

    /** cancel SimpleBets
     * @param Games $game
     */
    public function cancelUserbets($game)
    {
        /** @var Userbets $bet */
        foreach ($game->userbets as $bet) {
            /** @var Usercash $usercash */
            $usercash = User::model()->findByPk($bet->userId);
            //check if game is not finished
            if ($game->isNotFinished()) {
                $payout = $bet->summ;
                $usercash = User::model()->findByPk($bet->userId)->usercash;
                $bet->moneyType === Usercash::REAL_MONEY ? $usercash->realMoney += $payout : $usercash->cyberMoney += $payout;
                $usercash->save();
            } else if ($game->isFinished()) {
                if ($bet->status === Userbets::WIN_YES) {
                    $payout = $bet->summ * $bet->koef;

                    if ($bet->moneyType === Usercash::REAL_MONEY) {
                        $usercash->realMoney -= $payout;
                        $usercash->realMoney += $bet->summ;

                    } else {
                        $usercash->cyberMoney -= $payout;
                        $usercash->cyberMoney += $bet->summ;
                    }
                }
                if ($bet->status == Userbets::WIN_NO) {
                    if ($bet->moneyType === Usercash::REAL_MONEY) {
                        $usercash->realMoney += $bet->summ;

                    } else {
                        $usercash->cyberMoney += $bet->summ;
                    }
                }
                if ($bet->status == Userbets::CANCELED) {
                    //?! something goes wrong...
                }
            }
            $bet->status = Userbets::CANCELED;
            $bet->save();
        }
    }

    /** cancel Expressbets
     * @param Games $game
     * */
    public function cancelExpressbets($game)
    {
        /** @var Expressbetsdetails[] $expressbetsDetails */
        $expressbetsDetails = Expressbetsdetails::model()->findAllByAttributes(array('gameId' => $game->id));
        foreach($expressbetsDetails as $eDetail) {
            $expressBet = $eDetail->expressBet;
            if($eDetail->game->isNotFinished()) {
                $eDetail->delete();
            }
            if($eDetail->game->isFinished()) {
                if($expressBet->isWin === Userbets::WIN_YES) {
                    $this->withdrawExpressBetPayment($expressBet);
                    $this->returnExpressBetSumm($expressBet);
                }
                if($expressBet->isWin === Userbets::WIN_NO) {
                    $this->returnExpressBetSumm($expressBet);
                }
            }
            $expressBet->status = Userbets::CANCELED;
            $expressBet->save();
        }
    }

    /** handle canceled game
     * @param Games $game
     * */
    public function handleCanceledGame($game)
    {
        $game->status = Games::GAME_STATUS_CANCELED;
        $game->game_end = time();
        $game->save();
    }

    // to execute all bets payments after adding or updating game result
    public function handleBets()
    {
        $this->handleSimpleBets();
        $this->handleExpressBets();
        //handle tournament bets in TournamentResults
//        $this->handleTournamentBets();
        $this->handleGame();
    }

    protected function handleGame(){
        /** @var Games $game */
        $game = $this->game;
        if($game->status != Games::GAME_STATUS_CANCELED)
            $game->status = Games::GAME_STATUS_FINISHED;
        $game->game_end = time();
        $game->save();
    }

    // to execute simple bets payments
    protected function handleSimpleBets()
    {
        /** @var Userbets[] $userbets */
        $userbets = $this->game->userbets;
        foreach ($userbets as $bet) {
            if ($bet->status == Userbets::CANCELED) {
                continue;
            }
            //if {first result add } else {recalculate payments for this bet }
            if (!$bet->isWin) {
                if ($this->result == $bet->result) {
                    $this->makeSimpleBetPayment($bet);
                    $bet->isWin = 2;
                    $bet->save();
                } else {
                    $bet->isWin = 1;
                    $bet->save();
                }
            } else if ($bet->isWin) {
                if ($this->result == $bet->result) {
                    if ($bet->isWin == Userbets::WIN_YES) {
                        //if  result is same and bet is win do nothing
                    } elseif ($bet->isWin == Userbets::WIN_NO) {
                        //user shoul recieve his payment
                        $this->makeSimpleBetPayment($bet);
                        $bet->isWin = 2;
                        $bet->save();
                    }
                } else if ($this->result != $bet->result) {
                    if ($bet->isWin == Userbets::WIN_YES) {
                        //withdraw  payment from usercash
                        $this->withdrawSimpleBetPayment($bet);
                        $bet->isWin = 1;
                        $bet->save();
                    } elseif ($bet->isWin == Userbets::WIN_NO) {
                        //if user loose anyway do nothing
                    }
                }

            }
        }
    }

// to execute express bets payments
    protected
    function handleExpressBets()
    {
        /**@var Expressbetsdetails[] $expressBetsDetails */
        $expressBetsDetails = Expressbetsdetails::model()->findAllByAttributes(array(
            'gameId' => $this->game_id));
        foreach ($expressBetsDetails as $expBetsDet) {
            /** @var Expressbets $expressBet */
            $expressBet = $expBetsDet->expressBet;
            if($expressBet->status === Userbets::CANCELED) {
                continue;
            }
            //check if need repayment / rehandling
            if (!$expressBet->isWin) {
                if ($this->result != $expBetsDet->rateResult) {
                    $expressBet->isWin = 1;
                    $expressBet->save();
                    continue;
                }
                //check current expressBet
                $this->handleCurrentExpressBet($expressBet);
            } else {
                $this->reHandleCurrentExpressBet($expressBet);
            }
        }
    }

    /** handle express bet
     * @param Expressbets $expressBet
     */
    protected function handleCurrentExpressBet($expressBet)
    {
        //check current express bet ( if all details win)
        /** @var Expressbetsdetails[] $currentExpressBetsDetails */
        $currentExpressBetsDetails = Expressbetsdetails::model()->findAllByAttributes(array(
            'expressBetId' => $expressBet->id,
        ));
        foreach ($currentExpressBetsDetails as $curExpBetDet) {
            /** @var Result $curExpBetDetResult */
            $curExpBetDetResult = Result::model()->findByAttributes(array(
                'game_id' => $curExpBetDet->gameId
            ));
            if (empty($curExpBetDetResult)) {
                return;
            }
            if ($curExpBetDetResult->result != $curExpBetDet->rateResult) {
                $expressBet->isWin = 1;
                $expressBet->save();
                return;
            }
        }
        //if here => express bet is win
        $expressBet->isWin = 2;
        $expressBet->save();
        $this->makeExpressBetPayment($expressBet);

    }

    /** @param ExpressBets $expressBet */
    protected function reHandleCurrentExpressBet($expressBet)
    {
        /** @var Expressbetsdetails $expressBetsDetail */
        $expressBetsDetail = Expressbetsdetails::model()->findByAttributes(array(
            'expressBetId' => $expressBet->id,
            'gameId' => $this->game_id,
        ));

        switch (true) {
            case ($this->result == $expressBetsDetail->rateResult && $expressBet->isWin == (Userbets::WIN_YES)) :
                ;
                break;
            case ($this->result == $expressBetsDetail->rateResult && $expressBet->isWin == (Userbets::WIN_NO)) :
                $this->makeExpressBetPayment($expressBet);
                $expressBet->isWin = Userbets::WIN_YES;
                $expressBet->save();
                break;
            case ($this->result != $expressBetsDetail->rateResult && $expressBet->isWin == (Userbets::WIN_YES)) :
                $this->withdrawExpressBetPayment($expressBet);
                $expressBet->isWin = Userbets::WIN_NO;
                $expressBet->save();
                break;
            case ($this->result != $expressBetsDetail->rateResult && $expressBet->isWin == (Userbets::WIN_NO)) :
                break;
        }
    }

// to execute tournament bets payments
    protected
    function handleTournamentBets()
    {
        //Todo: implement
    }

    /**
     * make payment for bet
     * @param UserBets $bet
     * */
    protected
    function makeSimpleBetPayment($bet)
    {
        /** @var User $user */
        $user = User::model()->findByPk($bet->userId);
        $payment = $bet->summ * $bet->koef;
        switch ($bet->moneyType) {
            case Usercash::REAL_MONEY:
                $user->usercash->realMoney += intval($payment);
                break;
            case Usercash::PLAY_MONEY:
                $user->usercash->cyberMoney += intval($payment);
                break;
        }
        $user->usercash->save();
    }


    /**
     * withdraw payment for bet
     * @param UserBets $bet
     * */
    protected
    function withdrawSimpleBetPayment($bet)
    {
        /** @var User $user */
        $user = User::model()->findByPk($bet->userId);
        $payment = $bet->summ * $bet->koef;
        switch ($bet->moneyType) {
            case Usercash::REAL_MONEY:
                $user->usercash->realMoney -= intval($payment);
                break;
            case Usercash::PLAY_MONEY:
                $user->usercash->cyberMoney -= intval($payment);
                break;
        }
        $user->usercash->save();
    }

    /** @var Expressbets $expressBet */
    protected function makeExpressBetPayment($expressBet)
    {
        /** @var ExpressBetsDetails[] $expressBetDetails */
        $expressBetDetails = $expressBet->expressbetsdetails;
        $commonKoef = 1;
        foreach ($expressBetDetails as $expressBetDetail) {
            $commonKoef *= $expressBetDetail->koef;
        }
        $payment = $expressBet->summ * $commonKoef;
        $user = User::model()->findByPk($expressBet->userId);
        /** @var Usercash $usercash */
        $usercash = $user->usercash;
        $usercash->realMoney += intval($payment);
        $usercash->save();
    }

    /** @var Expressbets $expressBet */
    protected function withdrawExpressBetPayment($expressBet)
    {
        /** @var ExpressBetsDetails[] $expressBetDetails */
        $expressBetDetails = $expressBet->expressbetsdetails;
        $commonKoef = 1;
        foreach ($expressBetDetails as $expressBetDetail) {
            $commonKoef *= $expressBetDetail->koef;
        }
        $payment = $expressBet->summ * $commonKoef;
        $user = User::model()->findByPk($expressBet->userId);
        /** @var Usercash $usercash */
        $usercash = $user->usercash;
        $usercash->realMoney -= intval($payment);
        $usercash->save();
    }

    /** @param ExpressBets $expressBet */
    protected function returnExpressBetSumm($expressBet){
        $user = User::model()->findByPk($expressBet->userId);
        /** @var Usercash $usercash */
        $usercash = $user->usercash;
        $usercash->realMoney += $expressBet->summ;
        $usercash->save();
    }
}
