<?php
/**
 * Created by PhpStorm.
 * User: lldl
 * Date: 10/30/14
 * Time: 8:41 PM
 */

class SelectGameCategoryForm extends CFormModel {
    public $category_id;
    public function attributeLabels()
    {
        return array(
            'category_id'=>Yii::t('general','Category'),
        );
    }
} 