<?php

/**
 * This is the model class for table "news".
 *
 * The followings are the available columns in table 'news':
 * @property string $id
 * @property integer $category_id
 * @property string $title
 * @property string $title_eng
 * @property string $text
 * @property string $text_eng
 * @property integer $date
 * @property string $img
 * @property string $img_eng
 */
class News extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'news';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('category_id, title, title_eng, text, text_eng', 'required'),
			array('category_id, date', 'numerical', 'integerOnly'=>true),
			array('img', 'length', 'max'=>255),
            array('img', 'file', 'types'=>'jpg, gif, png'),
            array('img_eng', 'file', 'types'=>'jpg, gif, png'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, category_id, title, title_eng, text,text_eng, date, img, img_eng', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'category_id' =>  Yii::t('site','Категория'),
			'title' =>  Yii::t('site','Заголовок'),
            'title_eng' =>  Yii::t('site','Заголовок Англ'),
			'text' =>  Yii::t('site','Текст'),
            'text_eng' =>  Yii::t('site','Текст Англ'),
			'date' => Yii::t('site','Дата') ,
			'img' =>  Yii::t('site','Картинка'),
            'img_eng' =>  Yii::t('site','Картинка Англ'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('title',$this->title,true);
        $criteria->compare('title_eng',$this->title,true);
		$criteria->compare('text',$this->text_eng,true);
        $criteria->compare('text_eng',$this->text_eng,true);
		$criteria->compare('date',$this->date);
		$criteria->compare('img',$this->img,true);
        $criteria->compare('img_eng',$this->img_eng,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return News the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
