<?php

/**
 * payoutForm class.
 * payoutForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of UsercasheController'.
 */
class PayoutForm extends CFormModel
{
    public $system;
    public $eWalletNumber;
    public $amount;


    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules()
    {
        return array(

            array('system, eWalletNumber, amount', 'required'),
            array('amount', 'numerical','max'=>100),
            array('amount', 'numerical','min'=>1),
//            array('summ', 'checkSum'),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array(
            'amount'=>Yii::t('site','Amount ($)'),
            'eWalletNumber'=>Yii::t('site','Personal Ident ( e-wallet number or other ident)'),
            'System'=>Yii::t('site','System'),
        );
    }
}
