<?php

/**
 * This is the model class for table "gamesbetshistory".
 *
 * The followings are the available columns in table 'gamesbetshistory':
 * @property integer $id
 * @property integer $gameId
 * @property double $koef1
 * @property double $koef2
 * @property double $draw
 * @property string $betId
 * @property integer $time
 *
 * The followings are the available model relations:
 * @property Userbets $bet
 * @property Games $game
 */
class Gamesbetshistory extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gamesbetshistory';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('gameId, koef1, koef2, betId, time', 'required'),
			array('gameId, time', 'numerical', 'integerOnly'=>true),
			array('koef1, koef2, draw', 'numerical'),
			array('betId', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, gameId, koef1, koef2, draw, betId, time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'bet' => array(self::BELONGS_TO, 'Userbets', 'betId'),
			'game' => array(self::BELONGS_TO, 'Games', 'gameId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'gameId' => 'Game',
			'koef1' => 'Koef1',
			'koef2' => 'Koef2',
			'draw' => 'Draw',
			'betId' => 'Bet',
			'time' => 'Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('gameId',$this->gameId);
		$criteria->compare('koef1',$this->koef1);
		$criteria->compare('koef2',$this->koef2);
		$criteria->compare('draw',$this->draw);
		$criteria->compare('betId',$this->betId,true);
		$criteria->compare('time',$this->time);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Gamesbetshistory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function beforeSave() {
        if ($this->draw) {
            $this->draw = Helper::formatKoef($this->draw);
            $this->koef1 = Helper::formatKoef($this->koef1);
            $this->koef2 = Helper::formatKoef($this->koef2);
        }
        return parent::beforeSave();
    }
}
