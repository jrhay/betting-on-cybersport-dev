<?php

/**
 * This is the model class for table "Emails".
 *
 * The followings are the available columns in table 'Emails':
 * @property integer $id
 * @property string $code
 * @property string $subject
 * @property string $body
 * @property integer $lang_id
 */
class Email extends CActiveRecord {

    /**
     * @var
     */
    public $language_search;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Emails the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'emails';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('code, subject, body, lang_id', 'required'),
            array('lang_id', 'numerical', 'integerOnly' => true),
            array('subject, code, hint', 'length', 'max' => 255),
            //array('body', 'filter', 'filter' => array($obj = new CHtmlPurifier(), 'purify')),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, code, subject, body, hint, language_search', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
            'lang_name' => array(self::BELONGS_TO, 'Languages', 'lang_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => Yii::t('general', 'ID'),
            'code' => Yii::t('general', 'Code'),
            'subject' => Yii::t('general', 'Subject'),
            'body' => Yii::t('general', 'Body'),
            'hint' => Yii::t('general', 'Hint'),
            'language_search' => Yii::t('general', 'Language'),
            'lang_id' => Yii::t('general', 'Language'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CybActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
        $criteria = new CDbCriteria;

        $criteria->with = array('lang_name');
        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.subject', $this->subject, true);
        $criteria->compare('t.body', $this->body, true);
        $criteria->compare('t.code', $this->code, true);
        $criteria->compare('lang_name.name', $this->language_search, true);




        return new CybActiveDataProvider($this, array(
            'pagination'=>array(
                'pageSize'=>  Helper::getPageSize(),
            ),
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => array('subject'=>false),
            ),
        ));
    }

    /**
     * @param $code
     * @param null $lang_id
     * @return string
     */
    public function findEmail($code, $lang_id = null) {
        if (!$lang_id) {
            $lang_id = Yii::app()->user->language;
        }
        $default_id = Yii::app()->params['default_lang_id'];
        $return = '';
        
        $values = Email::model()->findAllByAttributes(array('code' => $code, 'lang_id' => array($lang_id, $default_id)));                
        foreach ($values as $value) {
            if ($value->lang_id == $lang_id) {
                $return = $value;
            } elseif (!$return) {
                $return = $value;
            }
        }
        
        return $return;
    }
}