<?php

/**
 * This is the model class for table "jackpot".
 *
 * The followings are the available columns in table 'jackpot':
 * @property string $id
 * @property integer $active
 * @property double $koef
 * @property double $percent
 * @property double $current_sum
 * @property Datetime $last_reset_time
 */
class Jackpot extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'jackpot';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('active, koef, percent, current_sum, last_reset_time', 'required'),
			array('active', 'numerical', 'integerOnly'=>true),
			array('koef, current_sum', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, active, koef, percent, current_sum, last_reset_time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('site','id'),
			'active' => Yii::t('site','active'),
			'koef' => Yii::t('site','Koef'),
			'percent' => Yii::t('site','Percent'),
			'current_sum' => Yii::t('site','Current Sum'),
            'last_reset_time' => Yii::t('site','Last Reset Time'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
        $criteria->compare('id',1);
		$criteria->compare('active',1);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    /** add money to current jackpot */
    private  function addMoney($sum){
        $this->current_sum += $sum;
        $result = $this->save();
        return $result ? true : false;
    }

    /** add Money to Jackpot if koef is higher then $this->koef
     * @param integer $sum
     * @param double $koef
     * @return bool
     */
    public function addMoneyWithKoefCheck($sum, $koef){
        if(!isset($this->koef) or !isset($this->current_sum)) {
            return false;
        }
        if($koef >= $this->koef) {
            $addSum = $sum * $this->percent * 0.01;
            $this->addMoney($addSum);
            $result = $this->save();
            return $result ? true : false;
        } else {
            return false;
        }
    }

    /** returns current Jackpot
     * @param Jackpot */
    public function getCurrentJackpot(){
        $dataProvider = $this->search();
        $data = $dataProvider->getData();
        if(!count($data)) {
            $newJackpot = new Jackpot();
            $newJackpot->id = 1;
            $newJackpot->koef = 0;
            $newJackpot->percent = 0;
            $last_reset_time = new DateTime();
            $last_reset_time = $last_reset_time->format(Yii::app()->params['dbTimeFormat']);
            $newJackpot->last_reset_time = $last_reset_time;
            $newJackpot->current_sum = 0;
            $newJackpot->active = 1;
            $result = $newJackpot->save();
            return $result ? $newJackpot : false;
        } else {
            return $data[0];
        }
    }

    /** setting new koef and percent to current Jackpot
     * @param double $koef
     * @param integer $percent
     */
    public function setJackpot($koef, $percent){
        /** @var Jackpot $currentJackpot */
        if(!$currentJackpot = $this->getCurrentJackpot()){
            $currentJackpot = new Jackpot();
            //not necessary
            $currentJackpot->setAttribute('id',1);
        };
        $currentJackpot->setAttribute('koef', $koef);
        $currentJackpot->setAttribute('percent', $percent);
        $result = $currentJackpot->save();
        return $result ? true : false;
    }
    /** issueMoney to user and reset Jackpot */
    public function issueMoney($user_id){
        /** @var Usercash $usercash */
        $usercash = new Usercash();
        $usercash = $usercash->findByAttributes(array('userId' => $user_id));
        $usercash->realMoney += $this->current_sum;
        $saveResult = $usercash->save();
        $resetResult = $this->resetJackpot();
        if($saveResult && $resetResult) {
            return true;
        } else {
            return false;
        }

    }
    /** clear sum ( make it 0 )  */
    public function  resetJackpot(){
        $this->current_sum = 0;
        $datetime = new DateTime();
        $this->last_reset_time = $datetime->format(Yii::app()->params['dbTimeFormat']);
        $result = $this->save();
        return $result ? true : false;
    }
    /** return list of contenders,who has made a bet > 50
     * @return User[]
     */
    public function getContendersList(){
        $dateTime = new DateTime();
        $lastResetTime = $dateTime->createFromFormat(Yii::app()->params['dbTimeFormat'], $this->last_reset_time);
        $lastResetTimeTimestamp = $lastResetTime->getTimestamp();
        /** @var Gamesbetshistory $gamesBetsHistory */
        $gamesBetsHistory = new Gamesbetshistory();
        $gamesBetsHistory = $gamesBetsHistory->findAllByAttributes(array(),'time > :lrt', array('lrt' => $lastResetTimeTimestamp));
        if(!count($gamesBetsHistory)) {
            return array();
        }

        $users = array();
        foreach($gamesBetsHistory as $histBet) {
            /** @var Userbets $bet */
            $bet = $histBet->bet;
            if($bet->koef >= $this->koef && $bet->summ >= 50) {
                $user = $bet->user;
                $users[$user->id] =$user;
            }
        }
        return $users;
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Jackpot the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
