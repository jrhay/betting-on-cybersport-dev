<?php

/**
 * This is the model class for table "group_bonus".
 *
 * The followings are the available columns in table 'group_bonus':
 * @property integer $id
 * @property integer $bonus_category_id
 * @property Datetime $start_date
 * @property DateTime $end_date
 * @property integer $value
 * @property bool $in_percents
 * @property integer $percent_to
 * @property string $title
 * @property string $text
 * @property bool $active
 */
class GroupBonus extends CActiveRecord
{
    const PERCENT_TO_DEPOSIT = 1;
    const PERCENT_T0_BET = 2;
    const PERCENT_TO_INCOME_DEPOSIT = 3;

    /**
     * Returns the static model of the specified AR class.
     * @return PersonalMessage the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'group_bonus';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('value, in_percents, bonus_category_id ', 'required'),
            array('title,text', 'safe'),
            array('title, text', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'bonus_category' => array(self::BELONGS_TO, 'BonusCategory', 'bonus_category_id')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {

        return array(
            'id' => 'ID',
            'bonus_category_id' => Yii::t('site','Category'),
            'start_date' => Yii::t('site','Start Date'),
            'end_date' => Yii::t('site','End Date'),
            'value' => Yii::t('site','Value'),
            'in_percents' => Yii::t('site','In percents'),
            'percent_to' => Yii::t('site','Percents to'),
            'title' => Yii::t('site','Title'),
            'text' => Yii::t('site','Text'),
            'active' => Yii::t('site','Active'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
// Warning: Please modify the following code to remove attributes that
// should not be searched.
        $criteria = new CDbCriteria;
//        $criteria->compare('id',$this->id,true);
//        $criteria->compare('sender_id',$this->sender);
//        $criteria->compare('recipient_id',2);
//        $criteria->compare('subject',$this->subject,true);
//        $criteria->compare('text',$this->text,true);
        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Retrieves a list of models using category id
     * @param bool $dateVerification if need to compare start_date , current date , end_date
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function getActiveBonusesByCategoryId($bonusCategoryId, $dateVerification = true)
    {
// Warning: Please modify the following code to remove attributes that
// should not be searched.
        $criteria = new CDbCriteria;
        $criteria->compare('bonus_category_id', $bonusCategoryId);
        $criteria->compare('active', 1);
        $dataProvider = new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 100)
        ));
        $groupBonuses = $dataProvider->getData();
        if ($dateVerification) {
            /**@var GroupBonus $bonus */
            foreach ($groupBonuses as $key => $bonus) {
                if (!$bonus->dateVerification()) {
                    unset($groupBonuses[$key]);
                }
            }
        }
        return $groupBonuses;
    }

    /**
     * Scopes list
     *
     * @return array
     */
    public function scopes()
    {
        return array(
            'active' => array(
                'condition' => '`active`=1'
            ),
        );
    }

    /**
     * adds bonus to group of users
     *
     * @param integer $value
     * @param bool $inPercents
     * @param integer $percentTo
     * @param integer $categoryId
     * @param DateTime $startDate
     * @param DateTime $endDate
     * @return bool
     */
    public function addGroupBonus($value, $inPercents, $percentTo, $bonusCategoryId, $startDate, $endDate)
    {
        $this->value = $value;
        $this->in_percents = $inPercents;
        $this->percent_to = $percentTo;
        $this->bonus_category_id = $bonusCategoryId;
        $this->start_date = $startDate;
        $this->end_date = $endDate;
        $result = $this->save();
        return $result ? $this : false;
    }

    /** getting result value
     * @param integer $userId
     * @param integer $percentToSum
     * @return integer
     */
    public function getResultValue($userId, $percentToSum)
    {
        $value = 0;
        switch ($this->percent_to) {
            case null:
                $value = $this->value;
                break;
            case self::PERCENT_TO_DEPOSIT:
                /** @var Usercash $usercash */
                $usercash = new Usercash();
                /** @var Usercash $currentUsercash */
                $currentUsercash = $usercash->searchByUserId($userId)->getData();
                $currentUsercash = $currentUsercash[0];
                $deposit = $currentUsercash->realMoney;
                if($this->value >= 100) {
                    $this->value = 100;
                }
                $value = ($this->value) * 0.01 * $deposit;
                break;
            case self::PERCENT_T0_BET:
                if($this->value >= 100) {
                    $this->value = 100;
                }
                $value = ($this->value) * 0.01 * $percentToSum;
                break;
            case self::PERCENT_TO_INCOME_DEPOSIT;
                if($this->value >= 100) {
                    $this->value = 100;
                }
                $value = ($this->value) * 0.01 * $percentToSum;
                break;
        }
        return $value;
    }

    public function getErrorsModels()
    {
        return $this->_errorModels;
    }

    /**
     * Mark message as read
     */
    public function markAsInactive()
    {
        if ($this->active) {
            $this->active = 0;
            $this->save(false, array('active'));
        }
    }

    /**
     *checks if current date is in date interval of group bonus
     * @return bool
     */
    public function dateVerification()
    {
        $today = new DateTime();
        $today = $today->getTimestamp();
        $startDate = new DateTime();
        $startDate = $startDate->createFromFormat('Y-m-d H:i:s', $this->start_date);
        $startDate = $startDate->getTimestamp();

        $endDate = new DateTime();
        $endDate = $endDate->createFromFormat('Y-m-d H:i:s', $this->end_date);
        $endDate = $endDate->getTimestamp();

        if ($startDate <= $today && $today <= $endDate) {
            return true;
        }
        return false;
    }
}