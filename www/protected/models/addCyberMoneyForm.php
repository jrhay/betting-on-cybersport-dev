<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class addCyberMoneyForm extends CFormModel
{
    public $summ;


	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(

			array('summ', 'required'),
			array('summ', 'numerical','max'=>100),
//            array('summ', 'checkSum'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
            'summ'=>Yii::t('site','restore to  ($)'),
		);
	}
}
