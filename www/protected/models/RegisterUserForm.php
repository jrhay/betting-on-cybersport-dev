<?php

/**
 * Class RegisterUserForm
 */
class RegisterUserForm extends CFormModel {

    public $firstName;
    public $lastName;
    public $userName;
    public $password;
    public $repeat_password;
    public $verifyCode;
    public $language;
    public $email;
    public $usertypeId;
    public $status;
    public $country;

    /**
     * Model rules
     * @return type
     */
    public function rules() {
        return array(
            array('usertypeId, status, email, userName,firstName, lastName, language, country', 'required'),
            array('firstName, lastName', 'length', 'max'=>45),
            array('usertypeId', 'length', 'max'=>10),
            array('password, repeat_password', 'required', 'on' => 'create'),
            array('password', 'match', 'pattern' => '/(?=.*\d)/', 'message' => Yii::t('general', "Password must include at least one number")),
            array('password', 'match', 'pattern' => '/(?=.*[A-Z])/', 'message' => Yii::t('general', "Password must include at least one uppercase letter!")),
            array('password', 'match', 'pattern' => '/(?=.*[a-z])/', 'message' => Yii::t('general', "Password must include at least one lowercase letter!")),
            array('repeat_password', 'compare', 'compareAttribute' => 'password'),
            array('email', 'email'),
            array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),

        );
    }

    /**
     * Model attribute labels
     * @return type
     */
    public function attributeLabels() {
        return array(
            'id' => Yii::t('register','ID'),
            'userName' => Yii::t('register','User Name'),
            'language' => Yii::t('register', 'Language'),
            'firstName' => Yii::t('register','First Name'),
            'lastName' => Yii::t('register','Last Name'),
            'usertypeId' => Yii::t('register','Usertype'),
            'email' => Yii::t('register','Email'),
            'verifyCode' => Yii::t('register', 'Verification Code'),
            'country' => Yii::t('register', 'Country'),
            'password' => Yii::t('register','Password'),
            'repeat_password' => Yii::t('register', 'repeat_password'),
        );
    }
}