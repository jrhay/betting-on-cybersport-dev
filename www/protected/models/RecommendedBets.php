<?php

/**
 * This is the model class for table "recommended_bets".
 *
 * The followings are the available columns in table 'recommended_bets':
 * @property string $id
 * @property string $date
 * @property string $text
 * @property double $koef
 * @property double $summ
 * @property string $game_id
 */
class RecommendedBets extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'recommended_bets';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array(' game, summ, game_id, text', 'required'),
			array('koef, summ', 'numerical'),
			array('date', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, date, game, koef, summ,game_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'game' => array(self::BELONGS_TO, 'Games', 'game_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'date' => 'Date',
			'game' => 'Game',
			'koef' => 'Koef',
			'summ' => 'Summ',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('game',$this->game,true);
		$criteria->compare('koef',$this->koef,true);
		$criteria->compare('summ',$this->summ);
        $criteria->compare('game_id',$this->game_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return RecommendedBets the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
