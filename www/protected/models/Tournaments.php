<?php

/**
 * This is the model class for table "tournaments".
 *
 * The followings are the available columns in table 'tournaments':
 * @property integer $id
 * @property integer $start_date
 * @property integer $status
 * @property integer $isFloatingKoef
 * @property integer $winnerId
 * @property double $winnerKoef
 * @property string $logo
 * @property integer $gameCategory
 * @property string $name
 * @property  integer $activeStatus
 * @property Tournamentsbets[] $bets
 *
 * The followings are the available model relations:
 * @property Tournamentsdetails[] $tournamentsdetails
 */
class Tournaments extends CActiveRecord
{
	const IS_FLOATING_KOEF_YES = 1;
	const IS_FLOATING_KOEF_NO = 0;

    public static function getStatusFilter() {
        $filter = Tournaments::alias('Status');
        return $filter;
    }

    public static function alias($type, $code = NULL) {
        $_items = array(
            'Status' => array(
                Games::GAME_STATUS_NOT_STARTED => Yii::t('general', 'Not Started'),
                Games::GAME_STATUS_IN_PROGRESS => Yii::t('general', 'In Progress'),
                Games::GAME_STATUS_FINISHED => Yii::t('general', 'Finished'),
            ),
            'YesNo' => array(
                CybController::ANSWER_YES => Yii::t('general', 'Yes'),
                CybController::ANSWER_NO => Yii::t('general', 'No'),
            ),
			'ActiveStatus' => array(
				CybController::ACTIVE_STATUS => Yii::t('general', 'Active'),
				CybController::INACTIVE_STATUS => Yii::t('general', 'Inactive')
			)
        );

        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

	public static function getActiveStatusFilter() {
		$filter = Tournaments::alias('ActiveStatus');
		return $filter;
	}

    public static function getYesNoFilter() {
        $filter = Tournaments::alias('YesNo');
        return $filter;
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Tournaments the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tournaments';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('start_date, status, name, gameCategory', 'required'),
			array('status, isFloatingKoef, winnerId,gameCategory', 'numerical', 'integerOnly'=>true),
			array('winnerKoef,activeStatus', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, start_date, status, isFloatingKoef, winnerId, winnerKoef, name,logo, gameCategory', 'safe', 'on'=>'search'),
            // this will allow empty field when page is update (remember here i create scenario update)
            array('logo', 'file','types'=>'jpg, gif, png', 'allowEmpty'=>true, 'on'=>'update','maxSize' => 10048576),
            array('logo','unsafe',)
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tournamentsdetails' => array(self::HAS_MANY, 'Tournamentsdetails', 'tournamentId'),
            'bets' => array(self::HAS_MANY, 'Tournamentsbets', 'tournamentId'),
            'result' => array(self::HAS_ONE, 'TournamentResult', 'tournament_id'),
            'winner' => array(self::BELONGS_TO,'Teams','winnerId'),
            'gameCategory' => array(self::BELONGS_TO, 'GameCategories', 'gameCategory' )
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('site','ID'),
			'start_date' => Yii::t('site','Date'),
			'status' => Yii::t('site','Status'),
			'activeStatus' => Yii::t('site','Active Status'),
			'isFloatingKoef' => Yii::t('site','Is floating koef'),
			'winnerId' => Yii::t('site','Winner'),
			'winnerKoef' => Yii::t('site','Winner koef'),
			'name' => Yii::t('site','Name'),
            'logo' => Yii::t('site','Logo'),
            'gameCategory' => Yii::t('site', 'Game Catagory'),
		);
	}

    public function scopes() {
        return array(
            'onlyNotStarted' => array(
                'condition' => 't.start_date>' . time(),
            ),
            'gameStartTimeSort' => array(
                'order' => 'start_date DESC',
            ),
        );
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('start_date',$this->start_date);
		$criteria->compare('status',$this->status);
		$criteria->compare('activeStatus',$this->activeStatus);
		$criteria->compare('isFloatingKoef',$this->isFloatingKoef);
		$criteria->compare('winnerId',$this->winnerId);
		$criteria->compare('winnerKoef',$this->winnerKoef);
        $criteria->compare('logo', $this->logo);
        $criteria->compare('gameCategory', $this->gameCategory);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
