<?php
/**
 * Created by PhpStorm.
 * User: lldl
 * Date: 10/29/14
 * Time: 11:12 PM
 */

class TimeRangeForm extends CFormModel{
    public $start_time;
    public $end_time;
    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array(
            'start_time'=>Yii::t('general','Start Time'),
            'end_time'=>Yii::t('general','End Time'),
        );
    }
} 