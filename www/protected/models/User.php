<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property string $id
 * @property string $firstName
 * @property string $lastName
 * @property string $usertypeId
 * @property string $createTime
 * @property string $country
 *
 * The followings are the available model relations:
 * @property Usertype $usertype
 */
class User extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */

    const ADMIN_USER = 1;
    const SUPPORT_USER = 2;
    const MANAGER_USER = 3;
    const REGISTERED_USER = 4;

    const ACTIVE_USER_STATUS = '2';
    const INACTIVE_USER_STATUS = '1';
    const DELETED_USER_STATUS = '0';

    public $repeat_password;
    public $initialPassword;
    public $change_password;
    public $language;

	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('usertypeId, status, email, userName,firstName, lastName, timezone', 'required'),
			array('firstName, lastName', 'length', 'max'=>45),
			array('usertypeId', 'length', 'max'=>10),
            array('password, repeat_password', 'required', 'on' => 'create'),
            array('password', 'match', 'pattern' => '/(?=.*\d)/', 'message' => Yii::t('general', "Password must include at least one number")),
            array('password', 'match', 'pattern' => '/(?=.*[A-Z])/', 'message' => Yii::t('general', "Password must include at least one uppercase letter!")),
            array('password', 'match', 'pattern' => '/(?=.*[a-z])/', 'message' => Yii::t('general', "Password must include at least one lowercase letter!")),
            array('repeat_password', 'compare', 'compareAttribute' => 'password'),
            array('email', 'email'),
            array('userName', 'unique', 'message' => Yii::t('general', 'This username already exists.')),
            array('email', 'unique', 'message' => Yii::t('general', 'This email already exists.')),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, firstName, lastName, usertypeId, userName, email, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'messages' => array(self::HAS_MANY, 'PersonalMessage', 'sender_id'),
            'games' => array(self::HAS_MANY, 'Games', 'manager_id'),
            'teamplayers' => array(self::HAS_MANY, 'Teamplayers', 'user_id'),
            'usertype' => array(self::BELONGS_TO, 'Usertype', 'usertypeId'),
            'userbets' => array(self::HAS_MANY, 'Userbets', 'userId'),
            'usercash' => array(self::HAS_ONE, 'Usercash', 'userId'),
            'userlanguages' => array(self::HAS_MANY, 'Userlanguage', 'user_id'),
            'bonus_balance' => array(self::HAS_ONE, 'BonusBalance', 'user_id'),
            'tickets' => array(self::HAS_MANY, 'Ticket', 'user_id'),
            'income_deposit_operations' => array(self::HAS_MANY, 'IncomeDepositOperationsHistory','userId'),
            'country' => array(self::BELONGS_TO, 'Countries', 'country'),
            'payouts' => array(self::HAS_MANY, 'Payout', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('site','ID'),
			'firstName' => Yii::t('site','First Name'),
			'lastName' => Yii::t('site','Last Name'),
            'userName' => Yii::t('account', 'User Name'),
			'usertypeId' => Yii::t('site','Usertype'),
			'language' => Yii::t('site','Language'),
			'email' => Yii::t('site','Email'),
            'password' => Yii::t('site','Password'),
            'status' => Yii::t('site','Status'),
            'timezone' => Yii::t('site','Timezone'),
            'country' => Yii::t('site','Country'),
		);
	}

    public static function alias($type, $code = NULL) {
        $_items = array(
            'Status' => array(
                self::ACTIVE_USER_STATUS => Yii::t('general', 'Active'),
                self::INACTIVE_USER_STATUS => Yii::t('general', 'Inactive'),
                self::DELETED_USER_STATUS => Yii::t('general', 'Deleted'),
            ),
        );

        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

    public static function getStatusFilter() {
        $filter = User::alias('Status');
        return $filter;
    }

    //Todo: check it
    public function scopes() {
        return array(
            'onlyManagers' => array(
                'condition' => 't.usertypeId=' . User::MANAGER_USER,
            ),
            'OnlyActive' => array(
                'condition' => 't.status=' . User::ACTIVE_USER_STATUS,
            ),
        );
    }
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

        $criteria->compare('firstName',$this->firstName,true);
        $criteria->compare('lastName',$this->lastName,true);
        $criteria->compare('usertypeId',$this->usertypeId,true);
        $criteria->compare('userName',$this->userName,true);
        $criteria->compare('password',$this->password,true);
        $criteria->compare('status',$this->status);
        $criteria->compare('timezone',$this->timezone,true);
        $criteria->compare('email',$this->email,true);
        $criteria->compare('privateKey',$this->privateKey,true);
        $criteria->compare('country', $this->country, true);

		return new CybActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    /**
     * Retrieves a list of models of not deleted users.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function searchActiveUsers()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

//        $criteria->compare('firstName',$this->firstName,true);
//        $criteria->compare('lastName',$this->lastName,true);
//        $criteria->compare('usertypeId',$this->usertypeId,true);
//        $criteria->compare('userName',$this->userName,true);
//        $criteria->compare('password',$this->password,true);

        //Todo: uncomment this to search only active users
//        $criteria->compare('status', 2 );

//        $criteria->compare('timezone',$this->timezone,true);
//        $criteria->compare('email',$this->email,true);
//        $criteria->compare('privateKey',$this->privateKey,true);

        return new CybActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function beforeSave() {

        // in this case, we will use the old hashed password.
        if (empty($this->password) && empty($this->repeat_password) && !empty($this->initialPassword)) {
            $this->password = $this->initialPassword;
        } else {
            $this->password = crypt($this->password, md5($this->password)) . md5($this->password);
        }

        return parent::beforeSave();
    }

    public function afterSave() {
        if (!Usercash::model()->findByAttributes(array('userId'=>$this->id))) {
            Helper::createUserCash($this->id);
        }
        return parent::afterSave();
    }

    public function afterFind() {
        //reset the password to null because we don't want the hash to be shown.
        $this->initialPassword = $this->password;
        if (Yii::app()->controller && (Yii::app()->controller->action->id == 'create' || Yii::app()->controller->action->id == 'update')) {
            $this->password = null;
        }
        parent::afterFind();
    }

    public static function generatePassword($length = 12) {
        $chars = array_merge(range(0, 9), range('a', 'z'), range('A', 'Z'));
        shuffle($chars);
        $password = implode(array_slice($chars, 0, $length));
        while (!preg_match('/(?=.*\d)(?=.*[a-z])(?=.*[A-Z])/',$password)) {
            $chars = array_merge(range(0, 9), range('a', 'z'), range('A', 'Z'));
            shuffle($chars);
            $password = implode(array_slice($chars, 0, $length));
        }
        return $password;
    }

    public static function getTimeZoneSelect() {
        $UTCArray = array();
        $timezone_identifiers = DateTimeZone::listIdentifiers();
        $need = array(
            'Europe/London', //0
            'Europe/Paris', //+1
            'Europe/Kiev', //+2
            'Europe/Minsk', //+3
            'Europe/Samara', //+4
            'Indian/Maldives', //+5
            'Asia/Almaty', //+6
            'Asia/Bangkok', //+7
            'Asia/Hong_Kong', //+8
            'Asia/Tokyo', //+9
            'Australia/Brisbane', //+10
            'Australia/Melbourne', //+11
            'Asia/Kamchatka', //+12
            'America/Scoresbysund', //-1
            'America/Sao_Paulo', //-2
            'America/Argentina/Buenos_Aires', //-3
            'Atlantic/Bermuda', //-4
            'America/Detroit', //-5
            'America/Chicago', //-6
            'America/Cambridge_Bay', //-7
            'America/Los_Angeles', //-8
            'America/Anchorage', //-9
            'Pacific/Johnston', //-10
            'Pacific/Midway', // -11
        );

        foreach($timezone_identifiers as $timezone) {
            if(in_array($timezone, $need)) {
                $UTCArray[$timezone] = $timezone.' ('.Yii::t('general','UTC'). ' ' .(Helper::getTimezoneOffset($timezone)>0?'+':'').Helper::getTimezoneOffset($timezone).')';
            }
        }

        return $UTCArray;
    }

    public function getTimestampFromDate($date) {
        return $dateTimestamp = strtotime($date);
    }

    public function getTimeFromUnixtime($unixtime) {
        $userOffset = Yii::app()->user->timeZoneUsr;
        return Helper::getDateFormatted(Yii::t('information','Date format 1::MMMM dd, y hh:mm a'),(int) $unixtime + ((int) $userOffset * 60 * 60));
    }
}
