<?php

/**
 * This is the model class for table "games".
 *
 * The followings are the available columns in table 'games':
 * @property integer $id
 * @property string $name
 * @property integer $category_id
 * @property integer $team1_id
 * @property integer $team2_id
 * @property double $team1_koef
 * @property double $team2_koef
 * @property double $draw_koef
 * @property string $game_ico
 * @property integer $event_id
 * @property string $create_at
 * @property string $game_start_at
 * @property string $game_end
 * @property integer $status
 * @property string $manager_id
 * @property integer $max_sum
 * @property integer $betPercents
 * @property string $betvideo
 * @property string $map

 *
 * The followings are the available model relations:
 * @property User $manager
 * @property GameCategories $category
 * @property Teams $team1
 * @property Teams $team2
 * @property Userbets[] $userbets
 * @property Result $result
 */
class Games extends CActiveRecord
{

    const GAME_STATUS_NOT_STARTED = 0;
    const GAME_STATUS_IN_PROGRESS = 1;
    const GAME_STATUS_FINISHED = 2;
    const GAME_STATUS_CANCELED = 3;

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Games the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public static function getStatusFilter() {
        $filter = Games::alias('Status');
        return $filter;
    }

    public static function alias($type, $code = NULL) {
        $_items = array(
            'Status' => array(
                self::GAME_STATUS_NOT_STARTED => Yii::t('general', 'Not Started'),
                self::GAME_STATUS_IN_PROGRESS => Yii::t('general', 'In Progress'),
                self::GAME_STATUS_FINISHED => Yii::t('general', 'Finished'),
                self::GAME_STATUS_CANCELED => Yii::t('general', 'Canceled'),
            ),
        );

        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'games';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name,game_start_at, manager_id, max_sum, event_id, betPercents', 'required'),
			array('category_id, team1_id, team2_id, event_id, status, max_sum', 'numerical', 'integerOnly'=>true),
			array('team1_koef, team2_koef, draw_koef', 'numerical'),
			array('game_ico', 'length', 'max'=>255),
			array('betPercents', 'numerical','min'=>0, 'max'=>150),
			array('manager_id', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, category_id, team1_id, team2_id, team1_koef, team2_koef, draw_koef, game_ico, event_id, game_start_at, game_end, status, manager_id, max_sum', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'manager' => array(self::BELONGS_TO, 'User', 'manager_id'),
			'category' => array(self::BELONGS_TO, 'GameCategories', 'category_id'),
			'team1' => array(self::BELONGS_TO, 'Teams', 'team1_id'),
			'team2' => array(self::BELONGS_TO, 'Teams', 'team2_id'),
            'event' => array(self::BELONGS_TO, 'Events', 'event_id'),
            'gamesbetshistories' => array(self::HAS_MANY, 'Gamesbetshistory', 'gameId'),
            'userbets' => array(self::HAS_MANY, 'Userbets', 'gameId'),
            'result' => array(self::HAS_ONE, 'Result', 'game_id'),
            'recommendedBets' => array(self::HAS_MANY, 'RecommendedBets', 'game_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'category_id' => 'Category',
			'team1_id' => 'Team1',
			'team2_id' => 'Team2',
			'team1_koef' => 'Team1 Koef',
			'team2_koef' => 'Team2 Koef',
			'draw_koef' => 'Draw Koef',
			'game_ico' => 'Game Ico',
			'event_id' => 'Event',
			'create_at' => 'Create At',
			'game_start_at' => 'Game Start At',
			'game_end' => 'Game End',
			'status' => 'Status',
			'manager_id' => 'Manager',
			'max_sum' => 'Max Sum',
			'betPercents' => 'Bet Percents',
            'map' => 'Map',
		);
	}

    public function scopes() {
        return array(
            'onlyNotStarted' => array(
                'condition' => 't.game_start_at>' . time(),
            ),
            'onlyEightHours' => array(
                'condition' => ' t.status not in('. Games::GAME_STATUS_CANCELED .','. Games::GAME_STATUS_FINISHED . ') or t.game_end > ' . (time() - 8 * 60 * 60) .'',
            ),
            'gameStartTimeSort' => array(
                'order' => 'game_start_at DESC',
            ),
            'descSort' => array(
                'order' => 'DESC'
            )
        );
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
        $criteria->compare('name', $this->name);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('team1_id',$this->team1_id);
		$criteria->compare('team2_id',$this->team2_id);
		$criteria->compare('team1_koef',$this->team1_koef);
		$criteria->compare('team2_koef',$this->team2_koef);
		$criteria->compare('draw_koef',$this->draw_koef);
		$criteria->compare('game_ico',$this->game_ico,true);
		$criteria->compare('event_id',$this->event_id);
		$criteria->compare('create_at',$this->create_at,true);
		$criteria->compare('game_start_at',$this->game_start_at,true);
		$criteria->compare('game_end',$this->game_end);
		$criteria->compare('status',$this->status);
		$criteria->compare('manager_id',$this->manager_id,true);
		$criteria->compare('max_sum',$this->max_sum);
		$criteria->compare('betPercents',$this->betPercents);
        $criteria->compare('map',$this->map);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    /** get status as string ( status name) */
    public function showStatusTitle(){
        $filter = $this->alias('Status');
        return $filter[$this->status];
    }

    /** check if game is not started using it's status and result */
    public function isNotStarted(){
        if (($this->status == Games::GAME_STATUS_NOT_STARTED) && empty($this->result)) {
            return true;
        }
        return false;
    }

    /** check if game is not finished using it's status and result */
    public function isNotFinished(){

        if ((($this->status == Games::GAME_STATUS_IN_PROGRESS) or ($this->status == Games::GAME_STATUS_NOT_STARTED)) && empty($this->result)) {
            return true;
        }
        return false;
    }
    /** check if game is  finished using it's status and result */
    public function isFinished(){
        if (($this->status == Games::GAME_STATUS_FINISHED) && !empty($this->result)) {
            return true;
        }
        return false;
    }
    /** check if game is canceled by it's status */
    public function isCanceled(){
        if ($this->status == Games::GAME_STATUS_CANCELED) {
            return true;
        }
        return false;
    }

    public function beforeSave() {
        if ($this->draw_koef) {
            $this->draw_koef = Helper::formatKoef($this->draw_koef);
        }

        $this->team1_koef = Helper::formatKoef($this->team1_koef);
        $this->team2_koef = Helper::formatKoef($this->team2_koef);

        return parent::beforeSave();
    }
}
