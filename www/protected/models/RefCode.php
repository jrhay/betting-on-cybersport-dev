<?php

/**
 * This is the model class for table "ref_code".
 *
 * The followings are the available columns in table 'ref_code':
 * @property string $id
 * @property string $user_id
 * @property string $code
 * @property integer $reg_count
 * @property string $reg_users
 */
class RefCode extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'ref_code';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('user_id, code', 'required'),
            array('reg_count', 'numerical', 'integerOnly' => true),
            array('user_id', 'length', 'max' => 10),
            array('reg_users', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, user_id, code, reg_count, reg_users', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'user_id' => 'user id',
            'code' => 'code',
            'reg_count' => 'reg count',
            'reg_users' => 'reg users',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('user_id', $this->user_id, true);
        $criteria->compare('code', $this->code, true);
        $criteria->compare('reg_count', $this->reg_count);
        $criteria->compare('reg_users', $this->reg_users, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**get user ref code or  generates new ref code for user if not exists
     * @param integer $userId
     * @param bool $stringOnly // if needs only string nol all object
     * @return string
     */
    public function getRefCode($userId, $stringOnly = false)
    {
        $criteria = new CDbCriteria;

        $criteria->compare('user_id', $userId);

        $dataProvider = new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
        $data = $dataProvider->getData();
        if (!count($data)) {
            $newRefCode = new RefCode();
            $newRefCode->code = $this->generateRefCode($userId);
            $newRefCode->user_id = $userId;
            $result = $newRefCode->save();
            if (!$result) return false;
            return ($stringOnly) ? $newRefCode->code : $newRefCode;
        }
        /** @var RefCode $refCode */
        $refCode = $data[0];
        return $stringOnly ? $refCode->code : $refCode;
    }

    /** generates ref code
     * @param integer $userId
     * @return string
     */
    private function generateRefCode($userId)
    {
        $url = Yii::app()->params['currentDomain'];
        $url .= Yii::app()->createUrl('user/register', array('ruid' => $userId));
        return $url;
    }

    /**add information about new registered user
     * @param integer $newUserId
     */
    public function addNewUser($newUserId)
    {
        //referal bonus
        $groupBonusActivator = new GroupBonusActivator();
        $resultSumBonus = $groupBonusActivator->ActivateGroupBonuses(5, $this->user_id);

        $this->reg_count += 1;
        $regUsers = array();
        if (!empty($this->reg_users)) {
            $regUsers = unserialize($this->reg_users);
        }
        $regUsers[] = array($newUserId => $resultSumBonus);
        $this->reg_users = serialize($regUsers);
        $result = $this->save();
        return $result ? true : false;
    }

    /** returns count of registered users
     * @return integer
     */
    public function getRegCount()
    {
        $count = 0;
        if (!empty($this->reg_count)) {
            $count = $this->reg_count;
        }
        return $count;
    }

    public function getRegBonuses()
    {

    }

    /** returns array of users, who registered by current ref code
     * $return array[]
     */
    public function getRegUsers()
    {
        if (!empty($this->reg_users)) {
            $usersAr = unserialize($this->reg_users);
            $userModels = array();
            foreach ($usersAr as $uAr) {
                foreach ($uAr as $userId => $bonusSum) {
                    $user = User::model()->findByAttributes(array('id' => $userId));
                    $userModels[] = array('sum' => $bonusSum,'user' => $user);
                }
            }
            return $userModels;
        }
        return false;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return RefCode the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
