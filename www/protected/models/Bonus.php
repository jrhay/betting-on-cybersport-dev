<?php

/**
 * This is the model class for table "bonus".
 *
 * The followings are the available columns in table 'bonus':
 * @property integer $id
 * @property integer $user_id
 * @property integer $bonus_category_id
 * @property DateTime  $create_date
 * @property DateTime $end_date
 * @property integer $value
 * @property string $title
 * @property string $text
 * @property bool $active
 */
class Bonus extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @return PersonalMessage the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'bonus';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('user_id, value, bonus_category_id ', 'required'),
            array('title,subject', 'safe'),
            array('title, subject', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
            'bonus_category' => array(self::BELONGS_TO, 'BonusCategory', 'bonus_category_id')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {

        return array(
            'id' => Yii::t('site', 'id'),
            'user_id' => Yii::t('site','User'),
            'bonus_category_id' =>Yii::t('site', 'Category'),
            'create_date' => Yii::t('site','Create Date'),
            'end_date' => Yii::t('site','End Date'),
            'value' => Yii::t('site','Value'),
            'title' => Yii::t('site','Title'),
            'text' =>Yii::t('site', 'Text'),
            'active' => Yii::t('site','Active'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
// Warning: Please modify the following code to remove attributes that
// should not be searched.
        $criteria = new CDbCriteria;
//        $criteria->compare('id',$this->id,true);
//        $criteria->compare('sender_id',$this->sender);
//        $criteria->compare('recipient_id',2);
//        $criteria->compare('subject',$this->subject,true);
//        $criteria->compare('text',$this->text,true);
        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Retrieves a list of models using user_id
     * @return BonusBalance
     */
    public function getActiveBonusesByUserId($userId, $dateVerification = true)
    {
// Warning: Please modify the following code to remove attributes that
// should not be searched.
        $criteria = new CDbCriteria;
        $criteria->compare('user_id', $userId);
        $criteria->compare('active', 1);
        $dataProvider = new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 100)
        ));
        $bonuses = $dataProvider->getData();
        if ($dateVerification) {
            /**@var GroupBonus $bonus */
            foreach ($bonuses as $key => $bonus) {
                if (!$bonus->dateVerification()) {
                    unset($bonuses[$key]);
                }
            }
        }
        return $bonuses;
    }

    /**
     * Scopes list
     *
     * @return array
     */
    public function scopes()
    {
        return array(
            'active' => array(
                'condition' => '`active`=1'
            ),
        );
    }

    /**
     * adds bonus to user
     *
     * @param integer $userId
     * @param integer $value
     * @param integer $categoryId
     * @param DateTime $endDate
     * @return bool
     */
    public function addBonus($userId, $value, $bonusCategoryId, $endDate = null)
    {
        if ($bonusCategoryId == 7) {
            /** @var Usercash $userCash */
            $userCash = new Usercash();
            $userCash = $userCash->searchByUserId($userId)->getData();
            if (isset($userCash[0])) {
                $userCash = $userCash[0];
            } else {
                return false;
            }
            $userCash->realMoney += $value;
            $result = $userCash->save();
            $value = 0;

//            return $result ? true : false;
        }

        $this->user_id = $userId;
        $this->value = $value;
        $this->bonus_category_id = $bonusCategoryId;

        if (empty($this->title)) {
            /** @var BonusCategory $bonusCategory */
            $bonusCategory = new BonusCategory();
            $bonusCategory = $bonusCategory->getCategoryById($bonusCategoryId);
            $this->title = $bonusCategory->bonus_title;
        }

        if (empty($this->text)) {
            /** @var BonusCategory $bonusCategory */
            $bonusCategory = new BonusCategory();
            $bonusCategory = $bonusCategory->getCategoryById($bonusCategoryId);
            $this->title = $bonusCategory->bonus_message;
        }

        if (!empty ($endDate)) {
            $this->end_date = $endDate;
        }
        $result = $this->save();

        /** @var BonusBalance $bonusBalance */
        $bonusBalance = new BonusBalance();
        $bonusBalance = $bonusBalance->getBBalanceModelByUserid($userId);
        $bonusBalance->addSum($value);

        /** send personal message */
        if(!$endDate) {
            /** @var PersonalMessage $message */
            $message = new PersonalMessage();
            //admin
            $message->sender_id= 2;
            $message->subject = $this->title;
            $message->text = $this->text;
            $message->send($this->user_id);
        }
        return $result ? $this : false;
    }

    public function getErrorsModels()
    {
        return $this->_errorModels;
    }

    /**
     * Mark bonus as inactive
     */
    public function markAsInactive()
    {
        if ($this->active) {
            $this->active = 0;
            $this->save(false, array('active'));
        }
    }

    /**
     *date verification for bonus
     * @return bool
     */
    public function dateVerification()
    {
        $today = new DateTime();
        $today = $today->getTimestamp();
        $createDate = new DateTime();
        $createDate = $createDate->createFromFormat('Y-m-d H:i:s', $this->create_date);
        $createDate = $createDate->getTimestamp();

        $endDate = new DateTime();
        $endDate = $endDate->createFromFormat('Y-m-d H:i:s', $this->end_date);
        $endDate = $endDate->getTimestamp();

        if (!$endDate or $endDate < 0 or ($createDate <= $today && $today <= $endDate)) {
            return true;
        }
        return false;
    }
}