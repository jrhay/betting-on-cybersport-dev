<?php

/**
 * This is the model class for table "usercash".
 *
 * The followings are the available columns in table 'usercash':
 * @property integer $id
 * @property string $userId
 * @property integer $realMoney
 * @property integer $cyberMoney
 * @property integer $isBlocked
 *
 * The followings are the available model relations:
 * @property User $user
 */
class Usercash extends CActiveRecord
{

    const REAL_MONEY = 2;
    const PLAY_MONEY = 1;

	/**
	 * @return string the associated database table name
	 */

    public static function alias($type, $code = NULL) {
        $_items = array(
            'Status' => array(
                CybController::ANSWER_YES => Yii::t('general', 'Yes'),
                CybController::ANSWER_NO => Yii::t('general', 'No'),
            ),
            'MoneyType' => array(
                Usercash::PLAY_MONEY=> Yii::t('general', 'Play Money'),
                Usercash::REAL_MONEY => Yii::t('general', 'Real Money'),
            ),
        );

        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

    public static function getStatusFilter() {
        $filter = Usercash::alias('Status');
        return $filter;
    }
    public static function getMoneyTypeFilter() {
        $filter = Usercash::alias('MoneyType');
        return $filter;
    }

	public function tableName()
	{
		return 'usercash';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('userId, realMoney, cyberMoney', 'required'),
			array('realMoney, cyberMoney, isBlocked', 'numerical', 'integerOnly'=>true),
			array('userId', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, userId, realMoney, cyberMoney, isBlocked', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'userId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'userId' => 'User',
			'realMoney' => 'Real Money',
			'cyberMoney' => 'Cyber Money',
			'isBlocked' => 'Is Blocked',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

//		$criteria->compare('id',$this->id);
//		$criteria->compare('userId',$this->userId,true);
//		$criteria->compare('realMoney',$this->realMoney);
//		$criteria->compare('cyberMoney',$this->cyberMoney);
//		$criteria->compare('isBlocked',$this->isBlocked);

		return new CybActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     * @param integer $userId
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function searchByUserId($userId)
    {

        $criteria=new CDbCriteria;

//		$criteria->compare('id',$this->id);
		$criteria->compare('userId',$userId,true);
//		$criteria->compare('realMoney',$this->realMoney);
//		$criteria->compare('cyberMoney',$this->cyberMoney);
//		$criteria->compare('isBlocked',$this->isBlocked);

        return new CybActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Usercash the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
