<?php

/**
 * This is the model class for table "income_deposit_history".
 *
 * The followings are the available columns in table 'income_deposit_history':
 * @property string $id
 * @property integer $userId
 * @property integer $operationType
 * @property integer $summ
 * @property string $accountIdent
 * @property string $secureId
 * @property string $system
 * @property string $time
 * @property integer $status
 * @property string $sign
 */
class IncomeDepositHistory extends CActiveRecord
{
     const STATUS_NOT_PAID = 0;
     const STATUS_PAID = 1;
     const STATUS_PAID_WITH_ERRORS = 2;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'income_deposit_history';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('userId, operationType, summ, system, time', 'required'),
			array('userId, operationType, summ', 'numerical', 'integerOnly'=>true),
			array('time', 'length', 'max'=>20),
			array('accountIdent, secureId', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, userId, operationType, summ, accountIdent, secureId, system, time, status,sign', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'user' => array(self::BELONGS_TO,'User', 'userId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'userId' => 'User',
			'operationType' => 'operationType',
			'summ' => 'summ',
			'accountIdent' => 'accountIdent',
			'secureId' => 'secureId',
			'system' => 'System',
			'time' => 'Time',
            'status' => 'Status',
            'sign' => 'Sign'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('userId',$this->userId);
		$criteria->compare('operationType',$this->operationType);
		$criteria->compare('summ',$this->summ);
		$criteria->compare('accountIdent',$this->accountIdent,true);
		$criteria->compare('secureId',$this->secureId,true);
		$criteria->compare('system',$this->system,true);
		$criteria->compare('time',$this->time,true);
        $criteria->compare('status', $this->status,true);
        $criteria->compare('sign', $this->sign, true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return IncomeDepositHistory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
