<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class standartBetForm extends CFormModel
{
	public $gameID;
	public $teamID;
	public $summ;
	public $betTimestamp;
	public $moneyType;


	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(

			array('gameID, teamID, summ, moneyType', 'required'),
            array('summ', 'checkSum'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(

		);
	}

    public function checkSum($attribute,$params) {

		if($this->checkIfGuest($attribute)) {
			return;
		}

        //TODO: checks only cyber money
        $usercash = Usercash::model()->findByAttributes(array('userId'=>Yii::app()->user->id));
        $type = ($this->moneyType==2)?'realMoney':'cyberMoney';
        if ($usercash->$type < $this->summ) {
            $this->addError($attribute, 'you don\'t have enough money');
        }
    }

	public function checkIfGuest($attribute){
		if(Yii::app()->user->isGuest) {
			$this->addError($attribute, Yii::t('site','register and make a bet'));
			return true;
		}
		return false;
	}
}
