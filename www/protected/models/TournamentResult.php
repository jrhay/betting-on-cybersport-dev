<?php

/**
 * This is the model class for table "tournament_result".
 *
 * The followings are the available columns in table 'tournament_result':
 * @property string $id
 * @property string $tournament_id
 * @property string $time
 * @property string $winner_id
 * @property float $winner_koef
 */
class TournamentResult extends CActiveRecord
{
    /** @var  string $this ->logicError
     * put here sitring with error message if need to show it to user ( ''
     */
    public $logicError;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'tournament_result';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('tournament_id, time, winner_id, winner_koef', 'required'),
            array('tournament_id, time, winner_id', 'length', 'max' => 20),
            array('winner_koef,winner_id', 'numerical',  'min'=>0),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, tournament_id, time, winner_id,winner_koef', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'tournament' => array(self::BELONGS_TO, 'Tournaments', 'tournament_id'),
            'winner' => array(self::BELONGS_TO, 'Teams', 'winner_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('site', 'ID'),
            'tournament_id' => Yii::t('site', 'Tournament'),
            'time' => Yii::t('site', 'Time'),
            'winner_id' => Yii::t('site', 'Winner'),
            'winner_koef' => Yii::t('site', 'Winner_koef')
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('tournament_id', $this->tournament_id, true);
        $criteria->compare('time', $this->time, true);
        $criteria->compare('winner_id', $this->winner_id, true);
        $criteria->compare('winner_koef', $this->winner_koef, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TournamentResult the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    // make payments or recalculate payments
    public function handleTournamentBets()
    {
        if (!$this->handleTournament()) {
            return;
        }
        /** @var Tournamentsbets[] $bets */
        $bets = Tournamentsbets::model()->findAllByAttributes(array(
            'tournamentId' => $this->tournament_id
        ));
        foreach ($bets as $bet) {
            if (!$bet->isWin) {
                $this->handleBet($bet);
            } else {
                $this->reHandleTournamentBet($bet);
            }
        }
    }

    /** @param TournamentsBets $bet */
    protected function handleBet($bet)
    {
        if ($bet->teamId == $this->winner_id) {
            /** @var Tournaments $tournament */
            $this->makePayment($bet);
            $bet->isWin = Userbets::WIN_YES;
            $bet->save();
        } else {
            $bet->isWin = Userbets::WIN_NO;
            $bet->save();
        }
    }

    /** @param TournamentsBets $bet */
    protected function reHandleTournamentBet($bet)
    {
        switch (true) {
            case $this->winner_id == $bet->teamId && $bet->isWin == Userbets::WIN_YES :
                //do nothing
                break;
            case $this->winner_id == $bet->teamId && $bet->isWin == Userbets::WIN_NO:
                $this->makePayment($bet);
                $bet->isWin = Userbets::WIN_YES;
                $bet->save();
                break;
            case $this->winner_id != $bet->teamId && $bet->isWin == Userbets::WIN_YES :
                $this->withdrawPayment($bet);
                $bet->isWin = Userbets::WIN_NO;
                $bet->save();
                break;
            case $this->winner_id != $bet->teamId && $bet->isWin == Userbets::WIN_NO:
                //do nothing
                break;
        }
    }

    /**@var Tournamentsbets $bet */
    protected function makePayment($bet)
    {
        $tournament = $bet->tournament;
        if ($tournament->isFloatingKoef) {
            $koef = $this->winner_koef;
            $summ = $bet->summ;
            $payment = $koef * $summ;
        } else {
            $koef = $bet->koef;
            $summ = $bet->summ;
            $payment = $koef * $summ;

        }
        $user = User::model()->findByPk($bet->userId);
        /** @var Usercash $usercash */
        $usercash = $user->usercash;
        $usercash->realMoney += intval($payment);
        $usercash->save();
    }

    /**@var Tournamentsbets $bet */
    protected function withdrawPayment($bet)
    {
        $tournament = $bet->tournament;
        if ($tournament->isFloatingKoef) {
            $koef = $this->winner_koef;
            $summ = $bet->summ;
            $payment = $koef * $summ;

        } else {
            $koef = $bet->koef;
            $summ = $bet->summ;
            $payment = $koef * $summ;

        }
        $user = User::model()->findByPk($bet->userId);
        /** @var Usercash $usecash */
        $usecash = $user->usercash;
        $usecash->realMoney -= intval($payment);
        $usecash->save();
    }

    /** @param Tournamentsbets $bet
     * @return bool
     */
    protected function handleTournament()
    {
        /** @var Tournaments $tournament */
        $tournament = Tournaments::model()->findByPk($this->tournament_id);
        /** @var Tournamentsdetails $tdetails */
        $tdetails = Tournamentsdetails::model()->findByAttributes(array(
            'tournamentId' => $tournament->id,
            'teamId' => $this->winner_id));
        if (empty($tdetails)) {
            $this->logicError = Yii::t('site', 'No such team in selected tournament!');
            return false;
        }
        $tournament->winnerId = $this->winner_id;
        if (!$tournament->isFloatingKoef) {
            if(!empty($this->winner_koef)) {
                $this->logicError = Yii::t('site','You are trying to set winner koef to tournament with not floating koef');
                return false;
            }
            $tournament->winnerKoef = $tdetails->teamKoef;
        } else {
            if(empty($this->winner_koef)) {
                $this->logicError = Yii::t('site','You should  set winner koef to tournament with  floating koef');
                return false;
            }
            $tournament->winnerKoef = $this->winner_koef;
        }
        $tournament->status = Games::GAME_STATUS_FINISHED;
        $tournament->save();
        return true;
    }
}
