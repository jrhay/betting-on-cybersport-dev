<?php

/**
 * This is the model class for table "bonus_category".
 *
 * The followings are the available columns in table 'bonus_category':
 * @property integer $id
 * @property string $category_name
 * @property string $bonus_title
 * @property string $bonus_message
 */
class BonusCategory extends CActiveRecord
{
    //Todo: add constants defining categories
    /**
     * Returns the static model of the specified AR class.
     * @return PersonalMessage the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'bonus_category';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('category_name', 'required'),
            array('title, bonus_message', 'safe'),
            array('id, category_name, title, bonus_message ', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'bonus' => array(self::HAS_MANY, 'Bonus', 'bonus_category_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'category_name' => 'Category Name',
            'bonus_title' => 'Bonus Title',
            'bonus_message' => 'Bonus Message',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
// Warning: Please modify the following code to remove attributes that
// should not be searched.
        $criteria = new CDbCriteria;
//        $criteria->compare('id',$this->id,true);
//        $criteria->compare('sender_id',$this->sender);
//        $criteria->compare('recipient_id',2);
//        $criteria->compare('subject',$this->subject,true);
//        $criteria->compare('text',$this->text,true);
        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Retrieves a list of models using category_id
     * @param integer $bonus_category_id
     * @return BonusCategory
     */
    public function getCategoryById($bonus_category_id)
    {
// Warning: Please modify the following code to remove attributes that
// should not be searched.
        $criteria = new CDbCriteria;
        $criteria->compare('id', $bonus_category_id);
        $dataProvider = new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 100)
        ));
        $categoryArr = $dataProvider->getData();
        if (isset($categoryArr[0])) {
            $category = $categoryArr[0];
        } else {
            return false;
        }
        return $category;
    }

    /**
     * Retrieves a list of models using category name
     * @param string $bonus_category_id
     * @return BonusCategory
     */
    public function getCategoryByName($bonus_category_name)
    {
// Warning: Please modify the following code to remove attributes that
// should not be searched.
        $criteria = new CDbCriteria;
        $criteria->compare('category_name', $bonus_category_name);
        $dataProvider = new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 100)
        ));
        $categoryArr = $dataProvider->getData();
        $category = $categoryArr[0];
        return $category;
    }

    public function getErrorsModels()
    {
        return $this->_errorModels;
    }

}