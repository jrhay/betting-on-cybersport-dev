<?php

/**
 * Class AccountEditForm
 */
class AccountEditForm extends CFormModel {

    /**
     * @var
     */
    public $firstName;
    /**
     * @var
     */
    public $lastName;
    /**
     * @var
     */
    public $userName;
    /**
     * @var
     */

    public $password;
    /**
     * @var
     */
    public $repeat_password;
    /**
     * @var
     */
    public $old_password;
    /**
     * @var
     */
    public $change_password;
    public $language;
    public $timezone;
    /**
     * @var
     */

    /**
     * Model rules
     * @return type
     */
    public function rules() {
        return array(
            array('firstName, lastName,userName,repeat_password,password,old_password,language,change_password, timezone', 'ext.validators.CRequiredValidatorExt'),
            array('password', 'length', 'min' => 8, 'max' => 12),
            array('password', 'match', 'pattern' => '/(?=.*\d)/', 'message' => Yii::t('general', "Password must include at least one number")),
            array('password', 'match', 'pattern' => '/(?=.*[A-Z])/', 'message' => Yii::t('general', "Password must include at least one uppercase letter!")),
            array('password', 'match', 'pattern' => '/(?=.*[a-z])/', 'message' => Yii::t('general', "Password must include at least one lowercase letter!")),
            array('repeat_password', 'compare', 'compareAttribute' => 'password'),
            array('old_password', 'initialPassword'),

        );
    }

    /**
     * Model attribute labels
     * @return type
     */
    public function attributeLabels() {
        return array(
            'firstName' => Yii::t('account', 'First Name'),
            'lastName' => Yii::t('account', 'Last Name'),
            'userName' => Yii::t('account', 'User Name'),
            'mobilePhone1' => Yii::t('account', 'Mobile Phone'),
            'userTimeZoneUTC_Plus' => Yii::t('account', 'Timezone'),
            'careProvider' => Yii::t('account', 'Care provider'),
            'old_password' => Yii::t('account', 'Old password'),
            'password' => Yii::t('account', 'New Password'),
            'repeat_password' => Yii::t('account', 'Repeat password'),
            'organizations' => Yii::t('account', 'Organization'),
            'timezone' => Yii::t('account','Timezone'),
            'language' => Yii::t('account','Language'),
//            'userName' => Yii::t('account',' Username'),
        );
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function initialPassword($attribute, $params) {
        if ($this->change_password) {
            $user = User::model()->findByPk(Yii::app()->user->id);
            if ($user->password != crypt($this->$attribute, md5($this->$attribute)) . md5($this->$attribute))
                $this->addError($attribute, Yii::t('general', 'Old password is wrong!'));
        }
    }
}