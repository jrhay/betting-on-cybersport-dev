<?php
/**
 * Created by PhpStorm.
 * User: lldl
 * Date: 10/29/14
 * Time: 4:30 PM
 */

class MoneyForm extends CFormModel {
    public $realMoney;
    public $cyberMoney;
//    public function rules()
//    {
//        return array(
//
//            array('realMoney', 'd'),
//            array('summ', 'numerical','max'=>100),
////            array('summ', 'checkSum'),
//        );
//    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array(
            'realMoney'=>Yii::t('site','Real Money'),
            'cybetMoney'=> Yii::t('site', 'Cyber Money'),
        );
    }
}
