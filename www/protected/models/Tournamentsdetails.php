<?php

/**
 * This is the model class for table "tournamentsdetails".
 *
 * The followings are the available columns in table 'tournamentsdetails':
 * @property integer $id
 * @property integer $tournamentId
 * @property integer $teamId
 * @property double $teamKoef
// * @property double $baseKoef
 *
 * The followings are the available model relations:
 * @property Teams $team
 * @property Tournaments $tournament
 */
class Tournamentsdetails extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tournamentsdetails';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tournamentId, teamId, teamKoef', 'required'),
			array('tournamentId, teamId', 'numerical', 'integerOnly'=>true),
			array('teamKoef', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, tournamentId, teamId, teamKoef', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'team' => array(self::BELONGS_TO, 'Teams', 'teamId'),
			'tournament' => array(self::BELONGS_TO, 'Tournaments', 'tournamentId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tournamentId' => 'Tournament',
			'teamId' => 'Team',
			'teamKoef' => 'Team Koef',
//			'baseKoef' => 'Base Koef',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('tournamentId',$this->tournamentId);
		$criteria->compare('teamId',$this->teamId);
		$criteria->compare('teamKoef',$this->teamKoef);
//		$criteria->compare('baseKoef',$this->baseKoef);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Tournamentsdetails the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function beforeSave() {
        $this->teamKoef = Helper::formatKoef($this->teamKoef);
        return parent::beforeSave();
    }
}
