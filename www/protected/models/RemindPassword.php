<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class RemindPassword extends CFormModel {

    public $email;
    public $verifyCode;

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules() {
        return array(
            // username and password are required
            array('email', 'required'),
            array('email', 'email'),
            array('verifyCode','captcha','allowEmpty'=>false,
            ),

        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels() {
        return array(
            'email' => Yii::t('site', 'Email'),
            'verifyCode' =>  Yii::t('site', 'Verify Code:'),

        );
    }

}
