<?php

/**
 * This is the model class for table "betsvideos".
 *
 * The followings are the available columns in table 'betsvideos':
 * @property integer $id
 * @property string $title
 * @property string $link
 * @property boolean $lang
 * @property boolean $status
 * @property boolean $pars_inter
 */
class Betsvideos extends CActiveRecord
{
    //language
    const LANG_EN = 0;
    const LANG_RU = 1;

    //can be deleted
    const PARSE_INTER_YES = 1;
    const PARSE_INTER_NO = 0;

    public static function getStatusFilter()
    {
        $filter = Betsvideos::alias('Status');
        return $filter;
    }

    public static function alias($type, $code = NULL)
    {
        $_items = array(
            'Status' => array(
                CybController::ACTIVE_STATUS => Yii::t('general', 'Active'),
                CybController::INACTIVE_STATUS => Yii::t('general', 'Inactive'),
            ),
            'Lang' => array(
                Betsvideos::LANG_RU => Yii::t('general', 'Russian'),
                Betsvideos::LANG_EN => Yii::t('general', 'English'),
            ),
            'ParseInter' => array(
                Betsvideos::PARSE_INTER_YES => Yii::t('general', 'interaction with parser'),
                Betsvideos::PARSE_INTER_NO => Yii::t('general', 'no interaction'),
            )
        );

        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

    public static function getLangFilter()
    {
        $filter = Betsvideos::alias('Lang');
        return $filter;
    }

    public static function getParseInterFilter()
    {
        $filter = Betsvideos::alias('ParseInter');
        return $filter;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Betsvideos the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'betsvideos';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('title, link, lang, status, parse_inter', 'required'),
            array('title', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, title, link', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'title' => 'Title',
            'link' => 'Link',
            'lang' => 'Language',
            'status' => 'Status',
            'parse_inter' => 'Parse Interaction'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('link', $this->link, true);
        $criteria->compare('lang', $this->lang, true);
        $criteria->compare('status', $this->status, true);
        $criteria->compare('parse_inter', $this->parse_inter, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function scopes()
    {
        return array(
            'onlyActive' => array(
                'condition' => 't.status = ' . CybController::ACTIVE_STATUS,
            ),
            'onlyParseInter' => array(
                'condition' => 't.parse_inter = ' . Betsvideos::PARSE_INTER_YES,
            ),
            'onlyRu' => array(
                'condition' => 't.lang = ' . Betsvideos::LANG_RU,
            ),
            'onlyEn' => array(
                'condition' => 't.lang = ' . Betsvideos::LANG_EN,
            )
        );
    }
}
