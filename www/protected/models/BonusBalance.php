<?php

/**
 * This is the model class for table "bonus_balance".
 *
 * The followings are the available columns in table 'bonus_balance':
 * @property integer $id
 * @property integer $user_id
 * @property integer $balance
 */
class BonusBalance extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @return PersonalMessage the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'bonus_balance';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('user_id', 'required'),
            array('id, user_id, balance', 'safe'),
            array('id, user_id, balance', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'user_id' => 'User Id',
            'balance' => 'Balance',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
// Warning: Please modify the following code to remove attributes that
// should not be searched.
        $criteria = new CDbCriteria;
        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function getBBalanceModelByUserid($userId)
    {
        $criteria = new CDbCriteria;
        $criteria->compare('user_id',$userId);
        $dataProvider =  new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
        ));
        $data = $dataProvider->getData();
        if(!count($data)){
            $newBonusBalance = new BonusBalance();
            $newBonusBalance->user_id = $userId;
            $newBonusBalance->save();
            return $newBonusBalance;

        }
        $bonusBalance = $data[0];
        return $bonusBalance;
    }

    /**
     * Retrieves a list of models using user_id
     * @param integer $userId
     * @return integer
     */
    public function getBalanceByUserId($userId)
    {
// Warning: Please modify the following code to remove attributes that
// should not be searched.
        $criteria = new CDbCriteria;
        $criteria->compare('user_id', $userId);
        $providerData = new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 100)
        ));
        $data = $providerData->getData();
        /** @var BonusBalance $balanceObject */
        if(isset($data[0])) {
            $balanceObject = $data[0];
        } else {
            return false;
        }

        $balance = $balanceObject->getBalance();
        return $balance;
    }

    /**
     * return balance
     * @return integer
     */
    public function getBalance()
    {
// Warning: Please modify the following code to remove attributes that
// should not be searched.
        if (!empty($this->balance)) {
            return $this->balance;
        } else {
            return false;
        }
    }

    /** set new  balance
     * @param integer $balance
     * @return bool
     **/
    public function setBalance($balance)
    {
        $this->balance = $balance;
        $result = $this->save();
        return $result ? true : false;
    }

    /** add sum
     * @param integer $sum
     * @return bool
     **/
    public function addSum($sum)
    {
        $this->balance += $sum;
        $result = $this->save();
        return $result ? true : false;
    }

    public function getErrorsModels()
    {
        return $this->_errorModels;
    }
}