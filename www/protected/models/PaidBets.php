<?php

/**
 * This is the model class for table "paid_bets".
 *
 * The followings are the available columns in table 'paid_bets':
 * @property string $id
 * @property string $title
 * @property string $title_eng
 * @property string $text
 * @property string $text_eng
 * @property string $img
 * @property string $img_eng
 * @property integer $date
 */
class PaidBets extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'paid_bets';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, title_eng, text, text_eng, img, img_eng, date', 'required'),
			array('title, title_eng, img, img_eng, date', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, title_eng, text, text_eng, img, img_eng, date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('paidbets','ID'),
			'title' => Yii::t('paidbets','Title'),
			'title_eng' => Yii::t('paidbets','Title Eng'),
			'text' => Yii::t('paidbets','Text'),
			'text_eng' => Yii::t('paidbets','Text Eng'),
			'img' => Yii::t('paidbets','Img'),
			'img_eng' => Yii::t('paidbets','Img Eng'),
            'date' => Yii::t('paidbets', 'Date'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('title_eng',$this->title_eng,true);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('text_eng',$this->text_eng,true);
		$criteria->compare('img',$this->img,true);
		$criteria->compare('img_eng',$this->img_eng,true);
        $criteria->compare('date', $this->date, true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PaidBets the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
