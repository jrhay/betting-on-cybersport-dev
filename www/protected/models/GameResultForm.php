<?php
/**
 * Created by PhpStorm.
 * User: lldl
 * Date: 10/30/14
 * Time: 3:40 PM
 */

class GameResultForm extends CModel{
    public $result;
//    public $gameEndTime;
    public $videoFrameCode;
    public $score;

    public function rules() {
        return array(
            array('result,score', 'required'),
        );
    }

    /**
     * Returns the list of attribute names of the model.
     * @return array list of attribute names.
     */
    public function attributeNames()
    {
        // TODO: Implement attributeNames() method.
    }
}