<?php

/**
 * This is the model class for table "promo_code".
 *
 * The followings are the available columns in table 'promo_code':
 * @property string $id
 * @property integer $active
 * @property string $type
 * @property string $code
 * @property integer $value
 */
class PromoCode extends CActiveRecord
{
    const REAL_MONEY = 1;
    const BONUS_MONEY = 2;
    const PERCENT_TO_REAL_MONEY = 3;
    const PERCENT_TO_BONUS_MONEY = 4;
    const FREE_PURCHASE = 5;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'promo_code';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('type, code, value', 'required'),
            array('type', 'length', 'max' => 5),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, active, type, code, value', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'active' => 'is active',
            'type' => 'type',
            'code' => 'code',
            'value' => 'value',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('active', $this->active);
        $criteria->compare('type', $this->type, true);
        $criteria->compare('code', $this->code, true);
        $criteria->compare('value', $this->value);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }


    /** create new promo code
     * @param integer $type
     * @param string $code
     * @param integer $value
     */
    public function addNewPromoCode($type, $code, $value)
    {
        // check if promocode already exists
        $criteria = new CDbCriteria;
        $criteria->compare('code', $code);
        $dataProvider = new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
        $data = $dataProvider->getData();

        if(count($data)) {
            return false;
        }

        $this->active = true;
        $this->code = $code;
        $this->type = intval($type);
        $this->value = intval($value);
        $result = $this->save();
        return $result ? true : false;
    }

    /** activates promo code and makes bonuses and other
     * @promo string $code
     */
    public function activatePromoCode($code)
    {
        $criteria = new CDbCriteria;
        $criteria->compare('code', $code);
        $dataProvider = new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
        $data = $dataProvider->getData();
        if (isset($data[0])) {
            /** @var PromoCode $pCode */
            $pCode = $data[0];
        } else {
            return false;
        }
        if (!$pCode->isActive()) {
            return false;
        }
        return $pCode->doBonus();
    }

    /** do bonus from promo code */
    public function doBonus()
    {
        $userId = Yii:: app()->getUser()->id;
        switch ($this->type) {
            case 1:
                /** @var Usercash $usercash */
                $usercash = new Usercash();
                $data = $usercash->searchByUserId($userId)->getData();
                if (isset($data[0])) {
                    $usercash = $data[0];
                } else {
                    return false;
                }
                $usercash->realMoney += intval($this->value);
                $result = $usercash->save();
                if ($result) {
                    $this->markAsInactive();
                }
                return $result ? true : false;
                break;
            case 2:
                //realizing using bonus balance
//                $bonulBalance = new BonusBalance();
//                /** @var BonusBalance $bonulBalance */
//                $bonulBalance = $bonulBalance->getBalanceByUserId($userId);
//                $result = $bonulBalance->addSum($this->value);
//                return $result ? true: false;
//                break;

                $bonus = new Bonus();
                $result = $bonus->addBonus($userId, $this->value, 6, null);
                if ($result) {
                    $this->markAsInactive();
                }
                return $result ? true : false;
                break;

            case 3:
                /** @var Usercash $usercash */
                $usercash = new Usercash();
                /** @var Usercash $currentUsercash */
                $data = $usercash->searchByUserId($userId)->getData();
                if (isset($data[0])) {
                    $currentUsercash = $data[0];
                } else {
                    return false;
                }
                $deposit = $currentUsercash->realMoney;
                if($this->value >= 100) {
                    $this->value = 100;
                }
                $value = ($this->value) * 0.01 * $deposit;
                $currentUsercash->realMoney += intval($value);
                $result = $currentUsercash->save();
                if ($result) {
                    $this->markAsInactive();
                }
                return $result ? true : false;
                break;
            case 4:
                /** @var Usercash $usercash */
                $usercash = new Usercash();
                /** @var Usercash $currentUsercash */
                $data = $usercash->searchByUserId($userId)->getData();
                if (isset($data[0])) {
                    $currentUsercash = $data[0];
                } else {
                    return false;
                }
                $deposit = $currentUsercash->realMoney;
                if($this->value >= 100) {
                    $this->value = 100;
                }
                $value = ($this->value) * 0.01 * $deposit;

                $bonus = new Bonus();
                $result = $bonus->addBonus($userId, $value, 6, null);
                if ($result) {
                    $this->markAsInactive();
                }
                return $result ? true : false;

                break;
            case 5:
                //todo: implement free purchase
                return true;
                break;
            default:
                //todo: implement
                return true;
        }
    }

    /**
     * Mark promo as inactive
     */
    public function markAsInactive()
    {
        if ($this->active) {
            $this->active = 0;
            $this->save(false, array('active'));
        }
    }

    /**
     * Mark if promo activee
     */
    public function isActive()
    {
        if ($this->active) {
            return true;
        } else return false;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return PromoCode the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
