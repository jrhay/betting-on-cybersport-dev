<?php

/**
 * This is the model class for table "tournamentsbets".
 *
 * The followings are the available columns in table 'tournamentsbets':
 * @property integer $id
 * @property string $userId
 * @property integer $tournamentId
 * @property integer $teamId
 * @property double $koef
 * @property integer $status
 * @property integer $summ
 * @property integer $isWin
 *
 * The followings are the available model relations:
 * @property Tournaments $tournament
 * @property User $user
 * @property Teams $team
 */
class Tournamentsbets extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tournamentsbets';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('userId, tournamentId, teamId, koef, status', 'required'),
			array('tournamentId, teamId, status', 'numerical', 'integerOnly'=>true),
			array('koef', 'numerical'),
			array('userId', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, userId, tournamentId, teamId, koef, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tournament' => array(self::BELONGS_TO, 'Tournaments', 'tournamentId'),
			'user' => array(self::BELONGS_TO, 'User', 'userId'),
			'team' => array(self::BELONGS_TO, 'Teams', 'teamId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'userId' => 'User',
			'tournamentId' => 'Tournament',
			'teamId' => 'Team',
			'koef' => 'Koef',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('userId',$this->userId,true);
		$criteria->compare('tournamentId',$this->tournamentId);
		$criteria->compare('teamId',$this->teamId);
		$criteria->compare('koef',$this->koef);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Tournamentsbets the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function beforeSave() {
        $this->koef = Helper::formatKoef($this->koef);
        return parent::beforeSave();
    }
}
