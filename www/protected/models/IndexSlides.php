<?php

/**
 * This is the model class for table "index_slides".
 *
 * The followings are the available columns in table 'index_slides':
 * @property string $id
 * @property string $slide
 * @property string $slide_eng
 * @property string $title
 * @property string $text
 * @property string $title_eng
 * @property string $text_eng
 */
class IndexSlides extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'index_slides';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('slide, title,title_eng, text, text_eng', 'required'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, slide, title, text', 'safe', 'on'=>'search'),
            array('slide', 'unsafe'),
            array('slide', 'file', 'types'=>'jpg, gif, png','allowEmpty'=>true,'on'=>'update'),
            array('slide_eng', 'file', 'types'=>'jpg, gif, png','allowEmpty'=>true,'on'=>'update'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'slide' => 'Slide',
            'slide_eng' => 'Slide Eng',
			'title' => 'Title',
            'title_eng' => 'Title Eng',
			'text' => 'Text',
            'text_eng' => 'Text Eng'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('slide',$this->slide,true);
        $criteria->compare('slide_eng',$this->slide_eng,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('text',$this->text,true);
        $criteria->compare('text_eng',$this->text_eng,true);
        $criteria->compare('title_eng',$this->text_eng,true);


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return IndexSlides the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
