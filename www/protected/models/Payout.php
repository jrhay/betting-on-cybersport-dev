<?php

/**
 * This is the model class for table "payout".
 *
 * The followings are the available columns in table 'payout':
 * @property string $id
 * @property string $date
 * @property string $system_id
 * @property double $summ
 * @property string $e_wallet
 * @property string $user_id
 * @property integer $status
 * @property string $details
 *
 * The followings are the available model relations:
 * @property PaymentSystem $system
 */
class Payout extends CActiveRecord
{
     const STATUS_CREATED = 0;
     const STATUS_PAID_MANUALLY = 1;
     const STATUS_PAID_AUTOMATED = 2;
     const STATUS_BLOCKED = 3;
     const STATUS_AUTOMATED_PAYMENT_ERROR = 4;


    public static function getStatusFilter() {
        $filter = Payout::alias('Status');
        return $filter;
    }
    public static function alias($type, $code = NULL) {
        $_items = array(
            'Status' => array(
                self::STATUS_CREATED => Yii::t('general', 'Created'),
                self::STATUS_PAID_AUTOMATED => Yii::t('general', 'Paid automated'),
                self::STATUS_PAID_MANUALLY => Yii::t('general', 'Paid manually'),
                self::STATUS_AUTOMATED_PAYMENT_ERROR => Yii::t('general', 'Error automated payout'),
                self::STATUS_BLOCKED => Yii::t('general', 'Blocked Payout')
            ),
        );

        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'payout';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('date, system_id, summ, e_wallet, user_id', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('summ', 'numerical'),
			array('date, system_id, user_id', 'length', 'max'=>20),
			array('e_wallet', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, date, system_id, summ, e_wallet, user_id, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'system' => array(self::BELONGS_TO, 'PaymentSystem', 'system_id'),
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'date' => Yii::t('payout','Date'),
			'system_id' => Yii::t('payout','System'),
			'summ' => Yii::t('payout','Summ'),
			'e_wallet' => Yii::t('payout','Personal Ident ( e_wallet number )'),
			'user_id' => Yii::t('payout','User'),
			'status' => Yii::t('payout','Status'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('system_id',$this->system_id,true);
		$criteria->compare('summ',$this->summ);
		$criteria->compare('e_wallet',$this->e_wallet,true);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Payout the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /** make automated payout using */
    public function autoPayout(){
        $sysName = PaymentSystem::model()->findByPk($this->system_id)->name;
        switch ($sysName) {
            case 'payeer':
                $payeer = New Payeer();
                if($payeer->payout($this->summ, $this->e_wallet)) {
                    $this->status = self::STATUS_PAID_AUTOMATED;
                    $this->details = serialize(unserialize($this->details) . '------------------ successfully automated payment time: '. gmdate('Y-m-d H:i:s',time()));
                    return $this->save();
                } else {
                    $this->status = self::STATUS_AUTOMATED_PAYMENT_ERROR;
                    $error = json_encode($payeer->errors);
                    $this->details = serialize(unserialize($this->details) . '------------------ error trying to make auto payment time: '. gmdate('Y-m-d H:i:s',time()) .'error: ' . $error);
                    $this->save();
                    return false;
                }
                break;
            default: return false;
        }
    }
}
