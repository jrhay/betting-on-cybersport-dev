<?php

/**
 * This is the model class for table "usertype".
 *
 * The followings are the available columns in table 'usertype':
 * @property string $id
 * @property string $name
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property User[] $users
 */
class Usertype extends CActiveRecord
{

    const USERTYPE_ADMINISTRATOR = 1;
    const USERTYPE_MODERATOR = 3;
    const USERTYPE_REGISTERED = 4;
    const USERTYPE_CLIENT_PRO = 5;
    const USERTYPE_ANALYST = 6;
    const USERTYPE_ADMIN_MODERATOR = 7;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'usertype';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('status', 'numerical', 'integerOnly' => true),
            array('type', 'length', 'max' => 45),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, type, status', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'users' => array(self::HAS_MANY, 'User', 'usertypeId'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'type' => 'Type',
            'status' => 'Status',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('type', $this->name, true);
        $criteria->compare('status', $this->status);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Usertype the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function ifAdminModeratorAccess($userId = null)
    {
        if ($userId) {
            $user = User::model()->findByPk($userId);
        }

        $user = Yii::app()->user;

        if (in_array($user->usertype, array(Usertype::USERTYPE_ADMIN_MODERATOR, Usertype::USERTYPE_ADMINISTRATOR))) {
            return true;
        }
        return false;
    }

    /** check if current user has access ,access level is given in param
     * @param integer $accessLevel
     */
    public static function  needAccess($accessLevel)
    {
        /** @var User $user */
        //we need User model, not WebUxer
        if(Yii::app()->user->isGuest) {
            return false;
        }
        $user = Yii::app()->user;
        switch ($accessLevel) {
            case self::USERTYPE_ADMINISTRATOR:
                if (in_array($user->usertypeId, array(self::USERTYPE_ADMINISTRATOR))) {
                    return true;
                }
                break;
            case self::USERTYPE_ADMIN_MODERATOR:
                if (in_array($user->usertypeId, array(self:: USERTYPE_ADMIN_MODERATOR, self::USERTYPE_ADMINISTRATOR))) {
                    return true;
                }
                break;
            case self::USERTYPE_ANALYST:
                if (in_array($user->usertypeId, array(self:: USERTYPE_ANALYST, self::USERTYPE_ADMINISTRATOR))) {
                    return true;
                }
                break;
            case self::USERTYPE_MODERATOR:
                if (in_array($user->usertypeId, array(self::USERTYPE_MODERATOR, self:: USERTYPE_ADMIN_MODERATOR, self::USERTYPE_ADMINISTRATOR))) {
                    return true;
                }
                break;
            case self::USERTYPE_CLIENT_PRO:
                if (in_array($user->usertypeId, array(self::USERTYPE_CLIENT_PRO, self::USERTYPE_ANALYST, self::USERTYPE_MODERATOR, self:: USERTYPE_ADMIN_MODERATOR, self::USERTYPE_ADMINISTRATOR))) {
                    return true;
                }
                break;
            case self::USERTYPE_REGISTERED:
                return true;
                break;
        }
        return false;
    }
}
