<?php

/**
 * Class Message
 */
class Message extends CActiveRecord {

    /**
     * @var
     */
    /**
     * @var
     */
    public $message, $category;

    /**
     * @param string $className
     * @return CActiveRecord
     */
    static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return mixed
     */
    function tableName() {
        return Yii::app()->getMessages()->translatedMessageTable;
    }

    /**
     * @return array
     */
    function rules() {
        return array(
            array('id,language', 'required'),
            array('id', 'numerical', 'integerOnly' => true),
            array('language', 'length', 'max' => 16),
            array('translation', 'safe'),
            array('id, language, translation, category, message', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array
     */
    function relations() {
        return array(
            'source' => array(self::BELONGS_TO, 'MessageSource', 'id'),
            'lang' => array(self::BELONGS_TO, 'Languages', array('language'=>'dialect')),
        );
    }

    /**
     * @return array
     */
    function attributeLabels() {
        return array(
            'id' => Yii::t('general','ID'),
            'language' => Yii::t('general','Language'),
            'translation' => Yii::t('general','Translation'),
            'category' => MessageSource::model()->getAttributeLabel('category'),
            'message' => MessageSource::model()->getAttributeLabel('message'),
        );
    }

    /**
     * @return MGHActiveDataProvider
     */
    function search() {
        $criteria = new CDbCriteria;
        $criteria->select = 't.*,source.message as message,source.category as category';
        $criteria->with = array('source');

        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.language', $this->language, true);
        $criteria->compare('t.translation', $this->translation, true);
        $criteria->compare('source.category', $this->category, true);
        $criteria->compare('source.message', $this->message, true);

        return new CybActiveDataProvider(get_class($this), array(
            'pagination'=>array(
                'pageSize'=>  Helper::getPageSize(),
            ),
            'criteria' => $criteria,
        ));
    }

}