<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class expressBetForm extends CFormModel
{

	public $summ;
	public $time;



	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(

			array('summ', 'required'),
            array('summ', 'checkSum'),
            array('summ', 'numerical','min'=>1,'max'=>100),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
            'summ'=>Yii::t('general','Bet Summ')
		);
	}

    public function checkSum($attribute,$params) {

		if($this->checkIfGuest($attribute)) {
			return;
		}
        //TODO: checks only cyber money
        $usercash = Usercash::model()->findByAttributes(array('userId'=>Yii::app()->user->id));
        if ($usercash->realMoney < $this->summ) {
            $this->addError($attribute, 'you don\'t have enough money');
        }
    }

	public function checkIfGuest($attribute){
		if(Yii::app()->user->isGuest) {
			$this->addError($attribute, Yii::t('site','register and make a bet'));
			return true;
		}
		return false;
	}
}
