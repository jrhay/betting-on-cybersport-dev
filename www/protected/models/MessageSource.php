<?php

/**
 * Class MessageSource
 */
class MessageSource extends CActiveRecord {

    /**
     * @var
     */
    /**
     * @var
     */
    public $language, $translation;

    /**
     * @param string $className
     * @return CActiveRecord
     */
    static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return mixed
     */
    function tableName() {
        return Yii::app()->getMessages()->sourceMessageTable;
    }

    /**
     * @return array
     */
    function rules() {
        return array(
            array('category, message', 'required'),
            array('category', 'length', 'max' => 32),
            array('message', 'safe'),
            array('id, category, message, language', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array
     */
    function relations() {
        return array(
            'mt' => array(self::HAS_MANY, 'Message', 'id', 'joinType' => 'left join'),
        );
    }

    /**
     * @return array
     */
    function attributeLabels() {
        return array(
            'id' => Yii::t('general','ID'),
            'category' => Yii::t('general','Category'),
            'message' => Yii::t('general','Message'),
        );
    }

    /**
     * @return CybActiveDataProvider
     */
    function search() {
        $language_count = Languages::model()->enabled()->count();
        $criteria = new CDbCriteria;
        $criteria->select = 't.*, COUNT(mt.translation) AS translation';
        $criteria->join = " LEFT JOIN `".Yii::app()->getMessages()->translatedMessageTable."` AS `mt` ON `mt`.`id` = `t`.`id` ";
        
        if (Yii::app()->user->getState('language_filter', '')) {            
            $criteria->join .= " AND `mt`.`language` = :lang ";            
            $criteria->params[':lang'] = Yii::app()->user->getState('language_filter', '');
            $language_count = 1;
        }
        
        if (Yii::app()->user->getState('missing', 0)) {
            $criteria->having = 'translation < '.$language_count;            
        }        
        
        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.category', $this->category);
        $criteria->compare('t.message', $this->message, true);
                
        $criteria->group = 't.id';
        
        $total = self::model()->count($criteria);
        
        return new CybActiveDataProvider(get_class($this), array(
            'totalItemCount' => $total,
            'pagination'=>array(
                'pageSize'=>  Helper::getPageSize(),
            ),
            'criteria' => $criteria,
        ));
    }

}