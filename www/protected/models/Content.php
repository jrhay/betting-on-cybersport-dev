<?php

/**
 * This is the model class for table "Content".
 *
 * The followings are the available columns in table 'Content':
 * @property integer $id
 * @property string $title
 * @property string $content
 * @property integer $lang_id
 */
class Content extends CActiveRecord {

    /**
     * @var
     */
    public $language_search;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Content the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'content';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('title, content, lang_id', 'required'),
            array('lang_id', 'numerical', 'integerOnly' => true),
            array('title', 'length', 'max' => 255),
            array('content', 'filter', 'filter' => array($obj = new CHtmlPurifier(), 'purify')),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, title, content, language_search', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
            'lang_name' => array(self::BELONGS_TO, 'Languages', 'lang_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => Yii::t('general', 'ID'),
            'title' => Yii::t('general', 'Title'),
            'content' => Yii::t('general', 'Content'),
            'language_search' => Yii::t('general', 'Language'),
            'lang_id' => Yii::t('general', 'Language'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CybActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
        $criteria = new CDbCriteria;

        $criteria->with = array('lang_name');
        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.title', $this->title, true);
        $criteria->compare('t.content', $this->content, true);
        $criteria->compare('lang_name.name', $this->language_search, true);




        return new CybActiveDataProvider($this, array(
            'pagination'=>array(
                'pageSize'=> Helper::getPageSize(),
            ),
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => array('title'=>false),
            ),
        ));
    }

    /**
     * @param $id
     * @param null $lang_id
     * @return string
     * @throws CHttpException
     */
    public function findContent($id, $lang_id = null) {

        if (!Yii::app()->user->id) {
            $lang_id_Object = Languages::model()->findByAttributes(array('dialect'=>Yii::app()->language));
            $lang_id = (int)$lang_id_Object->id;
        } else {
            $lang_id = Yii::app()->user->language;
        }

        $default_id = Yii::app()->params['default_lang_id'];
        $return = '';
        
        $values = Content::model()->findAllByAttributes(array('id' => $id, 'lang_id' => array($lang_id, $default_id)));                
        if (!$values) {
            throw new CHttpException(404,Yii::t('general','Page not found'));
        } else {


        foreach ($values as $value) {
            if ($value->lang_id == $lang_id) {
                $return = $value;
            } elseif (!$return) {
                $return = $value;
            }
        }
        
        return $return;
        }
    }

    /**
     * @return bool
     */
    public function beforeSave() {
        preg_match_all('/font-family:[^;.]*;/',$this->content, $matches);
        $this->content = preg_replace("/font-family:[^;.]*;/","",$this->content, -1,$count);

//        var_dump($this->content);die;
        return parent::beforeSave();
    }
}