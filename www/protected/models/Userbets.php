<?php

/**
 * This is the model class for table "userbets".
 *
 * The followings are the available columns in table 'userbets':
 * @property string $id
 * @property integer $betType
 * @property string $userId
 * @property integer $gameId
 * @property double $koef
 * @property integer $result
 * @property integer $status
 * @property integer $summ
 * @property integer $moneyType
 * @property User $user
 * @property Games $game
 */
class Userbets extends CActiveRecord
{
    const TYPE_BET_SIMPLE = 1;
    const TYPE_BET_GROUP = 2;
    const TYPE_BET_LIVE = 4;
    const TEAM1_WIN = 1;
//    const TYPE_BET_SIMPLE = 1;
    const TEAM2_WIN = 2;
    const DRAW_WIN = 0;
    const WIN_YES = '2';
    const WIN_NO = '1';
    const NOT_YET = '0';
    const CANCELED = '3';
    public $userName;
    public $gameName;
    public $ABtime;
    public $ABevent;
    public $ABresult;
    public $Time;
    public $Event;
    public $Result;

    public static function getStatusFilter() {
        $filter = Userbets::alias('Status');
        return $filter;
    }

    public static function alias($type, $code = NULL) {
        $_items = array(
            'Status' => array(
                CybController::ACTIVE_STATUS => Yii::t('general', 'Active'),
                CybController::INACTIVE_STATUS => Yii::t('general', 'Incative'),
                CybController::DELETED_STATUS => Yii::t('general', 'Deleted'),
            ),
            'isWin' => array(
                Userbets::WIN_YES => Yii::t('general', 'Yes'),
                Userbets::WIN_NO => Yii::t('general', 'No'),
                Userbets::NOT_YET => Yii::t('general', 'Pending'),
            ),
            'Result' => array(
                Userbets::TEAM1_WIN => Yii::t('general', 'Team 1'),
                Userbets::TEAM2_WIN => Yii::t('general', 'Team 2'),
                Userbets::DRAW_WIN => Yii::t('general', 'Draw'),
            ),
        );

        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

    public static function getisWinFilter() {
        $filter = Userbets::alias('isWin');
        return $filter;
    }

    public static function getResultFilter() {
        $filter = Userbets::alias('Result');
        return $filter;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Userbets the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'userbets';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('userId, gameId, koef, result, status, summ, isWin,moneyType', 'required'),
            array('betType, gameId, result, status, summ, isWin', 'numerical', 'integerOnly'=>true),
            array('koef', 'numerical'),
            array('userId', 'length', 'max'=>20),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, betType, userId, gameId, koef, result, isWin, status, summ, userName, gameName', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'game' => array(self::BELONGS_TO, 'Games', 'gameId'),
            'user' => array(self::BELONGS_TO, 'User', 'userId'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'betType' => 'Bet Type',
            'userId' => 'User',
            'gameId' => 'Game',
            'koef' => 'Koef',
            'result' => 'Result',
            'isWin' => 'isWin',
            'status' => 'Status',
            'summ' => 'Summ',
        );
    }

    public function scopes() {
        return array(
            'OnlyActive' => array(
                'condition' => 't.status=' . CybController::ACTIVE_STATUS,
            ),
        );
    }

    /** search and get result int order desc */
    public function searchOrderDesc(){
        /**@var CybActiveDataProvider $cybActiveDataProvider*/
        $cybActiveDataProvider = $this->search();
        $cybActiveDataProvider->criteria->order = 't.id desc';
        return $cybActiveDataProvider;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;
        $criteria->with = array(array('game'=>array('category', array('together'=>true), 'team1', array('together'=>true),'team2', array('together'=>true)),'together'=>true),array('user','together'=>true));
        $criteria->with[] = 'user';
        $criteria->with[] = 'game';
        $criteria->compare('t.id',$this->id,true);
        $criteria->compare('betType',$this->betType);
        $criteria->compare('userId',$this->userId,true);
        $criteria->compare('gameId',$this->gameId);
        $criteria->compare('koef',$this->koef);
        $criteria->compare('result',$this->result);
        $criteria->compare('status',$this->status);
        $criteria->compare('summ',$this->summ);
        $criteria->compare('moneyType',$this->moneyType);
        $criteria->compare('isWin',$this->isWin);
        $criteria->compare('userName',$this->userName, true);
        $criteria->compare('name', $this->gameName, true);

        return new CybActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
}
