<?php
/**
 * Created by PhpStorm.
 * User: lldl
 * Date: 08.12.14
 * Time: 22:09
 */

/** parse tournametns from egamingbets.com and creates tournaments if they are not exist */
abstract class EGBAbstractParser
{
    protected $countAll;
    protected $countAdded;
    protected $countNotAdded;
    protected $countExisted;
    protected $countAddedResultTo;
    protected $countAddedResultToNew;
    protected $countAddedResultToExisted;
    protected $userAgentStrings = array(
        'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; Acoo Browser; SLCC1; .NET CLR 2.0.50727; Media Center PC 5.0; .NET CLR 3.0.04506)',
        'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0; Trident/4.0; Acoo Browser; GTB5; Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1) ; InfoPath.1; .NET CLR 3.5.30729; .NET CLR 3.0.30618)',
        'Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 6.0; Trident/4.0; Acoo Browser 1.98.744; .NET CLR 3.5.30729)',
        'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; Acoo Browser; GTB5; Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1) ; Maxthon; InfoPath.1; .NET CLR 3.5.30729; .NET CLR 3.0.30618)',
        'Mozilla/5.0 (Windows; U; Windows NT 5.1; pl; rv:1.9.2.3) Gecko/20100401 Lightningquail/3.6.3',
        'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; MyIE2; .NET CLR 2.0.50727)',
    );
    /** tournaments categories egb => our  */
    protected $gamesCategories =
        array(
            'Dota2' => 'Dota 2',
            'StarCraft2' => 'StarCraft 2',
            'Counter-Strike' => 'CS:GO',
            'WorldOfTanks' => 'WoT',
            'WarCraft3' => 'WarCraft 3',
            'Hearthstone' => 'Hearthstone',
            'Quake' => 'Quake',
            'LoL' => 'LoL'
        );

    /** @var array  errors */
    protected $errors;

    /** get errors
     * @return string
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /** add error */
    protected function addNewError($msg)
    {
        $this->errors[] = $msg;
    }

    /** return serialized errors serialize errors
     * @return string*/
    protected function serializeErrors()
    {
        $msg = "\n";
        foreach ($this->errors as $error) {
            $msg .= json_encode($error) . "\n";
        }
        return $msg;
    }

    /** get Response using curl
     * @param string $url
     * @param array $params
     */
    protected function getResponse($url, $params = array())
    {
        if (!empty($params)) {
            $paramString = '&';
            if (strpos($url, '?') === false)
                $paramString = '?';

            foreach ($params as $name => $value) {
                $paramString .= "$name=$value&";
            }
            $url .= $paramString;
        }
        $c = curl_init($url);
        curl_setopt($c, CURLOPT_USERAGENT, $this->getUserAgent());
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);

        // Grab the data.
        $response = curl_exec($c);
        if (!$response) {
            $this->addNewError('curl error : ' . json_encode(curl_error($c)));
        }
        Yii::log('url : ' . $url);
        Yii::log('response: ' . json_encode($response));
        return $response;
    }

    /** return user agent string for requests
     * @return string
     */
    protected function getUserAgent()
    {
        return array_rand($this->userAgentStrings);
    }
} 