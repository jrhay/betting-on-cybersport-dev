<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
include(Yii::app()->basePath.'/vendors/geoip/geoip.inc');
class CybController extends CController
{

    const ACTIVE_STATUS = '2';
    const INACTIVE_STATUS = '1';
    const DELETED_STATUS = '0';

    const ANSWER_YES = 1;
    const ANSWER_NO = 0;

    const WIN_YES = '2';
    const WIN_NO = '1';
    const NOT_YET = '0';

    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout ='//layouts/index';
    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu=array();
    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs=array();

    public static function getStatusFilter() {
        $filter = CybController::alias('Status');
        return $filter;
    }

    public static function alias($type, $code = NULL) {
        $_items = array(
            'Status' => array(
                CybController::ACTIVE_STATUS => Yii::t('general', 'Active'),
                CybController::INACTIVE_STATUS => Yii::t('general', 'Incative'),
                CybController::DELETED_STATUS => Yii::t('general', 'Deleted'),
            ),
            'isWin' => array(
                Userbets::WIN_YES => Yii::t('general', 'Yes'),
                Userbets::WIN_NO => Yii::t('general', 'No'),
                Userbets::NOT_YET => Yii::t('general', 'Pending'),
            ),
            'Result' => array(
                Userbets::TEAM1_WIN => Yii::t('general', 'Team 1'),
                Userbets::TEAM2_WIN => Yii::t('general', 'Team 2'),
                Userbets::DRAW_WIN => Yii::t('general', 'Draw'),
            ),
        );

        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

    public static function getisWinFilter() {
        $filter = CybController::alias('isWin');
        return $filter;
    }

    public static function getResultFilter() {
        $filter = CybController::alias('Result');
        return $filter;
    }

    public function beforeAction($action) {

        if (isset($_GET['pageSize'])) {
            Yii::app()->cache->set('pageSize_'.Yii::app()->user->id, (int)$_GET['pageSize']);
            unset($_GET['pageSize']);
        }

        $lang = $language = Yii::app()->user->getState('userLang');

        if (isset($_REQUEST['lang']) && in_array($_REQUEST['lang'], array('ru', 'en'))) {
            $lang = $_REQUEST['lang'];
            if($lang === 'ru') {
                $this->redirect('http://ru.cybbet.com');
            } else {
                $this->redirect('http://cybbet.com');
            }
        }
        if(!$lang) {
            $ip = $_SERVER['REMOTE_ADDR'];
            $gi = geoip_open(Yii::app()->basePath . "/vendors/geoip/GeoIP.dat", GEOIP_STANDARD);
            $code = mb_strtolower(geoip_country_code_by_addr($gi, $ip));
            if (!in_array($code, array('ru', 'ua', 'by', 'az', 'am', 'kz', 'kg', 'md', 'tj', 'tm', 'uz'))) {
                $lang = 'en';
            } else {
                $lang = 'ru';
            }
        }

        if(strpos($_SERVER['HTTP_HOST'], 'ru.') !== false) {
            $lang = 'ru';;
        } else {
            $lang= 'en';
        }

        Yii::app()->language = $lang;
        Yii::app()->user->setState('userLang',$lang);


        //old language integration
//        $language = Yii::app()->request->getQuery('lang', '');
//        if ($language) {
//            Yii::app()->user->setState('userLang',$language);
//        } else {
//            $language = Yii::app()->user->getState('userLang');
//        }
//
//        if ($language && Languages::model()->enabled()->findByAttributes(array('dialect'=>$language))) //TODO: language integration
//            Yii::app()->language = $language;




        $moduleId = isset($this->module) ? $this->module->id . '.' : '';
        if (Yii::app()->user->checkAccess($moduleId . '*')) {
            return true;
        } else { //moduleID is empty
            if (Yii::app()->user->checkAccess($moduleId . $this->action->controller->id . '.*')) {
                return true;
            } else {
                if (Yii::app()->user->checkAccess($moduleId . $this->action->controller->id . '.' . $this->action->id)) {
                    return true;
                } else {
                    $this->failedAccesCheck();
                }
            }
        }

        $catCriteria = new CDbCriteria();
        $catCriteria->addColumnCondition(array('status' => CybController::ACTIVE_STATUS));
        /** @var GameCategories[] $gameCategories */
        $gameCategories = GameCategories::model()->findAll($catCriteria);

//        $parts = explode('/', Yii::app()->request->requestUri);
//        if(isset($parts[1]) && $parts[1] == 'games' && isset($parts[2])){
//            foreach($gameCategories as $category) {
//                if($parts[2] === $category->alias) {
////                    $_REQUEST[$category->alias] = '';
////                    $this->actionIndex();
//                }
//            }
//        }


        return true;
    }

    public function failedAccesCheck() {

        $url = '';
        $moduleId = isset($this->module) ? $this->module->id . '.' : '';
        if ($moduleId)
            $url.='/' . $this->module->id;
        $url.='/user';
        $url.='/login';
        $this->redirect(Yii::app()->createUrl($url));
    }
}