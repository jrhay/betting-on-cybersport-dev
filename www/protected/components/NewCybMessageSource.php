<?php

class NewCybMessageSource extends NewCDbMessageSource {
    public function translate($category, $message, $language = null) {
        $translation = parent::translate($category, $message, $language);
        if (Yii::app()->params['enable_i18n_debug'] && $category!='information') {
            return $translation;
        }
        return $translation;

    }
}
    