<?php

class CybActiveDataProvider extends CActiveDataProvider {
    public function __construct($modelClass, $config = array()) {
        if (!isset($config['pagination'])) {
            $config['pagination'] = array(
                'pageSize'=> Helper::getPageSize(),
            );
        }
        parent::__construct($modelClass, $config);
    }
}