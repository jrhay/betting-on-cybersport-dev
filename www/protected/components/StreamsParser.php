<?php

/**
 * Created by PhpStorm.
 * User: lldl
 * Date: 23.12.14
 * Time: 19:59
 */
class StreamsParser extends EGBAbstractParser
{

    public $countSuccessfullyManaged;
    public $countCanceledStreams;
    public $streamUrl = "http://egamingbets.com/ajax.php?key=modules_home_update_LoadStrim&type=modules&ind=streams&ajax=update&act=LoadStrim&id=";
    public $countDeletedStreams;


    /** receive json decoded streams from egb and manage our streams
     * @param string $streamsData
     */
    public function createStreams($streamsData)
    {
        /** @var Array $streamsArr */
        $streamsArr = $streamsData['strim'];
        /** @var Array $streams */
        $streams = array();
        /** @var array $part */
        foreach ($streamsArr as $part) {
            foreach ($part as $stream) {
                $this->countAll++;
                $streams[] = $stream;
                if ($this->ManageStream($stream)) {
                    $this->countSuccessfullyManaged++;
                } else {
                    $this->countNotAdded++;
                }
            }
        }

        $this->deleteFromDB($streams);
        if (!empty($this->errors)) {
            $msg = 'streams were created with errors : ' . $this->serializeErrors();
            Yii::log($msg, CLogger::LEVEL_ERROR, 'streams');
        } else {
            $msg = 'streams were created with no errors';
            Yii::log($msg, CLogger::LEVEL_ERROR, 'streams');
        }

        $msg = "\nrequest is proceed , all streams : {$this->countAll}
        |successfully managed : {$this->countSuccessfullyManaged}
        | added streams : {$this->countAdded}
        | not added : {$this->countNotAdded}
        ( errors: " . count($this->errors) . " / exists: {$this->countExisted} )
        | canceled streams : {$this->countCanceledStreams}
        | deleted streams: {$this->countDeletedStreams}
        ";
        Yii::log($msg, CLogger::LEVEL_INFO, 'streams');
        return true;

    }

    /** manage stream, compare with streams in our db and make something with new stream or ignore it
     * @param array $stream
     * */
    public function ManageStream($stream)
    {
        $link = $this->getLink($stream);
        $channel = $this->getChannelName($stream['id']);

        /** @var Betsvideos $existedBetsVideo */
        if($existedBetsVideo = Betsvideos::model()->findByAttributes(array('link' => $link))) {
            if($existedBetsVideo->parse_inter == Betsvideos::PARSE_INTER_YES) {
                $existedBetsVideo->status = CybController::ACTIVE_STATUS;
                $existedBetsVideo->save();
            }
            $this->countExisted++;
            $this->countNotAdded++;
            return true;
        }

        /** @var Betsvideos $newBetsVideo */
        $newBetsVideo = new Betsvideos();
        $newBetsVideo->link  = $link;
        $newBetsVideo->title = $stream['title'];
        $newBetsVideo->parse_inter = Betsvideos::PARSE_INTER_YES;
        $newBetsVideo->status = CybController::ACTIVE_STATUS;
        $newBetsVideo->lang = $this->detectLang($stream);
        if($newBetsVideo->lang === false){
            $this->addNewError('error, trying to detect language : '. json_encode($stream));
            return false;
        }
        if(!$newBetsVideo->save()) {
            $this->addNewError('error, trying to save new video stream : '. json_encode($newBetsVideo->getErrors()));
            return false;
        }
        $this->countAdded++;
        Yii::log('successfully added video '. $channel);

        return true;
    }

    /** construct link to stream
     * @param array $stream
     * @return string/boolean
     */
    protected function getLink($stream){
        $id = $stream['id'];
        if(!$channel = $this->getChannelName($id))
            return false;
        $link = 'http://www.twitch.tv/' . $channel . '/embed' ;
        return $link;
    }

    /** make request to egb, parse responce, get channel name
     * @param integer $id
     * @return string/boolean
     */
    public function getChannelName($id){
        $url = $this->streamUrl . $id;
        $channelData = $this->getResponse($url);
        $pattern = '/channel=([a-zA-Z0-9].*)" bgcolor/';
        preg_match($pattern, $channelData, $matches);
        if (!isset($matches[1])) {
            $msg = "can't find match in channelData, no channel name parsed, data : ". $channelData;
            $this->addNewError($msg);
            return false;
        }
        $channel = $matches[1];
        return $channel;
    }

    /** detect lang using title of stream
     * @param array $stream
     * @return boolean
     * */
    protected function detectLang($stream){
        $title = $stream['title'];
        $title = mb_strtolower($title);
        if(strpos($title, '(ru)') !== false) {
            return Betsvideos::LANG_RU;
        }
        if(strpos($title, '(eng)') !== false or strpos($title, '(en)') !== false) {
            return Betsvideos::LANG_EN;
        }
        return false;
    }

    /** delete stream in our DB if it is deleted in egb site
     * @param array $streams
     */
    protected function deleteFromDB($streams){
        /** @var Betsvideos[] $betsvideos */
        $betsvideos = Betsvideos::model()->onlyParseInter()->findAll();
        foreach($betsvideos as $video) {
            if(!$this->inArrayOfStrims($video->link, $streams)) {
                $video->status = CybController::INACTIVE_STATUS;
                if(!$video->save()) {
                    $this->addNewError('can\'t set inactive status to betsvideo : '. json_encode($video->getErrors()));
                    $this->countDeletedStreams++;
                    return false;
                }
            }
        }
        return true;
    }

    /** check if link exists in array of streams
     * @param string $link
     * @param array $streams
     * @return boolean
     */
    protected function inArrayOfStrims($link, $streams){
        foreach($streams as $stream) {
            if($this->getLink($stream) === $link) {
                return true;
            }
        }
        return false;
    }
}