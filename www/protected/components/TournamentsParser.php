<?php
/**
 * Created by PhpStorm.
 * User: lldl
 * Date: 08.12.14
 * Time: 22:09
 */

/** parse tournametns from egamingbets.com and creates tournaments if they are not exist */
class TournamentsParser extends EGBAbstractParser
{
    public $countCanceledTournaments;
    public $countAddedTournamentsDetails;
    /** @var  DOMXPath */
    protected $xpath;

    /** create tournaments using data from egamingbets.com
     * @param string $tournamentsData
     */
    public function createTournaments($tournamentsData)
    {
        $tournamentsData = $tournamentsData['html'];
        $tournamentsData = $this->getTournamentsData($tournamentsData);

        $this->countAll = count($tournamentsData);
        foreach ($tournamentsData as $tournamentData) {
            try {
                $createResult = $this->createTournament($tournamentData);
                if (!$createResult)
                    $this->countNotAdded += 1;
            } catch (Exception $e) {
                $msg = " exception when creating tournament : \n" . json_encode($e);
                Yii::log($msg, CLogger::LEVEL_ERROR, 'tournaments');
                return false;
            }
        }
        if (!empty($this->errors)) {
            $msg = 'tournaments were created with errors : ' . $this->serializeErrors();
            Yii::log($msg, CLogger::LEVEL_ERROR, 'tournaments');
        } else {
            $msg = 'tournaments were created with no errors';
            Yii::log($msg, CLogger::LEVEL_ERROR, 'tournaments');
        }
        $errorTournaments = $this->countAll - $this->countExisted - $this->countAdded;
        $this->countAddedResultTo = $this->countAddedResultToNew + $this->countAddedResultToExisted;
        $msg = "\nrequest is proceed , all tournaments : {$this->countAll}
        | added tournaments : {$this->countAdded} with tournaments details : {$this->countAddedTournamentsDetails}
        | not added : {$this->countNotAdded}
        ( errors: {$errorTournaments} / exists: {$this->countExisted} )

        | added results : {$this->countAddedResultTo}
        ( added results to       new tournaments : {$this->countAddedResultToNew} / existed tournaments : {$this->countAddedResultToExisted}

        | canceled tournaments : {$this->countCanceledTournaments}
        ";
        Yii::log($msg, CLogger::LEVEL_INFO, 'tournaments');
        return true;
    }

    /** create single tournament
     * @param array $tournamentData
     */
    protected function createTournament($tournamentData)
    {
        /** @var Tournaments $tournament */
        $tournament = new Tournaments;

        $tournament->name = $this->getTournamentName($tournamentData);
        $tournament->isFloatingKoef = Tournaments::IS_FLOATING_KOEF_YES;
        $tournament->gameCategory = $this->getTournamentCategoryId($tournamentData);
        $tournament->logo = $this->getTournamentLogo($tournamentData);
        $tournament->start_date = $this->getTournamentStartDate($tournamentData);
        $tournament->status = $tournament->start_date > time() ? Games::GAME_STATUS_NOT_STARTED : Games::GAME_STATUS_IN_PROGRESS;
        $tournament->activeStatus = CybController::ACTIVE_STATUS;

        if(! $existed = $this->ifExisted($tournament)) {
            if (!$tournament->save()) {
                $this->addNewError(json_encode($tournament->getErrors()));
                return false;
            }
            $this->countAdded++;

            if ($this->addTournamentsDetails($tournamentData, $tournament->id))
                return true;
            return false;
        } else {
            $this->countExisted++;
            $this->addNewError('tournament is already exists');
            return false;
        }
    }

    /** check if parser exists in our database
     * @param Tournaments $tournament*/
    protected function ifExisted($tournament){
        if($existed = Tournaments::model()->findByAttributes(array(
            'name' => $tournament->name,
            'gameCategory' => $tournament->gameCategory,
            'start_date' => $tournament->start_date,
        ))) {
            return $existed;
        }
        return false;
    }
    /** add turnament details
     * @param DOMNode $tournamentData
     * @param int $tournamentId
     * @return $boolean
     */
    protected function addTournamentsDetails($tournamentData, $tournamentId){
        $tournamentsDetailsData = array();
        $query = './/table/tr[position() > 1]';
        $td = $this->xpath->query($query, $tournamentData);

        for($i = 0 ; $i < $td->length; $i++)
            $tournamentsDetailsData[] = $td->item($i);
            Yii::log('tournamentDetailsCount : '. count($tournamentsDetailsData));
        foreach($tournamentsDetailsData as $tDetailsData) {
            /** @var Tournamentsdetails $tournamentDetails */
            $tournamentDetails = new Tournamentsdetails();
            $tournamentDetails->tournamentId = $tournamentId;
            if(!$tournamentDetails->teamId = $this->getTDetailsTeam($tDetailsData, $tournamentDetails))

                continue;
            $tournamentDetails->teamKoef = $this->getTDetailsKoef($tDetailsData);
            if(!$tournamentDetails->save()) {
                $this->addNewError('error trying to save tournament Details'.json_encode($tournamentDetails->getErrors()));
            } else {
                $this->countAddedTournamentsDetails++;
            }
        }
        return true;
    }

    /** getTDetailsTeam
     * @param DOMNode $tDetailsData
     * @param Tournamentsdetails $tDetails
     * @return boolean
     */
    protected function  getTDetailsTeam($tDetailsData, $tDetails){
        $query = './td[1]';
        /** @var DOMNode $teamName */
        $teamName = $this->xpath->query($query, $tDetailsData)->item(0);
        $teamName = $teamName->nodeValue;
        /** @var Teams $team */
        $team = Teams::model()->findByAttributes(array('name' => $teamName));
        if($team) {
            return $team->id;
        }

        /** @var Teams $team */
        $newTeam = new Teams;
        /** @var Tournaments $tournament */
        $tournament = Tournaments::model()->findByPk($tDetails->tournamentId);
        $newTeam->category_id = $tournament->gameCategory;
        $newTeam->name = $teamName;
        $newTeam->country_id = Countries::model()->findByAttributes(array('name' => 'World'))->id;
        $newTeam->register_date = time();
        $newTeam->status = CybController::ACTIVE_STATUS;
        if(!$newTeam->save()) {
            $this->addNewError('can\'t save new team :' . json_encode($newTeam->getErrors()));
            return false;
        }
        return $newTeam->id;
    }

    /** getTDetailsKoef
     * @param DOMNode $tDetailsData
     * @return boolean
     */
    protected function  getTDetailsKoef($tDetailsData){
        $query = './td[2]';
        $koef = $this->xpath->query($query, $tDetailsData)->item(0)->textContent;
        return floatval($koef);
    }

    /** get tournament name
     * @param DOMNode $tournamentData
     * @return string
     */
    protected function getTournamentName($tournamentData){
        $query = './div/div/span';
        $name = $this->xpath->query($query, $tournamentData)->item(0)->textContent;
        return $name;
    }
    /** get tournament start date */
    protected function getTournamentStartDate($tournamentData){
        $query = './div/div/span[2]';
        $item = $this->xpath->query($query, $tournamentData);
        $startDate = $item->item(0)->textContent;
        $startDate = DateTime::createFromFormat('H:i:s d.m.y',$startDate)->getTimestamp();
        return $startDate;
    }
    /** get tournaents logo
     * @param  DOMNode $tournamentsNode
     * @return int
     * */
    protected function getTournamentLogo($tournamentsNode) {
        $query = './div/div/img/@src';
        /** @var DOMAttr $src */
        $src = $this->xpath->query($query, $tournamentsNode)->item(0);
        $src = $src->value;
        $srcArr = explode('.', $src);
        $extension = end($srcArr);
        $filename ='logo-'.uniqid().'.'.$extension;
        $fullName =  Yii::app()->basePath.'/../images/tournaments/logo/'.$filename;
        $file = file_get_contents($src);
        file_put_contents($fullName, $file);
        return $filename;
    }
    /** get tournament category from html
     * @param DOMNode $tournamentNode
     * @return int
     */
    protected function getTournamentCategoryId($tournamentsNode)
    {
        $query = './ul/center/b';
        $title = $this->xpath->query($query, $tournamentsNode)->item(0)->textContent;
        Yii::log($title);
        foreach ($this->gamesCategories as $category) {
            $categoryLow = mb_strtolower($category);
            $titleLow = mb_strtolower($title);
            if (strpos($titleLow, $categoryLow) !== false) {
                /** @var GameCategories $gameCategory */
                $gameCategory = GameCategories::model()->findByAttributes(array('title' => $category));
                Yii::log('detected category : ', $gameCategory->title);
                return $gameCategory->id;
            }
        }
        return 6;
    }

    /** @param string $data
     * @return array
     */
    protected function getTournamentsData($data)
    {
        $doc = new DOMDocument;

        //data from egb site usually incorrect ( duplicated ids ) , we rename it to _id
        $data = str_replace(' id=', ' _id=', $data);
        if (!$loadRes = $doc->loadHTML($data)) {
            Yii::log('tournaments error, loading html', CLogger::LEVEL_ERROR);
        }

        $this->xpath = new DOMXPath($doc);

        $query = '//ul/li';
        $items = $this->xpath->query($query);
        Yii::log('parsing tournaments : items  - ' . $items->length);
        $liItems = array();

        for ($i = 0; $i < $items->length; $i++) {
            $liItems[] = $items->item($i);
        }

        return $liItems;
    }

    protected function countCanceledTournaments()
    {

    }

}