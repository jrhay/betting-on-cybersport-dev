<?php
/**
 * Created by PhpStorm.
 * User: lldl
 * Date: 08.12.14
 * Time: 22:09
 */

/** parse date from egamingbets.com and creates games if they are not exist */
class EGBParser
{
    protected $countAll;
    protected $countAdded;
    protected $countNotAdded;
    protected $countExisted;
    protected $countAddedResultTo;
    protected $countAddedResultToNew;
    protected $countAddedResultToExisted;
    protected $countCanceledGames;
    protected $resultUrl = 'http://egamingbets.com/ajax.php?key=modules_home_view_ViewBets';
    protected $userAgentStrings = array(
        'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; Acoo Browser; SLCC1; .NET CLR 2.0.50727; Media Center PC 5.0; .NET CLR 3.0.04506)',
        'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0; Trident/4.0; Acoo Browser; GTB5; Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1) ; InfoPath.1; .NET CLR 3.5.30729; .NET CLR 3.0.30618)',
        'Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 6.0; Trident/4.0; Acoo Browser 1.98.744; .NET CLR 3.5.30729)',
        'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; Acoo Browser; GTB5; Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1) ; Maxthon; InfoPath.1; .NET CLR 3.5.30729; .NET CLR 3.0.30618)',
        'Mozilla/5.0 (Windows; U; Windows NT 5.1; pl; rv:1.9.2.3) Gecko/20100401 Lightningquail/3.6.3',
        'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; MyIE2; .NET CLR 2.0.50727)',
    );
    /** games categories  */
    protected $gamesCategories =
        array(
            'Dota2' => 'Dota 2',
            'StarCraft2' => 'StarCraft 2',
            'Counter-Strike' => 'CS:GO',
            'WorldOfTanks' => 'WoT',
            'WarCraft3' => 'WarCraft 3',
        );

    /** @var array  errors */
    protected $errors;
    /** @var Games $currentGame */
    protected $currentGame;

    /** create games using data from egamingbets.com
     * @param array $gamesData
     */
    public function createGames($gamesData)
    {
        $this->countAll = count($gamesData);
        foreach ($gamesData as $gameData) {
            try {
                $createResult = $this->createGame($gameData);
                if (!$createResult)
                    $this->countNotAdded += 1;
            } catch (Exception $e) {
                $msg = " exception when creating game : \n" . json_encode($e);
                Yii::log($msg, CLogger::LEVEL_ERROR);
                return false;
            }
        }
        if (!empty($this->errors)) {
            $msg = 'games were created with errors : ' . $this->serializeErrors();
            Yii::log($msg);
        } else {
            $msg = 'games were created with no errors';
            Yii::log($msg);
        }
        $errorGames = $this->countAll - $this->countExisted - $this->countAdded;
        $this->countAddedResultTo = $this->countAddedResultToNew + $this->countAddedResultToExisted;
        $msg = "\nrequest is proceed , all games : {$this->countAll}
        | added games : {$this->countAdded}
        | not added : {$this->countNotAdded}
        ( errors: {$errorGames} / exists: {$this->countExisted} )

        | added results : {$this->countAddedResultTo}
        ( added results to       new games : {$this->countAddedResultToNew} / existed games : {$this->countAddedResultToExisted}

        | canceled games : {$this->countCanceledGames}
        ";
        Yii::log($msg);
        return true;
    }

    /** create single game
     * @param array $gameData
     */
    protected function createGame($gameData)
    {
        $game = new Games;
        $this->currentGame = $game;
        $game->game_start_at = $gameData['date'];
        $game->create_at = time();
        $game->team1_koef = round($gameData['coef_1'], 2);
        $game->team2_koef = round($gameData['coef_2'], 2);
        $game->draw_koef = round($gameData['coef_draw'], 2);

        /** @var GameCategories $category */
        $category = $this->detectGameCategory($gameData['game']);
        if (!$category) {
            $this->addNewError("error, trying to get game category with name {$gameData['game']}");
            return false;
        }
        $game->category_id = $category->id;

        /** @var Teams $team1 */
        $team1 = $this->detectTeam($gameData['gamer_1']);
        $game->team1_id = $team1->id;
        $game->team1 = $team1;


        /** @var Teams $team2 */
        $team2 = $this->detectTeam($gameData['gamer_2']);
        $game->team2_id = $team2->id;
        $game->team2 = $team2;


        $game->name =  $game->team1->name . ' vs ' . $game->team2->name ;
        $game->manager_id = 2;
        $game->max_sum = 100;
        $game->betPercents = 10;
        $game->map = $this->getMap($gameData);
        $event = $this->detectEvent($gameData['tourn']);
        $game->event_id = $event->id;

        /** @var Games $existedGame */
        $existedGame = $this->gameExists($gameData);
        if (!$existedGame) {
            $saveResult = $game->save();
            if ($saveResult) {
                $this->countAdded += 1;

                /** @var EgbGames $egbGame */
                $egbGame = new EgbGames();
                $egbGame->egb_id = $gameData['id'];
                $egbGame->cyb_id = $game->id;
                $egbGame->add_time = $game->create_at;

                /** @var Result result */
                $game->result = $this->getResult($gameData, $game);
                if ($game->result) {
                    $egbGame->egb_result = EgbGames::RESULT_STATUS_PARSED;
                    $egbGame->add_egb_result_time = $game->result->add_result_time;
                    $this->countAddedResultToNew += 1;
                }
                $game->status = $this->getGameStatus($game);
                $game->save();
                if(!$egbGame->save()) {
                    $this->addNewError("error trying to save egb_game" . json_encode($egbGame->getErrors()));
                }

            } else {
                $msg = "error trying save new game: " . serialize($game->getErrors());
                $this->addNewError($msg);
                return false;
            }
        } else {
            $this->addNewError("error,such game already exists : " . serialize($game));
            $this->countExisted += 1;

            Yii::log(json_encode($existedGame));
            $existedGame->game_start_at = $gameData['date'];
            $existedGame->save();
            if (!$existedGame->result && $existedGame->status != Games::GAME_STATUS_CANCELED) {
                Yii::log('existed game has no result => try to get it');
                $existedGame->result = $this->getResult($gameData, $existedGame);
                if ($existedGame->result) {
                    $this->countAddedResultToExisted += 1;
                }
            }
            return false;
        }
        return true;
    }

    /** check if game exists
     * @param array $gameData
     * @return Games/bool
     */
    protected function gameExists($gameData)
    {
        /** @var EgbGames $searchedEGBGame */
        $searchedEGBGame = EgbGames::model()->findByAttributes(array('egb_id' =>  $gameData['id']));
        if (!empty($searchedEGBGame)) {
            return Games::model()->findByPk($searchedEGBGame->cyb_id);
        }
        return false;
    }
    
    /** get map
     * @param array $gameData*/
    protected function getMap($gameData){
            $name = $gameData['gamer_1']['nick'];
            $pattern =
                '/\([Mm]ap ([0-9])\)/';
//                '/\([0-9a-z].* bo[0-9]\)/',
            $mapNum = preg_match($pattern, $name, $matches);
            if(isset($matches[1]))
                return'M'.$matches[1];
            return false;
    }

    /** Detect and return games category for single game
     * @param string $categoryName
     * @return GameCategories
     */
    protected function detectGameCategory($categoryName)
    {
        /** @var GameCategories $category */
        if (array_key_exists($categoryName, $this->gamesCategories)) {
            $categoryName = $this->gamesCategories[$categoryName];
        }
        $category = GameCategories::model()->findByAttributes(array('title' => $categoryName));
        if (!$category)
            return false;
        return $category;
    }

    /** Detect team form single game
     * @param array $gamer
     * @return Teams
     */
    protected function detectTeam($gamer)
    {
        $name = $this->getTeamName($gamer['nick']);
        /** @var Teams $team */
        $team = Teams::model()->findByAttributes(array('name' => $name));
        if (!$team) {
            /** @var Teams $team */
            $team = new Teams();
            $team->country_id = Countries::model()->findByAttributes(array('name' => 'World'))->id;
            $team->name = $name;
            $team->register_date = time();
            $team->status = CybController::ACTIVE_STATUS;
            $team->category_id = $this->currentGame->category_id;
            if (!$team->save()) {
                $msg = "error trying save new team: " . serialize($team->getErrors());
                $this->addNewError($msg);
                return false;
            }
        }
        return $team;
    }

    /** return team name ( with no map
     * @param string $name
     */
    protected function getTeamName($name)
    {
        $patterns = array(
            '/\([Mm]ap [0-9]\)/',
            '/\([0-9a-z].* bo[0-9]\)/',
        );
        $name = preg_replace($patterns, '', $name);
        $name = trim($name);
        return $name;
    }



    /** get errors
     * @return string
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /** add error */
    protected function addNewError($msg)
    {
        $this->errors[] = $msg;
    }

    /** serialize errors */
    protected function serializeErrors()
    {
        $msg = "\n";
        foreach ($this->errors as $error) {
            $msg .= json_encode($error) . "\n";
        }
        return $msg;
    }

    /** detect event or create it if exists
     * @param string $eventName
     */
    protected function detectEvent($eventName)
    {
        /** @var $Events $searchedEvent */
        $searchedEvent = Events::model()->findByAttributes(array('text' => $eventName));
        if ($searchedEvent)
            return $searchedEvent;
        /** @var Events $event */
        $event = new Events();
        $event->text = $eventName;
        if (!$event->save()) {
            $msg = "error trying save new event: " . serialize($event->getErrors());
            $this->addNewError($msg);
            return false;
        }
        return $event;
    }

    /** get status
     * @param Games $game
     */
    protected function getGameStatus($game)
    {
        $game_start_at = $game->game_start_at;
        $game_end = $game->game_end;
        $now = time();
        if ($game_start_at > $now) {
            return Games::GAME_STATUS_NOT_STARTED;
        } else if ($game_start_at < $now && !$game_end && !$game->result) {
            return Games::GAME_STATUS_IN_PROGRESS;
        } else if ($game_start_at < $now && $game_end && !$game->result) {
            // may be canceled?
            return Games::GAME_STATUS_FINISHED;
        } else if ($game_start_at < $now && $game_end && $game->result) {
            return Games::GAME_STATUS_FINISHED;
        }
        return Games::GAME_STATUS_NOT_STARTED;
    }

    /** try to get result for game
     * @param array $gameData
     * @param Games $game ;
     * @return bool/Result
     */
    protected function getResult($gameData, $game)
    {

        $params = array(
            'type' => 'modules',
            'ind' => 'home',
            'ajax' => 'view',
            'act' => 'ViewBets',
            'id' => $gameData['id']
        );
        $data = $this->getResponse($this->resultUrl, $params);
        $data = json_decode($data, true);
        $html = $data['html'];
        if (strpos($html, 'Match is over')) {
            Yii::log('Match is over, parsing result');
            if ($gameData['gamer_1']['win']) {
                $winner = 1;
            } else if ($gameData['gamer_2']['win']) {
                $winner = 2;
            } else {
                $winner = 0;
            }
            $pattern = '/\[[0-9].* : [0-9].*\]/';
            if (preg_match($pattern, $html, $matches)) {
                $score = $matches[0];
                /** Result $result */
                $result = new Result();
                $result->add_result_time = time();
                $result->game_id = $game->id;
                $result->result = $winner;
                $result->score = $score;
                $result->handleBets();
                if ($result->save()) {
                    Yii::log('successfully added result to game ' . $game->id . " : " . json_encode($result));
                    $game->game_end = time();
                    $game->result = $result;
                    $game->status = Games::GAME_STATUS_FINISHED;
                    $game->save();
                    return $result;
                } else {
                    $this->addNewError('error trying to add result to game ' . $game->id . " : " . json_encode($result->getErrors()) . " result Object : " . json_encode($result));
                    return false;
                }
            }
        } else if (strpos($html, 'Match was cancelled')) {
            if ($game->status != Games::GAME_STATUS_CANCELED) {
                $result = new Result();
                $result->cancelGame($game->id);
                $this->countCanceledGames += 1;
            }
        }
        return false;
    }

    /** get Response using curl
     * @param string $url
     * @param array $params
     */
    protected function getResponse($url, $params = array())
    {
        if (!empty($params)) {
            $paramString = '&';
            if (strpos($url, '?') === false)
                $paramString = '?';

            foreach ($params as $name => $value) {
                $paramString .= "$name=$value&";
            }
            $url .= $paramString;
        }
        $c = curl_init($url);
        curl_setopt($c, CURLOPT_USERAGENT, $this->getUserAgent());
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);

        // Grab the data.
        $response = curl_exec($c);
        if (!$response) {
            $this->addNewError('curl error : ' . json_encode(curl_error($c)));
        }
        Yii::log('url : ' . $url);
        Yii::log('response: ' . json_encode($response));
        return $response;
    }

    /** return user agent string for requests
     * @return string
     */
    protected function getUserAgent()
    {
        return array_rand($this->userAgentStrings);
    }
} 