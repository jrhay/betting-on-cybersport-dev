<?php

/**
 * Created by PhpStorm.
 * User: lldl
 * Date: 11/24/14
 * Time: 8:58 PM
 */
class Payeer extends CApplicationComponent
{
    /** make payment to payeer account
     * @var float $sum
     * @var string $recipientAccountIdent
     */
    /** @var  Array[] $errors */
    public $errors;
    public function payout($sum, $recipientAccountIdent)
    {

        $accountNumber = Yii::app()->params['payeerAccount'];
        $apiId = Yii::app()->params['payeerApiId'];
        $apiKey = Yii::app()->params['payeerSecretApiKey'];
//        include_once('')
        $payeer = new CPayeer($accountNumber, $apiId, $apiKey);
        if ($payeer->isAuth()) {
            $arTransfer = $payeer->transfer(array(
                'curIn' => 'USD',
                'sum' => $sum,
                'curOut' => 'USD',
                'to' => mb_strtoupper($recipientAccountIdent),
                //'to' => '+01112223344',
                //'to' => 'P8853259',
                'comment' => 'cybbet payout',
                //'anonim' => 'Y',
                //'protect' => 'Y',
                //'protectPeriod' => '3',
                //'protectCode' => '12345',
            ));
            if (!empty($arTransfer['historyId'])) {

                $flashMessage = Yii::t('site', 'Your  payment was successfully executed');
                Yii::app()->user->setFlash('success', $flashMessage);
                $er = json_encode($arTransfer);
                Yii::log($er,CLogger::LEVEL_INFO,'payeer');
                return true;
            } else {
                $flashMessage = Yii::t('site', 'Your  payment was not successfully executed');
                Yii::app()->user->setFlash('error', $flashMessage);
                $er = json_encode($payeer->getErrors());
                $er .="\n". json_encode($arTransfer);
                Yii::log($er,CLogger::LEVEL_ERROR,'payeer');

                $this->errors[] = $er;
                return false;
            }
        } else {
            $flashMessage = Yii::t('site', 'Your  payment was not successfully executed');
            Yii::app()->user->setFlash('error', $flashMessage);
            $er = json_encode($payeer->getErrors());
            $this->errors[] = $er;
            Yii::log($er,CLogger::LEVEL_ERROR,'payeer');
            return false;
        }
    }
}