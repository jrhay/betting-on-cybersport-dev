<?php

class Helper extends CApplicationComponent
{
	public static function getUserFullname($dbUser, $comma = false)
	{
		$middleName = (isset($dbUser->middleName) ? $dbUser->middleName : '');

		return self::getFullname($dbUser->firstName, $dbUser->lastName, $middleName, $dbUser->usertypeId, $comma);
	}
	
	public static function getFullname($firstName, $lastName, $middleName = '', $userType = null, $comma = false)
	{
		$prefix = '';
		if ($userType == User::DOCTOR_USER) $prefix = self::getDoctorPrefix().' ';
		
		return $prefix.$firstName.($comma ? ', ' : ' ').$lastName;
	}
	
	public static function getDoctorPrefix() {
		return Yii::t('general', 'Dr.');
	}
	
	public static function trimtext($text, $length = 45)
	{
//		if (mb_strlen($text) > $length) {
//			$pos = mb_strpos($text, ' ', $length);
//			$text = mb_substr($text, 0, $pos) . '...';
//		}

		return $text;
	}
	
	public static function getPatientStatus($id) {
		return '<div class="patient_status_'.$id.'"><img class="floatleft" src="images/status_0.gif" /></div>';
	}
	
	public static function getPhoneMatchPattern() {
		$patternString = Yii::t('information', 'Phone match pattern::/\+\d{2,17}$/');
		$patternArray = explode('::', $patternString);
		return $patternArray[1];
	}
	
	public static function getPhoneMatchPatternError() {
		return Yii::t('information', 'Please enter valid {attribute} starting with "+" sign and country code');
	}
	
	public static function getPageTitle($title = '', $pattern = 'general') {
		return Yii::t($pattern, 'Cybbet - {title}',array('{title}'=>$title));
//		return Yii::t($pattern,'{title} | M@gellan Global Health',array('{title}'=>$title));
	}
	
	public static function combinedates($start_date, $end_date = null)
	{
		$start_date = self::getDateFormatted(PhrForm::dateFormat(),$start_date - self::getServerTimezone() + self::getUserTimezone());
		if ($end_date) {
			$end_date = self::getDateFormatted(PhrForm::dateFormat(),$end_date - self::getServerTimezone() + self::getUserTimezone());
		} else {
			$end_date = Yii::t('phr_home', 'present');
		}

		return Yii::t('phr_home', '{start_date} to {end_date}', array('{start_date}' => $start_date, '{end_date}' => $end_date));
	}
	
	public static function getDateFormatted($formatString, $timestampForYii, $emptyDateReplacement = null, $user = null)
	{
		if (!isset($user)) $user = Yii::app()->user;


		if (!is_string($emptyDateReplacement)) $emptyDateReplacement = '';

		if (empty($timestampForYii)) return $emptyDateReplacement;
		if (isset($user))
		{
			if ((int) $timestampForYii + self::getServerTimezone() - self::getUserTimezone($user) == 0) return $emptyDateReplacement;
		}

		$format = self::getDateFormat($formatString);
		if (!preg_match('/(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})/',$timestampForYii)) {
			$timeStr = Yii::app()->dateFormatter->format($format, $timestampForYii);
		} else {
			$timeStr = Yii::app()->dateFormatter->format($format, strtotime($timestampForYii) + self::getUserTimezone($user));
		}

		return $timeStr;
	}
	
	public static function getServerTimezone() {
		$origin_tz = date_default_timezone_get();
		$origin_dtz = new DateTimeZone($origin_tz);
		$origin_dt = new DateTime("now", $origin_dtz);

		return $origin_dtz->getOffset($origin_dt);
	}

	public static function getUserTimezone($user = null)
	{
		if (!isset($user)) $user = Yii::app()->user;

		if (isset($user->userTimeZoneUTC_Plus)) {
			return $user->userTimeZoneUTC_Plus;
		}

		return 0;
	}
	
	public static function getDateFormat($formatString)
	{
		$formatArray = explode('::', $formatString);

		return isset($formatArray[1]) ? $formatArray[1]: $formatString;
	}
	
	public static function getDaysArray() {
		return array(
			'1' => Yii::t('general', 'Monday'),
			'2' => Yii::t('general', 'Tuesday'),
			'3' => Yii::t('general', 'Wednesday'),
			'4' => Yii::t('general', 'Thursday'),
			'5' => Yii::t('general', 'Friday'),
			'6' => Yii::t('general', 'Saturday'),
			'7' => Yii::t('general', 'Sunday'),
		);
	}
	
	public static function getUserOffset(CWebUser $user = null)
	{
		return self::getServerTimezone() - self::getUserTimezone($user);
	}
	
	public static function getDateFormattedForUser($formatString, $utcTimestamp, User $dbUser)
	{
		$dateTime = new DateTime('now', new DateTimeZone('UTC'));
		$dateTime->setTimestamp($utcTimestamp);

		if(!$dbUser) {
			$timezone = 'Europe/Kiev';
		}
		$userTimeZoneInHours = self::getTimezoneOffset($dbUser->timezone);
		$serverTimeZoneInHours = self::getServerTimezone() / 3600;

		$timestamp = $dateTime->getTimestamp();
		$timestampForYii = $timestamp - $serverTimeZoneInHours * 3600 + $userTimeZoneInHours * 3600;

		$format = self::getDateFormat($formatString);
		$timeStr = Yii::app()->dateFormatter->format($format, $timestampForYii);

		return $timeStr;
	}
	
	public static function getTimezoneOffset($timezone = null, $user = null)
	{

		$date = new DateTime('now', new DateTimeZone($timezone));

		return $date->getOffset() / 3600;
	}
	
	public static function getCurrentDateFormattedForUser($formatString, User $dbUser)
	{
		$dateTime = new DateTime('now', new DateTimeZone('UTC'));
		$userTimeZoneInHours = self::getTimezoneOffset();
		$serverTimeZoneInHours = self::getServerTimezone() / 3600;
		
		$timestamp = $dateTime->getTimestamp();
		$timestampForYii = $timestamp - $serverTimeZoneInHours * 3600 + $userTimeZoneInHours * 3600;
		
		$format = self::getDateFormat($formatString);
		$timeStr = Yii::app()->dateFormatter->format($format, $timestampForYii);
		
		return $timeStr;
	}
	
	public static function getUserAge(User $dbUser)
	{
		$dateTime1 = new DateTime('now', new DateTimeZone('UTC'));

		$dateTime2 = new DateTime('now', new DateTimeZone('UTC'));
		$dateTime2->setTimestamp($dbUser->birthdayDate);

		$interval = $dateTime1->diff($dateTime2, true);

		$age = (int) $interval->format('%y');

		return $age;
	}
	
	public static function getClosestTimezone($lat, $lng)
	{
		$diffs = array();

		foreach(DateTimeZone::listIdentifiers() as $timezoneID)
		{
			$timezone = new DateTimeZone($timezoneID);
			$location = $timezone->getLocation();
			$tLat = $location['latitude'];
			$tLng = $location['longitude'];
			$diffLat = abs($lat - $tLat);
			$diffLng = abs($lng - $tLng);
			$diff = $diffLat + $diffLng;
			$diffs[$timezoneID] = $diff;
		}

		$timezone = array_keys($diffs, min($diffs));
		$timezone = new DateTimeZone($timezone[0]);
		$offset = $timezone->getOffset(new DateTime);

		return $offset;
	}
	
	public static function fixLabelAsterisk($originalLabelHtml)
	{
		if (strpos($originalLabelHtml, '<span class="required">*</span>') === false || strpos($originalLabelHtml, '<label ') === false) return $originalLabelHtml;

		$labelPosStart = strpos($originalLabelHtml, '<label ');
		$labelPosEnd = strrpos($originalLabelHtml, '</label>');
		$modifiedPart = substr( $originalLabelHtml, $labelPosStart, $labelPosEnd - $labelPosStart);

		$modifiedPart = preg_replace('/<label .*?>/', '', $modifiedPart);
		$modifiedPart = str_replace('</label>', '', $modifiedPart);

		$labelText = trim(str_replace('<span class="required">*</span>', '', $modifiedPart));

		$words = explode(' ', $labelText);
		array_walk($words, function(&$item, $key) { $item = trim($item); });
		$words = array_values(array_filter($words));

		$replacement = '';

		for ($i = 0; $i < count($words) - 1; $i++)
		{
			$replacement .= $words[$i].' ';
		}

		$replacement .= '<span class="noBreak">'.end($words).' '.'<span class="required">*</span>'.'</span>';

		$newHtml = str_replace($modifiedPart, $replacement, $originalLabelHtml);

		return $newHtml;
	}
	
	// Prevents transferring of trailing asterisk symbol to new line.
	//

	public static function IdsToString($ids)
	{
		$result = '';

		foreach ($ids as $i => $id)
		{
			$result .= ($i == 0 ? '' : ', ') . $id;
		}

		return $result;
	}
	
	// Get IDs as comma-separated string (to use in DB queries, for example).
	// @param array - IDs
	// @return string - comma-separated IDs as string
	//

	public static function getPageSize($pattern = 'pageSize') {
		$pageSize = Yii::app()->cache->get($pattern.'_'.Yii::app()->user->id);
		if (!$pageSize) {
			$pageSize = Yii::app()->params['default_pagesize'];
		}
		return $pageSize;
	}
	
    public static function changeUserLanguage($userID,$language) {
        Yii::app()->db->createCommand("DELETE FROM userlanguage WHERE user_id = '" . $userID . "'")->execute();
        Yii::app()->db->createCommand("INSERT INTO userlanguage SET user_id = '" . $userID . "', Languages_id = '" . $language . "'")->execute();
        return true;

    }

    public static function sendEmail($category,$params,$userID) {
        $result = Yii::app()->mailer->sendmail(
            $category, $params, $userID
        );

        return $result;
    }

    public static function createUserCash($id) {
        $model = new Usercash();
        $model->userId = $id;
        $model->realMoney = 0;
        $model->cyberMoney = 100; //TODO: bonuses system
        $model->isBlocked = 0;
        return  $model->save();
    }

    public static function getNewKoefs($gameId, $summ,$betResult,$betID,$moneyType){

        $game = Games::model()->findByPk($gameId);
        $onePercentMoney = Yii::app()->params['onePercentMoney'];
        $betResultKoefCount = $summ / $onePercentMoney;

        if ($moneyType==Usercash::REAL_MONEY) {
        if ($game->draw_koef) {

            $gameInitial = Gamesinitial::model()->findByAttributes(array('gameId'=>$gameId));

            switch ($betResult) {
                case 1:
                    $game->team1_koef = 100/(100/$game->team1_koef+$betResultKoefCount);
                    $percentage = 0.8;
                    if ($gameInitial->draw_koef>(100/(100/$game->draw_koef-0.2*$betResultKoefCount))) {
                        $game->draw_koef = 100/(100/$game->draw_koef-0.2*$betResultKoefCount);
                        $percentage = 1;
                    }
                    $game->team2_koef =100/(100/$game->team2_koef-$percentage*$betResultKoefCount);
                    break;
                case 2:
                    $game->team2_koef = 100/(100/$game->team2_koef+1*$betResultKoefCount);
                    $percentage = 0.8;
                    if ($gameInitial->draw_koef>(100/(100/$game->draw_koef-0.2*$betResultKoefCount))) {
                        $game->draw_koef = 100/(100/$game->draw_koef-0.2*$betResultKoefCount);
                        $percentage = 1;
                    }
                    $game->team1_koef =100/(100/$game->team1_koef-$percentage*$betResultKoefCount);
                    break;
                case 0:
                    $game->draw_koef = 100/(100/$game->draw_koef+1*$betResultKoefCount);
                    $game->team1_koef = 100/(100/$game->team1_koef-0.5*$betResultKoefCount);
                    $game->team2_koef = 100/(100/$game->team2_koef-0.5*$betResultKoefCount);
                    break;
            }
        } else {
            switch ($betResult) {
                case 1:
                    $game->team1_koef = 100/(100/$game->team1_koef+$betResultKoefCount);
                    $game->team2_koef = 100/(100/$game->team2_koef-$betResultKoefCount);
                    break;
                case 2:
                    $game->team1_koef = 100/(100/$game->team1_koef-$betResultKoefCount);
                    $game->team2_koef = 100/(100/$game->team2_koef+$betResultKoefCount);
                    break;
            }
        }
        }
        $gameHistoryItem = new Gamesbetshistory();
        $gameHistoryItem->gameId = $gameId;
        $gameHistoryItem->koef1 = $game->team1_koef;
        $gameHistoryItem->koef2 = $game->team2_koef;
        $gameHistoryItem->draw = $game->draw_koef;
        $gameHistoryItem->betId = $betID;
        $gameHistoryItem->time = time();
        $gameHistoryItem->validate();

        $gameHistoryItem->save();

        return $game;
    }

    public static function addUserCashHistory($type,$summ,$moneyType) {
        $model = new Usercashhistory();
        $model->moneyType = $moneyType;
        $model->summ = $summ;
        $model->operationType = $type;
        $model->userId = Yii::app()->user->id;

        $model->save();
        return true;

    }

    public static function updateUserCashOnBetAdded($summ,$moneyType){
        $usercash = Usercash::model()->findByAttributes(array('userId'=>Yii::app()->user->id));
        switch ($moneyType){
            case Usercash::PLAY_MONEY:
                $usercash->cyberMoney-=$summ;
                break;
            case Usercash::REAL_MONEY:
                $usercash->realMoney-=$summ;
                break;
        }
        $usercash->save();

    }

    public static function getTournamentPercentOfBetsForTeams($tournamentID) {
        $returnArr = array();
        $count = Yii::app()->db->createCommand()->select('id')->from('tournamentsbets')->where('tournamentId=:tournamentId',
            array(':tournamentId'=>$tournamentID))->queryAll();
        $list = Yii::app()->db->createCommand()->select('teamId, COUNT( id ) AS `percent`')->from('tournamentsbets')->where('tournamentId=:tournamentId',
            array(':tournamentId'=>$tournamentID))->group('teamId')->queryAll();
        if ($list) {
            foreach ($list as $item) {
                $returnArr[$item['teamId']] = Helper::formatMoney($item['percent']/count($count))*100;
            }
        }

        return $returnArr;

    }

    public static function formatMoney($money) {
        return number_format($money,2,'.',' ');
    }

    public static function getTournamentIfBetForThisTournamentTeamsExists($tournamentID) {
        $returnArr = array();
        $list = Yii::app()->db->createCommand()->select('*')->from('tournamentsbets')->where('userId=:userId AND tournamentId=:tournamentId',
            array(':tournamentId'=>$tournamentID,'userId'=>Yii::app()->user->id))->queryAll();
        if ($list) {
            foreach ($list as $item) {
                $returnArr[$item['teamId']]['canWin'] = Helper::formatMoney($item['summ'] * $item['koef']);
                $returnArr[$item['teamId']]['summ'] = Helper::formatMoney($item['summ']);
            }
        }

        return $returnArr;
    }

    public static function updateTournamentKoefs($activeBet) {
        $onePercentMoney = Yii::app()->params['onePercentMoney'];
        $betResultKoefCount = $activeBet->summ / $onePercentMoney;
        $newTeamKoef =  100/(100/$activeBet->koef+$betResultKoefCount);

        //Search for all other teams in this tournament

        $tournamentInfo = Tournamentsdetails::model()->findAllByAttributes(array('tournamentId' => $activeBet->tournamentId));
        $teamsCount = count($tournamentInfo) - 1;
        foreach ($tournamentInfo as $tournamentTeam) {
            if ($tournamentTeam->teamId==$activeBet->teamId) {
                $tournamentTeam->saveAttributes(array('teamKoef' => Helper::formatKoef($newTeamKoef)));
            } else {
                $newKoefForTeam = 100/(100/$activeBet->koef-(1/$teamsCount)*$betResultKoefCount);
                $tournamentTeam->saveAttributes(array('teamKoef' => Helper::formatKoef($newKoefForTeam)));
            }

        }
        return true;
    }

    public static function formatKoef($koef) {
        return number_format($koef,2,'.',' ');
    }

    /** get $count Pupular Countries ( by regisered users)
     * @param integer/string $count*/
    public static function getPopularCountries($count = 10 ){
            $count = intval($count);

            /** @var Countries[] $arr */
            $arr = Countries::model()->findAll();

            $index = array();
            foreach($arr as $a) $index[] = Helper::countUsersInCountry($a);
            array_multisort($index, $arr);
            $arr = array_reverse($arr);
            $index = array_reverse($index);

            $arr = array_slice($arr, 0, $count);
            $index = array_slice($index,0,$count);

            //delete if no users in countries
            foreach($index as $key => $countUsers) {
                if($countUsers == 0) {
                    unset($index[$key]);
                    unset($arr[$key]);
                }
            }

            foreach($arr as $key => $country) {
                unset ($arr [$key]);
                $arr[" $country->id "] = $country->name;
            }
            return $arr;
    }

    /** @var Countries $Country*/
    protected static function countUsersInCountry($Country){
        $users = $Country->user;
        return count($users);
    }
    /** Count users in Country */

	public function init() {
	}
}