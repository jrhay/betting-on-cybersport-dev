<?php

class WebUser extends CWebUser
{
	private $_model = null;
	
	public function init() {
		parent::init();
		$this->setState('language', Yii::app()->params['default_lang_id']);
	}
	
	public function getRole() {
		if ($user = $this->getModel()) {
			return $user->usertype->type;
		} else {
			return 'Guest';
		}
	}
	
	protected function afterLogin($fromCookie) {
		parent::afterLogin($fromCookie);
		$this->updateSession();

	}
	
	public function updateSession() {
        
		$user = $this->getModel();
//        $user->lastLoginTime = $user->lastLoginTime?$user->lastLoginTime:time();
//        $user->lastLoginTime = time();
//        Yii::app()->setGlobalState('lastLoginTime',$user->lastLoginTime);
		if ($user) {
            $time = time();
			$userAttributes = array(
				'email' => $user->userName,
				'username' => $user->userName,
				'firstName' => $user->firstName,
				'lastName' => $user->lastName,
				'usertypeId' => $user->usertypeId,
                'lastLoginTime' => time(),
                'timeZoneUsr' => Helper::getTimezoneOffset($user->timezone),
			);
            Yii::app()->session['uid'] = $user->id;
			foreach ($userAttributes as $attrName => $attrValue) {
				$this->setState($attrName, $attrValue);

			}

            $command = Yii::app()->db->createCommand();
//            $command->update('user', array(
//                'lastLoginTime'=>$time,
//            ), 'id=:id', array(':id'=>$user->id));
		}
	}
	
	private function getModel() {
		$language = 0;
		if (!$this->isGuest && $this->_model === null) {
			$this->_model = User::model()->with('usertype')->findByPk($this->id);
			$language = Yii::app()->db->createCommand("SELECT Languages_id FROM userlanguage WHERE user_id = '" . $this->id . "'")->queryScalar();

		}
		$language = $language ? $language : Yii::app()->params['default_lang_id'];
		$this->setState('language', $language);
        if (Yii::app()->user->getState('userLang')) {
            Yii::app()->language = Yii::app()->user->getState('userLang');
        } else {
            Yii::app()->language = Languages::getName($language);
        }

		
		return $this->_model;
	}
	
	public function logout($destroySession = true)
	{
		parent::logout(false);
	}
}