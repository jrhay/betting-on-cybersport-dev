<?php

/**
 * TinyEditor extension for Yii.
 *
 * @author Eugene V <evc22rus@gmail.com>
 * @link http://www.scriptiny.com/2010/02/javascript-wysiwyg-editor/
 * @version 0.1
 *
 * TinyEditor is light-weight standalone pure javascript HTML5 WYSIWYG editor
 *
 * Usage example with model:
 *
 * <pre>
 * $this->widget('application.extensions.tinyeditor.ETinyEditor',array(
 * 		'model'=>$model,
 * 		'attribute'=>'text',
 * 		// additional javscript options (see below)
 * 		'options'=>array(
 * 			'width'=>'100%',
 * 		),
 * ));
 * </pre>
 *
 * Usage example without model:
 *
 * <pre>
 * $this->widget('application.extensions.tinyeditor.ETinyEditor',array(
 * 		'name'=>'text',
 * 		'value'=>'Contents of editing text',
 * ));
 * </pre>
 */
class ETinyEditor extends CInputWidget {

    /**
     * @var array Toolbar labels
     */
    public $labels = array();

    /**
     * Options description.
     * All of them is optional except note as required.
     *
     * options.id (required) - id of textarea element
     * options.labels - overridden labels in localization purpose (see tinyeditor.js line 9)
     * options.autosync - automated syncronization between textarea and iframe if any changes has been made
     * options.xhtml - use XHTML in iframe design mode
     * options.width - width of iframe, can be in px and % format
     * options.height - height of iframe
     * options.controls - array of controls in toolbar (see tinyeditor.js line 67)
     * options.cssclass - css class of wrapper div
     * options.rowclass - css class of header row div
     * options.dividerclass - css class of divider in toolbar
     * options.fontclass - css class of fonts selector
     * options.sizeclass - css class of font sizes selector
     * options.styleclass - css class of block styles selector
     * options.controlclass - css class of control element in toolbar
     * options.footerclass - css class of footer part
     * options.fonts - array of fonts available for selection
     * options.sizes - array of font sizes available for selection
     * options.footer - render footer part or not (boolean)
     * options.addfooter - array configuring additional button in footer:
     * options.addfooter.cssclass - css class of button
     * options.addfooter.text - text of button
     * options.addfooter.callback - onclick function callback
     * options.toggle - render button to toggle source/wysiwyg view
     * options.toggletextsource - text for toggle button in source view
     * options.toggletextwysiwyg - text for toggle button in wysisyg view
     * options.resize - array configuring iframe resize control
     * options.resize.cssclass - css class of resize control
     * options.bodyid - if of iframe body
     * options.cssfile - URI of iframe' css file
     * options.css - style block of iframe
     * options.content - optional content for iframe. It is defaults to use textarea content
     *
     * @var array Plugin options
     */
    public $options = array();

    /**
     * @var CClientScript Client script
     */
    private $cs;

    /**
     * Extension initializer
     */
    public function init() {
        parent::init();
        $this->cs = Yii::app()->clientScript;
        $this->publishAssets();
        $this->setLabels();
    }

    /**
     * Run widget
     */
    public function run() {
        if ($this->hasModel()) {
            echo CHtml::activeTextArea($this->model, $this->attribute, $this->htmlOptions);
            $this->htmlOptions['id'] = CHtml::activeId($this->model, $this->attribute);
        } else {
            echo CHtml::textArea($this->name, $this->value, $this->htmlOptions);
            $this->htmlOptions['id'] = $this->name;
        }

        $options = array(
            'id' => $this->hasModel() ? CHtml::activeId($this->model, $this->attribute) : $this->name,
        );
        $options = CMap::mergeArray($options, $this->options);
        $options['labels'] = $this->labels;
        $this->cs->registerScript(
                __CLASS__ . '#' . $this->htmlOptions['id'], "tinyEditor_" . $this->htmlOptions['id'] . " = new TINY.editor.edit('tinyEditor_" . $this->htmlOptions['id'] . "'," . CJavaScript::encode($options) . ");", CClientScript::POS_READY
        );
    }

    /**
     * Assets publisher
     */
    public function publishAssets() {
        $assets = dirname(__FILE__) . '/assets';
        $assetsUrl = Yii::app()->assetManager->publish($assets);
        if (defined('YII_DEBUG') && YII_DEBUG) {
            $this->cs->registerScriptFile($assetsUrl . '/js/tinyeditor.js');
            $this->cs->registerCssFile($assetsUrl . '/css/tinyeditor.css');
        } else {
            $this->cs->registerScriptFile($assetsUrl . '/js/tinyeditor.min.js');
            $this->cs->registerCssFile($assetsUrl . '/css/tinyeditor.min.css');
        }
    }

    /**
     * Locale settings
     */
    public function setLabels() {
        $this->labels = CMap::mergeArray(array(
                    'cut' => Yii::t(__CLASS__, 'Cut'),
                    'copy' => Yii::t(__CLASS__, 'Copy'),
                    'paste' => Yii::t(__CLASS__, 'Paste'),
                    'bold' => Yii::t(__CLASS__, 'Bold'),
                    'italic' => Yii::t(__CLASS__, 'Italic'),
                    'underline' => Yii::t(__CLASS__, 'Underline'),
                    'strikethrough' => Yii::t(__CLASS__, 'Strikethrough'),
                    'subscript' => Yii::t(__CLASS__, 'Subscript'),
                    'superscript' => Yii::t(__CLASS__, 'Superscript'),
                    'orderedlist' => Yii::t(__CLASS__, 'Insert Ordered List'),
                    'unorderedlist' => Yii::t(__CLASS__, 'Insert Unordered List'),
                    'outdent' => Yii::t(__CLASS__, 'Outdent'),
                    'indent' => Yii::t(__CLASS__, 'Indent'),
                    'leftalign' => Yii::t(__CLASS__, 'Left Align'),
                    'centeralign' => Yii::t(__CLASS__, 'Center Align'),
                    'rightalign' => Yii::t(__CLASS__, 'Right Align'),
                    'blockjustify' => Yii::t(__CLASS__, 'Block Justify'),
                    'undo' => Yii::t(__CLASS__, 'Undo'),
                    'redo' => Yii::t(__CLASS__, 'Redo'),
                    'image' => Yii::t(__CLASS__, 'Insert Image'),
                    'hr' => Yii::t(__CLASS__, 'Insert Horizontal Rule'),
                    'link' => Yii::t(__CLASS__, 'Insert Hyperlink'),
                    'unlink' => Yii::t(__CLASS__, 'Remove Hyperlink'),
                    'unformat' => Yii::t(__CLASS__, 'Remove Formatting'),
                    'print' => Yii::t(__CLASS__, 'Print'),
                    'font' => Yii::t(__CLASS__, 'Font'),
                    'style' => Yii::t(__CLASS__, 'Style'),
                    'insertimage' => Yii::t(__CLASS__, 'Enter Image URL:'),
                    'createlink' => Yii::t(__CLASS__, 'Enter URL:'),
                        ), $this->labels);
    }

}