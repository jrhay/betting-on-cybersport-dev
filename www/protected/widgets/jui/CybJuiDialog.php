<?php

Yii::import('zii.widgets.jui.CJuiDialog');

class CybJuiDialog extends CJuiDialog {

    public function init() {
//        parent::init();

        $id = $this->getId();
        if (isset($this->htmlOptions['id']))
            $id = $this->htmlOptions['id'];
        else
            $this->htmlOptions['id'] = $id;
        
        if (isset($this->options['buttons'])){
            $buttons = array();
            foreach($this->options['buttons'] as $button){
                $buttons[] = (object)$button;                
            }
            $this->options['buttons'] = $buttons;
        }
        parent::init();        
    }
}