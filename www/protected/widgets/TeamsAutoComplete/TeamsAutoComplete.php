<?php
/**
 * Created by PhpStorm.
 * User: lldl
 * Date: 11/13/14
 * Time: 5:20 PM
 */



/** Todo:  rewrite class using jui autocomplete as parentClass*/
class TeamsAutoComplete extends \CAutoComplete {
    public function registerClientScript()
    {
        $id=$this->htmlOptions['id'];

        $acOptions=$this->getClientOptions();
        $options=$acOptions===array()?'{}' : CJavaScript::encode($acOptions);
        /** @var CClientScript $cs */
        $cs=Yii::app()->getClientScript();
        $cs->registerCoreScript('autocomplete');
        if($this->data!==null)
            $data=CJavaScript::encode($this->data);
        else
        {
            $url=CHtml::normalizeUrl($this->url);
            $data='"'.$url.'"';
        }
        $script = "
        jQuery('#create-game-form').on('input', \"#{$id}\", function() {
           category_id = jQuery('#Games_category_id').val();
           jQuery(\"#{$id}\").legacyautocomplete($data + '?' + 'category_id=' + category_id,{$options}){$this->methodChain};
        });
        ";
        $cs->registerScript('Yii.CAutoComplete#'.$id, $script);

        if($this->cssFile!==false)
            self::registerCssFile($this->cssFile);
    }
} 