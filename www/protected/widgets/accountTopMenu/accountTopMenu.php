<?php
Yii::import('system.web.widgets.CWidget');

class accountTopMenu extends CWidget
{

    public function init() {

        return parent::init();
    }

    public function run() {
        $this->render('menu', array());
    }
}