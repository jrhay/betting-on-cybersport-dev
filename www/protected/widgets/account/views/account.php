<ul class="cabinet-panel style3">
    <li><a href="<?php echo Yii::app()->createUrl('/account/user/accountedit')?>" title="" class="panel01"><?php echo  Yii::app()->user->name?> / <?php echo Yii::t('account','My Account') ?></a></li>
    <li><a href="<?php echo Yii::app()->createUrl('/account/index/messages')?>" title="" class="panel02">(0)</a></li>
    <li><a href="<?php echo Yii::app()->createUrl('account/usercash/cash')?>" title="" class="panel03"><?php echo Helper::formatMoney($userCash->realMoney) ?> USD</a></li>
    <li><a href="<?php echo Yii::app()->createUrl('/account/index/bets')?>" title="" class="panel04"><?php echo Helper::formatMoney($playMoneyInBets) ?> USD</a></li>
    <li><a href="<?php echo Yii::app()->createUrl('/usercash/fillcybermoney')?>" title="" class="panel05"><?php echo Helper::formatMoney($userCash->cyberMoney) ?> CM</a></li>
    <li><a href="<?php echo Yii::app()->createUrl('/account/index/bets')?>" title="" class="panel06"><?php echo Yii::t('general', 'Открытые ставки')?></a></li>
    <li><a href="<?php echo Yii::app()->createUrl('/account/usercash/cash')?>" title="" class="panel07"><?php echo Yii::t('general', 'Депозит')?></a></li>
    <li><a href="javascript:location.reload();" title="" class="panel08"><?php echo  Yii::t('account','Обновить')?></a></li>
    <li><a href="<?php echo Yii::app()->createUrl('/user/logout')?>" title="" class="panel09"><?php echo Yii::t('general', 'Выход')?></a></li>
    <li>
        <div class="lang">
            <select class="select-styled style4">
                <option class="lang-ru" <?php echo (Yii::app()->language=='ru')?'selected="true"':""; ?>>ru</option>
                <option class="lang-en" <?php echo (Yii::app()->language=='en')?'selected="true"':""; ?>>en</option>
            </select>
        </div>
    </li>
</ul>

