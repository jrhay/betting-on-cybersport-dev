<div class="cabinet-enter">
    <?php
    Yii::import('application.modules.user.models.UserLogin');
    $model = new LoginForm();
    ?>
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'action'               => Yii::app()->createUrl('/user/login'),
        'method'               => 'post',
        'id'                   => 'login-form',
//    'enctype'              => "application/x-www-form-urlencoded",
//    'name'                 => 'signin',
        'enableAjaxValidation' => true,
        'clientOptions'        => array(
            'validateOnSubmit' => true,
            'validateOnChange' => false,
        ),
    ));
    ?>
    <p><?php echo  Yii::t('login','Вход в личный кабинет')?></p>
    <div class="g-clearfix">
        <div class="inlineblock">
            <?php echo $form->textField($model,'username', array('placeholder'=>Yii::t('login','Введите логин'), 'class'=>'login')); ?>
            <?php echo $form->error($model,'username'); ?>
        </div>

        <div class="inlineblock">
            <?php echo $form->passwordField($model,'password', array('placeholder'=>Yii::t('login','Введите пароль'), 'class'=>'pass')); ?>
            <?php echo $form->error($model,'password'); ?>
        </div>

        <?php echo CHtml::submitButton(Yii::t('site', 'Sign In'), array('class' => 'btn-style1 submit signin_submit', 'name'=>'signin_submit', 'id'=>'signin_submit')); ?>
    </div>
    <div class="g-clearfix">
        <div class="lang">
            <select class="select-styled style4">
                <option class="lang-ru" <?php echo (Yii::app()->language=='ru')?'selected="true"':""; ?>>ru</option>
                <option class="lang-en" <?php echo (Yii::app()->language=='en')?'selected="true"':""; ?>>en</option>
            </select>
        </div>
        <a class="pass-forget" href="<?php echo Yii::app()->createUrl('/user/remindpassword')?>" title=""><?php echo  Yii::t('login','Забыли пароль?')?></a>
        <a class="registr" href="<?php echo Yii::app()->createUrl('/user/register'); ?>" title=""><?php echo  Yii::t('login','Регистрация')?></a>
    </div>
    <?php $this->endWidget(); ?>
</div>