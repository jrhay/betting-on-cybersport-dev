<?php
Yii::import('system.web.widgets.CWidget');

class account extends CWidget
{
    public $isDoctor = null;

    public function init() {

        return parent::init();
    }

    public function run() {
        if (Yii::app()->user->isGuest) {
            $model = new LoginForm;
            $this->render('login', array(
                'model'=>$model
            ));
        } else {
            $accountInformation = new stdClass();
            $userCash = Usercash::model()->findByAttributes(array('userId'=>Yii::app()->user->id));
            $MoneyInBets = Userbets::model()->findAllByAttributes(array('isWin'=>'0','status'=>CybController::ACTIVE_STATUS));
            $playMoneyInBets = 0;
            foreach($MoneyInBets as $money) {
                $playMoneyInBets+=$money->summ;
            }

            $this->render('account',
                array('userCash'=>$userCash,
                    'playMoneyInBets'=>$playMoneyInBets,
            ));
        }

    }
}