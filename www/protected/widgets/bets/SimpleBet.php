<?php
Yii::import('system.web.widgets.CWidget');

class SimpleBet extends CWidget
{
    public $gameCategories;
    public $games;
    public function init() {

        return parent::init();
    }

    public function run() {
//        if (Yii::app()->user->isGuest) {
//            $this->render('simpleBetGuest',array('gameCategories'=>$this->gameCategories,'games'=>$this->games));
//        } else {
            $this->render('simpleBet',array('gameCategories'=>$this->gameCategories,'games'=>$this->games));
//        }
    }
}