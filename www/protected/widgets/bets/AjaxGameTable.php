<?php
Yii::import('system.web.widgets.CWidget');

class AjaxGameTable extends CWidget
{
    public $gameCategories;
    public $games;
    public function init() {

        return parent::init();
    }

    public function run() {

            $this->render('gameTab',array('gameCategories'=>$this->gameCategories,'games'=>$this->games));
    }
}