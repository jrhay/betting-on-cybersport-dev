<table class="game-tab">
    <thead>
    <tr class="heading">
        <th class="first"><?php echo  Yii::t('site', 'Дата') ?></th>
        <th><?php echo  Yii::t('site', 'M') ?></th>
        <th><?php echo  Yii::t('site', 'Игра') ?></th>
        <th><?php echo  Yii::t('site', 'Игрок 1') ?></th>
        <th><?php echo  Yii::t('site', 'Игрок 2') ?></th>
        <th><?php echo  Yii::t('site', 'Коэфф 1') ?></th>
        <th><?php echo  Yii::t('site', 'Ничья') ?></th>
        <th><?php echo  Yii::t('site', 'Коэфф 2') ?></th>
        <th class="last"><?php echo  Yii::t('site', 'Событие') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($games as $game) {
        $trClass = 'finished';
        if ($game->game_start_at > time() && $game->status != Games::GAME_STATUS_CANCELED) {
            $trClass = ' not-started';
        } elseif ($game->game_start_at < time() && empty($game->result) && $game->status != Games::GAME_STATUS_CANCELED) {
            $trClass = ' running-no-bet';
        }
        ?>
        <?php
        $bold = '';
        /** @var Userbets $bet */
        foreach ($game->userbets as $bet) {
            if ($bet->userId == Yii::app()->user->id) {
                $bold = 'bold';
            }
        }
        ?>
        <tr id="gameId-<?php echo $game->id ?>"
            class="game-tr-<?php echo $game->category->id . $trClass ?> <?php echo  $bold ?> ">
            <?php  if(Yii::app()->user->isGuest):?>
                <td class="first"><?php echo Helper::getDateFormatted('Date format 1::MMMM dd, y HH:mm ', $game->game_start_at) ?></td>
            <?php  else: ?>
            <td class="first"><?php echo Helper::getDateFormattedForUser('Date format 1:: dd MMM, HH:mm', $game->game_start_at, User::model()->findByPk(Yii::app()->user->id)) ?></td>
            <?php  endif?>
            <td><?php echo $game->map ?></td>
            <td><img width="20" height="20"
                     src="<?php echo  Yii::app()->baseUrl ?>/images/games/<?php echo $game->category->image ?>"
                     alt=""/>
            </td>
            <td style="text-align: left">
                <img class="flag "
                     src="<?php echo  Yii::app()->baseUrl . '/../images/countries/icons/' . $game->team1->country->icon ?>">
                <?php echo $game->team1->name ?>
            </td>
            <td style="text-align:left">
                <img class="flag"
                     src="<?php echo  Yii::app()->baseUrl . '/../images/countries/icons/' . $game->team2->country->icon ?>">
                <?php echo $game->team2->name ?>
            </td>
            <td><?php echo Helper::formatKoef($game->team1_koef) ?></td>
            <td><?php echo Helper::formatKoef($game->draw_koef) ? Helper::formatKoef($game->draw_koef) : Yii::t('general', 'None') ?></td>
            <td><?php echo Helper::formatKoef($game->team2_koef) ?></td>
            <td class="last"><?php echo $game->event->text ?></td>
        </tr>
    <?php } ?>


    </tbody>
</table>