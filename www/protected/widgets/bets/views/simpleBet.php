<div class="game-filter">
    <div class="scrollbar-janos theme-dark">
        <?php
        $user_agent = getenv("HTTP_USER_AGENT");
        $macOsChrome = false;
        if (strpos($user_agent, "Mac") !== FALSE) {
            if (strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome') !== false
                || strpos($_SERVER['HTTP_USER_AGENT'], 'CriOS') !== false
            ) {
                $macOsChrome = true;
            }
        } ?>
        <div class="g-clearfix h-link "
             id="game-categories-common" style="overflow-y: hidden;padding-bottom: 0%">

            <?php
            $active = 'active';
            if (isset($_REQUEST['category_id']) && $_REQUEST['category_id'] !== 'all') {
                $active = '';
            }
            ?>
            <a href="javascript:void" id="game-table-all" class=" filter-link first <?php echo  $active ?> "
               style="overflow-y: hidden;vertical-align: top<?php  //$macOsChrome ? ' min-height: 65px ' : '  ' ?>;width:10%"><span
                    style="overflow-y: hidden;font-size: 2em;position: relative;top:-4px"><?php echo  Yii::t('site', 'Все') ?></span></a>
            <?php
            /** @var GameCategories[] $gameCategories */
            foreach ($gameCategories as $gameCategory) {
                $active = '';
                if (isset($_REQUEST['category_id']) && $categoryId = intval($_REQUEST['category_id'])) {
                    $active = $gameCategory->id == $categoryId ? 'active' : '';
                }

                ?>
                <a id="game-table-<?php echo $gameCategory->alias ?>"
                   href="<?php echo  Yii::app()->createUrl('/games') . '?' . $gameCategory->alias ?>"
                   class="filter-link <?php echo  $active ?> " style="<?php  //$macOsChrome ? ' min-height: 65px ' : '  ' ?>;width:10%"
                    title="<?php echo $gameCategory->title ?>">
                <span><img width="25" height="25"
                           src="<?php echo  Yii::app()->baseUrl ?>/images/games/<?php echo $gameCategory->image ?>"
                           alt=""/><?php //echo $gameCategory->title ?>
                </span>
                </a>
            <?php
            }
            ?>
        </div>

    </div>
    <div class="filter-section">
        <?php  include_once('gameTab.php'); ?>
    </div>
</div>
