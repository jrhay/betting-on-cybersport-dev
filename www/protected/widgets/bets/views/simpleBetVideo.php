<?php
/** @var Betsvideos[] $videos */
?>
    <div class="table-video style3">
        <div class="h-video">
            <iframe id="video-stream" width="510" height="285" src="<?php echo Yii::app()->createUrl('games/frame') ?>"
                    frameborder="0" allowfullscreen></iframe>
        </div>
        <div class="video-links">
            <?php
            $active = true;
            $firstThreeVideos = array_slice($videos, 0, 5);
            $otherVideos = array_slice($videos, 5);
            ?>
            <ul class="g-clearfix">
                <?php
                foreach ($firstThreeVideos as $video) :?>
                    <li data-href="<?php echo $video->link ?>" class="<?php echo ($active) ? 'active' : '' ?>"><a
                            href="<?php echo $video->link ?>"
                            title="<?php echo $video->title ?>"><?php echo $video->title ?></a></li>
                    <?php $active = false;?>
                <?php  endforeach ?>



                <div id="other-videos" style="display: none">
                    <?php foreach ($otherVideos as $video) : ?>
                        <li data-href="<?php echo $video->link ?>" class="<?php echo ($active) ? 'active' : '' ?>"><a
                                href="<?php echo $video->link ?>"
                                title="<?php echo $video->title ?>"><?php echo $video->title ?></a></li>
                        <?php $active = false; ?>
                    <?php endforeach ?>
                </div>

                <?php if($otherVideos): ?>
                    <li style="" id="more-ref" class="more-button">
                        <div onclick="otherVideos()"><?php echo Yii::t('games', 'MORE') ?></div>
                    </li>
                <?php  endif ?>

            </ul>
        </div>
        <br>

    </div>
    <script>
        function otherVideos(){
           var otherVideos = document.getElementById('other-videos');

            if(otherVideos.style.display == 'none') {
                otherVideos.style.display = 'block';
//                document.getElementById('more-ref').style.display = 'none';
            } else {
                otherVideos.style.display = 'none';
            }
        }
        jQuery('.video-links ul li a').click(function () {

            jQuery('.video-links ul li').removeClass('active');
            jQuery(this).parent('li').addClass('active');
            jQuery('#video-stream').attr('src', jQuery(this).attr('href'));
            return false;
        })
    </script>
