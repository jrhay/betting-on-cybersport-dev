<div class="game-filter">
    <div class="scrollbar-janos theme-dark" style="overflow-y: hidden">
        <?php
        $user_agent = getenv("HTTP_USER_AGENT");
        $macOsChrome = false;
        if (strpos($user_agent, "Mac") !== FALSE) {
            if (strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome') !== false
                || strpos($_SERVER['HTTP_USER_AGENT'], 'CriOS') !== false
            ) {
                $macOsChrome = true;
            }
        }?>
        <div class="g-clearfix h-link" id="game-categories-common" style="overflow-y: hidden;">
            <?php
            $active = 'active';
            if(isset($_REQUEST['category_id']) && $_REQUEST['category_id'] !== 'all')
                $active = '';
            ?>
            <a href="javascript:void" id="game-table-all" class="filter-link <?php echo $active?> first"
               style="overflow-y: hidden; <?php echo  $macOsChrome ? ' min-height: 65px ' : '  ' ?>"><span style="overflow-y: hidden"><?php echo  Yii::t('site', 'Все') ?></span></a>
            <?php
            /** @var GameCategories[] $gameCategories */
            foreach ($gameCategories as $gameCategory) {
            $active = '';
            if(isset($_REQUEST['category_id']) && $categoryId = intval($_REQUEST['category_id']))
                $active = $gameCategory->id == $categoryId ? 'active' : '';
                ?>
                <a href="<?php echo  Yii::app()->createUrl('/games') . '?' .$gameCategory->alias ?>" id="game-table-<?php echo $gameCategory->alias ?>" class="filter-link <?php $active?> " style="<?php echo  $macOsChrome ? ' min-height: 65px ' : '  ' ?>">
                <span><img width="20" height="20"
                           src="<?php echo  Yii::app()->baseUrl ?>/images/games/<?php echo $gameCategory->image ?>"
                           alt=""/><?php echo $gameCategory->title ?></span></a>
            <?php
            }
            ?>
        </div>
    </div>
    <div class="filter-section">
        <table class="game-tab">
            <thead>
            <tr class="heading">
                <th class="first"><?php echo  Yii::t('site', 'Дата') ?></th>
                <th><?php echo  Yii::t('site', 'M') ?></th>
                <th><?php echo  Yii::t('site', 'Игра') ?></th>
                <th><?php echo  Yii::t('site', 'Игрок 1') ?></th>
                <th><?php echo  Yii::t('site', 'Игрок 2') ?></th>
                <th><?php echo  Yii::t('site', 'Коэфф 1') ?></th>
                <th><?php echo  Yii::t('site', 'Ничья') ?></th>
                <th><?php echo  Yii::t('site', 'Коэфф 2') ?></th>
                <th class="last"><?php echo  Yii::t('site', 'Событие') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($games as $game) {
                $trClass = 'finished';
                if ($game->game_start_at > time() && $game->status != Games::GAME_STATUS_CANCELED) {
                    $trClass = ' not-started';
                } elseif ($game->game_start_at < time() && empty($game->result) && $game->status != Games::GAME_STATUS_CANCELED) {
                    $trClass = ' running-no-bet';
                }
                ?>
                <?php
                $bold = '';
                /** @var Userbets $bet */
                foreach ($game->userbets as $bet) {
                    if ($bet->userId == Yii::app()->user->id) {
                        $bold = 'bold';
                    }
                }
                ?>
                <tr id="gameId-<?php echo $game->id ?>"
                    class="game-tr-<?php echo $game->category->id . $trClass ?> <?php echo  $bold ?>">
                    <td class="first"><?php echo Helper::getDateFormatted('Date format 1::MMMM dd, y HH:mm ', $game->game_start_at) ?></td>
                    <td ><?php echo $game->map ?></td>
                    <td><img width="20" height="20"
                             src="<?php echo  Yii::app()->baseUrl ?>/images/games/<?php echo $game->category->image ?>"
                             alt=""/>
                    </td>
                    <td style="text-align: left">
                        <img class="flag"
                             src="<?php echo  Yii::app()->baseUrl . '/../images/countries/icons/' . $game->team1->country->icon ?>">
                        <!--                        <img  class="team-logo" src="-->
                        <?php  //= Yii::app()->baseUrl.'/../images/teamsLogos/'.$game->team1->teamLogo?><!--">-->
                        <?php echo $game->team1->name ?>
                    </td>
                    <td style="text-align: left">
                        <img class="flag"
                             src="<?php echo  Yii::app()->baseUrl . '/../images/countries/icons/' . $game->team2->country->icon ?>">
                        <!--                        <img  class="team-logo" src="-->
                        <?php  //= Yii::app()->baseUrl.'/../images/teamsLogos/'.$game->team2->teamLogo?><!--">-->
                        <?php echo $game->team2->name ?>
                    </td>
                    <td><?php echo Helper::formatKoef($game->team1_koef) ?></td>
                    <td><?php echo Helper::formatKoef($game->draw_koef) ? Helper::formatKoef($game->draw_koef) : Yii::t('general', 'None') ?></td>
                    <td><?php echo Helper::formatKoef($game->team2_koef) ?></td>
                    <td class="last"><?php echo $game->event->text ?></td>
                </tr>
            <?php } ?>


            </tbody>
        </table>
    </div>
</div>