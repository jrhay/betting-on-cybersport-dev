<?php
Yii::import('system.web.widgets.CWidget');

class SimpleBetVideo extends CWidget
{

    public function init() {

        return parent::init();
    }

    public function run() {
        $lang = Yii::app()->language;
        $criteria = new CDbCriteria();
//        if($lang == 'ru') {
//            $criteria->addColumnCondition(array('lang' => Betsvideos::LANG_RU));
//        }
//        if($lang == 'en') {
//            $criteria->addColumnCondition(array('lang' => Betsvideos::LANG_EN));
//        }

        /** @var Betsvideos $betsVideos */
        $betsVideos = Betsvideos::model()->onlyActive()->findAll($criteria);
        $this->render('simpleBetVideo',array('videos'=>$betsVideos));
    }
}