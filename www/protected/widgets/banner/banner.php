<?php
Yii::import('system.web.widgets.CWidget');

class banner extends CWidget
{
    public $id = null;
    public $width = null;
    public $height = null;

    public function init()
    {

        return parent::init();
    }

    public function run()
    {
        if (!$this->width || !$this->height) {
            return false;
        }

        $lang = Yii::app()->language;
        $lang === 'ru' ? $lang = Banners::LANG_RU : $lang =
             Banners::LANG_ENG;
        $banner = Banners::model()->findByAttributes(
            array(
                'width' => $this->width,
                'height' => $this->height,
                'lang' => $lang,
            ),
            array(
                'order' => 'RAND()',
                'limit' => 1,
            )
        );

        if ($banner) {
            $this->render('banner', array('banner' => $banner));
        }

    }
}