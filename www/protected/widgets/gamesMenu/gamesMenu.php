<?php
Yii::import('system.web.widgets.CWidget');

class gamesMenu extends CWidget
{

    public function init() {

        return parent::init();
    }

    public function run() {
        $this->render('menu');
    }
}