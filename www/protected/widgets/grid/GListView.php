<?php

//Yii::import('bootstrap.widgets.BootGridView');
Yii::import('zii.widgets.CListView');

class GListView extends CListView
{
	public $display_number = true;
	public $doctor_list = false;
	
	public function init()
	{
		// TODO: move "minimal number of items" limit to settings
		if (!$this->doctor_list && $this->display_number && $this->dataProvider->getTotalItemCount() >= 5)
		{
			$pageSize =  Helper::getPageSize();
			$pageSize = '<div class="display-number" style="margin: 5px 0 5px 5px;float: left;"><div>'.Yii::t('general', 'Display #: ').'</div>'.CHtml::dropDownList('pageSize', $pageSize,
				array(6 => 6, 10 => 10, 20 => 20, 50 => 50),
				array('onchange' => "$.fn.yiiListView.update('".$this->id."',{ data:{pageSize: $(this).val() }})",
				)).'</div>';
			$this->template .= $pageSize;
		}
		
		if ($this->doctor_list && $this->display_number && $this->dataProvider->getTotalItemCount() >= 5)
		{
			$pageSize = Helper::getPageSize('pageSize_doctors');
			if (!$pageSize) {
				$pageSize = Yii::app()->params['default_pagesize'];
			}
			$pageSize = '<div class="display-number" style="margin: 5px 0 5px 5px;float: left;">'.Yii::t('general', 'Display #: ').CHtml::dropDownList('pageSize_doctors', $pageSize,
				array(6 => 6, 10 => 10, 20 => 20, 50 => 50),
				array('onchange' => "$('.content').addClass('loading_ajax_body');$.fn.yiiListView.update('".$this->id."',
					{ data:{pageSize_doctors: $(this).val()}, complete:function(x) { $('.content').removeClass('loading_ajax_body');;}})",
				)).'</div>';
			$this->template .= $pageSize;
		}
		
		parent::init();
	}
}