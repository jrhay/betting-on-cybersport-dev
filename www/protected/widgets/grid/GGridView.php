<?php

//Yii::import('bootstrap.widgets.BootGridView');
Yii::import('zii.widgets.grid.CGridView');
Yii::import('application.widgets.grid.CDataColumnJbw');

class GGridView extends CGridView
{
    public $theadCssClass = '';
    public $tableHtmlID = '';
    public $filter_top = false;
    public $addOrganizationTitleRow = false;
    protected $_currentOrganization = false;
    public $display_number = true;

    public function run()
    {
        // TODO: move "minimal number of items" limit to settings
        if ($this->display_number && $this->dataProvider->getTotalItemCount() >= 1) {
            $pageSize = Helper::getPageSize();
            $pageSize = '<div class="display-number" style="margin: 5px 0 5px 5px;float: left;"><div style="float: left;padding-top: 4px;padding-right: 10px;">' . Yii::t('general', 'Display #: ') . '</div>' . CHtml::dropDownList('pageSize', $pageSize,
                    array(1 => 1, 5 => 5, 10 => 10, 20 => 20, 50 => 50),
                    array('onchange' => "$.fn.yiiGridView.update('" . $this->id . "',{ data:{pageSize: $(this).val() }})",
                    )) . '</div>';
            $this->template .= $pageSize;
        }

        $this->registerClientScript();

        if ($this->tagName)
            echo CHtml::openTag($this->tagName, $this->htmlOptions) . "\n";

        $this->renderContent();
        $this->renderKeys();

        if ($this->tagName)
            echo CHtml::closeTag($this->tagName);
    }

    protected function initColumns()
    {

        if ($this->columns === array()) {
            if ($this->dataProvider instanceof CActiveDataProvider)
                $this->columns = $this->dataProvider->model->attributeNames();
            elseif ($this->dataProvider instanceof IDataProvider) {
                // use the keys of the first row of data as the default columns
                $data = $this->dataProvider->getData();
                if (isset($data[0]) && is_array($data[0]))
                    $this->columns = array_keys($data[0]);
            }
        }
        $id = $this->getId();
        foreach ($this->columns as $i => $column) {
            if (is_string($column))
                $column = $this->createDataColumn($column);
            else {
                if (!isset($column['class']))
                    $column['class'] = 'CDataColumnJbw';
                $column = Yii::createComponent($column, $this);
            }
            if (!$column->visible) {
                unset($this->columns[$i]);
                continue;
            }
            if ($column->id === null)
                $column->id = $id . '_c' . $i;
            $this->columns[$i] = $column;
        }

        foreach ($this->columns as $column)
            $column->init();
    }

    public function renderItems()
    {
        if ($this->dataProvider->getItemCount() > 0 || $this->showTableOnEmpty) {
            if ($this->filter_top) {
                $this->renderModifiedFilter();
            }
            echo "<table " . ($this->tableHtmlID ? 'id="' . $this->tableHtmlID . '"' : '') . " class=\"{$this->itemsCssClass}\">\n";

            $this->renderTableHeader();
            ob_start();
            $this->renderTableBody();
            $body = ob_get_clean();
            $this->renderTableFooter();
            echo $body; // TFOOT must appear before TBODY according to the standard.
            echo "</table>";
        } else
            $this->renderEmptyText();
    }

    public function renderTableHeader()
    {
        if (!$this->hideHeader) {
            echo "<thead " . ($this->theadCssClass ? 'class="' . $this->theadCssClass . '"' : '') . ">\n";

            if ($this->filterPosition === self::FILTER_POS_HEADER)
                $this->renderFilter();

            echo "<tr>\n";
            foreach ($this->columns as $column)
                $column->renderHeaderCell();
            echo "</tr>\n";

            if ($this->filterPosition === self::FILTER_POS_BODY)
                $this->renderFilter();

            echo "</thead>\n";
        } else if ($this->filter !== null && ($this->filterPosition === self::FILTER_POS_HEADER || $this->filterPosition === self::FILTER_POS_BODY)) {
            echo "<thead " . ($this->theadCssClass ? 'class="' . $this->theadCssClass . '"' : '') . ">\n";
            $this->renderFilter();
            echo "</thead>\n";
        }
    }

    public function renderHeaderCell()
    {
        $this->headerHtmlOptions['id'] = $this->id;
        echo CHtml::openTag('th class="sorting"', $this->headerHtmlOptions);
        $this->renderHeaderCellContent();
        echo "</th>";
    }

    public function renderModifiedFilter()
    {
        if ($this->filter !== null) {
            echo "<div class=\"search_user {$this->filterCssClass}\">\n";

            foreach ($this->columns as $column) {
                if ($column->filter !== false) {
                    echo '<label>' . User::model()->getAttributeLabel($column->name) . '</label>';
                }
                $column->renderFilterCell();
            }
            echo '<input class="standard-button" type="submit" value="' . Yii::t('admin_home', 'Search') . '" />';
            echo "</div>\n";
        }
    }

    public function renderFilter()
    {
        if ($this->filter !== null && !$this->filter_top) {
            echo "<tr class=\"{$this->filterCssClass}\">\n";
            foreach ($this->columns as $column)
                $column->renderFilterCell();
            echo "</tr>\n";
        }
    }

    public function renderTableBody()
    {
        $data = $this->dataProvider->getData();
        $n = count($data);
        echo "<tbody>\n";

        if ($n > 0) {
            for ($row = 0; $row < $n; ++$row)
                $this->renderTableRow($row);
        } else {
            echo '<tr><td colspan="' . count($this->columns) . '" class="empty">';
            $this->renderEmptyText();
            echo "</td></tr>\n";
        }
        echo "</tbody>\n";
    }

    public function renderTableRow($row)
    {
        $htmlOptions = array();
        if ($this->rowHtmlOptionsExpression !== null) {
            $data = $this->dataProvider->data[$row];
            $options = $this->evaluateExpression($this->rowHtmlOptionsExpression, array('row' => $row, 'data' => $data));
            if (is_array($options))
                $htmlOptions = $options;
        }

        if ($this->rowCssClassExpression !== null) {
            $data = $this->dataProvider->data[$row];
            $class = $this->evaluateExpression($this->rowCssClassExpression, array('row' => $row, 'data' => $data));
        } elseif (is_array($this->rowCssClass) && ($n = count($this->rowCssClass)) > 0)
            $class = $this->rowCssClass[$row % $n];

        if (!empty($class)) {
            if (isset($htmlOptions['class']))
                $htmlOptions['class'] .= ' ' . $class;
            else
                $htmlOptions['class'] = $class;
        }

        $colCtr = 0;
        foreach ($this->columns as $column) {
            $colCtr++;
        }
        if ($this->addOrganizationTitleRow) {
            $data = $this->dataProvider->data[$row];
            $OrganizationID = isset($data->organizations[0]->id) ? $data->organizations[0]->id : 0;

            if (!$this->_currentOrganization) {
                $this->_currentOrganization = $OrganizationID;
                if ($OrganizationID) {
                    echo "<tr class=\"alt-row\"><td colspan=\"$colCtr\">" . $data->organizations[0]->name . "</td></tr>";
                } else {
                    echo "<tr class=\"alt-row\"><td colspan=\"$colCtr\">" . $OrganizationID . "*</td></tr>";
                }
            }

            if ($this->_currentOrganization == $OrganizationID) {

            } else {
                $this->_currentOrganization = $OrganizationID;
                if ($OrganizationID) {
                    echo "<tr class=\"alt-row\"><td colspan=\"$colCtr\">" . $data->organizations[0]->name . "</td></tr>";
                }
            }
        }
        echo CHtml::openTag('tr', $htmlOptions) . "\n";

        foreach ($this->columns as $column)
            $column->renderDataCell($row);
        echo "</tr>\n";
    }

}
