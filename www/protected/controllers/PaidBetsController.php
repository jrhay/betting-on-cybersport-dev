<?php
/**
 * Created by PhpStorm.
 * User: lldl
 * Date: 24.12.14
 * Time: 21:13
 */

class PaidBetsController extends CybController {
    public function actionViewSinglePaidBets()
    {
        if (isset($_REQUEST['paidbets_id'])) {
            $paidbets = new PaidBets();
            $paidbets = $paidbets->findByPk($_REQUEST['paidbets_id']);
            return $this->render('singlePaidBets', array(
                'paidbets' => $paidbets
            ));
        }
    }

    public function actionIndex()
    {
        $model = new PaidBets;

        // uncomment the following code to enable ajax-based validation
        if (isset($_REQUEST['deletePaidBetsId'])) {
            $model->deleteByPk($_REQUEST['deletePaidBetsId']);
        }
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'paidbets-paidbetsForm2-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }


        if (isset($_POST['PaidBets'])) {
            $model->attributes = $_POST['PaidBets'];
            /** @var CUploadedFile img */
            $model->img = CUploadedFile::getInstance($model, 'img');
            $model->img_eng = CUploadedFile::getInstance($model, 'img_eng');
            $model->date = time();
            if ($model->save()) {
                $filename = $model->img->getName();
                $path = Yii::app()->basePath . '/../images/paidbets/' . $filename;
                $filename_eng = $model->img_eng->getName();
                $path_eng = Yii::app()->basePath . '/../images/paidbets/' . $filename_eng;
                if ($model->img->saveAs($path, true) && $model->img_eng->saveAs($path_eng, true)) {
                    Yii::app()->user->setFlash('success', Yii::t('site', 'Отзыв успешно добавлен'));
                } else {
                    Yii::app()->user->setFlash('error', Yii::t('site', 'Ошбика, отзыв не добавлен'));
                }
            }
        }

//        if (isset($_REQUEST['currentCategory'])) {
//            $currentCategory = $_REQUEST['currentCategory'];
//        } else {
//            $currentCategory = 1;
//        }

        $criteria = new CDbCriteria();
//        $criteria->addColumnCondition(array('category_id' => $currentCategory));
        $criteria->order = 'date DESC';
        $count = PaidBets::model()->count($criteria);
        $pages = new CPagination($count);

        // results per page
        $pages->pageSize = 5;
        $pages->applyLimit($criteria);

        $paidbets = PaidBets::model()->findAll($criteria);
//        $paidbetsCategories = new PaidBetsCategories();
//        $paidbetsCategories = $paidbetsCategories->findAll();

        $this->render('index',
            array(
                'paidbets' => $paidbets,
//                'paidbetsCategories' => $paidbetsCategories,
                'model' => $model,
                'pages' => $pages,
//                'currentCategory' => $currentCategory,
            )
        );
    }

}