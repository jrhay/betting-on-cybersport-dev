<?php

/**
 * Class UserController
 */
class UserController extends CybController
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
//			'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * @return array
     */
    public function actions()
    {
        return array(
            'captcha' => array(
                'class' => 'CCaptchaAction',
            ),
        );
    }


    /**
     * @param $id
     * @return CActiveRecord|EMongoDocument|null
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = User::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     *
     */
    public function actionLogin()
    {

        $model = new LoginForm;

        $this->performAjaxValidation($model, 'login-form');

        Yii::app()->user->returnUrl = array('/games');

        if (isset(Yii::app()->user->id)) {


            $this->redirect(Yii::app()->user->returnUrl);
        }

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];

//            Yii::log('User tried to login, credentials [U:'.$model->username.',P:'.$model->password.']', CLogger::LEVEL_INFO, 'session.info');
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login()) {
//                Yii::log('', CLogger::LEVEL_INFO, 'User LOGGED in [U:'.$model->username.',P:'.$model->password.']');
//                Yii::app()->cache->delete('userTimezoneChache_'.Yii::app()->user->id);
                $this->redirect(Yii::app()->user->returnUrl);
            } else {


            }

        }

        if (!Yii::app()->user->isGuest) {
            $this->render('logout', array('model' => $model));
        } else {
            Yii::app()->user->setFlash('notice', Yii::t('general', "Your are not logged in or your session was expired!"));
            $this->redirect(array('/../'));
        }
    }

    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(array('/../'));
        return true;
    }


    /**
     * Performs the AJAX validation.
     *
     * @param User $model the model to be validated
     */
    protected function performAjaxValidation($model, $form)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === $form) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     *
     */

    public function actionRegister()
    {
        $this->layout = '//layouts/rightcol';
        $model = new RegisterUserForm();

        $this->performAjaxValidation($model, 'register-form');

        if (isset($_POST['RegisterUserForm'])) {
            $user = new User('create');
            $user->attributes = $_POST['RegisterUserForm'];
            $user->status = User::INACTIVE_USER_STATUS;
            $user->usertypeId = Usertype::USERTYPE_REGISTERED;
            $user->createTime = time();
            $user->country = intval($_POST['RegisterUserForm']['country']);


            if ($user->validate()) {
                if ($user->save()) {
                    //uncomment if need register bonus before activation with mail
//                    $groupBonusActivator = new GroupBonusActivator();
//                    $groupBonusActivator->ActivateGroupBonuses(1, $user->id);
                    //if ref code
                    //creating bonus balance
                    $bonusBalance = new BonusBalance();
                    $bonusBalance->getBBalanceModelByUserid($user->id);
                    if (isset($_REQUEST['ruid'])) {
                        /** @var RefCode $refCode */
                        $refCode = new RefCode();
                        $refCode = $refCode->getRefCode($_REQUEST['ruid']);
                        $refCode->addNewUser($user->id);
                    }
                    Helper::changeUserLanguage($user->id, $_POST['RegisterUserForm']['language']);
                    $params = array(
                        '{username}' => $user->userName,
                        '{activation_url}' => Yii::app()->getBaseUrl(true) . '/user/activation?key=' . md5($user->userName) . '&email=' . $user->email,
                        '{name}' => $user->firstName,
                        '{password}' => $_POST['RegisterUserForm']['password'],
                        '{siteurl}' => Yii::app()->getBaseUrl(true));
                    Helper::sendEmail('new_user_registered', $params, $user->id);
                    Yii::app()->user->setFlash('success', Yii::t('doctor_home', 'New user was successfully added. You need to activate your account'));
                    $this->redirect(array('/games'));
                }
            } else {
                foreach ($user->getErrors() as $error) {
                    Yii::app()->user->setFlash('error', Yii::t('site', $error[0]));

                }
                $model->attributes = $_POST['RegisterUserForm'];
            }
        }

        $this->pageTitle = Helper::getPageTitle(Yii::t('site', 'Register User'));

        /** @var Countries[] $countriesModels */
        $countriesModels = Countries::model()->findAll(array('order' => 'name'));
        /** @var Countries[] $countries */
        $countries = array();
        $popularCountries = Helper::getPopularCountries();
        foreach ($countriesModels as $key => $cm) {
            if(in_array($cm->name,$popularCountries)) {
                continue;
            }
            $countries[$cm->id] = $cm->name;
        }
        $countries = array_merge($popularCountries, $countries);
        $this->render('register', array(
            'model' => $model,
            'countries' => $countries));
    }

    public function actionRemindPassword()
    {
        $this->layout = '//layouts/rightcol';

        $model = new RemindPassword();

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'user-password-reminder') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if (isset($_POST['RemindPassword'])) {
            $post = Yii::app()->request->getPost('RemindPassword');

            $userId = User::model()->findByAttributes(array('email' => $post['email']));

            if (isset($userId->id)) {
                $newPrivateKey = User::model()->generatePassword(12);
                $userModel = new User();
                $userModel->updateByPk($userId->id, array('privateKey' => $newPrivateKey));
                $encodedNewPrivateKey = base64_encode($newPrivateKey);
                $url = $this->createUrl('user/passwordchange', array('key' => $encodedNewPrivateKey));

                Yii::app()->user->setFlash('success', Yii::t('general', Yii::t('register', 'We sent you a link to regenerate your password, please check your email address.')));
                Yii::app()->mailer->sendmail('remindpassword', array('{link}' => Yii::app()->getBaseUrl(true) . $url), array($post['email']));
            } else {
                Yii::app()->user->setFlash('error', Yii::t('general', Yii::t('register', 'This user does not exist!')));
            }
        }

        $this->pageTitle = Helper::getPageTitle(Yii::t('site_home', 'Remind Password'));
        $this->render('remindpassword', array('model' => $model));
    }

    /**
     * @param $key
     */
    public function actionPasswordChange($key)
    {
        $model = new RemindPassword();
        $privateKey = base64_decode($key);
        $dbUser = User::model()->findByAttributes(array('privateKey' => $privateKey));

        if (isset($dbUser->id)) {
            $generatedPassword = User::model()->generatePassword(12);

            $dbUser->password = $dbUser->repeat_password = $generatedPassword;
            $dbUser->privateKey = NULL;
            if ($dbUser->validate()) {
                $dbUser->save();

//				$timeStr = Helper::getCurrentDateFormattedForUser(Yii::t('information','Date format 1::MMMM dd, y hh:mm a'), $dbUser);
                Yii::app()->mailer->sendmail('newpassword', array('{password}' => $generatedPassword, '{username}' => $dbUser->userName, '{time}' => date('Y-m-d H:i:s')), $dbUser->id);

                Yii::app()->user->setFlash('success', Yii::t('general', 'Password has been changed. Check your email address.'));

                $this->redirect(array('/site'));
            } else {
                Yii::app()->user->setFlash('error', Yii::t('general', 'Something wrong! Try again later!'));
            }

        } else {
            Yii::app()->user->setFlash('error', Yii::t('general', 'Your link has expired!'));
            $this->redirect(array('user/remindpassword'));
        }

        $this->pageTitle = Helper::getPageTitle(Yii::t('site_home', 'Remind Password'));
        $this->render('remindpassword', array('model' => $model));
    }

    public function actionActivation($key, $email)
    {
        /** @var User $user */
        $user = User::model()->findByAttributes(array('email' => $email));
        if ($user) {
            if (md5($user->userName) == $key) {
                $user = User::model()->findByAttributes(array('email' => $email));
                $user->setAttribute('status', User::ACTIVE_USER_STATUS);
                $user->saveAttributes(array('status' => User::ACTIVE_USER_STATUS));
                $flashMessage = Yii::t('site', 'User succesfully activated.');
                Yii::app()->user->setFlash('success', $flashMessage);

                //bonus
                $groupBonusActivator = new GroupBonusActivator();
                $groupBonusActivator->ActivateGroupBonuses(1, $user->id);
            } else {
                $flashMessage = Yii::t('site', 'Wrong security code. Try again');
                Yii::app()->user->setFlash('notice', $flashMessage);
            }

        } else {
            $flashMessage = Yii::t('site', 'Wrong data. Try again');
            Yii::app()->user->setFlash('notice', $flashMessage);
        }
        $this->redirect(array('/site'));
    }
}
