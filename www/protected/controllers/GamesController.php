<?php

class GamesController extends CybController
{


    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
//			'postOnly + delete', // we only allow deletion via POST request
        );
    }


    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Games the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Games::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {

        $model = new Games;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Games'])) {
            $_POST['Games']['game_start_at'] = User::model()->getTimestampFromDate($_POST['Games']['game_start_at']);
            $model->attributes = $_POST['Games'];
            if (!empty($uploadedFile)) {
                $uploadedFile = CUploadedFile::getInstance($model, 'game_ico');
                $fileName = crypt(time(), rand(0, 10000)) . $uploadedFile;
                $model->game_ico = $fileName;

            }

            $model->create_at = time();
            if ($model->save()) {
                if (!empty($uploadedFile)) {
                    $uploadedFile->saveAs(Yii::app()->basePath . '/../images/gameIcons/' . $fileName);
                }
                $this->redirect(array('index'));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Performs the AJAX validation.
     * @param Games $model the model to be validated
     */
    protected function performAjaxValidation($model, $form = 'games-form')
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === $form) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Games'])) {

            $_POST['Games']['game_start_at'] = User::model()->getTimestampFromDate($_POST['Games']['game_start_at']);
            $_POST['Games']['game_end'] = User::model()->getTimestampFromDate($_POST['Games']['game_end']);
            $model->attributes = $_POST['Games'];

            $uploadedFile = CUploadedFile::getInstance($model, 'game_ico');
            if (!empty($uploadedFile)) {
                $fileName = crypt(time(), rand(0, 10000)) . $uploadedFile;
                $model->game_ico = $fileName;
            }

            if ($model->save()) {
                if (!empty($uploadedFile)) {
                    $uploadedFile->saveAs(Yii::app()->basePath . '/../images/gameIcons/' . $fileName);
                    $this->redirect(array('index'));

                }
            }

        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
    }

    public function actionFrame()
    {

        /** @var DefaultGamesVideoFrame $defaultGamesVideoFrame */
        $defaultGamesVideoFrame = DefaultGamesVideoFrame::model()->findByPk('1');
        $this->renderFile(
            Yii::app()->basePath . '/views/games/_defaultVideo.php', array(
            'defaultGamesVideoFrame' => $defaultGamesVideoFrame,

        ));
    }

    /**
     * Manages all models.
     */
    public function actionIndex($category_id=null)
    {
        /** @var Games[] $model */
        $model = new Games('search');

        /** @var CDbCriteria $criteria */
        $criteria = new CDbCriteria();
        /** @var GameCategories $category */
        foreach (GameCategories::model()->findAllByAttributes(array('status' => CybController::ACTIVE_STATUS)) as $category) {
            if (isset($_REQUEST[$category->alias]))
                $criteria->addColumnCondition(array('category_id' => $category->id));
        }

        $games = Games::model()->onlyEightHours()->gameStartTimeSort()->with(array(array('category', 'together' => true), array('team1', 'together' => true), array('team2', 'together' => true)))->findAll($criteria);
        //if game has result added more then 8 hours ago, don't show it
//        $games = $this->handleGamesArr($games);
        $model->unsetAttributes(); // clear any default values
        /** @var GameCategories[] $gameCategories */
        $gameCategories = GameCategories::model()->findAll();

        $gameCategories = $this->sortByGames($gameCategories);

        if (isset($_GET['Games']))
            $model->attributes = $_GET['Games'];

        if (isset($_REQUEST['ajax'])) {
            $this->renderPartial('_ajaxSimpleBet', array(
                'gameCategories' => $gameCategories, 'games' => $games
            ));
        } else {
            $this->render('index', array(
                'model' => $model, 'gameCategories' => $gameCategories, 'games' => $games
            ));
        }
    }

    /** @var GameCategories[] $arr */
    protected function sortByGames($arr)
    {
        $index = array();
        foreach ($arr as $a) $index[] = $this->countGamesInCategory($a);
        array_multisort($index, $arr);
        $arr = array_reverse($arr);
        return $arr;
    }

    /** @var GameCategories $gameCategory */
    protected function countGamesInCategory($gameCategory)
    {
        return Games::model()->onlyEightHours()->countByAttributes(array('category_id' => $gameCategory->id));
    }

    public function actionMakeBet()
    {


        if (!isset($_POST['gameID']) && !$_POST['standartBetForm']) {
            //TODO: render 404 common error
            return;
        }
        $model = new standartBetForm();
        $this->performAjaxValidation($model, 'simpleBet-form');


        if (isset($_POST['standartBetForm'])) {

            if ($this->isGameStarted($_POST['standartBetForm']['gameID'])) {
                Yii::app()->user->setFlash('error', Yii::t('games', 'Your bet is not created, because game is already started!'));
                Yii::app()->end();
            }
//            if(Yii::app()->user->isGuest) {
//                $loginUrl = Yii::app()->createUrl('/user/register');
//                $this->redirect(Yii::app()->createUrl('/user/register'),true);
//            }

            $userBet = new Userbets();
            $userBet->attributes = $_POST['standartBetForm'];
            $userKoefFromState = Yii::app()->user->getState('userbet_' . $_POST['standartBetForm']['gameID']);

            $userBet->koef = $userKoefFromState[$_POST['standartBetForm']['teamID']];
            $userBet->betType = Userbets::TYPE_BET_SIMPLE;
            $userBet->userId = Yii::app()->user->id;
            $userBet->moneyType = $_POST['standartBetForm']['moneyType'];
            $userBet->gameId = $_POST['standartBetForm']['gameID'];
            $userBet->result = $_POST['standartBetForm']['teamID'];
            $userBet->status = CybController::ACTIVE_STATUS;
            $userBet->isWin = Userbets::NOT_YET;

            $userBet->save();

            //activating group bonuses
            $groupBonusActivator = new GroupBonusActivator();
            $groupBonusActivator->ActivateGroupBonuses(2, Yii::app()->getUser()->id, $userBet->summ);

            //jackpot
            $jackpot = new Jackpot();
            $jackpot = $jackpot->getCurrentJackpot();
            $jackpot->addMoneyWithKoefCheck($userBet->summ, $userBet->koef);

            //We need to store bets history and check koefs

            $game = Helper::getNewKoefs($userBet->gameId, $userBet->summ, $userBet->result, $userBet->id, $userBet->moneyType);
//            var_dump($game);die;
            $game->save();

            Helper::updateUserCashOnBetAdded($userBet->summ, $userBet->moneyType);

            Yii::app()->user->setFlash('success', Yii::t('games', 'Your bet added'));
            Yii::app()->end();
        }
        $gameID = explode('-', $_POST['gameID']);
        if (isset($gameID[1]) && is_numeric($gameID[1]) && $gameID[1] > 0) {
            $game = Games::model()->with(array(array('category', 'together' => true), array('team1', 'together' => true), array('team2', 'together' => true)))->findByPk($gameID[1]);
            if (!$game) {
                //TODO: render 404 common error
            }
            Yii::app()->clientScript->scriptMap = array(
                'jquery.js' => false,
                'jquery.min.js' => false,
            );

            //Set user state variables
            Yii::app()->user->setState('userbet_' . $game->id, array('1' => $game->team1_koef, '2' => $game->team2_koef, '0' => $game->draw_koef));
            $alreadyMadeBets = Userbets::model()->OnlyActive()->findAllByAttributes(array('userId' => Yii::app()->user->id, 'gameId' => $game->id));

            $model->moneyType = Usercash::REAL_MONEY;
            $this->renderPartial('_simplebet', array('game' => $game, 'model' => $model, 'alreadyMadeBets' => $alreadyMadeBets), false, true);
        } else {
            //TODO: render 404 common error
            return;
        }
    }

    /** check if game is started
     * @param integer $id
     * @return
     */
    protected function isGameStarted($id)
    {
        $now = time();
        $game = Games::model()->findByPk($id);
        if ($game->game_start_at > $now && $game->status == Games::GAME_STATUS_NOT_STARTED && empty($game->result)) {
            return false;
        }
        return true;
    }

    /** Using this func to sort array of gameCategories */

    /** Check if there more than 8 hours since games started */

    public function actionExpressbet()
    {


        $games = Games::model()->onlyNotStarted()->gameStartTimeSort()->findAll();

        $model = new expressBetForm();

        $error = null;

        $this->performAjaxValidation($model, 'expressBet-form');

        Yii::app()->user->setState('express_' . Yii::app()->user->id, $games);


        if ($_POST) {

            $choosedGames = $_POST['games'];

            foreach ($choosedGames as $id => $result) {
                /** @var Games $game */
                if ($this->isGameStarted($id)) {
                    Yii::app()->user->setFlash('error', Yii::t('games', 'Your bet is not created, because  game is already started!'));
                    Yii::app()->end();
                }
            }
            if (count($choosedGames) >= 10) {
                Yii::app()->user->setFlash('error', Yii::t('games', 'Too many games are chosen ( 10 max allowed)'));
                Yii::app()->end();
            }

            $gamesFromUserState = Yii::app()->user->getState('express_' . Yii::app()->user->id);

            $expressBetModel = new Expressbets();
            $expressBetModel->time = time();
            $expressBetModel->status = CybController::ACTIVE_STATUS;
            $expressBetModel->isWin = CybController::NOT_YET;
            $expressBetModel->userId = Yii::app()->user->id;
            $expressBetModel->summ = $_POST['expressBetForm']['summ'];


            if ($expressBetModel->save()) {
                $summKoef = 1;
                foreach ($choosedGames as $gameId => $result) {
                    $expressbetDetailsModel = new Expressbetsdetails();
                    $expressbetDetailsModel->expressBetId = $expressBetModel->id;
                    $expressbetDetailsModel->gameId = $gameId;
                    $expressbetDetailsModel->rateResult = key($result);

                    foreach ($gamesFromUserState as $game) {
                        if ($game->id == $expressbetDetailsModel->gameId) {
                            if ($expressbetDetailsModel->rateResult > 0) {
                                $koefKeyValue = 'team' . $expressbetDetailsModel->rateResult . '_koef';
                                $expressbetDetailsModel->koef = $game->$koefKeyValue;
                            } else {
                                $expressbetDetailsModel->koef = $game->draw_koef;
                            }
                        }
                    }
                    $summKoef = $summKoef * $expressbetDetailsModel->koef;

                    if ($summKoef > 200) {
                        /** @var Expressbetsdetails $detail */
                        foreach ($expressBetModel->expressbetsdetails as $detail) {
                            $detail->delete();
                        }
                        $expressBetModel->delete();
                        Yii::app()->user->setFlash('error', Yii::t('games', 'Too big koef, make another choice'));
                    } else {
                        $expressbetDetailsModel->save();



                        Yii::app()->user->setFlash('success', Yii::t('games', 'Your express bet added'));
                    }

                }
                Helper::updateUserCashOnBetAdded($expressBetModel->summ, Usercash::REAL_MONEY);
                // activating group bonuses for bet
                $groupBonusActivator = new GroupBonusActivator();
                $groupBonusActivator->ActivateGroupBonuses(2, Yii::app()->getUser()->id, $expressBetModel->summ);
            } else {
                $error = "Error saving your express bet";
            }

            Yii::app()->end();
        }

        Yii::app()->clientScript->scriptMap = array(
            'jquery.js' => false,
            'jquery.min.js' => false,
        );
        $model->summ = 0;
        $this->renderPartial('_expressBet', array('games' => $games, 'model' => $model), false, true);
        Yii::app()->end();
    }

    /** Count games in gameCategory using 8 hours condition (this->handleGamesArr) */

    public function actionTournaments($id = null)
    {


        if (Yii::app()->request->isAjaxRequest || Yii::app()->request->isPostRequest) {


            $model = new tournamentBetForm();

            $this->performAjaxValidation($model, 'tournaments-form');

            if (isset($_POST['tournamentBetForm'])) {
                $tournamentBet = new Tournamentsbets();
                $tournamentBet->attributes = $_POST['tournamentBetForm'];
                $tournamentBet->userId = Yii::app()->user->id;
                $tournamentBet->tournamentId = $_POST['tournamentBetForm']['tournamentID'];
                $tournamentBet->teamId = $_POST['tournamentBetForm']['teamID'];
                $tournamentBet->status = CybController::ACTIVE_STATUS;
                $tournamentBet->summ = $_POST['tournamentBetForm']['summ'];
                /** @var TournamentsDetails $activeTournament */
                $activeTournament = Tournamentsdetails::model()->findByAttributes(array('tournamentId' => $tournamentBet->tournamentId, 'teamId' => $tournamentBet->teamId));
                if (!$activeTournament) {
                    Yii::app()->user->setFlash('error', Yii::t('games', 'Tournament is not exists'));
                    Yii::app()->end();

                }
                /** @var Tournaments $tournament */
                $tournament = Tournaments::model()->findByPk($tournamentBet->tournamentId);
                if ($tournament->start_date < time() or !empty($tournament->result) or $tournament->status != Games::GAME_STATUS_NOT_STARTED) {
                    Yii::app()->user->setFlash('error', Yii::t('games', 'Your bet is not created, because  tournament is already started!'));
                    Yii::app()->end();
                }

                $tournamentBet->koef = $activeTournament->teamKoef;

                if ($tournamentBet->validate()) {
                    $tournamentBet->save();

                    // activating group bonuses for bet
                    $groupBonusActivator = new GroupBonusActivator();
                    $groupBonusActivator->ActivateGroupBonuses(2, Yii::app()->getUser()->id, $tournamentBet->summ);

                    Helper::updateUserCashOnBetAdded($tournamentBet->summ, Usercash::REAL_MONEY);
                    Helper::updateTournamentKoefs($tournamentBet);

                    Yii::app()->user->setFlash('success', Yii::t('games', 'Your tournament bet was added'));
                }
                Yii::app()->end();
            }

            /** @var CDbCriteria $criteria */
            $criteria = new CDbCriteria();
            $criteria->addColumnCondition(array('activeStatus' => CybController::ACTIVE_STATUS));
            /** @var Tournaments[] $tournaments */
            $tournaments = Tournaments::model()->gameStartTimeSort()->findAll($criteria);

            if (!$id) {
                if (count($tournaments)) {
                    $id = $tournaments[0]->id;
                } else {
                    exit();
                }
            }
            if ($id && Tournaments::model()->findByPk($id)) {
                $model->tournamentID = $id;
            }
            $this->performAjaxValidation($model, 'expressBet-form');


            if (!$model->tournamentID && $tournaments) {
                $model->tournamentID = $tournaments[0]->id;
            }

            $tournamentInfo = Tournamentsdetails::model()->findAllByAttributes(array('tournamentId' => $model->tournamentID));
            $userTournamentBetsInfo = Helper::getTournamentIfBetForThisTournamentTeamsExists($model->tournamentID);
            $userTournamentBetsPercentInfo = Helper::getTournamentPercentOfBetsForTeams($model->tournamentID);
            $choosedTournament = Tournaments::model()->findByPk($id);

            Yii::app()->clientScript->scriptMap = array(
                'jquery.js' => false,
                'jquery.min.js' => false,
            );
            $this->renderPartial('_tournaments', array('choosedTournament' => $choosedTournament, 'tournaments' => $tournaments, 'model' => $model, 'tournamentInfo' => $tournamentInfo,
                'userTournamentBetsInfo' => $userTournamentBetsInfo, 'userTournamentBetsPercentInfo' => $userTournamentBetsPercentInfo), false, true);

        }
        Yii::app()->end();
    }

    public function actionUpdateTable()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $this->layout = '//layouts/empty';
            $games = Games::model()->gameStartTimeSort()->with(array(array('category', 'together' => true), array('team1', 'together' => true), array('team2', 'together' => true)))->findAll();

            $gameCategories = GameCategories::model()->findAll();
            $this->widget('application.widgets.bets.SimpleBet', array('gameCategories' => $gameCategories, 'games' => $games));
        }
        Yii::app()->end();
    }

//    /** @var Games[] $games */
//    protected function handleGamesArr($games)
//    {
//        foreach ($games as $key => $game) {
//            /** @var Result $result */
//            if ($result = $game->result) {
//                $diff = time() - $result->add_result_time;
//                if ($diff > 8 * 60 * 60) {
//                    unset($games[$key]);
//                }
//            } else if ($game->status == Games::GAME_STATUS_CANCELED) {
//                $diff = time() - $game->game_end;
//                if ($diff > 8 * 60 * 60) {
//                    unset($games[$key]);
//                }
//            }
//        }
//        return $games;
//    }


}

