<?php
/**
 * Created by PhpStorm.
 * User: lldl
 * Date: 24.12.14
 * Time: 18:47
 */

class ResultsController extends CybController {
    public function actionIndex()
    {
        /** CDbCriteria*/
        $resultCriteria = new CDbCriteria();
        /** @var TimeRangeForm $timeRangeForm */
        $timeRangeForm = new TimeRangeForm();
        /** @var SelectGameCategoryForm $selectGameCategoryForm */
        $selectGameCategoryForm = new SelectGameCategoryForm();
        $categoryId = 0;
        if(isset($_POST['SelectGameCategoryForm']) && $_POST['SelectGameCategoryForm']['category_id']) {
            $categoryId = $_POST['SelectGameCategoryForm']['category_id'];
            $selectGameCategoryForm->category_id = $categoryId;
        }
        if($categoryId) {
            $resultCriteria->join = ' left join games as g on g.id = t.game_id';
            $resultCriteria->addCondition('g.category_id = '.$categoryId);
        }
        if ($timeRangeFormAr = Yii::app()->request->getParam('TimeRangeForm', false)) {

            if (!empty($timeRangeFormAr['start_time'])) {
                $timeRangeForm->start_time = $timeRangeFormAr['start_time'];
                $start_time = DateTime::createFromFormat('Y-m-d H:i:s', $timeRangeForm['start_time'])->getTimestamp();
                $resultCriteria->addCondition('add_result_time > :start_time');
                $resultCriteria->params['start_time'] = $start_time;
            }
            if ($timeRangeFormAr['end_time']) {
                $timeRangeForm->end_time = $timeRangeFormAr['end_time'];
                $end_time = DateTime::createFromFormat('Y-m-d H:i:s', $timeRangeForm['end_time'])->getTimestamp();
                $resultCriteria->addCondition('add_result_time < :end_time');
                $resultCriteria->params['end_time'] = $end_time;
            }
        }

        //game shold be in results only after 8 hours since it was added
        $now = time();
        $resultCriteria->addBetweenCondition('add_result_time',0,$now - 8 * 60 * 60);

        $resultCriteria->order = 'add_result_time DESC';

        $count = Result::model()->count($resultCriteria);
        $resultPages = new CPagination($count);
        $resultPages->pageVar = 'result_page';

        // results per page
        $resultPages->pageSize = 5;
        $resultPages->applyLimit($resultCriteria);
        $results = Result::model()->findAll($resultCriteria);
//        $results = array_reverse($results);

        //added canceled games to results
        $gamesCriteria = new CDbCriteria();
        if($categoryId) {
            $gamesCriteria->addColumnCondition(array('category_id' => $categoryId));
        }
        $now = time();
        $gamesCriteria->addBetweenCondition('game_end',0,$now - 8 * 60 * 60);
        $gamesCriteria->addColumnCondition(array('status' => Games::GAME_STATUS_CANCELED));
        $gamesCriteria->order = 'game_end DESC';

        $count = Games::model()->count($gamesCriteria);
        $gamesPages = new CPagination($count);
        $gamesPages->pageVar = 'gamesPage';

        $gamesPages->pageSize = 5;
        $gamesPages->applyLimit($gamesCriteria);
        $canceledGames = Games ::model()->findAll($gamesCriteria);
//        $canceledGames = array_reverse($canceledGames);

        /** @var GameCategories[] $gameCategories */
        $gameCategories = GameCategories::model()->findAll();
        if($categoryId) {
            $gamesCriteria->addColumnCondition(array('category_id' => $categoryId));
        }


        $this->render('index', array(
            'results' => $results,
            'canceledGames' => $canceledGames,
            'timeRangeForm' => $timeRangeForm,
            'gameCategories' => $gameCategories,
            'selectGameCategoryForm' => $selectGameCategoryForm,
            'resultPages' => $resultPages,
            'gamesPages' => $gamesPages,
            'currentGameCategoryId' => $categoryId,
        ));
    }
}