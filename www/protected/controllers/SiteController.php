<?php

class SiteController extends CybController
{

    //Todo split this fat controller or change action
    /**
     * Declares class-based actions.
     */

    public $layout = '//layouts/index';

    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    public function actionBet()
    {
        /** @var GamesController $gamesController */
        if (Yii::app()->user->isGuest) {
            $this->redirect(
                array('user/register')
            );
        } else {
            $this->redirect(array('games', 'gameId' => $_REQUEST['gameId'], 'openPopUp' => true));
        }
    }

    /**
     * Privacy and Police page
     */
    public function actionPp()
    {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
        $this->render('pp');
    }

    /**
     * Terms page
     */
    public function actionTerms()
    {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
        $this->render('terms');
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Displays the contact page
     */
    public function actionContact()
    {
        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
                $subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
                $headers = "From: $name <{$model->email}>\r\n" .
                    "Reply-To: {$model->email}\r\n" .
                    "MIME-Version: 1.0\r\n" .
                    "Content-Type: text/plain; charset=UTF-8";

                mail(Yii::app()->params['adminEmail'], $subject, $model->body, $headers);
                Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact', array('model' => $model));
    }

    /**
     * Displays the login page
     */
    public function actionLogin()
    {
        $model = new LoginForm;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login())
                $this->redirect(Yii::app()->user->returnUrl);
        }
        // display the login form
        $this->render('login', array('model' => $model));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    public function actionView($id)
    {
        $content = Content::model()->findContent($id)->attributes;
        $this->setPageTitle(Helper::getPageTitle(Yii::t('site_home', $content['title'])));
        $this->render('view', array('content' => $content));
    }

    public function actiontestEmails()
    {
        $params = array('{username}' => 'vadim', '{activation_url}' => Yii::app()->getBaseUrl(true) . '/user/activation&key=' . md5('admin'));
        var_dump(Helper::sendEmail('new_user_registered', $params, 2));
    }

    public function actionWebmoneySuccess()
    {
        // if prerequest we should say 'YES' :)
        if (isset($_REQUEST['LMI_PREREQUEST']) && $_REQUEST['LMI_PREREQUEST'] == 1) {

            // Yeah, this one *required*!
            die("YES");

        }
        $request = json_encode($_REQUEST);
        Yii::log($request, CLogger::LEVEL_INFO, 'webmoney success');
    }

    public function actionWebmoneyFail()
    {
        // if prerequest we should say 'YES' :)
        if (isset($_REQUEST['LMI_PREREQUEST']) && $_REQUEST['LMI_PREREQUEST'] == 1) {

            // Yeah, this one *required*!
            die("YES");

        }
        $request = json_encode($_REQUEST);
        Yii::log($request, CLogger::LEVEL_INFO, 'webmoney fail');
    }

    public function actionWebmoney()
    {
        $addr = $_SERVER["REMOTE_ADDR"];
        $addressArr = explode('.', $addr);
        unset($addressArr[3]);
        $addrTemp = implode('.', $addressArr);
        if (!in_array($addrTemp, array('212.118.48', '212.158.173', '91.200.28', '91.227.52'))) {
            Yii::app()->end();
        }

        // if prerequest we should say 'YES' :)
        if (isset($_REQUEST['LMI_PREREQUEST']) && $_REQUEST['LMI_PREREQUEST'] == 1) {

            // Yeah, this one *required*!
            die("YES");

        }

        if ($_REQUEST['LMI_MODE'] != 0) {
            Yii::app()->end();
        }


        $request = json_encode($_REQUEST);
        Yii::log($request, CLogger::LEVEL_INFO, 'webmoney result');
        $this->actionIndex();

        $secretKey = Yii::app()->params['webmoneySecretKey'];
        $paymentAmount = $_REQUEST['LMI_PAYMENT_AMOUNT'];
        $shaString = array(
            $_REQUEST['LMI_PAYEE_PURSE'],
            $paymentAmount,
            $_REQUEST['LMI_PAYMENT_NO'],
            $_REQUEST['LMI_MODE'],
            $_REQUEST['LMI_SYS_INVS_NO'],
            $_REQUEST['LMI_SYS_TRANS_NO'],
            $_REQUEST['LMI_SYS_TRANS_DATE'],
//            $_REQUEST['LMI_SECRET_KEY'],
            $secretKey,
            $_REQUEST['LMI_PAYER_PURSE'],
            $_REQUEST['LMI_PAYER_WM']);

        $sha = strtoupper(hash('sha256', implode($shaString)));
        if ($sha !== $_REQUEST['LMI_HASH']) {
            $request = json_encode($_REQUEST);
            Yii::log('failed sha check webmoney result request : ' . $request, CLogger::LEVEL_ERROR, 'webmoney');
            Yii::app()->end();
        }
        Yii::log(' successfully sha check webmoney result request : ' . $request, CLogger::LEVEL_INFO, 'webmoney success payment');


        //handle successfull payment
        $incomeDepositHistory = new IncomeDepositHistory();
        $user_id = $_REQUEST['USER_ID'];
        $incomeDepositHistory->status = IncomeDepositHistory::STATUS_PAID;
        $incomeDepositHistory->userId = $user_id;
        $incomeDepositHistory->system = 'webmoney';
        $incomeDepositHistory->summ = $paymentAmount;
        $incomeDepositHistory->time = time();
        $incomeDepositHistory->operationType = 3;
        if (isset($_REQUEST['accountIdent'])) {
            $incomeDepositHistory->accountIdent = $_POST['accountIdent'];
        }
        if (isset($_REQUEST['secureId'])) {
            $incomeDepositHistory->secureId = $_POST['secureId'];
        }
        $incomeDepositHistory->sign = json_encode($_REQUEST);
        $result = $incomeDepositHistory->save();

        //update balance
        $user = User::model()->findByPk($incomeDepositHistory->userId);
        $userId = $user->id;
        $usercash = Usercash::model()->findByAttributes(array('userId' => $userId));

        /** @var Usercash $usercash */
        $usercash = $usercash->findByAttributes(array('userId' => $userId));
        $usercash->realMoney += $paymentAmount;
        $result = $usercash->save();
        Yii::log("payment (incomeDepositHistory=$incomeDepositHistory->id is successfully paid", Clogger::LEVEL_INFO, 'webmoney');
        Yii::app()->end();
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'


        $this->redirect(Yii::app()->createUrl('games'));

        //main page is not used any more,
        $criteria = new CDbCriteria();
        $criteria->order = 'id DESC';
        $bets = array_slice(Userbets::model()->findAll($criteria), 0, 4);
        $recommendedBets = RecommendedBets::model()->findAll();

        $criteria = new CDbCriteria();
        $criteria->addColumnCondition(array('isWin' => 1));
        $criteria->order = 'id DESC';
        $paidBets = array_slice(Userbets::model()->findAll($criteria), 0, 4);

        $indexSlides = IndexSlides::model()->findAll();
        $indexMainSlides = IndexMainSlides::model()->findAll();

        $reviews = new Reviews();
        $reviews = Reviews::model()->findAll();

        $this->render('index', array(
            'bets' => $bets,
            'paidBets' => $paidBets,
            'recommendedBets' => $recommendedBets,
            'indexSlides' => $indexSlides,
            'indexMainSlides' => $indexMainSlides,
            'reviews' => $reviews,
        ));
    }

    public function actionIncomeDeposite()
    {
        ///cybbet/index.php/usercash/incomedeposite?m_operation_id=30705272&m_operation_ps=20916096&m_operation_date=05.11.2014%2011:25:40&m_operation_pay_date=05.11.2014%2011:27:04&m_shop=27805692&m_orderid=1&m_amount=1.00&m_curr=USD&m_desc=ZGVwb3NpdA%3D%3D&m_status=success
        if (isset($_REQUEST['m_operation_id']) && isset($_REQUEST['m_sign'])) {
            $m_key = Yii::app()->params['payeerSecretKey'];;
            $arHash = array(
                $_REQUEST['m_operation_id'],
                $_REQUEST['m_operation_ps'],
                $_REQUEST['m_operation_date'],
                $_REQUEST['m_operation_pay_date'],
                $_REQUEST['m_shop'],
                $_REQUEST['m_orderid'],
                $_REQUEST['m_amount'],
                $_REQUEST['m_curr'],
                $_REQUEST['m_desc'],
                $_REQUEST['m_status'],
                $m_key);
            $sign_hash = strtoupper(hash('sha256', implode(':', $arHash)));
            /** @var IncomeDepositHistory $incomeDepositHistory */
            $incomeDepositHistory = IncomeDepositHistory::model()->findByAttributes(array(
                'id' => $_REQUEST['m_orderid'],
                'status' => IncomeDepositHistory::STATUS_NOT_PAID,
            ));


            if ($_REQUEST['m_status'] == 'success' && !empty($incomeDepositHistory) && $_REQUEST['m_sign'] == $sign_hash) {
                $user = User::model()->findByPk($incomeDepositHistory->userId);
                $userId = $user->id;
                $usercash = Usercash::model()->findByAttributes(array('userId' => $userId));

                /** @var Usercash $usercash */
                $usercash = $usercash->findByAttributes(array('userId' => $userId));
                $usercash->realMoney += $_REQUEST['m_amount'];
                $result = $usercash->save();
                if ($result) {
                    $incomeDepositHistory->status = IncomeDepositHistory::STATUS_PAID;
                    $incomeDepositHistory->save();
                    Yii::log("payment (incomeDepositHistory=$incomeDepositHistory->id is successfully paid", Clogger::LEVEL_INFO, 'webmoney');
                } else {
                    $incomeDepositHistory->status = IncomeDepositHistory::STATUS_PAID_WITH_ERRORS;
                    $incomeDepositHistory->save();
                    Yii::log("payment (incomeDepositHistory=$incomeDepositHistory->id is successfully paid", Clogger::LEVEL_INFO, 'webmoney');
                }
            } else {
                if (!empty($incomeDepositHistory)) {
                    $incomeDepositHistory->status = IncomeDepositHistory::STATUS_NOT_PAID;
                    $incomeDepositHistory->save();
                }
            }
            $this->actionMakeBet();
//            Yii::app()->end();
        }
        $this->actionIndex();
        // $this->redirect(array('site/index'), true, 200);
    }

    /** this action receive json decoded games from parser ( form egamingbets.com )   */
    public function actionCreateGames()
    {
        $inputJSON = file_get_contents('php://input');
        $input = json_decode($inputJSON, TRUE);
        $msg = "new request : \n" . json_encode($input);
        Yii::log($msg);
        $games = $input['bets'];
        /** @var EGBParser $egbParser */
        $egbParser = new EGBParser();
        $res = $egbParser->createGames($games);
        if ($res)
            Yii::log('parsed new games successfully');
        else
            Yii::log('parsed new games unsuccessfully');
    }

    /** this action receive json decoded tournaments from parser ( from  egamingbets.com )  */
    public function actionCreateTournaments()
    {
        $inputJSON = file_get_contents('php://input');
        $input = json_decode($inputJSON, TRUE);
        $msg = "new request ( tournaments )  : \n" . json_encode($input);
        Yii::log($msg, CLogger::LEVEL_INFO, 'tournaments');
        $tournaments = $input;
        /** @var TournamentsParser $tournamentParser */
        $tournamentParser = new TournamentsParser();
        $res = $tournamentParser->createTournaments($tournaments);
        if ($res)
            Yii::log('parsed new tournaments successfully', CLogger::LEVEL_INFO, 'tournaments');
        else
            Yii::log('parsed new tournaments unsuccessfully', CLogger::LEVEL_ERROR, 'tournaments');
    }

    /** this action receive json decoded streams from egb site */
    public function actionCreateStreams()
    {
        $inputJSON = file_get_contents('php://input');
        $input = json_decode($inputJSON, TRUE);
        $msg = "new request ( streams )  : \n" . json_encode($input);
        Yii::log($msg, CLogger::LEVEL_INFO, 'streams');
        $streams = $input;
        /** @var StreamsParser $streamsParser */
        $streamsParser = new StreamsParser();
        $res = $streamsParser->createStreams($streams);
        if ($res)
            Yii::log('parsed new streams successfully', CLogger::LEVEL_INFO, 'streams');
        else
            Yii::log('parsed new streams unsuccessfully', CLogger::LEVEL_ERROR, 'streams');
    }
}