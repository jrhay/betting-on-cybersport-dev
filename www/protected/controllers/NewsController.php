<?php
/**
 * Created by PhpStorm.
 * User: lldl
 * Date: 10/20/14
 * Time: 2:48 PM
 */

class NewsController extends CybController {

    public function actionViewSingleNews()
    {
        if (isset($_REQUEST['news_id'])) {
            $news = new News();
            $news = $news->findByPk($_REQUEST['news_id']);
            return $this->render('singleNews', array(
                'news' => $news
            ));
        }
    }

    public function actionIndex()
    {
        $model = new News;

        // uncomment the following code to enable ajax-based validation
        if (isset($_REQUEST['deleteNewsId'])) {
            $model->deleteByPk($_REQUEST['deleteNewsId']);
        }
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'news-newsForm2-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }


        if (isset($_POST['News'])) {
            $model->attributes = $_POST['News'];
            /** @var CUploadedFile img */
            $model->img = CUploadedFile::getInstance($model, 'img');
            $model->img_eng = CUploadedFile::getInstance($model, 'img_eng');
            $model->date = time();
            $model->category_id = intval($model->category_id);
            if ($model->save()) {
                $filename = $model->img->getName();
                $path = Yii::app()->basePath . '/../images/news/' . $filename;
                $filename_eng = $model->img_eng->getName();
                $path_eng = Yii::app()->basePath . '/../images/news/' . $filename_eng;
                if ($model->img->saveAs($path, true) && $model->img_eng->saveAs($path_eng, true)) {
                    Yii::app()->user->setFlash('success', Yii::t('site', 'Новость успешно добавлена'));
                } else {
                    Yii::app()->user->setFlash('error', Yii::t('site', 'Ошбика, новость не добавлена'));
                }
            }
        }

        if (isset($_REQUEST['currentCategory'])) {
            $currentCategory = $_REQUEST['currentCategory'];
        } else {
            $currentCategory = 1;
        }

        $criteria = new CDbCriteria();
        $criteria->addColumnCondition(array('category_id' => $currentCategory));
        $criteria->order = 'date DESC';
        $count = News::model()->count($criteria);
        $pages = new CPagination($count);

        // results per page
        $pages->pageSize = 5;
        $pages->applyLimit($criteria);

        $news = News::model()->findAll($criteria);
        $newsCategories = new NewsCategories();
        $newsCategories = $newsCategories->findAll();

        $this->render('index',
            array(
                'news' => $news,
                'newsCategories' => $newsCategories,
                'model' => $model,
                'pages' => $pages,
                'currentCategory' => $currentCategory,
            )
        );
    }
} 