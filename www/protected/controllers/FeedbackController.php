<?php
/**
 * Created by PhpStorm.
 * User: lldl
 * Date: 24.12.14
 * Time: 21:03
 */

class FeedbackController extends CybController {
    public function actionViewSingleFeedback()
    {
        if (isset($_REQUEST['feedback_id'])) {
            $feedback = new Feedback();
            $feedback = $feedback->findByPk($_REQUEST['feedback_id']);
            return $this->render('singleFeedback', array(
                'feedback' => $feedback
            ));
        }
    }

    public function actionIndex()
    {
        $model = new Feedback;

        // uncomment the following code to enable ajax-based validation
        if (isset($_REQUEST['deleteFeedbackId'])) {
            $model->deleteByPk($_REQUEST['deleteFeedbackId']);
        }
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'feedback-feedbackForm2-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }


        if (isset($_POST['Feedback'])) {
            $model->attributes = $_POST['Feedback'];
            /** @var CUploadedFile img */
            $model->img = CUploadedFile::getInstance($model, 'img');
            $model->img_eng = CUploadedFile::getInstance($model, 'img_eng');
            $model->date = time();
            if ($model->save()) {
                $filename = $model->img->getName();
                $path = Yii::app()->basePath . '/../images/feedback/' . $filename;
                $filename_eng = $model->img_eng->getName();
                $path_eng = Yii::app()->basePath . '/../images/feedback/' . $filename_eng;
                if ($model->img->saveAs($path, true) && $model->img_eng->saveAs($path_eng, true)) {
                    Yii::app()->user->setFlash('success', Yii::t('site', 'Отзыв успешно добавлен'));
                } else {
                    Yii::app()->user->setFlash('error', Yii::t('site', 'Ошбика, отзыв не добавлен'));
                }
            }
        }

//        if (isset($_REQUEST['currentCategory'])) {
//            $currentCategory = $_REQUEST['currentCategory'];
//        } else {
//            $currentCategory = 1;
//        }

        $criteria = new CDbCriteria();
//        $criteria->addColumnCondition(array('category_id' => $currentCategory));
        $criteria->order = 'date DESC';
        $count = Feedback::model()->count($criteria);
        $pages = new CPagination($count);

        // results per page
        $pages->pageSize = 5;
        $pages->applyLimit($criteria);

        $feedback = Feedback::model()->findAll($criteria);
//        $feedbackCategories = new FeedbackCategories();
//        $feedbackCategories = $feedbackCategories->findAll();

        $this->render('index',
            array(
                'feedback' => $feedback,
//                'feedbackCategories' => $feedbackCategories,
                'model' => $model,
                'pages' => $pages,
//                'currentCategory' => $currentCategory,
            )
        );
    }

}