<?php

class UsercashController extends CybController
{

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
//			'postOnly + delete', // we only allow deletion via POST request
        );
    }
    public function accessRules()
    {
        return array(
            array('allow',
                'actions'=>array('webmoney'),
                'users'=>array('*'),
            ),
            array('allow',
                'actions'=>array('fillCyberMoney'),
                'users'=>array('@'),
            ),
            array('deny')
        );
    }

    /** Handle requests from webmoney */
    public function actionWebmoney(){
        $request = json_decode($_REQUEST);
        Yii::log($request, CLogger::LEVEL_INFO, 'webmoney');
    }
    public
    function actionFillCyberMoney()
    {
        $this->layout = '//layouts/rightcol';
        $userCash = Usercash::model()->findByAttributes(array('userId' => Yii::app()->user->id));
        $model = new addCyberMoneyForm();

        $this->performAjaxValidation($model, 'cybermoney-form');

        if (isset($_POST['addCyberMoneyForm'])) {
            //to add usermoney change  if(){} with $userCash->cyberMoney=$_POST['addCyberMoneyForm']['summ'];
            if ($_POST['addCyberMoneyForm']['summ']) {
                $userCash->cyberMoney = 100;
            }
            $userCash->save();

            Helper::addUserCashHistory(Usercashhistory::CASH_HISTORY_INCOME, $_POST['addCyberMoneyForm']['summ'], Usercash::PLAY_MONEY);

            $flashMessage = Yii::t('site', 'Cyber money succesfully restored .');
            Yii::app()->user->setFlash('success', $flashMessage);
        }

        $this->render('fillcybermoney', array('model' => $model));
    }

    /**
     * Performs the AJAX validation.
     * @param Games $model the model to be validated
     */
    protected
    function performAjaxValidation($model, $form = 'games-form')
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === $form) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Games the loaded model
     * @throws CHttpException
     */
    public
    function loadModel($id)
    {
        $model = Usercash::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
}
