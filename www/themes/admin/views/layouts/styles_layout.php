<?php
Yii::app()->clientscript
    // use it when you need it!
    /*
    ->registerCssFile( Yii::app()->theme->baseUrl . '/css/bootstrap.css' )
    ->registerCssFile( Yii::app()->theme->baseUrl . '/css/bootstrap-responsive.css' )
    ->registerCoreScript( 'jquery' )
    ->registerScriptFile( Yii::app()->theme->baseUrl . '/js/bootstrap-transition.js', CClientScript::POS_END )
    ->registerScriptFile( Yii::app()->theme->baseUrl . '/js/bootstrap-alert.js', CClientScript::POS_END )
    ->registerScriptFile( Yii::app()->theme->baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END )
    ->registerScriptFile( Yii::app()->theme->baseUrl . '/js/bootstrap-dropdown.js', CClientScript::POS_END )
    ->registerScriptFile( Yii::app()->theme->baseUrl . '/js/bootstrap-scrollspy.js', CClientScript::POS_END )
    ->registerScriptFile( Yii::app()->theme->baseUrl . '/js/bootstrap-tab.js', CClientScript::POS_END )
    ->registerScriptFile( Yii::app()->theme->baseUrl . '/js/bootstrap-tooltip.js', CClientScript::POS_END )
    ->registerScriptFile( Yii::app()->theme->baseUrl . '/js/bootstrap-popover.js', CClientScript::POS_END )
    ->registerScriptFile( Yii::app()->theme->baseUrl . '/js/bootstrap-button.js', CClientScript::POS_END )*/
    ->registerScriptFile( Yii::app()->theme->baseUrl . '/js/bootstrap-collapse.js', CClientScript::POS_END )
/*->registerScriptFile( Yii::app()->theme->baseUrl . '/js/bootstrap-carousel.js', CClientScript::POS_END )
->registerScriptFile( Yii::app()->theme->baseUrl . '/js/bootstrap-typeahead.js', CClientScript::POS_END )
*/
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <meta name="language" content="en" />
    <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- Le styles -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/style.css" />
    <!-- Le fav and touch icons -->
    <?php  include_once(__DIR__.'/../../../includes/googleanalytics.html');?>
</head>

<body>
<!--<div class="navbar navbar-inverse navbar-fixed-top">-->
<!--    <div class="navbar-inner">-->
<!--        <div class="container-fluid">-->
<!--            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">-->
<!--                <span class="icon-bar"></span>-->
<!--                <span class="icon-bar"></span>-->
<!--                <span class="icon-bar"></span>-->
<!--            </a>-->
<!--            <a class="brand" href="--><?php //echo Yii::app()->baseUrl; ?><!--"><img src="--><?php //echo Yii::app()->theme->baseUrl; ?><!--/img/logo.png" /></a>-->
<!--            <div class="nav-collapse">-->
<!--                --><?php //$this->widget('zii.widgets.CMenu',array(
//                    'htmlOptions' => array( 'class' => 'nav topnav' ),
//                    'activeCssClass'	=> 'active',
//                    'items'=>array(
////							array('label'=>'Home', 'url'=>array('/index')),
////							array('label'=>'About', 'url'=>array('/site/page', 'view'=>'about')),
////							array('label'=>'Contact', 'url'=>array('/site/contact')),
////							array('label'=>'RestAPI test page', 'url'=>array('/api/testApi')),
//                        array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
////                            array('label'=>'Admin Panel', 'url'=>array('/admin'), 'visible'=>(isset(Yii::app()->user->isAdmin) && Yii::app()->user->isAdmin)),
//                        array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/admin/user/logout'), 'visible'=>!Yii::app()->user->isGuest)
//                    ),
//                )); ?>
<!---->
<!--            </div><!--/.nav-collapse -->-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->

<div class="cont">
    <div class="container-fluid">
        <?php if(isset($this->breadcrumbs)):?>
            <?php $this->widget('zii.widgets.CBreadcrumbs', array(
                'links'=>$this->breadcrumbs,
                'homeLink'=>false,
                'tagName'=>'ul',
                'separator'=>'',
                'activeLinkTemplate'=>'<li><a href="{url}">{label}</a> <span class="divider">/</span></li>',
                'inactiveLinkTemplate'=>'<li><span>{label}</span></li>',
                'htmlOptions'=>array ('class'=>'breadcrumb')
            )); ?>
            <!-- breadcrumbs -->
        <?php endif?>
        <div class="data-content admin">
            <?php
            foreach (Yii::app()->user->getFlashes() as $key => $message) {
                echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
            }
            ?>
        </div>
        <?php echo $content ?>


    </div><!--/.fluid-container-->
</div>


<!--<div class="footer">-->
<!--    <div class="container">-->
<!--        <div class="row">-->
<!--            <div id="footer-terms" class="col-md-6">-->
<!--                © 2014 JBWeld. <a href="http://www.jbweld.com/" target="_blank">JBWeld Site</a>.-->
<!--            </div> <!-- /.span6 -->-->
<!--        </div> <!-- /row -->-->
<!--    </div> <!-- /container -->-->
<!--</div>-->
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter27897090 = new Ya.Metrika({id:27897090});
            } catch(e) { }
        });
        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n[removed].insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";
        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/27897090" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</body>
</html>
