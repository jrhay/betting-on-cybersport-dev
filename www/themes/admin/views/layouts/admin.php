<?php $this->beginContent('//layouts/main'); ?>
    <div class="row-fluid">
    <div class="span3">
    <div class="sumbit" style="font-size: 24px;">
        <a style="" href="<?php echo  Yii::app()->createUrl('admin/index/index') ?>">
            << <?php echo  Yii::t('general', 'Back') ?></a>
    </div>
    <br>
    <?php
    $this->beginWidget('application.widgets.CPortlet', array(
        'title' => 'Manage Users/Games',
    ));
    $items = array();
    if (Usertype::needAccess(Usertype::USERTYPE_ADMINISTRATOR))
        $items[] = array('label' => 'Users Management', 'url' => array('/admin/user/index'), 'active' => ((Yii::app()->controller->module->id == 'admin') && (Yii::app()->controller->getId() == 'user')));
    if (Usertype::needAccess(Usertype::USERTYPE_ADMINISTRATOR))
        $items[] = array('label' => Yii::t('admin', 'Manage Game Categories'), 'url' => array('/admin/gameCategories/index'), 'active' => ((Yii::app()->controller->module->id == 'admin') && (Yii::app()->controller->getId() == 'gameCategories')));
    if (Usertype::needAccess(Usertype::USERTYPE_ADMIN_MODERATOR))
        $items[] = array('label' => Yii::t('admin', 'Manage Teams'), 'url' => array('/admin/teams/index'), 'active' => ((Yii::app()->controller->module->id == 'admin') && (Yii::app()->controller->getId() == 'teams')));
    if (Usertype::needAccess(Usertype::USERTYPE_ADMIN_MODERATOR))
        $items[] = array('label' => Yii::t('admin', 'Manage Team Players'), 'url' => array('/admin/teamPlayers/index'), 'active' => ((Yii::app()->controller->module->id == 'admin') && (Yii::app()->controller->getId() == 'teamPlayers')));
    if (Usertype::needAccess(Usertype::USERTYPE_ADMIN_MODERATOR))
        $items[] = array('label' => Yii::t('admin', 'Manage Countries'), 'url' => array('/admin/countries/admin'), 'active' => ((Yii::app()->controller->module->id == 'admin') && (Yii::app()->controller->getId() == 'countries')));
    if (Usertype::needAccess(Usertype::USERTYPE_ADMIN_MODERATOR))
        $items[] = array('label' => Yii::t('admin', 'Manage Games'), 'url' => array('/admin/games/index'), 'active' => ((Yii::app()->controller->module->id == 'admin') && (Yii::app()->controller->getId() == 'games')));
    if (Usertype::needAccess(Usertype::USERTYPE_ADMINISTRATOR))
        $items[] = array('label' => Yii::t('admin', 'Manage Express Bets'), 'url' => array('/admin/expressbets/index'), 'active' => ((Yii::app()->controller->module->id == 'admin') && (Yii::app()->controller->getId() == 'expressbets')));
    if (Usertype::needAccess(Usertype::USERTYPE_ADMINISTRATOR))
        $items[] = array('label' => Yii::t('admin', 'Manage Users Cash'), 'url' => array('/admin/usercash/index'), 'active' => ((Yii::app()->controller->module->id == 'admin') && (Yii::app()->controller->getId() == 'usercash')));
    if (Usertype::needAccess(Usertype::USERTYPE_ADMINISTRATOR))
        $items[] = array('label' => Yii::t('admin', 'Manage Users Bets'), 'url' => array('/admin/userbets/index'), 'active' => ((Yii::app()->controller->module->id == 'admin') && (Yii::app()->controller->getId() == 'userbets')));
    if (Usertype::needAccess(Usertype::USERTYPE_ADMINISTRATOR))
        $items[] = array('label' => Yii::t('admin', 'Manage Tournaments Bets'), 'url' => array('/admin/tournamentsbets/admin'), 'active' => ((Yii::app()->controller->module->id == 'admin') && (Yii::app()->controller->getId() == 'tournamentsbets')));
    if (Usertype::needAccess(Usertype::USERTYPE_ADMIN_MODERATOR))
        $items[] = array('label' => Yii::t('admin', 'Manage Events'), 'url' => array('/admin/events/admin'), 'active' => ((Yii::app()->controller->module->id == 'admin') && (Yii::app()->controller->getId() == 'events')));
    if (count($items)) {
        $this->widget('zii.widgets.CMenu', array(
            'items' => $items,
            'htmlOptions' => array('class' => 'sidebar'),
        ));
    }
    $this->endWidget();
    ?>

    <?php
    $items = array();
    $this->beginWidget('application.widgets.CPortlet', array(
        'title' => 'Content Management',
    ));
    if (Usertype::needAccess(Usertype::USERTYPE_ADMINISTRATOR))
        $items[] = array('label' => Yii::t('admin', 'Manage Content'), 'url' => array('/admin/content/index'), 'active' => ((Yii::app()->controller->module->id == 'admin') && (Yii::app()->controller->getId() == 'content')));
    if (Usertype::needAccess(Usertype::USERTYPE_ADMINISTRATOR))
        $items[] = array('label' => Yii::t('admin', 'Manage i18n Content'), 'url' => array('/admin/translate/index'), 'active' => ((Yii::app()->controller->module->id == 'admin') && (Yii::app()->controller->getId() == 'translate')));
    if (Usertype::needAccess(Usertype::USERTYPE_ADMIN_MODERATOR))
        $items[] = array('label' => Yii::t('admin', 'Manage Banners'), 'url' => array('/admin/banners/index'), 'active' => ((Yii::app()->controller->module->id == 'admin') && (Yii::app()->controller->getId() == 'banners')));
    if (Usertype::needAccess(Usertype::USERTYPE_ADMIN_MODERATOR))
        $items[] = array('label' => Yii::t('admin', 'Manage Bet Videos'), 'url' => array('/admin/betsvideos/index'), 'active' => ((Yii::app()->controller->module->id == 'admin') && (Yii::app()->controller->getId() == 'betsvideos')));
    if (Usertype::needAccess(Usertype::USERTYPE_ADMINISTRATOR))
        $items[] = array('label' => Yii::t('admin', 'Manage Emails'), 'url' => array('/admin/email/index'), 'active' => ((Yii::app()->controller->module->id == 'admin') && (Yii::app()->controller->getId() == 'email')));
    if (Usertype::needAccess(Usertype::USERTYPE_ADMIN_MODERATOR))
        $items[] = array('label' => Yii::t('admin', 'Manage Recommended Bets'), 'url' => array('/admin/recommendedBets/admin'), 'active' => ((Yii::app()->controller->module->id == 'admin') && (Yii::app()->controller->getId() == 'recommendedBets')));
    if (Usertype::needAccess(Usertype::USERTYPE_ADMIN_MODERATOR))
        $items[] = array('label' => Yii::t('admin', 'Manage Index Slides\ Index Main Slides'), 'url' => array('/admin/indexSlides/admin'), 'active' => ((Yii::app()->controller->module->id == 'admin') && (Yii::app()->controller->getId() == 'indexSlides')));
    if (Usertype::needAccess(Usertype::USERTYPE_ADMINISTRATOR))
        $items[] = array('label' => Yii::t('admin', 'Manage Reviews on main page'), 'url' => array('/admin/reviews/admin'), 'active' => ((Yii::app()->controller->module->id == 'admin') && (Yii::app()->controller->getId() == 'reviews')));
    if (Usertype::needAccess(Usertype::USERTYPE_ADMINISTRATOR))
        $items[] = array('label' => Yii::t('admin', 'Manage Default Games Video Channel'), 'url' => array('/admin/defaultGamesVideoFrame/admin'), 'active' => ((Yii::app()->controller->module->id == 'admin') && (Yii::app()->controller->getId() == 'defaultGamesVideoChannel')));
    if (count($items)) {
        $this->widget('zii.widgets.CMenu', array(
            'items' => $items,
            'htmlOptions' => array('class' => 'sidebar'),
        ));
    }
    $this->endWidget();
    ?>

    <?php
    $items = array();
    ?>
    <?php
    $items = array();
    $this->beginWidget('application.widgets.CPortlet', array(
        'title' => 'Personal messages',
    ));

    if (Usertype::needAccess(Usertype::USERTYPE_ADMINISTRATOR))
        $items[] = array('label' => Yii::t('admin', 'Send personal message'), 'url' => array('/admin/messages/send'), 'active' => ((Yii::app()->controller->module->id == 'admin') && (Yii::app()->controller->getId() == 'messages')));

    if (count($items)) {
        $this->widget('zii.widgets.CMenu', array(
            'items' => $items,
            'htmlOptions' => array('class' => 'sidebar'),
        ));
    }
    $this->endWidget();
    ?>
    <?php
    $items = array();
    $this->beginWidget('application.widgets.CPortlet', array(
        'title' => 'Bonuses',
    ));
    if (Usertype::needAccess(Usertype::USERTYPE_ADMINISTRATOR))
        $items[] = array('label' => Yii::t('admin', 'Add personal bonus'), 'url' => array('/admin/bonuses/addpersonalbonus'), 'active' => ((Yii::app()->controller->module->id == 'admin') && (Yii::app()->controller->getId() == 'bonuses')));
    if (Usertype::needAccess(Usertype::USERTYPE_ADMINISTRATOR))
        $items[] = array('label' => Yii::t('admin', 'Create promo code'), 'url' => array('/admin/bonuses/createpromocode'), 'active' => ((Yii::app()->controller->module->id == 'admin') && (Yii::app()->controller->getId() == 'bonuses')));
    if (Usertype::needAccess(Usertype::USERTYPE_ADMINISTRATOR))
        $items[] = array('label' => Yii::t('admin', 'Add group bonus'), 'url' => array('/admin/bonuses/addgroupbonus'), 'active' => ((Yii::app()->controller->module->id == 'admin') && (Yii::app()->controller->getId() == 'bonuses')));
    if (Usertype::needAccess(Usertype::USERTYPE_ADMINISTRATOR))
        $items[] = array('label' => Yii::t('admin', 'Reset Jackpot'), 'url' => array('/admin/jackpot/reset'), 'active' => ((Yii::app()->controller->module->id == 'admin') && (Yii::app()->controller->getId() == 'jackpot')));
    if (Usertype::needAccess(Usertype::USERTYPE_ADMINISTRATOR))
        $items[] = array('label' => Yii::t('admin', 'Manage jackpot'), 'url' => array('/admin/jackpot/manage'), 'active' => ((Yii::app()->controller->module->id == 'admin') && (Yii::app()->controller->getId() == 'jackpot')));
    if (Usertype::needAccess(Usertype::USERTYPE_ADMINISTRATOR))
        $items[] = array('label' => Yii::t('admin', 'Pay jackpot to user'), 'url' => array('/admin/jackpot/pay'), 'active' => ((Yii::app()->controller->module->id == 'admin') && (Yii::app()->controller->getId() == 'jackpot')));

    if (count($items)) {
        $this->widget('zii.widgets.CMenu', array(
            'items' => $items,
            'htmlOptions' => array('class' => 'sidebar'),
        ));
    }
    $this->endWidget();
    ?>
    <?php
    $items = array();
    $this->beginWidget('application.widgets.CPortlet', array(
        'title' => 'Support',
    ));
    if (Usertype::needAccess(Usertype::USERTYPE_ADMIN_MODERATOR))
        $items[] = array('label' => Yii::t('admin', 'Tickets'), 'url' => array('/admin/support/index'), 'active' => ((Yii::app()->controller->module->id == 'admin') && (Yii::app()->controller->getId() == 'support')));
    if (Usertype::needAccess(Usertype::USERTYPE_ADMIN_MODERATOR))
        $items[] = array('label' => Yii::t('admin', 'Manage Ticket Categories'), 'url' => array('/admin/ticketCategories/index'), 'active' => ((Yii::app()->controller->module->id == 'admin') && (Yii::app()->controller->getId() == 'ticketCategories')));
    if (Usertype::needAccess(Usertype::USERTYPE_ADMIN_MODERATOR))
        $items[] = array('label' => Yii::t('admin', 'Statistics'), 'url' => array('/admin/support/statistics'), 'active' => ((Yii::app()->controller->module->id == 'admin') && (Yii::app()->controller->getId() == 'support')));

    if (count($items))
        $this->widget('zii.widgets.CMenu', array(
            'items' => $items,
            'htmlOptions' => array('class' => 'sidebar'),
        ));
    $this->endWidget();
    ?>
    <?php
    $items = array();
    $this->beginWidget('application.widgets.CPortlet', array(
        'title' => 'News',
    ));
    if (Usertype::needAccess(Usertype::USERTYPE_ADMINISTRATOR))
        $items[] = array('label' => Yii::t('admin', 'Manage News Categories'), 'url' => array('/admin/newsCategories/index'), 'active' => ((Yii::app()->controller->module->id == 'admin') && (Yii::app()->controller->getId() == 'newsCategories')));

    if (count($items))
        $this->widget('zii.widgets.CMenu', array(
            'items' => $items,
            'htmlOptions' => array('class' => 'sidebar'),
        ));
    $this->endWidget();
    ?>
    <?php
    $items = array();
    $this->beginWidget('application.widgets.CPortlet', array(
        'title' => 'Statistics',
    ));
    if (Usertype::needAccess(Usertype::USERTYPE_ADMIN_MODERATOR))
        $items[] = array('label' => Yii::t('admin', 'Site'), 'url' => array('/admin/statistics/site'), 'active' => ((Yii::app()->controller->module->id == 'admin') && (Yii::app()->controller->getId() == 'statistics')));
    if (Usertype::needAccess(Usertype::USERTYPE_ADMIN_MODERATOR))
        $items[] = array('label' => Yii::t('admin', 'Games'), 'url' => array('/admin/statistics/games'), 'active' => ((Yii::app()->controller->module->id == 'admin') && (Yii::app()->controller->getId() == 'statistics')));
    if (Usertype::needAccess(Usertype::USERTYPE_ADMINISTRATOR))
        $items[] = array('label' => Yii::t('admin', 'Users'), 'url' => array('/admin/statistics/users'), 'active' => ((Yii::app()->controller->module->id == 'admin') && (Yii::app()->controller->getId() == 'statistics')));
    if (Usertype::needAccess(Usertype::USERTYPE_ADMINISTRATOR))
        $items[] = array('label' => Yii::t('admin', 'Moderators'), 'url' => array('/admin/statistics/moderators'), 'active' => ((Yii::app()->controller->module->id == 'admin') && (Yii::app()->controller->getId() == 'statistics')));

    if (count($items))
        $this->widget('zii.widgets.CMenu', array(
            'items' => $items,
            'htmlOptions' => array('class' => 'sidebar'),
        ));
    $this->endWidget();
    ?>
    <?php
    $items = array();
    $this->beginWidget('application.widgets.CPortlet', array(
        'title' => 'Deposite',
    ));
    if (Usertype::needAccess(Usertype::USERTYPE_ADMINISTRATOR))
        $items[] = array('label' => Yii::t('admin', 'Real/cyber money statistics'), 'url' => array('/admin/statistics/deposit'), 'active' => ((Yii::app()->controller->module->id == 'admin') && (Yii::app()->controller->getId() == 'statistics')));
    if (Usertype::needAccess(Usertype::USERTYPE_ADMINISTRATOR))
        $items[] = array('label' => Yii::t('admin', 'deposit statistics'), 'url' => array('/admin/statistics/incomeDeposit'), 'active' => ((Yii::app()->controller->module->id == 'admin') && (Yii::app()->controller->getId() == 'statistics')));
    if (count($items))
        $this->widget('zii.widgets.CMenu', array(
            'items' => $items,
            'htmlOptions' => array('class' => 'sidebar'),
        ));
    $this->endWidget();

    $items = array();
    $this->beginWidget('application.widgets.CPortlet', array(
        'title' => 'Tournaments',
    ));
    if (Usertype::needAccess(Usertype::USERTYPE_ADMIN_MODERATOR))
        $items[] = array('label' => Yii::t('admin', 'Manage Tournaments'), 'url' => array('/admin/tournaments/index'), 'active' => ((Yii::app()->controller->module->id == 'admin') && (Yii::app()->controller->getId() == 'tournaments')));
    if (Usertype::needAccess(Usertype::USERTYPE_ADMIN_MODERATOR))
        $items[] = array('label' => Yii::t('admin', 'Manage results'), 'url' => array('/admin/tournamentResult/admin'), 'active' => ((Yii::app()->controller->module->id == 'admin') && (Yii::app()->controller->getId() == 'statistics')));
    if (Usertype::needAccess(Usertype::USERTYPE_ADMIN_MODERATOR))
        $items[] = array('label' => Yii::t('admin', 'Add result'), 'url' => array('/admin/tournamentResult/create'), 'active' => ((Yii::app()->controller->module->id == 'admin') && (Yii::app()->controller->getId() == 'statistics')));
    //            $items[] = array('label' => Yii::t('admin', 'deposit statistics'), 'url' => array('/admin/statistics/incomeDeposit'), 'active' => ((Yii::app()->controller->module->id == 'admin') && (Yii::app()->controller->getId() == 'statistics')));
    if (count($items))
        $this->widget('zii.widgets.CMenu', array(
            'items' => $items,
            'htmlOptions' => array('class' => 'sidebar'),
        ));
    $this->endWidget();

    $items = array();
    $this->beginWidget('application.widgets.CPortlet', array(
        'title' => 'Commerce',
    ));
    if (Usertype::needAccess(Usertype::USERTYPE_ADMINISTRATOR))
        $items[] = array('label' => Yii::t('admin', 'Manage Payouts'), 'url' => array('/admin/payout/admin'), 'active' => ((Yii::app()->controller->module->id == 'admin') && (Yii::app()->controller->getId() == 'payout')));

    if (count($items))
        $this->widget('zii.widgets.CMenu', array(
            'items' => $items,
            'htmlOptions' => array('class' => 'sidebar'),
        ));
    $this->endWidget();

    $items = array();
    $this->beginWidget('application.widgets.CPortlet', array(
        'title' => 'Egb Games',
    ));
    if (Usertype::needAccess(Usertype::USERTYPE_ADMINISTRATOR))
        $items[] = array('label' => Yii::t('admin', 'Manage EGb Games'), 'url' => array('/admin/egbGames/admin'), 'active' => ((Yii::app()->controller->module->id == 'admin') && (Yii::app()->controller->getId() == 'payout')));

    if (count($items))
        $this->widget('zii.widgets.CMenu', array(
            'items' => $items,
            'htmlOptions' => array('class' => 'sidebar'),
        ));
    $this->endWidget();

    ?>
    </div>
    <!-- sidebar span3 -->


    <div class="span9">
        <div class="main">
            <?php echo $content; ?>
        </div>
        <!-- content -->
    </div>
    </div>
<?php $this->endContent(); ?>

<?php
Yii::app()->clientScript->registerScript(uniqid(), "

$('.portlet-decoration').click(function(){
    $('#' + jQuery(this).parent().attr('id') + ' .sidebar').slideToggle();
});
$('ul.sidebar li.active').parent().show(0);

", CClientScript::POS_END);
?>