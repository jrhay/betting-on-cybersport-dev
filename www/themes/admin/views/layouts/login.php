<?php $this->beginContent('//layouts/main'); ?>
<div class="row-fluid">
	<div class="span12">
		<div class="main" style="margin: 0 auto; width: 450px">
			<?php echo $content; ?>
		</div>	
	</div><!-- content -->
</div>
<?php $this->endContent(); ?>
