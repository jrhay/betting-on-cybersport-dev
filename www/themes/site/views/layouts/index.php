<?php
/** @var ResultsController $this */
$assetsManager = Yii::app()->clientScript;
$assetsManager->registerCoreScript('jquery');
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <title><?php echo  Yii::t('title','Букмекерская контора CybBet - самый большой выбор ставок на киберспорт')?></title>
    <link rel="shortcut icon" href="<?php echo  Yii::app()->baseUrl?>/../images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?php echo  Yii::app()->baseUrl?>/../images/favicon.ico" type="image/x-icon">
    <meta name="discription" content="Лучшие ставки для турниров киберспорта. Вы можете делать ставки на дота 2, league of Legends, CS:GO, Smite, Танки, Hearthstone, Starcraft и другие esports">
    <meta charset="utf-8"/>

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl ?>/css/jquery.formstyler.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl ?>/css/style.css"/>
    <?php
    if($this->getId() == 'results' && $this->getAction()->getId() == 'index') {
        echo ' <link rel="canonical" href="http://cybbet.com/results" />';
    }

    ?>
    <?php  include_once(__DIR__.'/../../../includes/googleanalytics.html');?>
</head>
<body >

<div class="wrapper">

    <div class="header">
        <div class="wrap g-clearfix">
            <div class="flash-messages-container">
                <?php
                foreach (Yii::app()->user->getFlashes() as $key => $message) {
                    echo '<div class="homepage-flash flash-' . $key . '">' . $message . "</div>\n";
                }
                ?>
            </div>
            <br>
            <?php $this->widget('application.widgets.account.account'); ?>
            <a class="logo" href="/" title="">
                <img src="<?php echo Yii::app()->theme->baseUrl ?>/img/logo.png" alt=""/>
            </a>
        </div>
    </div>
    <div class="nav style3">
        <div class="wrap">

            <?php
            $this->widget('zii.widgets.CMenu', array(
                'items' => array(
//                    array(
//                        'label' => '<span>' . Yii::t('site', 'Home') . '</span>',
//                        'url' => array('/../'),
//                        'itemOptions' => array('class' => 'first'),
//                    ),
//                    array('label'=>'<span>'.Yii::t('site','How to make a bet?').'</span>', 'url'=>array('/site/makebet', 'id'=>'1')),
                    array('label' => '<span>' . Yii::t('site', 'Home') . '</span>', 'url' => array('/games')),
                    array('label' => '<span>' . Yii::t('site', 'How to make a bet?') . '</span>', 'url' => array('/makebet')),
                    array('label' => '<span>' . Yii::t('site', 'Результаты') . '</span>', 'url' => array('/results')),
                    array('label' => '<span>' . Yii::t('site', 'Новости') . '</span>', 'url' => array('/news')),
//                    array('label' => '<span>' . Yii::t('site', 'Make bet') . '</span>', 'url' => array('/games')),
                    array('label' => '<span>' . Yii::t('site', 'FAQ') . '</span>', 'url' => array('/faq')),
//                    array('label' => '<span>' . Yii::t('site', 'Отзывы и выплаты') . '</span>', 'url' => array('/feedback')),
                    array(
                        'label' => '<span>' . Yii::t('site', 'Контакты') . '</span>',
                        'url' => array('/contacts'),
                        'itemOptions' => array('class' => 'last'),
                    ),
                ),
                'encodeLabel' => false,
                'htmlOptions' => array(
                    'class' => 'g-clearfix',
                ),
            ));
            ?>
        </div>
    </div>
    <div class="page-content">
        <div class="wrap">
            <div class="g-clearfix">
                <?php echo $content; ?>
            </div>
        </div>
    </div>
    <div class="gag"></div>
</div>
<div class="footer">
    <div class="wrap g-clearfix">
        <a class="logo" title="">
            <img src="<?php echo Yii::app()->theme->baseUrl ?>/img/logo-footer.png" alt=""/>
        </a>
        <ul class="payment">
            <li>
                <a class="pay01" href="#" title=""></a>
                <a class="pay02" href="#" title=""></a>
                <a class="pay03" href="#" title=""></a>
            </li>
            <li>
                <a class="pay04" href="#" title=""></a>
                <a class="pay02" href="#" title=""></a>
                <a class="pay03" href="#" title=""></a>
            </li>
        </ul>

        <p class="text">
            <?php echo  Yii::t('account', '
                Киберспорт - это соревнования по компьютерным онлайн играм между игроками или командами. Среди
                киберспортивных
                дисцплин на данный момент можно выделить такие игры, как StarCraft2, Dota 2, Counter-Strike, League of
                legends,
                World of Tanks, Quake Live и др. Наш сайт принимает ставки на киберспортивные дисциплины с 2013-го года
                и активно
                поддерживает ряд турниров и лиг по всему миру.');
            ?>
        </p>
        <!--        Police and privacy-->

        <div style="text-align: center;">
            <a  style="color:darkgrey; text-decoration: none" class="link" href="<?php echo Yii::app()->createUrl('site/pp')?>">Security & Privacy</a>
            &nbsp&nbsp&nbsp
            <a style="color:darkgrey; text-decoration: none" class="link" href="<?php echo Yii::app()->createUrl('site/terms')?>">Terms</a>
        </div>
    </div>
</div>
<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/script.js"></script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter27897090 = new Ya.Metrika({id:27897090});
            } catch(e) { }
        });
        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n[removed].insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";
        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/27897090" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</body>
</html>