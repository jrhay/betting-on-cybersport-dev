$(document).ready(function() {
    mainNav();
    placeholder();
    itemToggle();
    datePicker();
    overviewWidth();
    initScroll();


    setTimeout(function(){$('.flash-messages-container div').hide(200);},5000);

    $('.flash-messages-container div').click(function(){
        jQuery(this).hide(200);

    });

    $('.select-styled').styler({ selectSmartPositioning: false });

    $("#slider1").easySlider({
        continuous: true,
        controlsShow: false,
        numeric: true,
        auto: true,
        pause: 7000
    });
    $("#slider2").easySlider();
    $("#slider-bet1").easySlider({
        prevId: 'bet1-prevBtn',
        nextId: 'bet1-nextBtn',
        numeric: true,
        numericId: 'bet1-controls',
        controlsFade:	true
    });
    $("#slider-bet2").easySlider({
        prevId: 'bet2-prevBtn',
        nextId: 'bet2-nextBtn',
        numeric: true,
        numericId: 'bet2-controls',
        controlsFade:	true
    });

    $("#tabs").myTabs();
    $("#tabs2").myTabs({firstLink: $(this).find('a[href="#tab4"]')});

    $(".tooltip").easyTooltip({ useElement: "tip-info" });

    $("#pop-up-live").myPopup({ btn: "#btn-create-live"});

    $('.scrolly').tinyscrollbar();

    /*
        Для поп-апа надо добавить
        $('.scrolly').tinyscrollbar();
        $('.scrolly').tinyscrollbar_update();
    */
    (function ($) {
        $.each(['show', 'hide'], function (i, ev) {
            var el = $.fn[ev];
            $.fn[ev] = function () {
                this.trigger(ev);
                return el.apply(this, arguments);
            };
        });
    })(jQuery);

    $('#login-form .errorMessage').on('show', function(){
        setTimeout(function(){$('#login-form .errorMessage').hide(600);},5000);
    });
    $('#login-form .errorMessage').on('click', function(){
        $('#login-form .errorMessage').hide(600);
    });
});
//$('#login-form .errorMessage').bind('show',function(){
//    alert('11');
//});




function mainNav() {
    var $el, leftPos, newWidth,
        $mainNav = $(".nav ul");

    if ($mainNav.children("li").hasClass("active")) {
        $mainNav.append("<li id='magic-line'></li>");
        var $magicLine = $("#magic-line");

        $magicLine
            .width($("li.active a span").width())
            .css("left", $("li.active a span").position().left)
            .data("origLeft", $magicLine.position().left)
            .data("origWidth", $magicLine.width());

        $(".nav ul li a").hover(function() {
            $el = $(this).children('span');
            leftPos = $el.position().left;
            newWidth = $el.parent().width();
            $magicLine.stop().animate({
                left: leftPos,
                width: newWidth
            });
        }, function() {
            $magicLine.stop().animate({
                left: $magicLine.data("origLeft"),
                width: $magicLine.data("origWidth")
            });
        });
    }
}

function itemToggle() {
    $('.hidden').hide();
    $('.item-list .toggle').click(
        function() {
            if ($(this).hasClass('active')) {
                $(this).siblings('.hidden').slideToggle('slow');
                $(this).removeClass('active');
            }
            else {
                $(this).addClass('active');
                $(this).siblings('.hidden').slideToggle('slow');
            }
        });
//    $('.toggle-bet').click(
    $('.pop-up-event').on( "click", ".toggle-bet", function() {
//            $(this).parents('.pop-up-event').find('.hidden').slideToggle('slow');
        jQuery('.to-bet').slideToggle('slow');

        });
    $('.pop-up-event').on( "click", ".show-user-bets", function() {
//        $(this).parents('.pop-up-event').find('.hidden').slideToggle('slow');
        jQuery('.my-bets').slideToggle('slow');
    });
}

function placeholder() {

    jQuery.support.placeholder = false;
    var test = document.createElement('input');
    if('placeholder' in test) jQuery.support.placeholder = true;

    if(!$.support.placeholder) {
        var active = document.activeElement;
        $("input[type='text'], input[type='email'], input[type='password']").focus(function () {
            if ($(this).attr('placeholder') != '' && $(this).val() == $(this).attr('placeholder')) {
                $(this).val('').removeClass('hasPlaceholder');
            }
        }).blur(function () {
                if ($(this).attr('placeholder') != '' && ($(this).val() == '' || $(this).val() == $(this).attr('placeholder'))) {
                    $(this).val($(this).attr('placeholder')).addClass('hasPlaceholder');
                }
            });
        $("input[type='text'], input[type='email'], input[type='password']").blur();
        $(active).focus();
        $('form').submit(function () {
            $(this).find('.hasPlaceholder').each(function() { $(this).val(''); });
        });
    }
}
function datePicker() {
    if ($('.datepicker').length){

        $('.input-append.date, .date.datepicker').datepicker({
            format: 'dd/mm/yyyy',
            weekStart: 1
        });
    }
}
function overviewWidth() {
    return $('.h-link').each(function() {
        var num = $(this).find('.filter-link').length;
        if (num) {
            var width = $(this).find('.filter-link:first-child').outerWidth();
            $(this).css({width: num * width});
        }
    });
}
function initScroll() {
    if ($('.scrollbar-janos').length){
        $('.scrollbar-janos').scrollbar({
            "showArrows": true,
            "type": "advanced"
        });
    }
}
(function($) {

    $.fn.myPopup = function(options){
        var settings = $.extend({
            popupId: this,
            btn: "#pop-up-bonus",
            overlayId: "popup-overlay",
            closeClass: "btn-close"
        }, options);

        function pos() {
            $(settings.popupId).css({
                top: ($(window).height() - $(settings.popupId).outerHeight()) / 2 + $(window).scrollTop() + "px",
                left: ($(window).width() - $(settings.popupId).outerWidth()) / 2 + $(window).scrollLeft() + "px"
            });
            $("#"+settings.overlayId).height($(document).height()).width($(document).width());
        }
        pos();

        return this.each(function() {
            $(settings.btn).click(function(e){
                e.preventDefault();
                $(settings.popupId).show();

                pos();

                $("#"+settings.overlayId).hide();
                $("#"+settings.overlayId).css({
                    display: "block",
                    background: "#111",
                    opacity: ".9",
                    filter: "alpha(opacity=90)"
                });
                $(window).resize( function() {
                   pos();
                });
            });
            $("#"+settings.overlayId).click(function(){
                $(settings.popupId).hide();
                $("#"+settings.overlayId).hide();
            });
            $("."+settings.closeClass).click(function(){
                $(settings.popupId).hide();
                $("#"+settings.overlayId).hide();
            });
            $(".btn-continue").click(function(){
                $(settings.popupId).hide();
                $("#"+settings.overlayId).hide();
            });
        });
    };

    $.fn.styler = function(opt) {

        var opt = $.extend({
            wrapper: 'form',
            idSuffix: '-styler',
            filePlaceholder: 'Файл не выбран',
            fileBrowse: 'Обзор...',
            selectSearch: true,
            selectSearchLimit: 10,
            selectSearchNotFound: 'Совпадений не найдено',
            selectSearchPlaceholder: 'Поиск...',
            selectVisibleOptions: 0,
            singleSelectzIndex: '100',
            selectSmartPositioning: true,
            onSelectOpened: function() {},
            onSelectClosed: function() {},
            onFormStyled: function() {}
        }, opt);

        return this.each(function() {
            var el = $(this);

            function attributes() {
                var id = '',
                    title = '',
                    classes = '',
                    dataList = '';
                if (el.attr('id') !== undefined && el.attr('id') != '') id = ' id="' + el.attr('id') + opt.idSuffix + '"';
                if (el.attr('title') !== undefined && el.attr('title') != '') title = ' title="' + el.attr('title') + '"';
                if (el.attr('class') !== undefined && el.attr('class') != '') classes = ' ' + el.attr('class');
                var data = el.data();
                for (var i in data) {
                    if (data[i] != '') dataList += ' data-' + i + '="' + data[i] + '"';
                }
                id += dataList;
                this.id = id;
                this.title = title;
                this.classes = classes;
            }

            // checkbox
            if (el.is(':checkbox')) {
                el.each(function() {
                    if (el.parent('div.jq-checkbox').length < 1) {

                        function checkbox() {

                            var att = new attributes();
                            var checkbox = $('<div' + att.id + ' class="jq-checkbox' + att.classes + '"' + att.title + '><div class="jq-checkbox__div"></div></div>');

                            // прячем оригинальный чекбокс
                            el.css({
                                position: 'absolute',
                                zIndex: '-1',
                                opacity: 0,
                                margin: 0,
                                padding: 0
                            }).after(checkbox).prependTo(checkbox);

                            checkbox.attr('unselectable', 'on').css({
                                '-webkit-user-select': 'none',
                                '-moz-user-select': 'none',
                                '-ms-user-select': 'none',
                                '-o-user-select': 'none',
                                'user-select': 'none',
                                display: 'inline-block',
                                position: 'relative',
                                overflow: 'hidden'
                            });

                            if (el.is(':checked')) checkbox.addClass('checked');
                            if (el.is(':disabled')) checkbox.addClass('disabled');

                            // клик на псевдочекбокс
                            checkbox.click(function() {
                                if (!checkbox.is('.disabled')) {
                                    if (el.is(':checked')) {
                                        el.prop('checked', false);
                                        checkbox.removeClass('checked');
                                    } else {
                                        el.prop('checked', true);
                                        checkbox.addClass('checked');
                                    }
                                    el.change();
                                    return false;
                                } else {
                                    return false;
                                }
                            });
                            // клик на label
                            el.closest('label').add('label[for="' + el.attr('id') + '"]').click(function(e) {
                                checkbox.click();
                                e.preventDefault();
                            });
                            // переключение по Space или Enter
                            el.change(function() {
                                if (el.is(':checked')) checkbox.addClass('checked');
                                else checkbox.removeClass('checked');
                            })
                                // чтобы переключался чекбокс, который находится в теге label
                                .keydown(function(e) {
                                    if (e.which == 32) checkbox.click();
                                })
                                .focus(function() {
                                    if (!checkbox.is('.disabled')) checkbox.addClass('focused');
                                })
                                .blur(function() {
                                    checkbox.removeClass('focused');
                                })

                        } // end checkbox()

                        checkbox();

                        // обновление при динамическом изменении
                        el.on('refresh', function() {
                            el.parent().before(el).remove();
                            checkbox();
                        });

                    }
                });
                // end checkbox

                // radio
            } else if (el.is(':radio')) {
                el.each(function() {
                    if (el.parent('div.jq-radio').length < 1) {

                        function radio() {

                            var att = new attributes();
                            var radio = $('<div' + att.id + ' class="jq-radio' + att.classes + '"' + att.title + '><div class="jq-radio__div"></div></div>');

                            // прячем оригинальную радиокнопку
                            el.css({
                                position: 'absolute',
                                zIndex: '-1',
                                opacity: 0,
                                margin: 0,
                                padding: 0
                            }).after(radio).prependTo(radio);

                            radio.attr('unselectable', 'on').css({
                                '-webkit-user-select': 'none',
                                '-moz-user-select': 'none',
                                '-ms-user-select': 'none',
                                '-o-user-select': 'none',
                                'user-select': 'none',
                                display: 'inline-block',
                                position: 'relative'
                            });

                            if (el.is(':checked')) radio.addClass('checked');
                            if (el.is(':disabled')) radio.addClass('disabled');

                            // клик на псевдорадиокнопке
                            radio.click(function() {
                                if (!radio.is('.disabled')) {
                                    radio.closest(opt.wrapper).find('input[name="' + el.attr('name') + '"]').prop('checked', false).parent().removeClass('checked');
                                    el.prop('checked', true).parent().addClass('checked');
                                    el.change();
                                    return false;
                                } else {
                                    return false;
                                }
                            });
                            // клик на label
                            el.closest('label').add('label[for="' + el.attr('id') + '"]').click(function(e) {
                                radio.click();
                                e.preventDefault();
                            });
                            // переключение стрелками
                            el.change(function() {
                                el.parent().addClass('checked');
                            })
                                .focus(function() {
                                    if (!radio.is('.disabled')) radio.addClass('focused');
                                })
                                .blur(function() {
                                    radio.removeClass('focused');
                                })

                        } // end radio()

                        radio();

                        // обновление при динамическом изменении
                        el.on('refresh', function() {
                            el.parent().before(el).remove();
                            radio();
                        });

                    }
                });
                // end radio

                // file
            } else if (el.is(':file')) {
                // прячем оригинальное поле
                el.css({
                    position: 'absolute',
                    top: 0,
                    right: 0,
                    width: '100%',
                    height: '100%',
                    opacity: 0,
                    margin: 0,
                    padding: 0
                }).each(function() {
                        if (el.parent('div.jq-file').length < 1) {

                            function file() {

                                var att = new attributes();
                                var file = $('<div' + att.id + ' class="jq-file' + att.classes + '"' + att.title + ' style="display: inline-block; position: relative; overflow: hidden"></div>');
                                var name = $('<div class="jq-file__name">' + opt.filePlaceholder + '</div>').appendTo(file);
                                var browse = $('<div class="jq-file__browse">' + opt.fileBrowse + '</div>').appendTo(file);
                                el.after(file);
                                file.append(el);
                                if (el.is(':disabled')) file.addClass('disabled');
                                el.change(function() {
                                    name.text(el.val().replace(/.+[\\\/]/, ''));
                                    if (el.val() == '') name.text(opt.filePlaceholder);
                                })
                                    .focus(function() {
                                        file.addClass('focused');
                                    })
                                    .blur(function() {
                                        file.removeClass('focused');
                                    })
                                    .click(function() {
                                        file.removeClass('focused');
                                    })

                            } // end file()

                            file();

                            // обновление при динамическом изменении
                            el.on('refresh', function() {
                                el.parent().before(el).remove();
                                file();
                            })

                        }
                    });
                // end file

                // select
            } else if (el.is('select')) {
                el.each(function() {
                    if (el.parent('div.jqselect').length < 1) {

                        function selectbox() {

                            // запрещаем прокрутку страницы при прокрутке селекта
                            function preventScrolling(selector) {
                                selector.off('mousewheel DOMMouseScroll').on('mousewheel DOMMouseScroll', function(e) {
                                    var scrollTo = null;
                                    if (e.type == 'mousewheel') { scrollTo = (e.originalEvent.wheelDelta * -1); }
                                    else if (e.type == 'DOMMouseScroll') { scrollTo = 40 * e.originalEvent.detail; }
                                    if (scrollTo) {
                                        e.stopPropagation();
                                        e.preventDefault();
                                        $(this).scrollTop(scrollTo + $(this).scrollTop());
                                    }
                                });
                            }

                            var option = $('option', el);
                            var list = '';
                            // формируем список селекта
                            function makeList() {
                                for (i = 0, len = option.length; i < len; i++) {
                                    var li = '',
                                        liClass = '',
                                        dataList = '',
                                        optionClass = '',
                                        optgroupClass = '',
                                        dataJqfsClass = '';
                                    var disabled = 'disabled';
                                    var selDis = 'selected sel disabled';
                                    if (option.eq(i).prop('selected')) liClass = 'selected sel';
                                    if (option.eq(i).is(':disabled')) liClass = disabled;
                                    if (option.eq(i).is(':selected:disabled')) liClass = selDis;
                                    if (option.eq(i).attr('class') !== undefined) {
                                        optionClass = ' ' + option.eq(i).attr('class');
                                        dataJqfsClass = ' data-jqfs-class="' + option.eq(i).attr('class') + '"';
                                    }

                                    var data = option.eq(i).data();
                                    for (var k in data) {
                                        if (data[k] != '') dataList += ' data-' + k + '="' + data[k] + '"';
                                    }

                                    li = '<li' + dataJqfsClass + dataList + ' class="' + liClass + optionClass + '">'+ option.eq(i).text() +'</li>';

                                    // если есть optgroup
                                    if (option.eq(i).parent().is('optgroup')) {
                                        if (option.eq(i).parent().attr('class') !== undefined) optgroupClass = ' ' + option.eq(i).parent().attr('class');
                                        li = '<li' + dataJqfsClass + ' class="' + liClass + optionClass + ' option' + optgroupClass + '">'+ option.eq(i).text() +'</li>';
                                        if (option.eq(i).is(':first-child')) {
                                            li = '<li class="optgroup' + optgroupClass + '">' + option.eq(i).parent().attr('label') + '</li>' + li;
                                        }
                                    }

                                    list += li;
                                }
                            } // end makeList()

                            // одиночный селект
                            function doSelect() {
                                var att = new attributes();
                                var selectbox =
                                    $('<div' + att.id + ' class="jq-selectbox jqselect' + att.classes + '" style="display: inline-block; position: relative; z-index:' + opt.singleSelectzIndex + '">' +
                                        '<div class="jq-selectbox__select"' + att.title + ' style="position: relative">' +
                                        '<div class="jq-selectbox__select-text"></div>' +
                                        '<div class="jq-selectbox__trigger"><div class="jq-selectbox__trigger-arrow"></div></div>' +
                                        '</div>' +
                                        '</div>');

                                el.css({margin: 0, padding: 0}).after(selectbox).prependTo(selectbox);

                                var divSelect = $('div.jq-selectbox__select', selectbox);
                                var divText = $('div.jq-selectbox__select-text', selectbox);
                                var optionSelected = option.filter(':selected');

                                // берем опцию по умолчанию
                                if (optionSelected.length) {
                                    divText.html(optionSelected.text());
                                } else {
                                    divText.html(option.first().text());
                                }

                                makeList();
                                var searchHTML = '';
                                if (opt.selectSearch) searchHTML =
                                    '<div class="jq-selectbox__search"><input type="search" autocomplete="off" placeholder="' + opt.selectSearchPlaceholder + '"></div>' +
                                        '<div class="jq-selectbox__not-found">' + opt.selectSearchNotFound + '</div>';
                                var dropdown =
                                    $('<div class="jq-selectbox__dropdown" style="position: absolute">' +
                                        searchHTML +
                                        '<ul style="position: relative; list-style: none; overflow: auto; overflow-x: hidden">' + list + '</ul>' +
                                        '</div>');
                                selectbox.append(dropdown);
                                var ul = $('ul', dropdown);
                                var li = $('li', dropdown);
                                var search = $('input', dropdown);
                                var notFound = $('div.jq-selectbox__not-found', dropdown).hide();
                                if (li.length < opt.selectSearchLimit) search.parent().hide();

                                // определяем самый широкий пункт селекта
                                var liWidth1 = 0,
                                    liWidth2 = 0;
                                li.each(function() {
                                    var l = $(this);
                                    l.css({'display': 'inline-block', 'white-space': 'nowrap'});
                                    if (l.innerWidth() > liWidth1) {
                                        liWidth1 = l.innerWidth();
                                        liWidth2 = l.width();
                                    }
                                    l.css({'display': 'block'});
                                });

                                // подстраиваем ширину псевдоселекта и выпадающего списка
                                // в зависимости от самого широкого пункта
                                var selClone = selectbox.clone().appendTo('body').width('auto');
                                var selCloneWidth = selClone.width();
                                selClone.remove();
                                if (selCloneWidth == selectbox.width()) {
                                    divText.width(liWidth2);
                                    liWidth1 += selectbox.find('div.jq-selectbox__trigger').width();
                                }
                                if ( liWidth1 > selectbox.width() ) {
                                    dropdown.width(liWidth1);
                                }

                                // прячем оригинальный селект
                                el.css({
                                    position: 'absolute',
                                    left: 0,
                                    top: 0,
                                    width: '100%',
                                    height: '100%',
                                    opacity: 0
                                });

                                var selectHeight = selectbox.outerHeight();
                                var searchHeight = search.outerHeight();
                                var isMaxHeight = ul.css('max-height');
                                var liSelected = li.filter('.selected');
                                if (liSelected.length < 1) li.first().addClass('selected sel');
                                if (li.data('li-height') === undefined) li.data('li-height', li.outerHeight());
                                var position = dropdown.css('top');
                                if (dropdown.css('left') == 'auto') dropdown.css({left: 0});
                                if (dropdown.css('top') == 'auto') dropdown.css({top: selectHeight});
                                dropdown.hide();

                                // если выбран не дефолтный пункт
                                if (liSelected.length) {
                                    // добавляем класс, показывающий изменение селекта
                                    if (option.first().text() != optionSelected.text()) {
                                        selectbox.addClass('changed');
                                    }
                                    // передаем селекту класс выбранного пункта
                                    selectbox.data('jqfs-class', liSelected.data('jqfs-class'));
                                    selectbox.addClass(liSelected.data('jqfs-class'));
                                }

                                // если селект неактивный
                                if (el.is(':disabled')) {
                                    selectbox.addClass('disabled');
                                    return false;
                                }

                                // при клике на псевдоселекте
                                divSelect.click(function() {
                                    el.focus();

                                    // колбек при закрытии селекта
                                    if ($('div.jq-selectbox').filter('.opened').length) {
                                        opt.onSelectClosed.call($('div.jq-selectbox').filter('.opened'));
                                    }

                                    // если iOS, то не показываем выпадающий список
                                    var iOS = navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false;
                                    if (iOS) return;

                                    // умное позиционирование
                                    if (opt.selectSmartPositioning) {
                                        var win = $(window);
                                        var topOffset = selectbox.offset().top;
                                        var bottomOffset = win.height() - selectHeight - (topOffset - win.scrollTop());
                                        var visible = opt.selectVisibleOptions;
                                        var liHeight = li.data('li-height');
                                        var	minHeight = liHeight * 5;
                                        var	newHeight = liHeight * visible;
                                        if (visible > 0 && visible < 6) minHeight = newHeight;
                                        if (visible == 0) newHeight = 'auto';

                                        // раскрытие вниз
                                        if (bottomOffset > (minHeight + searchHeight + 20))	{
                                            dropdown.height('auto').css({bottom: 'auto', top: position});
                                            function maxHeightBottom() {
                                                ul.css('max-height', Math.floor((bottomOffset - 20 - searchHeight) / liHeight) * liHeight);
                                            }
                                            maxHeightBottom();
                                            ul.css('max-height', newHeight);
                                            if (isMaxHeight != 'none') {
                                                ul.css('max-height', isMaxHeight);
                                            }
                                            if (bottomOffset < (dropdown.outerHeight() + 20)) {
                                                maxHeightBottom();
                                            }

                                            // раскрытие вверх
                                        } else {
                                            dropdown.height('auto').css({top: 'auto', bottom: position});
                                            function maxHeightTop() {
                                                ul.css('max-height', Math.floor((topOffset - win.scrollTop() - 20 - searchHeight) / liHeight) * liHeight);
                                            }
                                            maxHeightTop();
                                            ul.css('max-height', newHeight);
                                            if (isMaxHeight != 'none') {
                                                ul.css('max-height', isMaxHeight);
                                            }
                                            if ((topOffset - win.scrollTop() - 20) < (dropdown.outerHeight() + 20)) {
                                                maxHeightTop();
                                            }
                                        }
                                    }

                                    $('div.jqselect').css({zIndex: (opt.singleSelectzIndex - 1)}).removeClass('opened focused');
                                    selectbox.css({zIndex: opt.singleSelectzIndex});
                                    if (dropdown.is(':hidden')) {
                                        $('div.jq-selectbox__dropdown:visible').hide();
                                        dropdown.show();
                                        selectbox.addClass('opened');
                                        // колбек при открытии селекта
                                        opt.onSelectOpened.call(selectbox);
                                    } else {
                                        dropdown.hide();
                                        selectbox.removeClass('opened');
                                        // колбек при закрытии селекта
                                        if ($('div.jq-selectbox').filter('.opened').length) {
                                            opt.onSelectClosed.call(selectbox);
                                        }
                                    }

                                    // прокручиваем до выбранного пункта при открытии списка
                                    if (li.filter('.selected').length) {
                                        // если нечетное количество видимых пунктов,
                                        // то высоту пункта делим пополам для последующего расчета
                                        if ( (ul.innerHeight() / liHeight) % 2 != 0 ) liHeight = liHeight / 2;
                                        ul.scrollTop(ul.scrollTop() + li.filter('.selected').position().top - ul.innerHeight() / 2 + liHeight);
                                    }

                                    // поисковое поле
                                    if (search.length) {
                                        search.val('').keyup();
                                        notFound.hide();
                                        search.focus().keyup(function() {
                                            var query = $(this).val();
                                            li.each(function() {
                                                if (!$(this).html().match(new RegExp('.*?' + query + '.*?', 'i'))) {
                                                    $(this).hide();
                                                } else {
                                                    $(this).show();
                                                }
                                            });
                                            if (li.filter(':visible').length < 1) {
                                                notFound.show();
                                            } else {
                                                notFound.hide();
                                            }
                                        });
                                    }

                                    preventScrolling(ul);
                                    return false;
                                });

                                // при наведении курсора на пункт списка
                                li.hover(function() {
                                    $(this).siblings().removeClass('selected');
                                });
                                var selectedText = li.filter('.selected').text();
                                var selText = li.filter('.selected').text();

                                // при клике на пункт списка
                                li.filter(':not(.disabled):not(.optgroup)').click(function() {

                                    var t = $(this);
                                    var liText = t.text();

                                    if (selectedText != liText) {
                                        var index = t.index();
                                        if (t.is('.option')) index -= t.prevAll('.optgroup').length;
                                        t.addClass('selected sel').siblings().removeClass('selected sel');
                                        option.prop('selected', false).eq(index).prop('selected', true);
                                        selectedText = liText;
                                        divText.html(liText);

                                        // добавляем класс, показывающий изменение селекта
                                        if (option.first().text() != liText) {
                                            selectbox.addClass('changed');
                                        } else {
                                            selectbox.removeClass('changed');
                                        }

                                        // передаем селекту класс выбранного пункта
                                        if (selectbox.data('jqfs-class')) selectbox.removeClass(selectbox.data('jqfs-class'));
                                        selectbox.data('jqfs-class', t.data('jqfs-class'));
                                        selectbox.addClass(t.data('jqfs-class'));

                                        el.change();
                                        var buildUrl = function(base, key, value) {
                                            var sep = (base.indexOf('?') > -1) ? '&' : '?';
                                            return base + sep + key + '=' + value;
                                        }

                                        window.location.replace(buildUrl(document.URL.replace(/&lang=\w+/g,'').replace(/\?lang=\w+/g,''),'lang',liText));
                                    }
                                    if (search.length) {
                                        search.val('').keyup();
                                        notFound.hide();
                                    }
                                    dropdown.hide();
                                    selectbox.removeClass('opened');
                                    // колбек при закрытии селекта
                                    opt.onSelectClosed.call(selectbox);

                                });
                                dropdown.mouseout(function() {
                                    $('li.sel', dropdown).addClass('selected');
                                });

                                // изменение селекта
                                el.change(function() {
                                    divText.html(option.filter(':selected').text());
                                    li.removeClass('selected sel').not('.optgroup').eq(el[0].selectedIndex).addClass('selected sel');
                                })
                                    .focus(function() {
                                        selectbox.addClass('focused');
                                    })
                                    .blur(function() {
                                        selectbox.removeClass('focused');
                                    })
                                    // прокрутки списка с клавиатуры
                                    .on('keydown keyup', function(e) {
                                        divText.text(option.filter(':selected').text());
                                        li.removeClass('selected sel').not('.optgroup').eq(el[0].selectedIndex).addClass('selected sel');
                                        // вверх, влево, PageUp
                                        if (e.which == 38 || e.which == 37 || e.which == 33) {
                                            dropdown.scrollTop(dropdown.scrollTop() + li.filter('.selected').position().top);
                                        }
                                        // вниз, вправо, PageDown
                                        if (e.which == 40 || e.which == 39 || e.which == 34) {
                                            dropdown.scrollTop(dropdown.scrollTop() + li.filter('.selected').position().top - dropdown.innerHeight() + liHeight);
                                        }
                                        if (e.which == 13) {
                                            dropdown.hide();
                                        }
                                    });

                                // прячем выпадающий список при клике за пределами селекта
                                $(document).on('click', function(e) {
                                    // e.target.nodeName != 'OPTION' - добавлено для обхода бага в Opera на движке Presto
                                    // (при изменении селекта с клавиатуры срабатывает событие onclick)
                                    if (!$(e.target).parents().hasClass('jq-selectbox') && e.target.nodeName != 'OPTION') {

                                        // колбек при закрытии селекта
                                        if ($('div.jq-selectbox').filter('.opened').length) {
                                            opt.onSelectClosed.call($('div.jq-selectbox').filter('.opened'));
                                        }

                                        if (search.length) search.val('').keyup();
                                        dropdown.hide().find('li.sel').addClass('selected');
                                        selectbox.removeClass('focused opened');

                                    }
                                });

                            } // end doSelect()

                            // мультиселект
                            function doMultipleSelect() {
                                var att = new attributes();
                                var selectbox = $('<div' + att.id + ' class="jq-select-multiple jqselect' + att.classes + '"' + att.title + ' style="display: inline-block; position: relative"></div>');

                                el.css({margin: 0, padding: 0}).after(selectbox);

                                makeList();
                                selectbox.append('<ul>' + list + '</ul>');
                                var ul = $('ul', selectbox).css({
                                    'position': 'relative',
                                    'overflow-x': 'hidden',
                                    '-webkit-overflow-scrolling': 'touch'
                                });
                                var li = $('li', selectbox).attr('unselectable', 'on').css({'-webkit-user-select': 'none', '-moz-user-select': 'none', '-ms-user-select': 'none', '-o-user-select': 'none', 'user-select': 'none', 'white-space': 'nowrap'});
                                var size = el.attr('size');
                                var ulHeight = ul.outerHeight();
                                var liHeight = li.outerHeight();
                                if (size !== undefined && size > 0) {
                                    ul.css({'height': liHeight * size});
                                } else {
                                    ul.css({'height': liHeight * 4});
                                }
                                if (ulHeight > selectbox.height()) {
                                    ul.css('overflowY', 'scroll');
                                    preventScrolling(ul);
                                    // прокручиваем до выбранного пункта
                                    if (li.filter('.selected').length) {
                                        ul.scrollTop(ul.scrollTop() + li.filter('.selected').position().top);
                                    }
                                }

                                // прячем оригинальный селект
                                el.prependTo(selectbox).css({
                                    position: 'absolute',
                                    left: 0,
                                    top: 0,
                                    width: '100%',
                                    height: '100%',
                                    opacity: 0
                                });

                                // если селект неактивный
                                if (el.is(':disabled')) {
                                    selectbox.addClass('disabled');
                                    option.each(function() {
                                        if ($(this).is(':selected')) li.eq($(this).index()).addClass('selected');
                                    });

                                    // если селект активный
                                } else {

                                    // при клике на пункт списка
                                    li.filter(':not(.disabled):not(.optgroup)').click(function(e) {
                                        el.focus();
                                        selectbox.removeClass('focused');
                                        var clkd = $(this);
                                        if(!e.ctrlKey && !e.metaKey) clkd.addClass('selected');
                                        if(!e.shiftKey) clkd.addClass('first');
                                        if(!e.ctrlKey && !e.metaKey && !e.shiftKey) clkd.siblings().removeClass('selected first');

                                        // выделение пунктов при зажатом Ctrl
                                        if(e.ctrlKey || e.metaKey) {
                                            if (clkd.is('.selected')) clkd.removeClass('selected first');
                                            else clkd.addClass('selected first');
                                            clkd.siblings().removeClass('first');
                                        }

                                        // выделение пунктов при зажатом Shift
                                        if(e.shiftKey) {
                                            var prev = false,
                                                next = false;
                                            clkd.siblings().removeClass('selected').siblings('.first').addClass('selected');
                                            clkd.prevAll().each(function() {
                                                if ($(this).is('.first')) prev = true;
                                            });
                                            clkd.nextAll().each(function() {
                                                if ($(this).is('.first')) next = true;
                                            });
                                            if (prev) {
                                                clkd.prevAll().each(function() {
                                                    if ($(this).is('.selected')) return false;
                                                    else $(this).not('.disabled, .optgroup').addClass('selected');
                                                });
                                            }
                                            if (next) {
                                                clkd.nextAll().each(function() {
                                                    if ($(this).is('.selected')) return false;
                                                    else $(this).not('.disabled, .optgroup').addClass('selected');
                                                });
                                            }
                                            if (li.filter('.selected').length == 1) clkd.addClass('first');
                                        }

                                        // отмечаем выбранные мышью
                                        option.prop('selected', false);
                                        li.filter('.selected').each(function() {
                                            var t = $(this);
                                            var index = t.index();
                                            if (t.is('.option')) index -= t.prevAll('.optgroup').length;
                                            option.eq(index).prop('selected', true);
                                        });
                                        el.change();

                                    });

                                    // отмечаем выбранные с клавиатуры
                                    option.each(function(i) {
                                        $(this).data('optionIndex', i);
                                    });
                                    el.change(function() {
                                        li.removeClass('selected');
                                        var arrIndexes = [];
                                        option.filter(':selected').each(function() {
                                            arrIndexes.push($(this).data('optionIndex'));
                                        });
                                        li.not('.optgroup').filter(function(i) {
                                            return $.inArray(i, arrIndexes) > -1;
                                        }).addClass('selected');
                                    })
                                        .focus(function() {
                                            selectbox.addClass('focused');
                                        })
                                        .blur(function() {
                                            selectbox.removeClass('focused');
                                        });

                                    // прокручиваем с клавиатуры
                                    if (ulHeight > selectbox.height()) {
                                        el.keydown(function(e) {
                                            // вверх, влево, PageUp
                                            if (e.which == 38 || e.which == 37 || e.which == 33) {
                                                ul.scrollTop(ul.scrollTop() + li.filter('.selected').position().top - liHeight);
                                            }
                                            // вниз, вправо, PageDown
                                            if (e.which == 40 || e.which == 39 || e.which == 34) {
                                                ul.scrollTop(ul.scrollTop() + li.filter('.selected:last').position().top - ul.innerHeight() + liHeight * 2);
                                            }
                                        });
                                    }

                                }
                            } // end doMultipleSelect()
                            if (el.is('[multiple]')) doMultipleSelect(); else doSelect();
                        } // end selectbox()

                        selectbox();

                        // обновление при динамическом изменении
                        el.on('refresh', function() {
                            el.parent().before(el).remove();
                            selectbox();
                        });

                    }
                });
                // end select

                // reset
            } else if (el.is(':reset')) {
                el.click(function() {
                    setTimeout(function() {
                        el.closest(opt.wrapper).find('input, select').trigger('refresh');
                    }, 1)
                });
            }
            // end reset

        })

            // колбек после выполнения плагина
            .promise()
            .done(function() {
                opt.onFormStyled.call();
            });

    }

    $.fn.easySlider = function(options){

        // default configuration properties
        var defaults = {
            prevId: 		'prevBtn',
            prevText: 		'',
            nextId: 		'nextBtn',
            nextText: 		'',
            controlsShow:	true,
            controlsBefore:	'',
            controlsAfter:	'',
            controlsFade:	false,
            firstId: 		'firstBtn',
            firstText: 		'First',
            firstShow:		false,
            lastId: 		'lastBtn',
            lastText: 		'Last',
            lastShow:		false,
            vertical:		false,
            speed: 			800,
            auto:			false,
            pause:			2000,
            continuous:		false,
            numeric: 		false,
            numericId: 		'controls'
        };

        var options = $.extend(defaults, options);

        this.each(function() {
            var obj = $(this);
            var s = $("li", obj).length;
            var w = $("li", obj).width();
            var h = $("li", obj).height();
            var clickable = true;
            obj.width(w);
            obj.height(h);
            obj.css("overflow","hidden");
            var ts = s-1;
            var t = 0;
            $("ul", obj).css('width',s*w);

            if(options.continuous){
                $("ul", obj).prepend($("ul li:last-child", obj).clone().css("margin-left","-"+ w +"px"));
                $("ul", obj).append($("ul li:nth-child(2)", obj).clone());
                $("ul", obj).css('width',(s+1)*w);
            };

            if(!options.vertical) $("li", obj).css('float','left');

            if(options.controlsShow){
                var html = options.controlsBefore;

                if(options.firstShow) html += '<span id="'+ options.firstId +'"><a href=\"javascript:void(0);\">'+ options.firstText +'</a></span>';
                html += ' <span id="'+ options.prevId +'"><a href=\"javascript:void(0);\"><i>'+ options.prevText +'</i></a></span>';
                html += ' <span id="'+ options.nextId +'"><a href=\"javascript:void(0);\"><i>'+ options.nextText +'</i></a></span>';
                if(options.lastShow) html += ' <span id="'+ options.lastId +'"><a href=\"javascript:void(0);\">'+ options.lastText +'</a></span>';

                html += options.controlsAfter;
                $(obj).after(html);
            }

            if(options.numeric){
                var html = options.controlsBefore;
                html += '<ol id="'+ options.numericId +'"></ol>';
                html += options.controlsAfter;
                $(obj).after(html);
            }

            if(options.numeric){
                for(var i=0;i<s;i++){
                    $(document.createElement("li"))
                        .attr('id',options.numericId + (i+1))
                        .html('<a rel='+ i +' href=\"javascript:void(0);\">'+ (i+1) +'</a>')
                        .appendTo($("#"+ options.numericId))
                        .click(function(){
                            animate($("a",$(this)).attr('rel'),true);
                        });
                }
            }
            if(options.controlsShow){
                $("a","#"+options.nextId).click(function(){
                    animate("next",true);
                });
                $("a","#"+options.prevId).click(function(){
                    animate("prev",true);
                });
                $("a","#"+options.firstId).click(function(){
                    animate("first",true);
                });
                $("a","#"+options.lastId).click(function(){
                    animate("last",true);
                });
            }

            function setCurrent(i){
                i = parseInt(i)+1;
                $("li", "#" + options.numericId).removeClass("current");
                $("li#" + options.numericId + i).addClass("current");
            };

            function adjust(){
                if(t>ts) t=0;
                if(t<0) t=ts;
                if(!options.vertical) {
                    $("ul",obj).css("margin-left",(t*w*-1));
                } else {
                    $("ul",obj).css("margin-left",(t*h*-1));
                }
                clickable = true;
                if(options.numeric) setCurrent(t);
            };

            function animate(dir,clicked){
                if (clickable){
                    clickable = false;
                    var ot = t;
                    switch(dir){
                        case "next":
                            t = (ot>=ts) ? (options.continuous ? t+1 : ts) : t+1;
                            break;
                        case "prev":
                            t = (t<=0) ? (options.continuous ? t-1 : 0) : t-1;
                            break;
                        case "first":
                            t = 0;
                            break;
                        case "last":
                            t = ts;
                            break;
                        default:
                            t = dir;
                            break;
                    };
                    var diff = Math.abs(ot-t);
                    var speed = diff*options.speed;
                    if(!options.vertical) {
                        p = (t*w*-1);
                        $("ul",obj).animate(
                            { marginLeft: p },
                            { queue:false, duration:speed, complete:adjust }
                        );
                    } else {
                        p = (t*h*-1);
                        $("ul",obj).animate(
                            { marginTop: p },
                            { queue:false, duration:speed, complete:adjust }
                        );
                    };

                    if(!options.continuous && options.controlsFade){
                        if(t==ts){
                            $("a","#"+options.nextId).hide();
                            $("a","#"+options.lastId).hide();
                        } else {
                            $("a","#"+options.nextId).show();
                            $("a","#"+options.lastId).show();
                        };
                        if(t==0){
                            $("a","#"+options.prevId).hide();
                            $("a","#"+options.firstId).hide();
                        } else {
                            $("a","#"+options.prevId).show();
                            $("a","#"+options.firstId).show();
                        };
                    };

                    if(clicked) clearTimeout(timeout);
                    if(options.auto && dir=="next" && !clicked){;
                        timeout = setTimeout(function(){
                            animate("next",false);
                        },diff*options.speed+options.pause);
                    };

                };

            };
            // init
            var timeout;
            if(options.auto){;
                timeout = setTimeout(function(){
                    animate("next",false);
                },options.pause);
            };

            if(options.numeric) setCurrent(0);

            if(!options.continuous && options.controlsFade){
                $("a","#"+options.prevId).hide();
                $("a","#"+options.firstId).hide();
            };

        });

    };

    $.fn.myTabs =  function (options) {
        var settings = $.extend({
            tabs: this,
            tabLink: 'a.tab-link',
            firstLink: $(this).find('a.tab-link:first-child'),
            activeTab: 'active-tab',
            activeDiv: 'active-div'
        }, options);

        return this.each(function() {
            settings.firstLink.addClass(settings.activeTab);
            $(settings.firstLink.attr('href')).addClass(settings.activeDiv);
            $(settings.tabLink).click(function(event) {
                event.preventDefault();
                if ($(this).hasClass(settings.activeDiv)) { }
                else {
                    $(this).siblings(settings.tabLink).removeClass(settings.activeTab);
                    $(this).addClass(settings.activeTab);
                    $(this).parents(settings.tabs).children('div.section').removeClass(settings.activeDiv);
                    $(this).parents(settings.tabs).children($(this).attr('href')).addClass(settings.activeDiv);
                }
            });
        });
    };

    $.fn.easyTooltip = function(options){

        // default configuration properties
        var defaults = {
            xOffset: -5,
            yOffset: 25,
            tooltipId: "tip",
            clickRemove: false,
            content: "",
            useElement: ""
        };

        var options = $.extend(defaults, options);
        var content;

        this.each(function() {
            var tipHeight = 0;
            var title = $(this).attr("title");
            $(this).hover(function(e){
                    content = (options.content != "") ? options.content : title;
                    content = (options.useElement != "") ? $(this).find($("." + options.useElement)).html() : content;
                    $(this).attr("title","");
                    if (content != "" && content != undefined){
                        $("body").append("<div id='"+ options.tooltipId +"'>"+ content +"</div>");


                        tipHeight = $("#" + options.tooltipId).outerHeight();
                        $("#" + options.tooltipId)
                            .css("position","absolute")
                            .css("top",(e.pageY - tipHeight - options.yOffset) + "px")
                            .css("left",(e.pageX + options.xOffset) + "px")
                            .css("display","none")
                            .fadeIn("fast")
                    }
                },
                function(){
                    $("#" + options.tooltipId).remove();
                    $(this).attr("title",title);
                });
            $(this).mousemove(function(e){
                tipHeight = $("#" + options.tooltipId).outerHeight();
                $("#" + options.tooltipId)
                    .css("top",(e.pageY -  tipHeight - options.yOffset) + "px")
                    .css("left",(e.pageX + options.xOffset) + "px")
            });
            if(options.clickRemove){
                $(this).mousedown(function(e){
                    $("#" + options.tooltipId).remove();
                    $(this).attr("title",title);
                });
            }
        });

    };

    /* scrollbar (begin) */
    $.tiny = $.tiny || { };
    $.tiny.scrollbar = {
        options: {
            axis         : 'y'
            ,   wheel        : 40
            ,   scroll       : true
            ,   lockscroll   : true
            ,   size         : 'auto'
            ,   sizethumb    : 'auto'
            ,   invertscroll : false
        }
    };
    $.fn.tinyscrollbar = function( params )
    {
        var options = $.extend( {}, $.tiny.scrollbar.options, params );

        this.each( function()
        {
            $( this ).data('tsb', new Scrollbar( $( this ), options ) );
        });

        return this;
    };
    $.fn.tinyscrollbar_update = function(sScroll)
    {
        return $( this ).data( 'tsb' ).update( sScroll );
    };
    function Scrollbar( root, options )
    {
        var oSelf       = this
            ,   oWrapper    = root
            ,   oViewport   = { obj: $( '.viewport', root ) }
            ,   oContent    = { obj: $( '.overview', root ) }
            ,   oScrollbar  = { obj: $( '.scrollbar', root ) }
            ,   oTrack      = { obj: $( '.track', oScrollbar.obj ) }
            ,   oThumb      = { obj: $( '.thumb', oScrollbar.obj ) }
            ,   sAxis       = options.axis === 'x'
            ,   sDirection  = sAxis ? 'left' : 'top'
            ,   sSize       = sAxis ? 'Width' : 'Height'
            ,   iScroll     = 0
            ,   iPosition   = { start: 0, now: 0 }
            ,   iMouse      = {}
            ,   touchEvents = 'ontouchstart' in document.documentElement
            ;
        function initialize()
        {
            oSelf.update();
            setEvents();

            return oSelf;
        }
        this.update = function( sScroll )
        {
            oViewport[ options.axis ] = oViewport.obj[0][ 'offset'+ sSize ];
            oContent[ options.axis ]  = oContent.obj[0][ 'scroll'+ sSize ];
            oContent.ratio            = oViewport[ options.axis ] / oContent[ options.axis ];
            oScrollbar.obj.toggleClass( 'disable', oContent.ratio >= 1 );
            oTrack[ options.axis ] = options.size === 'auto' ? oViewport[ options.axis ] : options.size;
            oThumb[ options.axis ] = Math.min( oTrack[ options.axis ], Math.max( 0, ( options.sizethumb === 'auto' ? ( oTrack[ options.axis ] * oContent.ratio ) : options.sizethumb ) ) );
            oScrollbar.ratio = options.sizethumb === 'auto' ? ( oContent[ options.axis ] / oTrack[ options.axis ] ) : ( oContent[ options.axis ] - oViewport[ options.axis ] ) / ( oTrack[ options.axis ] - oThumb[ options.axis ] );
            iScroll = ( sScroll === 'relative' && oContent.ratio <= 1 ) ? Math.min( ( oContent[ options.axis ] - oViewport[ options.axis ] ), Math.max( 0, iScroll )) : 0;
            iScroll = ( sScroll === 'bottom' && oContent.ratio <= 1 ) ? ( oContent[ options.axis ] - oViewport[ options.axis ] ) : isNaN( parseInt( sScroll, 10 ) ) ? iScroll : parseInt( sScroll, 10 );
            setSize();
        };

        function setSize()
        {
            var sCssSize = sSize.toLowerCase();
            oThumb.obj.css( sDirection, iScroll / oScrollbar.ratio );
            oContent.obj.css( sDirection, -iScroll );
            iMouse.start = oThumb.obj.offset()[ sDirection ];
            oScrollbar.obj.css( sCssSize, oTrack[ options.axis ] );
            oTrack.obj.css( sCssSize, oTrack[ options.axis ] );
            oThumb.obj.css( sCssSize, oThumb[ options.axis ] );
        }

        function setEvents()
        {
            if( ! touchEvents )
            {
                oThumb.obj.bind( 'mousedown', start );
                oTrack.obj.bind( 'mouseup', drag );
            }
            else
            {
                oViewport.obj[0].ontouchstart = function( event )
                {
                    if( 1 === event.touches.length )
                    {
                        start( event.touches[ 0 ] );
                        event.stopPropagation();
                    }
                };
            }

            if( options.scroll && window.addEventListener )
            {
                oWrapper[0].addEventListener( 'DOMMouseScroll', wheel, false );
                oWrapper[0].addEventListener( 'mousewheel', wheel, false );
                oWrapper[0].addEventListener( 'MozMousePixelScroll', function( event ){
                    event.preventDefault();
                }, false);
            }
            else if( options.scroll )
            {
                oWrapper[0].onmousewheel = wheel;
            }
        }

        function start( event )
        {
            $( "body" ).addClass( "noSelect" );
            var oThumbDir   = parseInt( oThumb.obj.css( sDirection ), 10 );
            iMouse.start    = sAxis ? event.pageX : event.pageY;
            iPosition.start = oThumbDir == 'auto' ? 0 : oThumbDir;

            if( ! touchEvents )
            {
                $( document ).bind( 'mousemove', drag );
                $( document ).bind( 'mouseup', end );
                oThumb.obj.bind( 'mouseup', end );
            }
            else
            {
                document.ontouchmove = function( event )
                {
                    event.preventDefault();
                    drag( event.touches[ 0 ] );
                };
                document.ontouchend = end;
            }
        }

        function wheel( event )
        {
            if( oContent.ratio < 1 )
            {
                var oEvent = event || window.event
                    ,   iDelta = oEvent.wheelDelta ? oEvent.wheelDelta / 120 : -oEvent.detail / 3
                    ;
                iScroll -= iDelta * options.wheel;
                iScroll = Math.min( ( oContent[ options.axis ] - oViewport[ options.axis ] ), Math.max( 0, iScroll ));

                oThumb.obj.css( sDirection, iScroll / oScrollbar.ratio );
                oContent.obj.css( sDirection, -iScroll );

                if( options.lockscroll || ( iScroll !== ( oContent[ options.axis ] - oViewport[ options.axis ] ) && iScroll !== 0 ) )
                {
                    oEvent = $.event.fix( oEvent );
                    oEvent.preventDefault();
                }
            }
        }

        function drag( event )
        {
            if( oContent.ratio < 1 )
            {
                if( options.invertscroll && touchEvents )
                {
                    iPosition.now = Math.min( ( oTrack[ options.axis ] - oThumb[ options.axis ] ), Math.max( 0, ( iPosition.start + ( iMouse.start - ( sAxis ? event.pageX : event.pageY ) ))));
                }
                else
                {
                    iPosition.now = Math.min( ( oTrack[ options.axis ] - oThumb[ options.axis ] ), Math.max( 0, ( iPosition.start + ( ( sAxis ? event.pageX : event.pageY ) - iMouse.start))));
                }

                iScroll = iPosition.now * oScrollbar.ratio;
                oContent.obj.css( sDirection, -iScroll );
                oThumb.obj.css( sDirection, iPosition.now );
            }
        }

        function end()
        {
            $( "body" ).removeClass( "noSelect" );
            $( document ).unbind( 'mousemove', drag );
            $( document ).unbind( 'mouseup', end );
            oThumb.obj.unbind( 'mouseup', end );
            document.ontouchmove = document.ontouchend = null;
        }

        return initialize();
    }
})(jQuery);