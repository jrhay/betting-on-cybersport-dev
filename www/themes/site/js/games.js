var games = games || {};

games.timeout = 6000;
games.updateGamesUrl = 'games/updateTable';
games.checkGamesChange = function()
{
    $.post(
        games.updateGamesUrl + '&_=' + new Date().getTime(),
        {},
        function(html)
        {
            currentClickedTabID = $('#game-categories-common a.active').attr('id');
            $('.games-table-common-wrap').html(html);
//            currentCameId = $('#game-categories-common a.active').attr('id').split('game-table-')
            setTimeout(games.checkGamesChange, games.timeout);
            $( '#'+currentClickedTabID ).click();
        },
        'html'
    );

    return false;
}

games.init = function() {
    //add here any actions which have to be completed after load
    games.checkGamesChange();
}

$(document).ready(function() {
    games.init();
});